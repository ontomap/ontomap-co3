**Description and notes**
---------------------
This is the list of APIs currently available for the interaction with CO3OTM.
The list will be updated with new APIs when they will be implemented.

* Development/testing API endpoint: <a href="https://dev.co3.ontomap.eu" target="_blank">https://dev.co3.ontomap.eu</a> (dev/test Client Certificate)<br/>
* Prototype 1 endpoint: <a href="https://pt.co3.ontomap.eu" target="_blank">https://pt.co3.ontomap.eu</a> (pt Client Certificate)


<span style="color:firebrick;">**It should be noted that the ontology currently used in OnToMap is under heavy development;
this means that you should not rely on the specification of concepts available so far:
concepts and relations might be modified in the next future in order to accommodate the types of information to be handled in the CO3 pilots.
As soon as the ontology will be stable, a link to it will be published in this page.**</span>

***Authentications*** <a name="auth"></a>
-
CO3OTM requires three types of authentications depending on each API:
* Client Certificate: Every request made to this endpoint must include a client certificate signed by LiquidFeedback.
* Logged User access_token: The *access_token* of the logged user must be provided as **header** of the HTTP Request.
  >usage: headers["Authorization"]: "Bearer qhPxqbS2vSzZKF5R"
                                                                                                                   
* Application Token: This token is provided by CO3OTM using <a href="#api-Token_APIs">Token APIs</a>. It can be used as a REST parameter of certain APIs.
    >usage: rest_params["token"]: "ZGNmZDk0MTYtZjQ5YS00YTQ4LTg4NzUtMDE5M2EyMDg0M2Yy"
