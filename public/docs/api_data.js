define({ "api": [
  {
    "type": "post",
    "url": "/gamification/badgeContainers",
    "title": "Create new badges",
    "name": "Create_Badge_Container",
    "group": "Badge_APIs",
    "description": "<p>Creates and inserts into the CO3OTM Achievements Manager, one or more Badge Containers having at least one included badge.<br/> The request must have a <code>application/json</code> Content-Type and must include the JSON representation of the Badge Container(s) in its body (see provided example).<br/> Badge Container JSON Schema can be found at <a  target=\"_blan\" href=\"./api/v1/gamification/badgeContainers/schema/\">Schema Link</a></p>",
    "permission": [
      {
        "name": "Client Certificate."
      }
    ],
    "parameter": {
      "examples": [
        {
          "title": "Creation of Individual Badges (example)",
          "content": "POST https://dev.co3.ontomap.eu/api/v1/gamification/badgeContainers\n[\n   {\n        \"name\": \"Document Creator\",\n        \"type\":\"<b>individual</b>\",\n        \"badgeList\": [\n            {\n                \"name\": \"Bronze\",\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 10\n                        }\n                    ]\n                }\n            },\n            {\n                \"name\": \"Silver\",\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 20\n                        }\n                    ]\n                }\n            },\n            {\n                \"name\": \"Gold\",\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 30\n                        }\n                    ]\n                }\n            }\n        ]\n    }\n]",
          "type": "json"
        },
        {
          "title": "Creation of Weekly ACA Badges (example)",
          "content": "POST https://dev.co3.ontomap.eu/api/v1/gamification/badgeContainers\n[\n    {\n        \"name\": \"Document Creator ACA\",\n        \"type\":\"<b>aca</b>\",\n        \"timeframe\":\"WEEK\",\n        \"badgeList\": [\n            {\n                \"name\": \"Bronze\",\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 10\n                        }\n                    ]\n                }\n            },\n            {\n                \"name\": \"Silver\",\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 20\n                        }\n                    ]\n                }\n            },\n            {\n                \"name\": \"Gold\",\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 30\n                        }\n                    ]\n                }\n            }\n        ]\n    }",
          "type": "json"
        },
        {
          "title": "Creation of Weekly Ranking Badges (example)",
          "content": "POST https://dev.co3.ontomap.eu/api/v1/gamification/badgeContainers\n[\n    {\n        \"name\": \"Document Creator Ranking\",\n        \"type\": \"<b>ranking</b>\",\n        \"variable\": \"documentCreated\",\n        \"timeframe\": \"WEEK\",\n        \"badgeList\": [\n            {\n                \"name\": \"Bronze\",\n                \"position\": 2\n            },\n            {\n                \"name\": \"Silver\",\n                \"position\": 1\n            }\n        ]\n    }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/gamification/controllers/BadgeApiController.java",
    "groupTitle": "Badge_APIs",
    "error": {
      "fields": {
        "Error 400 Bad Request": [
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "malformed_json",
            "description": "<p>The payload sent is not a valid JSON string or it doesn't adhere to the JSON Schema.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 403 Forbidden": [
          {
            "group": "Error 403 Forbidden",
            "optional": false,
            "field": "gamification_disabled",
            "description": "<p>The CO3OTM Achievements Manager is not active in this CO3 instance.</p>"
          }
        ],
        "Error 415 Unsupported Media Type": [
          {
            "group": "Error 415 Unsupported Media Type",
            "optional": false,
            "field": "wrong_content_type",
            "description": "<p>The request has an unexpected Content-Type.<br/> The Content-Type for this request must be <code>application/json</code>.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "patch",
    "url": "/gamification/badgeContainers",
    "title": "Edit Badge Containers",
    "name": "Edit_Badge_Containers",
    "group": "Badge_APIs",
    "description": "<p>Changes the name of the BadgeContainer, the names of the badges included in the container and their logos.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Edit a Badge Container",
          "content": "PATCH https://dev.co3.ontomap.eu/api/v1/gamification/badgeContainers\n[\n   {\n        \"name\": \"Document Creator\",\n        \"type\":\"<b>individual</b>\",\n        \"badgeList\": [\n            {\n                \"name\": \"Bronze\",\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 10\n                        }\n                    ]\n                }\n            },\n            {\n                \"name\": \"Silver\",\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 20\n                        }\n                    ]\n                }\n            },\n            {\n                \"name\": \"Gold\",\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 30\n                        }\n                    ]\n                }\n            }\n        ]\n    }\n]",
          "type": "json"
        }
      ]
    },
    "permission": [
      {
        "name": "Client Certificate."
      }
    ],
    "version": "0.0.0",
    "filename": "app/gamification/controllers/BadgeApiController.java",
    "groupTitle": "Badge_APIs",
    "error": {
      "fields": {
        "Error 400 Bad Request": [
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "malformed_json",
            "description": "<p>The payload sent is not a valid JSON string or it doesn't adhere to the JSON Schema.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 403 Forbidden": [
          {
            "group": "Error 403 Forbidden",
            "optional": false,
            "field": "gamification_disabled",
            "description": "<p>The CO3OTM Achievements Manager is not active in this CO3 instance.</p>"
          }
        ],
        "Error 415 Unsupported Media Type": [
          {
            "group": "Error 415 Unsupported Media Type",
            "optional": false,
            "field": "wrong_content_type",
            "description": "<p>The request has an unexpected Content-Type.<br/> The Content-Type for this request must be <code>application/json</code>.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "post",
    "url": "/gamification/badge/<idBadge>",
    "title": "Enable or Disable a Badge",
    "name": "Enable_or_Disable_a_Badge",
    "group": "Badge_APIs",
    "description": "<p>Changes the <i>active</i> flag of a badge. A badge can not be disabled if it's a badge precondition of another badge. A badge can not be enabled if one of its badge preconditions is disabled.</p>",
    "permission": [
      {
        "name": "Client Certificate."
      }
    ],
    "parameter": {
      "fields": {
        "Field Attributes": [
          {
            "group": "Field Attributes",
            "type": "String",
            "optional": false,
            "field": "idBadge",
            "description": "<p><strong>Required.</strong> The <i>id</i> of the badge to be enabled/disabled.</p>"
          }
        ],
        "Required Attributes": [
          {
            "group": "Required Attributes",
            "type": "Boolean",
            "optional": false,
            "field": "enable",
            "description": "<p><strong>Required.</strong> Enable or disable the selected badge.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "optional": false,
            "field": "badge_enable_exception",
            "description": "<p>The selected badge can not be enabled.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "optional": false,
            "field": "badge_disable_exception",
            "description": "<p>The selected badge can not be disabled.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "optional": false,
            "field": "not_valid_id",
            "description": "<p>The provided <i>id</i> is not valid.</p>"
          }
        ],
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "optional": false,
            "field": "badge_not_found",
            "description": "<p>The provided <i>id</i> does not belong to an existing badge.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 403 Forbidden": [
          {
            "group": "Error 403 Forbidden",
            "optional": false,
            "field": "gamification_disabled",
            "description": "<p>The CO3OTM Achievements Manager is not active in this CO3 instance.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/gamification/controllers/BadgeApiController.java",
    "groupTitle": "Badge_APIs"
  },
  {
    "type": "post",
    "url": "/gamification/badgeContainers/<idBadgeContainer>",
    "title": "Enable or Disable a Badge Container",
    "name": "Enable_or_Disable_a_Badge_Container",
    "group": "Badge_APIs",
    "description": "<p>Changes the <i>active</i> flag of all badges inside the container. A badge can not be disabled if it's a badge precondition of another badge. A badge can not be enabled if one of its badge preconditions is disabled.</p>",
    "parameter": {
      "fields": {
        "Field Attributes": [
          {
            "group": "Field Attributes",
            "type": "String",
            "optional": false,
            "field": "idBadgeContainer",
            "description": "<p><strong>Required.</strong> The <i>id</i> of the badge container.</p>"
          }
        ],
        "Required Attributes": [
          {
            "group": "Required Attributes",
            "type": "Boolean",
            "optional": false,
            "field": "enable",
            "description": "<p><strong>Required.</strong> Enable or disable the selected badge container.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "optional": false,
            "field": "not_valid_id",
            "description": "<p>The provided <i>id</i> is not valid.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "optional": false,
            "field": "badge_enable_exception",
            "description": "<p>The selected badge container can not be enabled.</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "optional": false,
            "field": "badge_disable_exception",
            "description": "<p>The selected badge container can not be disabled.</p>"
          }
        ],
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "optional": false,
            "field": "badge_container_not_found",
            "description": "<p>The provided <i>id</i> does not belong to an existing badge container.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 403 Forbidden": [
          {
            "group": "Error 403 Forbidden",
            "optional": false,
            "field": "gamification_disabled",
            "description": "<p>The CO3OTM Achievements Manager is not active in this CO3 instance.</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "Client Certificate."
      }
    ],
    "version": "0.0.0",
    "filename": "app/gamification/controllers/BadgeApiController.java",
    "groupTitle": "Badge_APIs"
  },
  {
    "type": "get",
    "url": "/gamification/badgeContainers",
    "title": "Get All Badge Containers",
    "name": "Get_All_Badge_Containers",
    "group": "Badge_APIs",
    "description": "<p>Returns a list of all Badge Container. This list can be filtered by type of badge container.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Get full badge container List",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/gamification/badgeContainers\n[\n    {\n        \"id\": \"5eba7ee4c6502572e0db2fbc\",\n        \"name\": \"Document Creator Ranking\",\n        \"badgeList\": [\n            {\n                \"id\": \"5eba7ee4c6502572e0db2fbd\",\n                \"name\": \"Bronze\",\n                \"active\": true,\n                \"position\": 2\n            },\n            {\n                \"id\": \"5eba7ee4c6502572e0db2fbe\",\n                \"name\": \"Silver\",\n                \"active\": true,\n                \"position\": 1\n            }\n        ],\n        \"timeframe\": \"WEEK\",\n        \"type\": \"ranking\",\n        \"variable\": \"documentCreated\"\n    },\n    {\n        \"id\": \"5eba9f0fc6502572e0db2fc1\",\n        \"name\": \"Document Creator ACA\",\n        \"badgeList\": [\n            {\n                \"id\": \"5eba9f0fc6502572e0db2fc2\",\n                \"name\": \"Bronze\",\n                \"active\": true,\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 1.0\n                        }\n                    ]\n                }\n            },\n            {\n                \"id\": \"5eba9f0fc6502572e0db2fc3\",\n                \"name\": \"Silver\",\n                \"active\": true,\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 20.0\n                        }\n                    ],\n                    \"badgeList\": [\n                        \"5eba9f0fc6502572e0db2fc2\"\n                    ]\n                }\n            },\n            {\n                \"id\": \"5eba9f0fc6502572e0db2fc4\",\n                \"name\": \"Gold\",\n                \"active\": true,\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 30.0\n                        }\n                    ],\n                    \"badgeList\": [\n                        \"5eba9f0fc6502572e0db2fc3\"\n                    ]\n                }\n            }\n        ],\n        \"timeframe\": \"WEEK\",\n        \"type\": \"aca\"\n    }\n]",
          "type": "json"
        }
      ],
      "fields": {
        "Attributes": [
          {
            "group": "Attributes",
            "type": "String",
            "optional": true,
            "field": "type",
            "description": "<p>The type of Badge Container. Can be set to <code>individual, aca or ranking</code>.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/gamification/controllers/BadgeApiController.java",
    "groupTitle": "Badge_APIs"
  },
  {
    "type": "get",
    "url": "/gamification/badges/<idBadge>",
    "title": "Get a Badge",
    "name": "Get_a_Badge",
    "group": "Badge_APIs",
    "description": "<p>Retrieves information about a specified badge. The response includes some information about the container of that badge.</p>",
    "parameter": {
      "fields": {
        "Field Attributes": [
          {
            "group": "Field Attributes",
            "type": "String",
            "optional": false,
            "field": "idBadge",
            "description": "<p><strong>Required.</strong> The <i>id</i> of the badge to be retrieved.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Get a badge",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/gamification/badges/5eba9f0fc6502572e0db2fc3\n{\n    \"id\": \"5eba9f0fc6502572e0db2fc3\",\n    \"name\": \"Silver\",\n    \"active\": true,\n    \"logo\":\"https://co3.app.com/document_creator_aca_silver.jpg\"\n    \"containerReference\": {\n        \"id\": \"5eba9f0fc6502572e0db2fc1\",\n        \"name\": \"Document Creator ACA\",\n        \"logo\":\"https://co3.app.com/document_creator_aca.jpg\"\n        \"type\": \"aca\"\n    },\n    \"preconditions\": {\n        \"variables\": [\n            {\n                \"variable\": \"documentCreated\",\n                \"threshold\": 20.0\n            }\n        ],\n        \"badgeList\": [\n            \"5eba9f0fc6502572e0db2fc2\"\n        ]\n    }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "optional": false,
            "field": "not_valid_id",
            "description": "<p>The provided  <i>id</i> is not valid.</p>"
          }
        ],
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "optional": false,
            "field": "badge_not_found",
            "description": "<p>The provided  <i>id</i> does not belong to an existing badge.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/gamification/controllers/BadgeApiController.java",
    "groupTitle": "Badge_APIs"
  },
  {
    "type": "get",
    "url": "/gamification/badgeContainers/<idBadgeContainer>",
    "title": "Get a Badge Container",
    "name": "Get_a_Badge_Container",
    "group": "Badge_APIs",
    "description": "<p>Retrieves the information about a specific badge container.</p>",
    "parameter": {
      "fields": {
        "Field Attributes": [
          {
            "group": "Field Attributes",
            "type": "String",
            "optional": false,
            "field": "idBadgeContainer",
            "description": "<p><strong>Required.</strong> The <i>id</i> of the badge container to be retrieved.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Get a badge Container",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/gamification/badgeContainers/5eaf20506b644c21b3bb5d63\n{\n        \"id\":\"5eaf20506b644c21b3bb5d63\",\n        \"name\": \"Document Creator\",\n        \"type\":\"individual\",\n        \"badgeList\": [\n            {\n                \"name\": \"Bronze\",\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 10\n                        }\n                    ]\n                }\n            },\n            {\n                \"name\": \"Silver\",\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 20\n                        }\n                    ]\n                }\n            },\n            {\n                \"name\": \"Gold\",\n                \"preconditions\": {\n                    \"variables\": [\n                        {\n                            \"variable\": \"documentCreated\",\n                            \"threshold\": 30\n                        }\n                    ]\n                }\n            }\n        ]\n    }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "optional": false,
            "field": "not_valid_id",
            "description": "<p>The provided <i>id</i> is not valid.</p>"
          }
        ],
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "optional": false,
            "field": "badge_container_not_found",
            "description": "<p>The provided <i>id</i> does not belong to an existing badge container.</p>"
          }
        ],
        "Error 403 Forbidden": [
          {
            "group": "Error 403 Forbidden",
            "optional": false,
            "field": "gamification_disabled",
            "description": "<p>The CO3OTM Achievements Manager is not active in this CO3 instance.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/gamification/controllers/BadgeApiController.java",
    "groupTitle": "Badge_APIs"
  },
  {
    "type": "get",
    "url": "/concepts",
    "title": "Get all ontology concepts",
    "name": "GetAllConcepts",
    "group": "Concepts_APIs",
    "description": "<p>Returns a JSON Object, representing complete information about all the concepts of the CO3OTM Ontology.<br> The field <code>concepts</code> is a JSON Array of objects, each representing an ontology concept.<br> If <code>relations</code> is set to <code>true</code>, a JSON Array of objects named <code>relations</code> is included in the result; each object of the Array represents a semantic relation among two concepts of the ontology. All the relations defined in the ontology are included.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Example: Concepts and relations",
          "content": "GET http://co3.dev.ontomap.eu/api/v1/concepts?relations=true\n{\n  \"concepts\": [\n    {\n      \"label\": {\n        \"en\": \"Event\",\n        \"it\": \"Evento\"\n      },\n      \"description\": {\n        \"en\": \"Event definition\",\n        \"it\": \"Definizione di Evento\"\n      },\n      \"uri\": \"http://co3.dev.ontomap.eu/api/v1/concepts/Event\"\n    },\n    {\n      \"label\": {\n        \"en\": \"Place\",\n        \"it\": \"Luogo\"\n      },\n      \"description\": {\n        \"en\": \"Place definition\",\n        \"it\": \"Definizione di Luogo\"\n      },\n      \"uri\": \"http://co3.dev.ontomap.eu/api/v1/concepts/Place\"\n    }\n  ],\n  \"relations\": [\n    {\n      \"subject\": \"http://co3.dev.ontomap.eu/api/v1/concepts/Event\",\n      \"object\": \"http://co3.dev.ontomap.eu/api/v1/concepts/Place\",\n      \"predicate\": \"hasPlace\"\n    }\n  ]\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Information type": [
          {
            "group": "Information type",
            "type": "Boolean",
            "optional": true,
            "field": "relations",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the relations among the concepts are included in the result.</p>"
          },
          {
            "group": "Information type",
            "type": "String",
            "optional": true,
            "field": "lang",
            "description": "<p>A <a href=\"https://en.wikipedia.org/wiki/ISO_639-1\" target=\"_blank\">ISO 639-1 language code</a>.<br/> If specified, the <code>label</code> field of the JSON Objects representing the concepts will be a String containing the label of the requested concept in the specified language, if present.<br/> If not set, the <code>label</code> field of the JSON Objects representing the concepts will be a JSON Object containing the labels of each concept in all the available languages.</p>"
          }
        ],
        "Presentation": [
          {
            "group": "Presentation",
            "type": "Boolean",
            "optional": true,
            "field": "prettyprint",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the JSON string served is indented.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/ConceptsController.java",
    "groupTitle": "Concepts_APIs",
    "error": {
      "fields": {
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/concepts/<concept>",
    "title": "Get ontology concept",
    "name": "GetOntologyConcept",
    "group": "Concepts_APIs",
    "description": "<p>Returns a JSON Object, representing complete information about the concept passed as parameter in the request.<br> The field <code>relations</code> is a JSON Array of objects; each object represents a semantic relation in which <code>concept</code> is involved.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Example: Specify language",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/concepts/School?lang=en\n{\n  \"label\" : \"School\",\n  \"description\": \"Institution designed for the teaching of students under the direction of teachers\",\n  \"uri\" : \"http://co3.dev.ontomap.eu/api/v1/concepts/School\",\n  \"relations\": [\n    {\n      \"subject\": \"http://co3.dev.ontomap.eu/api/v1/concepts/School\",\n      \"object\": \"http://co3.dev.ontomap.eu/api/v1/concepts/HealthSocialService\",\n      \"predicate\": \"subClassOf\"\n    }\n  ],\n  \"propertyList\": [\n    {\n      \"name\": \"hasRector\"\n    },\n    {\n      \"name\": \"hasOffice\"\n    },\n    {\n      \"name\": \"hasCompany\"\n    },\n    {\n      \"name\": \"isPublic\"\n    },\n    {\n      \"name\": \"hasOpeningHours\",\n      \"range\": \"string\"\n    },,\n    ...\n    ]\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Required parameters": [
          {
            "group": "required_parameters",
            "type": "String",
            "optional": false,
            "field": "concept",
            "description": "<p><strong>Required.</strong> The concept about which the information is requested.</p>"
          }
        ],
        "Information type": [
          {
            "group": "Information type",
            "type": "String",
            "optional": true,
            "field": "lang",
            "description": "<p>A <a href=\"https://en.wikipedia.org/wiki/ISO_639-1\" target=\"_blank\">ISO 639-1 language code</a>.<br/> If specified, the <code>label</code> field of the JSON Object will be a String containing the label of the requested concept in the specified language, if present.<br/> If not set, the <code>label</code> field of the JSON Object will be a JSON Object containing the labels of the requested concept in all the available languages.</p>"
          }
        ],
        "Presentation": [
          {
            "group": "Presentation",
            "type": "Boolean",
            "optional": true,
            "field": "prettyprint",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the JSON string served is indented.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 404 Not Found": [
          {
            "group": "Error 404 Not Found",
            "optional": false,
            "field": "instance_not_found",
            "description": "<p>The instance requested does not exist.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/ConceptsController.java",
    "groupTitle": "Concepts_APIs"
  },
  {
    "type": "get",
    "url": "/concepts/root",
    "title": "Get root concept of the ontology",
    "name": "GetRootConcept",
    "group": "Concepts_APIs",
    "description": "<p>Returns a JSON Object, representing complete information about the root concept of the CO3OTM Ontology.<br> The field <code>relations</code> is a JSON Array of objects; each object represents a semantic relation in which the root concept is involved.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Example: Specify language",
          "content": "GET http://co3.dev.ontomap.eu/api/v1/concepts/root?lang=en\n{\n  \"uri\" : \"http://co3.dev.ontomap.eu/api/v1/concepts/SchemaThing\",\n  \"relations\" : [\n    {\n      \"subject\" : \"http://co3.dev.ontomap.eu/api/v1/concepts/AtomicThing\",\n      \"object\" : \"http://co3.dev.ontomap.eu/api/v1/concepts/SchemaThing\",\n      \"predicate\" : \"subClassOf\"\n    },\n    {\n      \"subject\" : \"http://co3.dev.ontomap.eu/api/v1/concepts/Place\",\n      \"object\" : \"http://co3.dev.ontomap.eu/api/v1/concepts/SchemaThing\",\n      \"predicate\" : \"subClassOf\"\n    },\n    {\n      \"subject\" : \"http://co3.dev.ontomap.eu/api/v1/concepts/Event\",\n      \"object\" : \"http://co3.dev.ontomap.eu/api/v1/concepts/SchemaThing\",\n      \"predicate\" : \"subClassOf\"\n    }\n  ],\n  \"label\" : \"SchemaThing\",\n  \"description\" : \"The root concept of the ontology.\"\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Information type": [
          {
            "group": "Information type",
            "type": "String",
            "optional": true,
            "field": "lang",
            "description": "<p>A <a href=\"https://en.wikipedia.org/wiki/ISO_639-1\" target=\"_blank\">ISO 639-1 language code</a>.<br/> If specified, the <code>label</code> field of the JSON Object will be a String containing the label of the root concept in the specified language, if present.<br/> If not set, the <code>label</code> field of the JSON Object will be a JSON Object containing the labels of the root concept in all the available languages.</p>"
          }
        ],
        "Presentation": [
          {
            "group": "Presentation",
            "type": "Boolean",
            "optional": true,
            "field": "prettyprint",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the JSON string served is indented.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/ConceptsController.java",
    "groupTitle": "Concepts_APIs",
    "error": {
      "fields": {
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "GET",
    "url": "/gamification/profile/aca",
    "title": "Retrieves ACA Achievements Profiles",
    "name": "Retrieves_ACA_Achievements_Profiles",
    "group": "Gamification_Achievements_Profiles_APIs",
    "description": "<p>Retrieves information about a specified ACA Profile.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Get ACA profile",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/gamification/profile/aca?aca=https://api.co3-dev.firstlife.org/v6/fl/Things/5eb17fec6bd1ca539013a507\n[\n    {\n        \"temporalVariableContainers\": [\n            {\n                \"timeframe\": \"MONTH\",\n                \"start\": 1604185200000,\n                \"end\": 1606777199999,\n                \"variables\": {\n                    \"Initiatives\": 4.0,\n                    \"Groupjoins\": 1.0,\n                    \"Contents\": 3.0,\n                    \"Activisms\": 4.0,\n                    \"Acticreas\": 4.0,\n                    \"points\": 164.0\n                }\n            }\n        ],\n        \"instance\": \"dev.co3.ontomap.eu\",\n        \"url\": \"https://api.co3-dev.firstlife.org/v6/fl/Things/5eb17fec6bd1ca539013a507\",\n        \"badges\": {\n            \"currentBadges\": [],\n            \"removedBadges\": [\n                {\n                    \"idBadge\": \"5ed76e90dbe6fc58300f8ec0\",\n                    \"assignedDate\": 1599226951894,\n                    \"expiredDate\": 1600034400000\n                },\n                {\n                    \"idBadge\": \"5ed76e90dbe6fc58300f8ec0\",\n                    \"assignedDate\": 1604226976071,\n                    \"expiredDate\": 1604876400000\n                }\n            ]\n        }\n    }\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 403 Forbidden": [
          {
            "group": "Error 403 Forbidden",
            "optional": false,
            "field": "no_access_token",
            "description": "<p>No valid access token provided</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "Client Certificate, access_token."
      }
    ],
    "version": "0.0.0",
    "filename": "app/gamification/controllers/AcaController.java",
    "groupTitle": "Gamification_Achievements_Profiles_APIs"
  },
  {
    "type": "GET",
    "url": "/gamification/profile",
    "title": "Retrieves User Achievements Profiles",
    "name": "Retrieves_User_Achievements_Profiles",
    "group": "Gamification_Achievements_Profiles_APIs",
    "description": "<p>This API offers three possible uses:</p> <ul> <li>HIERARCHY: Retrieves the subtree of the specified user achievements profile.</li> <li>SINGLE: Retrieves the specified user achievements profile.</li> <li>AGGREGATE: Retrieves the aggregated information for the specified user achievements profile (in the following denoted as U). This is the sum of achievements stored in the subtree of the hierarchy having U as root. U can be a user main profile (actor = CO3UUM ID, aca= null, group = null), an ACA (actor = CO3UUM ID, aca= ACA_NAME, group = null) or a group (actor = CO3UUM ID, aca= ACA_NAME, group = GROUP_NAME).</li> </ul>",
    "parameter": {
      "examples": [
        {
          "title": "Get user profile HIERARCHY",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/gamification/profile/user?actor=92&viewmode=HIERARCHY\n{\n    \"acaPersonas\": [\n        {\n            \"aca\": \"aca3\",\n            \"groupPersonas\": [],\n            \"badges\": {\n                \"currentBadges\": [\n                    {\n                        \"idBadge\": \"5eba9f0fc6502572e0db2fc2\",\n                        \"assignedDate\": 1589378722000,\n                        \"expiredDate\": 1590357600000\n                    }\n                ],\n                \"removedBadges\": []\n            },\n            \"variables\": {\n                \"points\": 40.0\n            },\n            \"variablesMaxValue\": {\n                \"points\": 40.0\n            },\n            \"temporalVariables\": {\n                \"MONTH\": {\n                    \"documentCreated\": 4.0,\n                    \"points\": 40.0\n                },\n                \"WEEK\": {\n                    \"documentCreated\": 4.0,\n                    \"points\": 40.0\n                }\n            }\n        }\n    ],\n    \"badges\": {\n        \"currentBadges\": [],\n        \"removedBadges\": []\n    },\n    \"variables\": {\n        \"points\": 30.0\n    },\n    \"variablesMaxValue\": {\n        \"points\": 30.0\n    },\n    \"temporalVariables\": {\n        \"MONTH\": {\n            \"documentCreated\": 3.0,\n            \"points\": 30.0\n        },\n        \"WEEK\": {\n            \"documentCreated\": 3.0,\n            \"points\": 30.0\n        }\n    },\n    \"actor\": 92,\n    \"view_mode\": \"full\"\n}",
          "type": "json"
        },
        {
          "title": "Get SINGLE user profile",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/gamification/profile/user?actor=92&viewmode=single\n{\n    \"badges\": {\n        \"currentBadges\": [],\n        \"removedBadges\": []\n    },\n    \"variables\": {\n        \"points\": 30.0\n    },\n    \"variablesMaxValue\": {\n        \"points\": 30.0\n    },\n    \"temporalVariables\": {\n        \"MONTH\": {\n            \"documentCreated\": 3.0,\n            \"points\": 30.0\n        },\n        \"WEEK\": {\n            \"documentCreated\": 3.0,\n            \"points\": 30.0\n        }\n    },\n    \"actor\": 92,\n    \"view_mode\": \"single\"\n}",
          "type": "json"
        },
        {
          "title": "Get AGGREGATE user profile",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/gamification/profile/user?actor=92&viewmode=aggregate\n{\n    \"badges\": {\n        \"currentBadges\": [\n            {\n                \"idBadge\": \"5eba9f0fc6502572e0db2fc2\",\n                \"assignedDate\": 1589378722000,\n                \"expiredDate\": 1590357600000\n            }\n        ],\n        \"removedBadges\": []\n    },\n    \"variables\": {\n        \"points\": 70.0\n    },\n    \"variablesMaxValue\": {\n        \"points\": 70.0\n    },\n    \"temporalVariables\": {\n        \"MONTH\": {\n            \"documentCreated\": 7.0,\n            \"points\": 70.0\n        },\n        \"WEEK\": {\n            \"documentCreated\": 7.0,\n            \"points\": 70.0\n        }\n    },\n    \"actor\": 92,\n    \"view_mode\": \"aggregate\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 403 Forbidden": [
          {
            "group": "Error 403 Forbidden",
            "optional": false,
            "field": "no_access_token",
            "description": "<p>No valid access token provided</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "Client Certificate, access_token."
      }
    ],
    "version": "0.0.0",
    "filename": "app/gamification/controllers/UserProfileApiController.java",
    "groupTitle": "Gamification_Achievements_Profiles_APIs"
  },
  {
    "type": "get",
    "url": "/gamification/logs",
    "title": "Get User Logs",
    "name": "Get_User_Logs",
    "group": "Gamification_Logs_APIs",
    "description": "<p>Returns the Gamification log list of a User Achievements Profile.</p>",
    "header": {
      "fields": {
        "Headers": [
          {
            "group": "Headers",
            "type": "String",
            "optional": true,
            "field": "access_token",
            "description": "<p>It's used to retrieve the actor ID and validate the request.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Filters": [
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "actor",
            "description": "<p>The CO3UUM ID of the user owner of these gamification logs. It is  <strong>Required</strong> if the request is performed using a valid Client Certificate.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "aca",
            "description": "<p>Returns only the logs concerning the actor's events occurred in that aca.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "group",
            "description": "<p>Returns only the logs concerning the actor's events occurred in that group (aca Required in this case).</p>"
          },
          {
            "group": "Filters",
            "type": "long",
            "optional": true,
            "field": "startTime",
            "description": "<p>Returns only the logs concerning the actor's events having a timestamp greater than or equal to the specified timestamp (Unix timestamp in ms).</p>"
          },
          {
            "group": "Filters",
            "type": "long",
            "optional": true,
            "field": "endTime",
            "description": "<p>Returns only the logs concerning the actor's events having a timestamp less than or equal to the specified timestamp (Unix timestamp in ms).</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "viewMode",
            "defaultValue": "AGGREGATE",
            "description": "<p>Can be <code>SINGLE</code> or <code>AGGREGATE</code>.</p> <ul> <li>If <code>viewMode=SINGLE</code> retrieves the logs of the specified user achievements profile.</li> <li>If <code>viewMode=AGGREGATE</code> retrieves the fusion of the logs concerning the hierarchy of user achievements profiles having the specified one as its root.</li> </ul>"
          },
          {
            "group": "Filters",
            "type": "Boolean",
            "optional": true,
            "field": "rules",
            "defaultValue": "true",
            "description": "<p>Specifies whether the retrieved logs must contain the entries about applied rules.</p>"
          },
          {
            "group": "Filters",
            "type": "Boolean",
            "optional": true,
            "field": "badges",
            "defaultValue": "true",
            "description": "<p>Specifies whether the retrieved logs must contain the entries about assigned badges.</p>"
          },
          {
            "group": "Filters",
            "type": "Boolean",
            "optional": true,
            "field": "resolve",
            "defaultValue": "false",
            "description": "<p>If <code>true</code> the system returns the description (structure) of the badge/rule instead of the badge/rule id</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Get Gamification Logs of a User",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/gamification/logs?page=1&pageSize=1&rules=false\n{\n\"results\": [\n{\n\"id\": \"5fb3a7063471d2759ca976af\",\n\"actor\": 1,\n\"aca\": \"https://api.co3-dev.firstlife.org/v6/fl/Things/5eb133d99ccf2b2e55ae1ebc\",\n\"idBadge\": \"5ed76e90dbe6fc58300f8ec1\",\n\"timestamp\": 1605609222407\n}\n],\n\"totalCount\": 714,\n\"page\": 1,\n\"pagesize\": 1\n}",
          "type": "json"
        },
        {
          "title": "Get Gamification Logs \"Resolved\" of a User",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/gamification/logs?page=1&pageSize=1&resolve=true&rules=false\n{\n\"results\": [\n{\n\"id\": \"5fb3a7063471d2759ca976af\",\n\"actor\": 1,\n\"aca\": \"https://api.co3-dev.firstlife.org/v6/fl/Things/5eb133d99ccf2b2e55ae1ebc\",\n\"timestamp\": 1605609222407,\n\"badge\": {\n\"id\": \"5ed76e90dbe6fc58300f8ec1\",\n\"name\": \"Silver\",\n\"active\": true,\n\"containerReference\": {\n\"id\": \"5ed76e90dbe6fc58300f8ebf\",\n\"name\": \"CO.management\",\n\"type\": \"aca\"\n},\n\"preconditions\": {\n\"variables\": [\n{\n\"variable\": \"Activisms\",\n\"threshold\": 5.0\n}\n],\n\"badgeList\": [\n\"5ed76e90dbe6fc58300f8ec0\"\n]\n}\n}\n}\n],\n\"totalCount\": 714,\n\"page\": 1,\n\"pagesize\": 1\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "optional": false,
            "field": "not_valid_view_mode",
            "description": "<p>The <code>viewMode</code> selected as filter is not valid .</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "optional": false,
            "field": "no_aca_specified",
            "description": "<p>Group specified without an ACA.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "Client Certificate, access_token."
      }
    ],
    "version": "0.0.0",
    "filename": "app/gamification/controllers/LoggerAssignmentApiController.java",
    "groupTitle": "Gamification_Logs_APIs"
  },
  {
    "type": "get",
    "url": "/gamification/admin/logs",
    "title": "Get Users Logs",
    "name": "Get_Users_Logs",
    "group": "Gamification_Logs_APIs",
    "description": "<p>Returns the Gamification log list of all User Achievements Profile.</p>",
    "header": {
      "fields": {
        "Headers": [
          {
            "group": "Headers",
            "type": "String",
            "optional": true,
            "field": "access_token",
            "description": "<p>It's used to retrieve the actor ID and validate the request.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Filters": [
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "actor",
            "description": "<p>The CO3UUM ID of the user owner of these gamification logs. It is  <strong>Required</strong> if the request is performed using a valid Client Certificate.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "aca",
            "description": "<p>Returns only the logs concerning the actor's events occurred in that aca.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "group",
            "description": "<p>Returns only the logs concerning the actor's events occurred in that group (aca Required in this case).</p>"
          },
          {
            "group": "Filters",
            "type": "long",
            "optional": true,
            "field": "startTime",
            "description": "<p>Returns only the logs concerning the actor's events having a timestamp greater than or equal to the specified timestamp (Unix timestamp in ms).</p>"
          },
          {
            "group": "Filters",
            "type": "long",
            "optional": true,
            "field": "endTime",
            "description": "<p>Returns only the logs concerning the actor's events having a timestamp less than or equal to the specified timestamp (Unix timestamp in ms).</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "variable",
            "description": "<p>Returns only the logs concerning rules have that variable as reward.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "viewMode",
            "defaultValue": "AGGREGATE",
            "description": "<p>Can be <code>SINGLE</code> or <code>AGGREGATE</code>.</p> <ul> <li>If <code>viewMode=SINGLE</code> retrieves the logs of the specified user achievements profile.</li> <li>If <code>viewMode=AGGREGATE</code> retrieves the fusion of the logs concerning the hierarchy of user achievements profiles having the specified one as its root.</li> </ul>"
          },
          {
            "group": "Filters",
            "type": "Boolean",
            "optional": true,
            "field": "rules",
            "defaultValue": "true",
            "description": "<p>Specifies whether the retrieved logs must contain the entries about applied rules.</p>"
          },
          {
            "group": "Filters",
            "type": "Boolean",
            "optional": true,
            "field": "badges",
            "defaultValue": "true",
            "description": "<p>Specifies whether the retrieved logs must contain the entries about assigned badges.</p>"
          },
          {
            "group": "Filters",
            "type": "Boolean",
            "optional": true,
            "field": "resolve",
            "defaultValue": "false",
            "description": "<p>If <code>true</code> the system returns the description (structure) of the badge/rule instead of the badge/rule id</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Get Gamification Logs of a User",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/gamification/admin/logs?page=0&pageSize=1&resolved=true\n[\n    \"results\": [\n        {\n            \"id\": \"5fb3b1523471d2759ca976e3\",\n            \"actor\": 93,\n            \"aca\": \"https://api.co3-dev.firstlife.org/v6/fl/Things/5eb133d99ccf2b2e55ae1ebc\",\n            \"event\": {\n                \"id\": \"5fb3b1523471d2759ca976e1\",\n                \"application\": \"co3.firstlife.org\",\n                \"instance\": \"dev.co3.ontomap.eu\",\n                \"actor\": 93,\n                \"timestamp\": 1605611858415,\n                \"aca\": \"https://api.co3-dev.firstlife.org/v6/fl/Things/5eb133d99ccf2b2e55ae1ebc\",\n                \"details\": {},\n                \"references\": [\n                    {\n                        \"application\": \"co3.firstlife.org\",\n                        \"external_url\": \"https://api.co3-dev.firstlife.org/v6/fl/Things/5fb3b151fc3c08384f650f7c\"\n                    }\n                ],\n                \"activity_type\": \"contribution_added\",\n                \"activity_objects\": [\n                    {\n                        \"geometry\": {\n                            \"type\": \"Point\",\n                            \"coordinates\": [\n                                7.742615222930908,\n                                45.11617660522461\n                            ]\n                        },\n                        \"properties\": {\n                            \"hasType\": \"Photo\",\n                            \"external_url\": \"https://api.co3-dev.firstlife.org/v6/fl/flresources/5fb3b152c38696634800d250\",\n                            \"hasName\": \"name\",\n                            \"hasFileId\": \"5f7725a73543064179920592\",\n                            \"hasFileType\": \"image/jpeg\"\n                        },\n                        \"externalURL\": \"https://api.co3-dev.firstlife.org/v6/fl/flresources/5fb3b152c38696634800d250\",\n                        \"hasType\": \"Photo\",\n                        \"current_visibility\": \"visible\"\n                    }\n                ]\n            },\n            \"rule\": {\n                \"id\": \"5ef1c97730d4e95532c9be7e\",\n                \"activity_type\": \"contribution_added\",\n                \"activityObjectProperties\": [\n                    {\n                        \"propertyName\": \"hasType\",\n                        \"propertyValue\": \"Photo\"\n                    }\n                ],\n                \"rewards\": {\n                    \"ACTOR\": [\n                        {\n                            \"name\": \"Contents\",\n                            \"assignment\": 1.0\n                        }\n                    ]\n                },\n                \"active\": true\n            },\n            \"role\": \"ACTOR\",\n            \"timestamp\": 1605611858415\n        }\n    ],\n    \"totalCount\": 7504,\n    \"page\": 1,\n    \"pagesize\": 1\n]",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "optional": false,
            "field": "not_valid_view_mode",
            "description": "<p>The <code>viewMode</code> selected as filter is not valid .</p>"
          },
          {
            "group": "Error 400 BadRequest",
            "optional": false,
            "field": "no_aca_specified",
            "description": "<p>Group specified without an ACA.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "Client Certificate, access_token (belong to an ACA Manager)."
      }
    ],
    "version": "0.0.0",
    "filename": "app/gamification/controllers/LoggerAssignmentApiController.java",
    "groupTitle": "Gamification_Logs_APIs"
  },
  {
    "type": "ws",
    "url": "/gamification/notifications/ws",
    "title": "Receive Gamification Notifications",
    "name": "Receive_Gamification_Notifications",
    "group": "Gamification_Logs_APIs",
    "description": "<p>WebSocket endpoint connection to receive notifications (concerning a user) when a badge is assigned or when a rule belonging to a dependency chain is triggered. Notifications have the same structure as a Gamification Entry Log with &quot;resolve&quot; set to <i>true</i> (see the <a href=\"#api-Gamification_Logs_APIs-Get_User_Logs\">Insert Gamification Logs APIs</a>).</p> <p>The Play framework used within CO3OTM does not support the management of permanent WebSockets. In order to address this issue, CO3OTM will send through the socket a ping message every 10 seconds in order to keep the socket “alive” - please ignore it: <code>{ &quot;info&quot;: &quot;keep-alive-ping&quot; }</code></p>",
    "error": {
      "fields": {
        "Error 403 Forbidden": [
          {
            "group": "Error 403 Forbidden",
            "optional": false,
            "field": "no_access_token",
            "description": "<p>No valid access token provided</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "authorization."
      }
    ],
    "version": "0.0.0",
    "filename": "app/gamification/controllers/WebSocketEndpoint.java",
    "groupTitle": "Gamification_Logs_APIs"
  },
  {
    "type": "get",
    "url": "/gamification/ranking",
    "title": "Get Variable Ranking",
    "name": "Get_Variable_Ranking",
    "group": "Gamification_Ranking_APIs",
    "description": "<p>Returns the Gamification Ranking on the specified variable. A ranking can be weekly or monthly.</p> <p>The result is an object that contains:</p> <ul> <li>The users' current positions in the ranking - paginated by 30 entries per page and cached for 5 minutes.</li> <li>The time of the ranking update.</li> <li>The date when this ranking closes. The closure date is the upcoming Monday for weekly rankings while it is the first day of the upcoming month for monthly rankings.</li> </ul>",
    "parameter": {
      "fields": {
        "Filters": [
          {
            "group": "Filters",
            "type": "String",
            "optional": false,
            "field": "variable",
            "description": "<p><strong>Required</strong> It specifies the variable of the ranking.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "timeframe",
            "defaultValue": "WEEK",
            "description": "<p>It can be <code>WEEK</code> or <code>MONTH</code>. It's used to select the weekly or monthly ranking.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "aca",
            "description": "<p>The ranking is calculated only for events done in that ACA.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "page",
            "description": "<p>The page of the ranking entries to be returned.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Get \"points\" weekly ranking",
          "content": "https://dev.co3.ontomap.eu/api/v1/gamification/ranking?variable=points\n{\n    \"variable\": \"points\",\n    \"timeframe\": \"WEEK\",\n    \"rankList\": [\n        {\n            \"actor\": 92,\n            \"value\": 50,\n            \"position\": 1\n        },\n        {\n            \"actor\": 12,\n            \"value\": 20,\n            \"position\": 2\n        },\n        {\n            \"actor\": 30,\n            \"value\": 20,\n            \"position\": 2\n        },\n        {\n            \"actor\": 36,\n            \"value\": 10,\n            \"position\": 4\n        },\n        {\n            \"actor\": 37,\n            \"value\": 0,\n            \"position\": 5\n        }\n    ],\n    \"totalCount\": 6,\n    \"endingDate\": 1589148000000,\n    \"lastUpdate\": 1588695982000,\n    \"page\": 1,\n    \"pageSize\": 30\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/gamification/controllers/RankingApiController.java",
    "groupTitle": "Gamification_Ranking_APIs",
    "error": {
      "fields": {
        "Error 400 Bad Request": [
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "invalid_timeframe",
            "description": "<p>The <i>timeframe</i> selected is not valid. Value can be <code>WEEK or MONTH</code></p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/gamification/rank",
    "title": "Get a User Rank",
    "name": "Get_a_User_Rank",
    "group": "Gamification_Ranking_APIs",
    "description": "<p>Returns the Gamification Rank of a specific user on the specified variable. A ranking can be weekly or monthly.</p> <p>The result is an object that contains:</p> <ul> <li>The users' current positions in the ranking - paginated by 30 entries per page and cached for 5 minutes.</li> <li>The time of the ranking update.</li> <li>The date when this ranking closes. The closure date is the upcoming Monday for weekly rankings while it is the first day of the upcoming month for monthly rankings.</li> </ul>",
    "header": {
      "fields": {
        "Headers": [
          {
            "group": "Headers",
            "type": "String",
            "optional": true,
            "field": "access_token",
            "description": "<p>It's used to retrieve the actor ID and validate the request.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Filters": [
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "actor",
            "description": "<p>The CO3UUM ID of the user. It is  <strong>Required</strong> if the request is performed using a valid Client Certificate.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": false,
            "field": "variable",
            "description": "<p><strong>Required</strong> It specifies the variable of the ranking.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "timeframe",
            "defaultValue": "WEEK",
            "description": "<p>It can be <code>WEEK</code> or <code>MONTH</code>. It's used to select the weekly or monthly ranking.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "aca",
            "description": "<p>The ranking is calculated only for events done in that ACA.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Get \"points\" weekly ranking",
          "content": "https://dev.co3.ontomap.eu/api/v1/gamification/rank?variable=points\n{\n    \"variable\": \"points\",\n    \"timeframe\": \"WEEK\",\n    \"endingDate\": 1589148000000,\n    \"lastUpdate\": 1588695982000,\n    \"rank\": {\n        \"actor\": 92,\n        \"value\": 50,\n        \"position\": 1\n    }\n}",
          "type": "json"
        }
      ]
    },
    "permission": [
      {
        "name": "Client Certificate, access_token."
      }
    ],
    "version": "0.0.0",
    "filename": "app/gamification/controllers/RankingApiController.java",
    "groupTitle": "Gamification_Ranking_APIs",
    "error": {
      "fields": {
        "Error 400 Bad Request": [
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "invalid_timeframe",
            "description": "<p>The <i>timeframe</i> selected is not valid. Value can be <code>WEEK or MONTH</code></p>"
          }
        ]
      }
    }
  },
  {
    "type": "POST",
    "url": "/gamification/rules/<idRule>",
    "title": "Enables or disables a rule",
    "name": "Enables_or_disables_a_rule",
    "group": "Gamification_Rules_APIs",
    "description": "<p>Enables or disables a Gamification rule.</p>",
    "header": {
      "fields": {
        "Headers": [
          {
            "group": "Headers",
            "type": "String",
            "optional": true,
            "field": "access_token",
            "description": "<p>It's used to retrieve the actor ID and validate the request and must belong to an ACA Manager.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Field Attributes": [
          {
            "group": "Field Attributes",
            "type": "String",
            "optional": true,
            "field": "actor",
            "description": "<p>The CO3UUM ID of the user (ACA Manager) who is updating the rule. It is  <strong>Required</strong> if the request is performed using a valid Client Certificate.</p>"
          },
          {
            "group": "Field Attributes",
            "type": "String",
            "optional": false,
            "field": "idRule",
            "description": "<p><strong>Required.</strong> The <i>id</i> of the rule.</p>"
          }
        ],
        "Required Attributes": [
          {
            "group": "Required Attributes",
            "type": "Boolean",
            "optional": false,
            "field": "enable",
            "description": "<p><strong>Required.</strong> Enable or disable the selected rule.</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "Client Certificate."
      }
    ],
    "version": "0.0.0",
    "filename": "app/gamification/controllers/RuleApiController.java",
    "groupTitle": "Gamification_Rules_APIs",
    "error": {
      "fields": {
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "optional": false,
            "field": "not_valid_id",
            "description": "<p>The provided <i>id</i> is not valid.</p>"
          }
        ],
        "Error 403 Forbidden": [
          {
            "group": "Error 403 Forbidden",
            "optional": false,
            "field": "gamification_disabled",
            "description": "<p>The CO3OTM Achievements Manager is not active in this CO3 instance.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "GET",
    "url": "/gamification/rules/<idRule>",
    "title": "Get a rule",
    "name": "Get_a_rule",
    "group": "Gamification_Rules_APIs",
    "description": "<p>Retrieves information about a rule.</p>",
    "parameter": {
      "fields": {
        "Field Attributes": [
          {
            "group": "Field Attributes",
            "type": "String",
            "optional": false,
            "field": "idRule",
            "description": "<p><strong>Required.</strong> The <i>id</i> of the rule to be retrieved.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Get a specific rule",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/gamification/rules/5ebaa075c6502572e0db2fc5\n\n{\n    \"id\": \"5ef1c97730d4e95532c9be7e\",\n    \"activity_type\": \"contribution_added\",\n    \"activityObjectProperties\": [\n        {\n            \"propertyName\": \"hasType\",\n            \"propertyValue\": \"Comment\"\n        }\n    ],\n    \"rewards\": {\n        \"ACTOR\": [\n            {\n                \"name\": \"Contents\",\n                \"assignment\": 1.0\n            }\n        ]\n    },\n    \"active\": true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 400 BadRequest": [
          {
            "group": "Error 400 BadRequest",
            "optional": false,
            "field": "not_valid_id",
            "description": "<p>The provided <i>id</i> is not valid.</p>"
          }
        ],
        "Error 404 NotFound": [
          {
            "group": "Error 404 NotFound",
            "optional": false,
            "field": "rule_not_found",
            "description": "<p>The provided <i>id</i> does not belong to an existing rule.</p>"
          }
        ],
        "Error 403 Forbidden": [
          {
            "group": "Error 403 Forbidden",
            "optional": false,
            "field": "gamification_disabled",
            "description": "<p>The CO3OTM Achievements Manager is not active in this CO3 instance.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/gamification/controllers/RuleApiController.java",
    "groupTitle": "Gamification_Rules_APIs"
  },
  {
    "type": "GET",
    "url": "/gamification/rules",
    "title": "Get all rules",
    "name": "Get_all_rules",
    "group": "Gamification_Rules_APIs",
    "description": "<p>Retrieves all rules in the CO3OTM Achievements Manager.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Get all Rules",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/gamification/rules\n[\n    {\n        \"id\": \"5ef1c97630d4e95532c9be7d\",\n        \"activity_type\": \"contribution_added\",\n        \"activityObjectProperties\": [\n            {\n                \"propertyName\": \"hasType\",\n                \"propertyValue\": \"Comment\"\n            }\n        ],\n        \"rewards\": {\n            \"ACTOR\": [\n                {\n                    \"name\": \"points\",\n                    \"assignment\": 1.0\n                },\n                {\n                    \"name\": \"contents\",\n                    \"assignment\": 1.0\n                 }\n            ]\n        },\n        \"active\": true\n    },\n    {\n        \"id\": \"5ef1c97730d4e95532c9be7e\",\n        \"activity_type\": \"contribution_added\",\n        \"activityObjectProperties\": [\n            {\n                \"propertyName\": \"hasType\",\n                \"propertyValue\": \"Photo\"\n            }\n        ],\n        \"rewards\": {\n            \"ACTOR\": [\n                {\n                    \"name\": \"photos\",\n                    \"assignment\": 1.0\n                }\n            ]\n        },\n        \"active\": true\n    }\n]",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "app/gamification/controllers/RuleApiController.java",
    "groupTitle": "Gamification_Rules_APIs"
  },
  {
    "type": "post",
    "url": "/gamification/rules",
    "title": "Insert new rules",
    "name": "Insert_new_rules",
    "group": "Gamification_Rules_APIs",
    "description": "<p>Creates and inserts into the CO3OTM Achievements Manager one or more rules.<br/> The request must have a <code>application/json</code> Content-Type and must include the JSON representation of the rule(s) in its body (see provided example).<br/> Rules JSON Schema can be found at <a  target=\"_blan\" href=\"./api/v1/gamification/rules/schema/\">Schema Link</a></p>",
    "header": {
      "fields": {
        "Headers": [
          {
            "group": "Headers",
            "type": "String",
            "optional": true,
            "field": "access_token",
            "description": "<p>It's used to retrieve the actor ID and validate the request and must belong to an ACA Manager.</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Filters": [
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "actor",
            "description": "<p>The CO3UUM ID of the user (ACA Manager) who is creating these rules. It is  <strong>Required</strong> if the request is performed using a valid Client Certificate.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Creation of rules",
          "content": "POST https://dev.co3.ontomap.eu/api/v1/gamification/rules\n\n[\n    {\n        \"id\": \"5ef1c97630d4e95532c9be7d\",\n        \"activity_type\": \"contribution_added\",\n        \"activityObjectProperties\": [\n            {\n                \"propertyName\": \"hasType\",\n                \"propertyValue\": \"Comment\"\n            }\n        ],\n        \"rewards\": {\n            \"ACTOR\": [\n                {\n                    \"name\": \"points\",\n                    \"assignment\": 1.0\n                }\n            ]\n        },\n        \"active\": true\n    },\n    {\n        \"id\": \"5ef1c97730d4e95532c9be7e\",\n        \"activity_type\": \"contribution_added\",\n        \"activityObjectProperties\": [\n            {\n                \"propertyName\": \"hasType\",\n                \"propertyValue\": \"Comment\"\n            }\n        ],\n        \"rewards\": {\n            \"ACTOR\": [\n                {\n                    \"name\": \"Contents\",\n                    \"assignment\": 1.0\n                }\n            ]\n        },\n        \"active\": true\n    }\n]",
          "type": "json"
        }
      ]
    },
    "permission": [
      {
        "name": "Client Certificate."
      }
    ],
    "version": "0.0.0",
    "filename": "app/gamification/controllers/RuleApiController.java",
    "groupTitle": "Gamification_Rules_APIs",
    "error": {
      "fields": {
        "Error 400 Bad Request": [
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "malformed_json",
            "description": "<p>The payload sent is not a valid JSON string or it doesn't adhere to the JSON Schema.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 415 Unsupported Media Type": [
          {
            "group": "Error 415 Unsupported Media Type",
            "optional": false,
            "field": "wrong_content_type",
            "description": "<p>The request has an unexpected Content-Type.<br/> The Content-Type for this request must be <code>application/json</code>.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ],
        "Error 403 Forbidden": [
          {
            "group": "Error 403 Forbidden",
            "optional": false,
            "field": "gamification_disabled",
            "description": "<p>The CO3OTM Achievements Manager is not active in this CO3 instance.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/instances/<concept>",
    "title": "Get concept instances",
    "name": "GetConceptInstances",
    "group": "Instances_APIs",
    "description": "<p>Returns a <a href=\"http://geojson.org/geojson-spec.html#feature-collection-objects\" target=\"_blank\">GeoJSON FeatureCollection</a>, containing a list of <a href=\"http://geojson.org/geojson-spec.html#feature-objects\" target=\"_blank\">Features</a>. <br/> Each <a href=\"http://geojson.org/geojson-spec.html#feature-objects\" target=\"_blank\">Feature</a> of the collection represents an instance of the specified concept.<br/> The <code>provenance</code> field of each Feature, if present, is a URL that can be invoked to retrieve the description of the provenance of the Feature, in JSON format.<br/> The <code>application</code> field of each Feature, if present, is a string containing the domain name of the application managing the Feature.<br/> By default, only the labels and direct URLs to the instances are included in the <code>properties</code> field of each Feature.<br/></p>",
    "parameter": {
      "examples": [
        {
          "title": "Example: Descriptions, Geometries, Source",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/instances/Park?descriptions=true&geometries=true&source=ontomap.eu,co3.app1.eu\n{\n \"type\": \"FeatureCollection\",\n \"features\": [\n {\n   \"type\": \"Feature\",\n   \"geometry\": {\"type\": \"Point\", \"coordinates\": [45.5, 7]},\n   \"id\": \"http://ontomap.eu/ontology/Park/1\",\n   \"application\": \"ontomap.eu\",\n   \"properties\": {\n     \"external_url\": \"https://co3.app1.eu/Parks/1\",\n     \"hasType\": \"Park\",\n     \"hasID\": \"1\",\n     \"hasAca\": \"https://co3.app3.eu/ACA/5\",,\n     \"hasAddress\": \"Address for Park 1\",\n   }\n },\n {\n   \"type\": \"Feature\",\n   \"geometry\": {\"type\": \"Point\", \"coordinates\": [45, 7.5]},\n   \"id\": \"https://co3.app1.eu/Parks/5\",\n   \"application\": \"co3.app1.eu\",\n   \"properties\": {\n     \"external_url\": \"https://co3.app1.eu/Parks/5\",\n     \"hasType\": \"Park\",\n     \"hasID\": \"5\",\n     \"hasAca\": \"https://co3.app3.eu/ACA/5\",,\n     \"hasAddress\": \"Address for Park 5\",\n   }\n }]\n}",
          "type": "json"
        },
        {
          "title": "Example: Count",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/instances/Park?count=true\n{\"count\": 1}",
          "type": "json"
        },
        {
          "title": "Example: Bounding box, language",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/instances/Park?lang=en&boundingbox=7.4,45.1,7.6,44.9\n{\n \"type\": \"FeatureCollection\",\n \"features\": [\n {\n   \"type\": \"Feature\",\n   \"geometry\": null,\n   \"id\": \"http://ontomap.eu/ontology/Park/2\",\n   \"application\": \"ontomap.eu\",\n   \"properties\": {\n     \"external_url\": \"https://dev.co3.ontomap.eu/api/v1/instances/Park/2\",\n     \"hasID\": \"2\",\n     \"hasAca\": \"https://co3.app3.eu/ACA/5\",,\n     \"hasType\": \"Park\"\n   }\n }]\n}",
          "type": "json"
        },
        {
          "title": "Example: Distance",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/instances/Park?distance=7.4,45.1,200\n{\n \"type\": \"FeatureCollection\",\n \"features\": [\n {\n   \"type\": \"Feature\",\n   \"geometry\": null,\n   \"id\": \"http://ontomap.eu/ontology/Park/2\",\n   \"application\": \"ontomap.eu\",\n   \"provenance\": \"http://ontomap.eu/ontology/Provenance/1\",\n   \"properties\": {\n     \"external_url\": \"https://dev.co3.ontomap.eu/api/v1/instances/Park/2\",\n     \"hasID\": \"2\",\n     \"hasAca\": \"https://co3.app3.eu/ACA/5\",,\n     \"hasType\": \"Park\"\n   }\n }]\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Required parameters": [
          {
            "group": "required_parameters",
            "type": "String",
            "optional": false,
            "field": "concept",
            "description": "<p><strong>Required.</strong> The concept whose instances are requested.</p>"
          }
        ],
        "Information type": [
          {
            "group": "Information type",
            "type": "Boolean",
            "optional": true,
            "field": "count",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, returns the number of instances of the selected concept.</p>"
          },
          {
            "group": "Information type",
            "type": "Boolean",
            "optional": true,
            "field": "descriptions",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, all the properties of the instances are included in the result.</p>"
          },
          {
            "group": "Information type",
            "type": "Boolean",
            "optional": true,
            "field": "geometries",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the <a href=\"http://geojson.org/geojson-spec.html#geometry-objects\" target=\"_blank\">geometries</a> of the instances (using EPSG:4326 as CRS) are included in the result.</p>"
          }
        ],
        "Filters": [
          {
            "group": "Filters",
            "type": "Boolean",
            "optional": true,
            "field": "subconcepts",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, all instances of the specified concept and its subconcepts are returned.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "boundingbox",
            "description": "<p>A string representing the coordinates of the North-East and South-West points of a bounding box (using EPSG:4326 as CRS).<br/> If specified, only the instances within the bounding box are returned.<br/> String format: <code>NE_lng,NE_lat,SW_lng,SW_lat</code> (see example below).</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "distance",
            "description": "<p>A string representing the center coordinates (using EPSG:4326 as CRS) and radius (in meters) of a circle.<br/> If specified, only the instances within the circle are returned.<br/> String format: <code>lng,lat,distance</code> (see example below).</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "applications",
            "defaultValue": "All applications",
            "description": "<p>A string containing one or more application domain names, separated by commas.<br/> If specified, only the instances managed by the specified applications are returned.<br/></p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "validFrom",
            "description": "<p>A <a href=\"https://en.wikipedia.org/wiki/ISO_8601\" target=\"_blank\">ISO 8601 date/time string</a>.<br/> If specified, only the instances created/valid after the specified instant are returned.<br/> Warning: this feature is not yet implemented.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "validTo",
            "description": "<p>A <a href=\"https://en.wikipedia.org/wiki/ISO_8601\" target=\"_blank\">ISO 8601 date/time string</a>.<br/> If specified, only the instances created/valid before the specified instant are returned.<br/> Warning: this feature is not yet implemented.</p>"
          }
        ],
        "Pagination": [
          {
            "group": "Pagination",
            "type": "Number",
            "size": ">=0",
            "optional": true,
            "field": "page",
            "description": "<p>If specified, the results are divided in pages and the requested page is returned.<br/> The page size is set using the <code>limit</code> parameter (default: 200 instances).</p>"
          },
          {
            "group": "Pagination",
            "type": "Number",
            "size": "1-500",
            "optional": true,
            "field": "limit",
            "defaultValue": "200",
            "description": "<p>Sets the page size. Has effect only if <code>page</code> is set.</p>"
          }
        ],
        "Presentation": [
          {
            "group": "Presentation",
            "type": "Boolean",
            "optional": true,
            "field": "prettyprint",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the JSON string served is indented.</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "Client Certificate, access_token, application token."
      }
    ],
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/InstancesController.java",
    "groupTitle": "Instances_APIs",
    "header": {
      "fields": {
        "Security Header": [
          {
            "group": "Security Header",
            "type": "String",
            "optional": true,
            "field": "access_token",
            "description": "<p>Allows to override the client certificate validation.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 Bad Request": [
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "missing_parameter",
            "description": "<p>The parameter indicated is missing.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "malformed_parameter",
            "description": "<p>The parameter indicated is malformed.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "missing_concept_mapping",
            "description": "<p>There is no corresponding mapping for the concept indicated.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "post",
    "url": "/logger/events",
    "title": "Post user activity events",
    "name": "InsertEvents",
    "group": "Logger_APIs",
    "description": "<p>Creates and inserts into CO3OTM one or more user activity events.<br/> The request must have a <code>application/json</code> Content-Type and must include the JSON representation of the event(s) in its body (see provided example).<br/></p> <p>The events are saved in the same format used for their posting, with the addition of an &quot;application&quot; (String) field. The application field contains the name of the application that posted the event, extracted from the certificate used to perform the HTTP POST Request.</p> <p>In order to post events to CO3OTM, every application must insert its mapping rules into the logger (see the <a href=\"#api-Mapping_APIs-InsertMappingRules\">Insert mapping rules API</a>).</p>",
    "permission": [
      {
        "name": "Client Certificate."
      }
    ],
    "header": {
      "fields": {
        "Request Headers": [
          {
            "group": "Request Headers",
            "optional": false,
            "field": "Idempotency-Key",
            "description": "<p>If specified, the request is considered idempotent, so it can be safely retried in case of network errors without saving duplicated events, as long as the request is resubmitted using the same key.<br/> The key is a client-generated random string; good examples of possible keys are <a href=\"https://en.wikipedia.org/wiki/Universally_unique_identifier#Version_4_.28random.29\" target=\"_blank\">V4 UUIDs</a>.<br/> Keys expire after 24 hours.<br/> It is strongly advised to always include an Idempotency Key, so that the events can be resubmitted safely in case of network problems.</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Request body (example)",
          "content": "POST https://dev.co3.ontomap.eu/api/v1/logger/events\n{\n  \"event_list\": [\n    {\n      \"actor\": 12345,\n      \"timestamp\": 1485338648883,\n      \"activity_type\": \"object_created\",\n      \"activity_objects\": [\n        {\n          \"type\": \"Feature\",\n          \"geometry\": {\n            \"type\": \"Point\",\n            \"coordinates\": [\n              7.643340826034546,\n              45.07558142970864\n            ]\n          },\n          \"properties\": {\n            \"hasType\": \"Asili\",\n            \"external_url\": \"http://co3.app1.eu/Asili/1\",\n             \"id\": \"1\",\n             \"aca\": \"https://co3.app3.eu/ACA/5\",,\n            \"nome\": \"Asilo 1\"\n          }\n        }\n      ],\n      \"references\": [\n        {\n          \"application\": \"co3.app1.eu\",\n          \"external_url\": \"http://co3.app1.eu/Asili/22\"\n        }\n      ],\n      \"visibility_details\": [\n        {\n          \"external_url\": \"http://co3.app1.eu/Asili/56\",\n          \"hidden\": true\n        }\n      ],\n      \"details\": { ... }\n    },\n    { ... }\n  ]\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Required attributes": [
          {
            "group": "Required attributes",
            "type": "Int",
            "optional": false,
            "field": "actor",
            "description": "<p><strong>Required.</strong> The CO3UUM ID of the user performing the action.</p>"
          },
          {
            "group": "Required attributes",
            "type": "Long",
            "optional": false,
            "field": "timestamp",
            "description": "<p><strong>Required.</strong> The Unix timestamp of the action (in ms).</p>"
          },
          {
            "group": "Required attributes",
            "type": "String",
            "optional": false,
            "field": "activity_type",
            "description": "<p><strong>Required.</strong> The user activity type.</p>"
          }
        ],
        "Attributes": [
          {
            "group": "Attributes",
            "type": "Array",
            "optional": true,
            "field": "activity_objects",
            "description": "<p>An Array of <a href=\"http://geojson.org/geojson-spec.html#feature-objects\" target=\"_blank\">GeoJSON Features</a>.<br/> The feature properties must include a <code>hasType</code> field (the concept to which the feature belongs) and a <code>external_url</code> field (deep link to the feature). The remaining properties (as well as the feature concept) are expressed in the terminology of the client application, and must be included in the mapping previously sent to the CO3OTM by the application.<br/> The properties can contain a <code>additionalProperties</code> object: every field contained in this object will not be translated and it is stored as-is.<br/> The same event must not contain multiple activity_objects having the same external_url.</p>"
          },
          {
            "group": "Attributes",
            "type": "Array",
            "optional": true,
            "field": "references",
            "description": "<p>An Array of objects.<br/> Every object contains three fields. The first two are mandatory: <code>external_url</code> (String) and <code>application</code> (String). The third one is optional: <code>type</code> (String) referring to a specific feature managed by a specific application and the type of reference.</p>"
          },
          {
            "group": "Attributes",
            "type": "Array",
            "optional": true,
            "field": "visibility_details",
            "description": "<p>An Array of objects.<br/> Every object contains two fields: <code>external_url</code> (String) and <code>hidden</code> (Boolean).<br/> When <code>visibility_details</code> is included in a JSON event, the feature corresponding to each <code>external_url</code> is hidden/shown from the data retrieval results, based on the corresponding <code>hidden</code> value.<br/> <code>visibility_details</code> is used when the action to be logged implies a change of visibility for some features (for example, if a feature is deleted or made private/public).<br/> The visibility change of a feature is applied only if the timestamp of the event is greater than or equal to the timestamp of the last logged event containing a visibility change for same feature.</p>"
          },
          {
            "group": "Attributes",
            "type": "Object",
            "optional": true,
            "field": "details",
            "description": "<p>An Object that can include further details regarding the user action to be logged. This field is stored as-is.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 Bad Request": [
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "duplicated_activity_object",
            "description": "<p>An event contains two or more activity_objects with the same external_url.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "not_valid_geometry",
            "description": "<p>One or more activity_objects have an invalid geometry.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "malformed_json",
            "description": "<p>The payload sent is not a valid JSON string or it doesn't adhere to the JSON Schema.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "missing_concept_mapping",
            "description": "<p>There is no corresponding mapping for the concept indicated.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "missing_property_mapping",
            "description": "<p>There is no corresponding mapping for the property indicated.</p>"
          }
        ],
        "Error 409 Conflict": [
          {
            "group": "Error 409 Conflict",
            "optional": false,
            "field": "idempotency_conflict",
            "description": "<p>A different request using the same Idempotency Key was sent previously.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 415 Unsupported Media Type": [
          {
            "group": "Error 415 Unsupported Media Type",
            "optional": false,
            "field": "wrong_content_type",
            "description": "<p>The request has an unexpected Content-Type.<br/> The Content-Type for this request must be <code>application/json</code>.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/LoggerController.java",
    "groupTitle": "Logger_APIs"
  },
  {
    "type": "get",
    "url": "/logger/events",
    "title": "Retrieve user activity events",
    "name": "RetrieveEvents",
    "group": "Logger_APIs",
    "description": "<p>Returns a JSON Object containing a list of all the logged events, sorted from the most recent to the least recent one.<br/> The events are returned in the same format used for their posting, with the addition of an &quot;application&quot; (String) field. The application field contains the name of the application that posted the event.<br/> It is possible to combine multiple filters in a single request.<br/></p>",
    "parameter": {
      "fields": {
        "Filters": [
          {
            "group": "Filters",
            "type": "Int",
            "optional": true,
            "field": "actor",
            "description": "<p>Returns only the events related to actions performed by the user having the specified CO3UUM ID.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "application",
            "description": "<p>Returns only the events related to the specified application.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "activity_type",
            "description": "<p>Returns only the events related to actions of the specified type.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "concept",
            "description": "<p>Returns only the events which contain activity_objects belonging to the specified concept (CO3OTM Ontology).</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "external_url",
            "description": "<p>Returns only the events which contain the activity_object having the specified external_url.</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "reference_concept",
            "description": "<p>Returns only the events which refer to activity_objects belonging to the specified concept (CO3OTM Ontology).</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "reference_external_url",
            "description": "<p>Returns only the events which refer to the activity_object having the specified external_url.</p>"
          },
          {
            "group": "Filters",
            "type": "Boolean",
            "optional": true,
            "field": "subconcepts",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code> and <code>concept</code> or <code>reference_concept</code> are specified, returns only the events which contain or refer to activity_objects belonging to the specified concept and its subconcepts. Has effect only if <code>concept</code> or <code>reference_concept</code> are specified.</p>"
          },
          {
            "group": "Filters",
            "type": "Long",
            "optional": true,
            "field": "start_time",
            "description": "<p>Returns only the events having a timestamp greater than or equal to the specified timestamp (Unix timestamp in ms).</p>"
          },
          {
            "group": "Filters",
            "type": "Long",
            "optional": true,
            "field": "end_time",
            "description": "<p>Returns only the events having a timestamp less than or equal to the specified timestamp (Unix timestamp in ms).</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "boundingbox",
            "description": "<p>A string representing the coordinates of the North-East and South-West points of a bounding box (using EPSG:4326 as CRS).<br/> Returns only the events including or referring to activity_objects located in the specified bounding box.<br/> String format: <code>SW_lng,SW_lat,NE_lng,NE_lat</code></p>"
          },
          {
            "group": "Filters",
            "type": "Boolean",
            "optional": true,
            "field": "aggregate",
            "description": "<p>If set to <code>true</code>, each event of the returned list will have at most one activity_object. If an activity_object (identified by application/external_url) appears in more than one event, only the most recent version of it is returned.</p>"
          },
          {
            "group": "Filters",
            "type": "Boolean",
            "optional": true,
            "field": "visibility",
            "description": "<p>If set to <code>true</code>, returns only the events which contain visible activity_objects (current_visibility = true).</p>"
          },
          {
            "group": "Filters",
            "type": "String",
            "optional": true,
            "field": "additionalProperty_X",
            "description": "<p>Returns only the events containing activity_objects whose X <code>additionalProperty</code> has the specified value (Int/Double, String and Boolean only supported).</p> <p>E.g.: <code>additionalProperty_moderation = true</code>) only returns the events which contain activity_objects have that additionalProperty <code>moderation = true</code>.</p>"
          }
        ],
        "Pagination": [
          {
            "group": "Pagination",
            "type": "Number",
            "size": ">=0",
            "optional": true,
            "field": "page",
            "description": "<p>If specified, the results are divided in pages and the requested page is returned.<br/> The page size is set using the <code>limit</code> parameter (default: 200 instances).</p>"
          },
          {
            "group": "Pagination",
            "type": "Number",
            "size": "1-500",
            "optional": true,
            "field": "limit",
            "defaultValue": "200",
            "description": "<p>Sets the page size. Has effect only if <code>page</code> is set.</p>"
          }
        ],
        "Presentation": [
          {
            "group": "Presentation",
            "type": "Boolean",
            "optional": true,
            "field": "prettyprint",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the JSON string served is indented.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Retrieve events",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/logger/events?actor=67890\n{\n  \"event_list\": [\n    {\n      \"application\": \"co3.app1.eu\",\n      \"actor\": 67890,\n      \"timestamp\": 14967425487654,\n      \"activity_type\": \"profile_updated\",\n    },\n    {\n      \"application\": \"co3.app2.eu\",\n      \"actor\": 67890,\n      \"timestamp\": 1485338648883,\n      \"activity_type\": \"object_created\",\n      \"activity_objects\": [\n        {\n          \"type\": \"Feature\",\n          \"geometry\": {\n            \"type\": \"Point\",\n            \"coordinates\": [\n              7.643340826034546,\n              45.07558142970864\n            ]\n          },\n          \"properties\": {\n            \"hasType\": \"Asili\",\n            \"external_url\": \"http://co3.app1.eu/Asili/1\",\n            \"id\": \"1\",\n            \"aca\": \"https://co3.app3.eu/ACA/5\",,\n            \"nome\": \"Asilo 1\"\n          }\n        }\n      ],\n      \"references\": [\n        {\n          \"application\": \"co3.app1.eu\",\n          \"external_url\": \"http://co3.app1.eu/Asili/22\"\n        }\n      ],\n      \"visibility_details\": [\n        {\n          \"external_url\": \"http://co3.app1.eu/Asili/56\",\n          \"hidden\": true\n        }\n      ],\n      \"details\": {\n          \"action_id\": \"76b66b1a-d846-4a56-8bfb-5aa5b4a3227f\"\n      }\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "permission": [
      {
        "name": "Client Certificate, access_token, application token."
      }
    ],
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/LoggerController.java",
    "groupTitle": "Logger_APIs",
    "header": {
      "fields": {
        "Security Header": [
          {
            "group": "Security Header",
            "type": "String",
            "optional": true,
            "field": "access_token",
            "description": "<p>Allows to override the client certificate validation.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 400 Bad Request": [
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "malformed_parameter",
            "description": "<p>The parameter indicated is malformed.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/logger/hiddenlist",
    "title": "Retrieve hidden data objects",
    "name": "RetrieveHiddenList",
    "group": "Logger_APIs",
    "description": "<p>Returns a list of all of the data objects (activity_objects) managed by the application making the request which are currently hidden from the <a href=\"#api-Instances_APIs-GetConceptInstances\">data retrieval API</a> results.<br/></p>",
    "parameter": {
      "examples": [
        {
          "title": "Retrieve list of hidden data objects",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/logger/hiddenlist\n{\n  \"hidden_list\": [\n    \"http://co3.app1.eu/Asili/1\",\n    \"http://co3.app1.eu/Asili/2\"\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "permission": [
      {
        "name": "Client Certificate."
      }
    ],
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/InstancesController.java",
    "groupTitle": "Logger_APIs",
    "error": {
      "fields": {
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/api/v1/logger/events/<id>",
    "title": "Retrieve a specific user activity event",
    "name": "RetrieveSingleEvent",
    "group": "Logger_APIs",
    "description": "<p>Returns a JSON Object containing the event from the logger having the specified <i>id</i>. The event is returned in the same format used for its posting, with the addition of an &quot;application&quot; (String) field containing the name of the application that posted the event. The name of the application is extracted from the certificate sent by the application which inserted the event.<br/> Every request made to this endpoint must include a client certificate signed by LiquidFeedback or contain the token as param.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Retrieve a single event",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/logger/events/5b103de8a1d1cd2e98cf05ed\n{\n     \"actor\": 12345,\n     \"timestamp\": 1485338648883,\n     \"activity_type\": \"object_created\",\n     \"references\": [\n       {\n          \"application\": \"co3.app1.eu\",\n          \"external_url\": \"http://co3.app1.eu/Asili/22\"\n       }\n      ],\n      \"details\": {},\n      \"application\": \"ontomap.eu\",\n      \"id\": \"5b103de8a1d1cd2e98cf05ed\",\n      \"applicationName\": \"OnToMap\"\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Required parameter": [
          {
            "group": "Required parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p><strong>Required.</strong> The id of the event that is requested.</p>"
          }
        ],
        "Presentation": [
          {
            "group": "Presentation",
            "type": "Boolean",
            "optional": true,
            "field": "prettyprint",
            "defaultValue": "false",
            "description": "<p>If set to <code>true</code>, the JSON string served is indented.</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "Client Certificate, access_token, application token."
      }
    ],
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/LoggerController.java",
    "groupTitle": "Logger_APIs",
    "error": {
      "fields": {
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "post",
    "url": "/logger/mappings",
    "title": "Insert mapping rules",
    "name": "InsertMappingRules",
    "group": "Mapping_APIs",
    "description": "<p>Publishes to CO3OTM the mapping rules for translating concepts and properties to the CO3OTM Ontology.<br/> The request must have a <code>application/json</code> Content-Type and must include the JSON representation of the mapping rules in its body (see provided example).<br/> Every request made to this endpoint must include a client certificate signed by LiquidFeedback.<br/> The mapping rules consist in a list of concept mappings, which associate a concept in the application terminology to one in the OnToMap terminology. For each concept it is possible to include a list of property mappings, each one associating a property in the app terminology to one in the OTM terminology.<br/> All the concept and property mappings must refer to concepts and properties in the  CO3OTM Ontology.<br/> When a property represents a measure, it is possible to include its measure unit, in order to avoid ambiguities.<br/> When new mapping rules are submitted, any previous rules associated to the application performing the request are overwritten.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Insert mapping rules",
          "content": "POST https://dev.co3.ontomap.eu/api/v1/logger/mappings\n{\n  \"mappings\": [\n    {\n      \"app_concept\": \"ChildCare\",\n      \"ontomap_concept\": \"Kindergarten\",\n      \"properties\": [\n        {\n          \"app_property\": \"denominazione\",\n          \"ontomap_property\": \"hasName\"\n        },\n        {\n          \"app_property\": \"id\",\n          \"ontomap_property\": \"hasID\"\n        },\n        {\n          \"app_property\": \"aca\",\n          \"ontomap_property\": \"hasAca\"\n        },\n        {\n          \"app_property\": \"indirizzo\",\n          \"ontomap_property\": \"hasAddress\"\n        },\n        {\n          \"app_property\": \"telefono\",\n          \"ontomap_property\": \"hasPhoneNumber\"\n        },\n        {\n          \"app_property\": \"retta_mensile\",\n          \"ontomap_property\": \"hasMonthlyRate\",\n          \"unit\": \"EUR\"\n        }\n      ]\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "permission": [
      {
        "name": "Client Certificate."
      }
    ],
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/MappingController.java",
    "groupTitle": "Mapping_APIs",
    "error": {
      "fields": {
        "Error 400 Bad Request": [
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "malformed_json",
            "description": "<p>The payload sent is not a valid JSON string or it doesn't adhere to the JSON Schema.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "missing_concept_mapping",
            "description": "<p>There is no corresponding mapping for the concept indicated.</p>"
          },
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "missing_property_mapping",
            "description": "<p>There is no corresponding mapping for the property indicated.</p>"
          }
        ],
        "Error 415 Unsupported Media Type": [
          {
            "group": "Error 415 Unsupported Media Type",
            "optional": false,
            "field": "wrong_content_type",
            "description": "<p>The request has an unexpected Content-Type.<br/> The Content-Type for this request must be <code>application/json</code>.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/logger/mappings",
    "title": "Retrieve mapping rules",
    "name": "RetrieveMappingRules",
    "group": "Mapping_APIs",
    "description": "<p>Returns a JSON Object containing a list of all the concept mappings associated with the application performing the request.<br/></p>",
    "parameter": {
      "examples": [
        {
          "title": "Retrieve mapping rules",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/logger/mappings\n{\n  \"application\": \"co3.app1.eu\",\n  \"mappings\": [\n    {\n      \"app_concept\": \"ChildCare\",\n      \"ontomap_concept\": \"Kindergarten\",\n      \"properties\": [\n        {\n          \"app_property\": \"denominazione\",\n          \"ontomap_property\": \"hasName\"\n        },\n        {\n          \"app_property\": \"indirizzo\",\n          \"ontomap_property\": \"hasAddress\"\n        },\n        {\n          \"app_property\": \"telefono\",\n          \"ontomap_property\": \"hasPhoneNumber\"\n        },\n        {\n          \"app_property\": \"retta_mensile\",\n          \"ontomap_property\": \"hasMonthlyRate\",\n          \"unit\": \"EUR\"\n        }\n      ]\n    }\n  ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 404 Not Found": [
          {
            "group": "Error 404 Not Found",
            "optional": false,
            "field": "mapping_not_found",
            "description": "<p>There is no mapping associated with the application yet.</p>"
          }
        ],
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "Client Certificate."
      }
    ],
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/MappingController.java",
    "groupTitle": "Mapping_APIs"
  },
  {
    "type": "DELETE",
    "url": "/logger/subscriptions/<id>",
    "title": "Delete a subscription",
    "name": "DeleteSubscription",
    "group": "Subscriptions_APIs",
    "description": "<p>Delete a subscription with the given id.</br></p>",
    "parameter": {
      "fields": {
        "Required parameter": [
          {
            "group": "Required parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p><strong>Required.</strong> The id of the subscription that has to be deleted.</p>"
          }
        ]
      }
    },
    "permission": [
      {
        "name": "Client Certificate."
      }
    ],
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/SubscriptionsController.java",
    "groupTitle": "Subscriptions_APIs",
    "error": {
      "fields": {
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/logger/subscriptions",
    "title": "Get current subscriptions",
    "name": "GetSubscriptions",
    "group": "Subscriptions_APIs",
    "description": "<p>Returns a JSON object containing a list of all subscriptions from the requesting application.</p>",
    "parameter": {
      "examples": [
        {
          "title": "Get subscriptions",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/logger/subscriptions\n{\n     \"subscriptions\":\n     [\n      {\n           \"application\": \"ontomap.eu\",\n           \"activity_type\": \"object_created\",\n           \"url\": \"https://webhook.site/4ce26b87-4252-4253-a163-9b8f8b48b8d4\",\n           \"id\": \"5b72c4ef9c71fe288a1f829b\"\n      }\n     ]\n}",
          "type": "json"
        }
      ]
    },
    "permission": [
      {
        "name": "Client Certificate."
      }
    ],
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/SubscriptionsController.java",
    "groupTitle": "Subscriptions_APIs",
    "error": {
      "fields": {
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "post",
    "url": "/logger/subscriptions",
    "title": "Create a new subscription",
    "name": "InsertSubscription",
    "group": "Subscriptions_APIs",
    "description": "<p>Create a new subscription to an event type. The subscription enables the application to receive a notification when a user performs a specific action type in a specific application. In this way an application does not need to make multiple requests to the CO3OTM in order to check if there are new events.<br/> The request must have a <code>application/json</code> Content-Type and it must include a JSON representation of the subscription in its body (see the example below).<br/> The notification received by the subscription system will contain the <i>id</i> and the <i>timestamp</i> of the event that triggered it.</p>",
    "permission": [
      {
        "name": "Client Certificate."
      }
    ],
    "parameter": {
      "examples": [
        {
          "title": "Add a subscription",
          "content": "POST https://dev.co3.ontomap.eu/api/v1/logger/subscriptions\n{\n     \"application\": \"ontomap.eu\",\n     \"activity_type\": \"object_created\",\n     \"url\": \"https://webhook.site/4ce26b87-4252-4253-a163-9b8f8b48b8d4\"\n}",
          "type": "json"
        },
        {
          "title": "Response of a subscription trigged",
          "content": "POST (from) https://dev.co3.ontomap.eu/\n{\n     \"id\": \"5b740a414620ac240c142173\",\n     \"application\": \"ontomap.eu\",\n     \"activity_type\": \"object_created\",\n     \"timestamp\": 1485338648885\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Required attributes": [
          {
            "group": "Required attributes",
            "type": "String",
            "optional": false,
            "field": "application",
            "description": "<p><strong>Required.</strong> The application where the event takes place.</p>"
          },
          {
            "group": "Required attributes",
            "type": "String",
            "optional": false,
            "field": "activity_type",
            "description": "<p><strong>Required.</strong> The user activity type.</p>"
          },
          {
            "group": "Required attributes",
            "type": "String",
            "optional": false,
            "field": "url",
            "description": "<p><strong>Required.</strong> The url where CO3OTM will send the notification (POST)</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/SubscriptionsController.java",
    "groupTitle": "Subscriptions_APIs",
    "error": {
      "fields": {
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ],
        "Error 429 Too Many Requests": [
          {
            "group": "Error 429 Too Many Requests",
            "optional": false,
            "field": "too_many_requests",
            "description": "<p>The API has received too many requests in a short time. Exponential backoff is suggested for the retrying of the requests.</p>"
          }
        ],
        "Error 400 Bad Request": [
          {
            "group": "Error 400 Bad Request",
            "optional": false,
            "field": "missing_parameter",
            "description": "<p>The parameter indicated is missing.</p>"
          }
        ],
        "Error 5xx Internal Server Error": [
          {
            "group": "Error 5xx Internal Server Error",
            "optional": false,
            "field": "internal_server_error",
            "description": "<p>There was an error during the elaboration of the request.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "get",
    "url": "/access_token",
    "title": "Get the access token",
    "name": "GetToken",
    "group": "Token_APIs",
    "description": "<p>Returns a JSON Object containing an access token for read-only use of the API. This token can be used when it is not possible or practical to use a client certificate (e.g. AJAX requests). This token can be used within a GET HTTP Request to retrieve an event list. However every event will only contain non-sensitive information (e.g.: id, name, position of the feature).</p>",
    "parameter": {
      "examples": [
        {
          "title": "Example",
          "content": "GET https://dev.co3.ontomap.eu/api/v1/access_token\n{\n  \"token\" : \"ZGNmZDk0MTYtZjQ5YS00YTQ4LTg4NzUtMDE5M2EyMDg0M2Yy\"\n}",
          "type": "json"
        }
      ]
    },
    "permission": [
      {
        "name": "Client Certificate"
      }
    ],
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/AccessTokenController.java",
    "groupTitle": "Token_APIs",
    "error": {
      "fields": {
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ]
      }
    }
  },
  {
    "type": "post",
    "url": "/access_token/renew",
    "title": "Renew the access token",
    "name": "RenewToken",
    "group": "Token_APIs",
    "description": "<p>Returns a JSON Object containing a new access token for read-only use of the API. This token can be used when it is not possible or practical to use a client certificate (e.g. AJAX requests). This token can be used within a GET HTTP Request to retrieve an event list. However every event will only contain non-sensitive information (e.g.: id, name, position of the feature).</p>",
    "permission": [
      {
        "name": "Client Certificate"
      }
    ],
    "version": "0.0.0",
    "filename": "app/logger/controllers/api/v1/AccessTokenController.java",
    "groupTitle": "Token_APIs",
    "error": {
      "fields": {
        "Error 401 Unauthorized": [
          {
            "group": "Error 401 Unauthorized",
            "optional": false,
            "field": "invalid_certificate",
            "description": "<p>The client certificate was not provided or it is invalid.</p>"
          }
        ]
      }
    }
  }
] });
