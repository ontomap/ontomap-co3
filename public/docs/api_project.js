define({
  "name": "CO3OTM",
  "version": "1.0.0",
  "description": "CO3OTM Logger Service and Data Hub APIs",
  "title": "CO3OTM API",
  "url": "https://dev.co3.ontomap.eu/api/v1",
  "template": {
    "withCompare": false,
    "withGenerator": false,
    "forceLanguage": "en"
  },
  "header": {
    "title": "Description and notes",
    "content": "<h2><strong>Description and notes</strong></h2>\n<p>This is the list of APIs currently available for the interaction with CO3OTM.\nThe list will be updated with new APIs when they will be implemented.</p>\n<ul>\n<li>Development/testing API endpoint: <a href=\"https://dev.co3.ontomap.eu\" target=\"_blank\">https://dev.co3.ontomap.eu</a> (dev/test Client Certificate)<br/></li>\n<li>Prototype 1 endpoint: <a href=\"https://pt.co3.ontomap.eu\" target=\"_blank\">https://pt.co3.ontomap.eu</a> (pt Client Certificate)</li>\n</ul>\n<p><span style=\"color:firebrick;\"><strong>It should be noted that the ontology currently used in OnToMap is under heavy development;\nthis means that you should not rely on the specification of concepts available so far:\nconcepts and relations might be modified in the next future in order to accommodate the types of information to be handled in the CO3 pilots.\nAs soon as the ontology will be stable, a link to it will be published in this page.</strong></span></p>\n<h2><em><strong>Authentications</strong></em> <a name=\"auth\"></a></h2>\n<p>CO3OTM requires three types of authentications depending on each API:</p>\n<ul>\n<li>\n<p>Client Certificate: Every request made to this endpoint must include a client certificate signed by LiquidFeedback.</p>\n</li>\n<li>\n<p>Logged User access_token: The <em>access_token</em> of the logged user must be provided as <strong>header</strong> of the HTTP Request.</p>\n<blockquote>\n<p>usage: headers[&quot;Authorization&quot;]: &quot;Bearer qhPxqbS2vSzZKF5R&quot;</p>\n</blockquote>\n</li>\n<li>\n<p>Application Token: This token is provided by CO3OTM using <a href=\"#api-Token_APIs\">Token APIs</a>. It can be used as a REST parameter of certain APIs.</p>\n<blockquote>\n<p>usage: rest_params[&quot;token&quot;]: &quot;ZGNmZDk0MTYtZjQ5YS00YTQ4LTg4NzUtMDE5M2EyMDg0M2Yy&quot;</p>\n</blockquote>\n</li>\n</ul>\n"
  },
  "order": [
    "Token_APIs",
    "Concepts_APIs",
    "Mapping_APIs",
    "Logger_APIs",
    "Instances_APIs",
    "Badge_APIs",
    "Create_Badge_Container",
    "Get_All_Badge_Containers",
    "Get_a_Badge_Container",
    "Get_a_Badge",
    "Gamification_Rules_APIs",
    "Gamification_Achievements_Profiles_APIs",
    "Gamification_Ranking_APIs",
    "Gamification_Logs_APIs",
    "Subscriptions_APIs"
  ],
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2021-02-12T20:44:14.936Z",
    "url": "https://apidocjs.com",
    "version": "0.25.0"
  }
});
