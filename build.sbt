name := "ontomap_co3"

version := "1.0"

lazy val `ontomap_co3` = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.5"

//libraryDependencies ++= Seq( javaJdbc , cache , javaWs )
libraryDependencies ++= Seq(javaJdbc, guice, cacheApi, javaWs)

//unmanagedResourceDirectories in Test <+= baseDirectory(_ / "target/web/public/test")

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

routesGenerator := InjectedRoutesGenerator

scalacOptions in(Compile, doc) += "-no-java-comments"

libraryDependencies += filters

libraryDependencies ++= Seq(
  "org.apache.jena" % "apache-jena-libs" % "3.14.0" pomOnly(),
  "com.github.java-json-tools" % "json-schema-validator" % "2.2.13",

  //  "org.mongodb" % "mongo-java-driver" % "3.12.2",
  "org.mongodb" % "mongodb-driver-sync" % "4.0.1",
  "com.github.kevinsawicki" % "http-request" % "6.0",
  "org.jsoup" % "jsoup" % "1.13.1",
  "org.json" % "json" % "20190722"

  //  "com.lightbend.play" %% "play-socket-io" % "1.0.0-beta-2"

)
