# CO3OTM

CO3 instance of OnToMap

## Prerequisites

- __ApiDoc__: Installable by this link [ApiDoc](https://apidocjs.com/). It's used to generate the frontend API page.
- __scala-sbt__: Installable by this link [scala-sbt](https://www.scala-sbt.org/). It's used to compile the project.

## Nginx Configuration

OnToMap uses PEM certificates to identify the application sending the Http Requests.
The following code (that must be edited) defines the Nginx Configuration of out OnToMap Server.
Specifically, this configuration lets nginx add headers "X-Instance","X-Ontomap-AuthStatus", "X-Ontomap-DN" to a request.
Moreover, it lets the Java Server know "who is sending the request", "which instance/pilot (host) is involved":
that's because OnToMap can provide a multitenant structure and use the host to separate data inside the database.

``` commandline
server {
    server_name DOMAIN_NAME;
    ssl_client_certificate PATH_TO_PEM_CERITIFICATE;
   
    listen 443 ssl http2;

    add_header 'Access-Control-Allow-Origin' '*';
    add_header 'Access-Control-Allow-Headers' 'Origin, X-Requested-With, Content-Type, Accept';
    include HTTPS_CONF.conf;

    keepalive_timeout 70;

    ssl_verify_client optional;

    limit_req zone=api burst=200;

    error_page 429 = /toomanyrequestserror;
    error_page 495 = /invalidcerterror;
    error_page 496 = /missingcerterror;

    location / {
        proxy_pass  http://localhost:PORT;
        proxy_set_header X-Instance $host;
        proxy_set_header X-Ontomap-AuthStatus $ssl_client_verify;
        proxy_set_header X-Ontomap-DN $ssl_client_s_dn;
    }

    location /api/v1/gamification/notifications/ws {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $host;

        proxy_pass http://localhost:PORT;

        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";

        proxy_set_header X-Instance $host;
        proxy_set_header X-Ontomap-AuthStatus $ssl_client_verify;
        proxy_set_header X-Ontomap-DN $ssl_client_s_dn;
    }
}

```

## Compile

### Frontend

If you need to update the API page simply type this commandline.

``` commandline
apidoc  -i app\gamification\controllers -i app\logger\controllers\api\v1\ -o public\docs\ -t conf\apiDoc_template
```

This passage update the API page.

### Backend

``` commandline
sbt clean stage
```

## Run

``` commandline
/target/universal/stage/bin/ontomap_co3 
    -Dhttp.port=PORT 
    -Dconfig.file=conf/co3.conf 
    -Dlogger.file=conf/prod_logging.xml 
    -DlogPath=LOG_PATH_FILE
    -Dplay.http.secret.key=YOUR_APPLICATION_SECRET
    -Dmongo.db_name=DATABASE_NAME
    -Dmongo.user=DATABASE_USER_NAME 
    -Dmongo.password=DATABASE_USER_PASSWORD
    -Dmongo.auth_source=DATABASE_AUTH_SOURCE
    -Dontomap.logger.enabled=true 
    -Dontomap.instance.url=dev.co3.ontomap.eu // It's used to generate links of Concepts
```
