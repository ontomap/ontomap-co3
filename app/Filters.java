import play.Environment;
import play.filters.cors.CORSFilter;
import play.filters.gzip.GzipFilter;
import play.http.DefaultHttpFilters;

import javax.inject.Inject;
import java.util.Arrays;

public class Filters extends DefaultHttpFilters {

    GzipFilter gzip;


    @Inject
    public Filters(Environment environment,GzipFilter gzip, CORSFilter corsFilter) {
        super(environment.isDev() ?
                Arrays.asList(gzip.asJava(),corsFilter.asJava()) :
                Arrays.asList(gzip.asJava()));
        this.gzip = gzip;
    }

}
