package logger.services;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Joiner;
import com.google.common.base.Stopwatch;
import com.google.common.base.Strings;
import com.typesafe.config.Config;
import logger.models.Concept;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerRegistry;
import org.apache.jena.vocabulary.RDFS;
import play.Environment;
import play.Logger.ALogger;
import logger.supports.QueryHelper;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class contains the methods used for interacting with the ontology.
 */
@Singleton
public class OntologyController {

    public static final String RDF_TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
    public static final String RDFS_LABEL = "http://www.w3.org/2000/01/rdf-schema#label";
    public static final String RDFS_COMMENT = "http://www.w3.org/2000/01/rdf-schema#comment";
    public static final String RDFS_DOMAIN = "http://www.w3.org/2000/01/rdf-schema#domain";
    public static final String RDFS_RANGE = "http://www.w3.org/2000/01/rdf-schema#range";
    public static final String RDFS_SUBCLASS = "http://www.w3.org/2000/01/rdf-schema#subClassOf";

    private Config configuration;
    private Environment environment;
    private QueryHelper queryHelper;
    private Lock lock;
    private ALogger logger;
    private final String HOST;

    @Inject
    public OntologyController(Config configuration, Environment environment, QueryHelper queryHelper) throws FileNotFoundException {
        this.configuration = configuration;
        this.environment = environment;
        this.queryHelper = queryHelper;
        lock = new ReentrantLock();
        this.logger = new ALogger(play.api.Logger.apply(getClass()));
        this.logger.info("Reading ontology...");
        this.HOST = configuration.getString("ontomap.instance.url");
        getOntologyModel();
    }

    /**
     * Reads the ontology from file
     * @return The OntModel representing the ontology
     * @throws FileNotFoundException If the ontology file could not be found
     */
    public OntModel getOntologyModel() throws FileNotFoundException {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        OntModel ontologyModel = ModelFactory.createOntologyModel(OntModelSpec.OWL_DL_MEM);
        File ontologyFile = environment.getFile(configuration.getString("ontomap.ontology.path"));
        lock.lock();
        try {
            ontologyModel.read(new FileInputStream(ontologyFile), null, null);
        }
        finally {
            lock.unlock();
        }
        return ontologyModel;
    }



    /**
     * Returns an ObjectNode containing all the information about a specific ontology concept.
     * @param conceptName The concept to retrieve
     * @return The ObjectNode containing the information about the concept; null if the concept doesn't exist
     */
    public Concept getOntologyConcept(String conceptName) throws FileNotFoundException {
        return getOntologyConcept(conceptName,null);
    }

    /**
     * Returns an ObjectNode containing all the information about a specific ontology concept. If lang != null, all the information
     * is returned in the language specified, if present
     * @param conceptName The concept to retrieve
     * @param lang If not null, the label/description of the concept are returned in the language selected, if they are available
     * @return The ObjectNode containing the information about the concept; null if the concept doesn't exist
     * @throws FileNotFoundException If the ontology file could not be found
     */
    public Concept getOntologyConcept(String conceptName,String lang) throws FileNotFoundException {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        OntModel ontologyModel = getOntologyModel();
        Resource conceptResource = ResourceFactory.createResource(baseURI + conceptName);

        // get all statements with the concept as subject
        StmtIterator resourceAsSubjectIterator = ontologyModel.listStatements(conceptResource, null, (RDFNode) null);
        if (!resourceAsSubjectIterator.hasNext()) return null;

        List<Concept.Property> properties = getConceptProperties(conceptName,lang);
        Concept concept = new Concept();
        concept.setPropertyList(properties);

        concept.setUri(resolveURI(baseURI + conceptName));
        concept.setId(conceptName);
        while (resourceAsSubjectIterator.hasNext()) {
            Statement statement = resourceAsSubjectIterator.nextStatement();
            if (statement.getPredicate().getURI().equals(RDFS_LABEL)) { // labels
//                Logger.debug("Label: {}",  statement.getObject().asLiteral().getString());
                if (Strings.isNullOrEmpty(lang)) {
                    concept.getLabels().put(statement.getObject().asLiteral().getLanguage(), statement.getObject().asLiteral().getString());
                }
                else if (lang.equals(statement.getObject().asLiteral().getLanguage()))
                    concept.setLabel(statement.getObject().asLiteral().getString());
            }
            else if (statement.getPredicate().getURI().equals(RDFS_COMMENT)) { // comments
                if (Strings.isNullOrEmpty(lang)) {
                    concept.getDescriptions().put(statement.getObject().asLiteral().getLanguage(), statement.getObject().asLiteral().getString());
                }
                else if (lang.equals(statement.getObject().asLiteral().getLanguage()))
                    concept.setDescription(statement.getObject().asLiteral().getString());
            }
            else if (!"SchemaThing".equals(conceptName) // subclass relations
                    && statement.getPredicate().getURI().equals(RDFS_SUBCLASS)
                    && statement.getObject().asResource().getURI().startsWith(baseURI)) {
                concept.addRelation(resolveURI(conceptResource.getURI()),resolveURI(statement.getObject().asResource().getURI()),"subClassOf");
            }
            else if (statement.getPredicate().getURI().startsWith(baseURI) && statement.getObject().isResource()) { // other relations
                concept.addRelation(resolveURI(conceptResource.getURI()),resolveURI(statement.getObject().asResource().getURI()),"");
            }
        }

        // get all statements with the concept as object
        StmtIterator resourceAsObjectIterator = getOntologyModel().listStatements(null, null, conceptResource);
        while (resourceAsObjectIterator.hasNext()) {
            Statement statement = resourceAsObjectIterator.nextStatement();
            if (statement.getPredicate().getURI().equals(RDFS_SUBCLASS)) { // subclass relations
                concept.addRelation(resolveURI(statement.getSubject().getURI()),resolveURI(conceptResource.getURI()),"subClassOf");
            }
            else if (statement.getPredicate().getURI().startsWith(baseURI)) { // other relations
//                Logger.debug("{} {} {}", statement.getSubject().getURI(), statement.getPredicate().getURI(), statement.getObject().asResource().getURI());
                concept.addRelation(resolveURI(statement.getSubject().getURI()),resolveURI(conceptResource.getURI()),resolveURI(statement.getPredicate().getURI().replace(baseURI, statement.getPredicate().getURI().replace(baseURI, ""))));
            }
        }
        return concept;
    }

    /**
     * Returns an ObjectNode containing the information about all the concepts in the ontology
     * @param lang If not null, the label/description of the concept are returned in the language selected, if they are available
     * @param relations If true, the information about the relations between concepts is returned
     * @return
     * @throws FileNotFoundException
     */
    public ObjectNode getAllConcepts(String lang, boolean relations) throws FileNotFoundException {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        OntModel ontologyModel = getOntologyModel();
        ObjectMapper om = new ObjectMapper();
        ObjectNode result = om.createObjectNode();
        ArrayNode conceptsArray = result.putArray("concepts");
        ArrayNode relationsArray = om.createArrayNode();
        if (relations) result.set("relations", relationsArray);

        Reasoner reasoner = ReasonerRegistry.getTransitiveReasoner();
        InfModel infModel = ModelFactory.createInfModel(reasoner, ontologyModel);

        Property subClassProperty = ResourceFactory.createProperty(RDFS_SUBCLASS);
        Resource schemaThingResource = ResourceFactory.createResource(baseURI + "SchemaThing");

        ResIterator resIterator = infModel.listResourcesWithProperty(subClassProperty, schemaThingResource);
        while (resIterator.hasNext()) {
            Resource resource = resIterator.nextResource();
            StmtIterator stmtIterator = ontologyModel.listStatements(resource, null, (RDFNode) null);
            ObjectNode conceptObject = om.createObjectNode();
            ObjectNode labelObject = om.createObjectNode();
            ObjectNode descriptionObject = om.createObjectNode();
            conceptObject.put("uri", resolveURI(resource.getURI()));
            conceptObject.put("id", resolveURI(resource.getURI().replace(baseURI,"")));
            while (stmtIterator.hasNext()) {
                Statement statement = stmtIterator.nextStatement();
                if (statement.getPredicate().getURI().equals(RDFS_LABEL)) { // labels
                    if (Strings.isNullOrEmpty(lang)) {
                        labelObject.put(statement.getObject().asLiteral().getLanguage(), statement.getObject().asLiteral().getString());
                    }
                    else if (lang.equals(statement.getObject().asLiteral().getLanguage()))
                        conceptObject.put("label", statement.getObject().asLiteral().getString());
                }
                else if (statement.getPredicate().getURI().equals(RDFS_COMMENT)) { // comments
                    if (Strings.isNullOrEmpty(lang)) {
                        descriptionObject.put(statement.getObject().asLiteral().getLanguage(), statement.getObject().asLiteral().getString());
                    }
                    else if (lang.equals(statement.getObject().asLiteral().getLanguage()))
                        conceptObject.put("description", statement.getObject().asLiteral().getString());
                }
                else if (relations
                        && !statement.getSubject().getURI().equals(baseURI + "SchemaThing")
                        && statement.getPredicate().getURI().equals(RDFS_SUBCLASS)
                        && statement.getObject().asResource().getURI().startsWith(baseURI)) {
                    relationsArray.add(
                            om.createObjectNode()
                                    .put("subject", resolveURI(statement.getSubject().getURI()))
                                    .put("object", resolveURI(statement.getObject().asResource().getURI()))
                                    .put("predicate", "subClassOf")
                    );
                }
                else if (relations
                        && statement.getPredicate().getURI().startsWith(baseURI)
                        && statement.getObject().isResource()) {
                    relationsArray.add(
                            om.createObjectNode()
                                    .put("subject", resolveURI(statement.getSubject().getURI()))
                                    .put("object", resolveURI(statement.getObject().asResource().getURI()))
                                    .put("predicate", resolveURI(statement.getPredicate().getURI().replace(baseURI, "")))
                    );
                }
            }
            if (Strings.isNullOrEmpty(lang)) {
                conceptObject.set("label", labelObject);
                conceptObject.set("description", descriptionObject);
            }
            conceptsArray.add(conceptObject);
        }
        return result;
    }

    /**
     * Given a concept name, this function return the list of all subconcepts
     *
     * @param concept       Name of the concept
     * @return              List al all subconcepts' name
     * @throws FileNotFoundException
     */
    public Set<String> getSubConceptsSet(String concept) throws FileNotFoundException {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        Set<String> concepts = new LinkedHashSet<>();
        String query = "SELECT ?class WHERE {?class rdfs:subClassOf* ?concept}";
        OntModel ontologyModel = getOntologyModel();
        ParameterizedSparqlString pss = queryHelper.getParameterizedQuery(query);
        pss.setIri("concept", baseURI + concept);
        Query q = pss.asQuery();
        QueryExecution queryExecution = QueryExecutionFactory.create(q, ontologyModel);
        ResultSet resultSet = queryExecution.execSelect();
        while(resultSet.hasNext()) {
            QuerySolution querySolution = resultSet.next();
            if (querySolution.contains("class"))
                concepts.add(querySolution.getResource("class").getURI().replace(baseURI, ""));
        }
        return concepts;
    }

    /**
     * @return      The List of all properties in the ontology
     * @throws FileNotFoundException
     */
    public Set<String> getPropertiesSet() throws FileNotFoundException {
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        Set<String> properties = new LinkedHashSet<>();
        String query = "SELECT DISTINCT ?prop WHERE {?prop rdf:type ?t. VALUES (?t) {(owl:DatatypeProperty) (owl:ObjectProperty)} }";
        OntModel ontologyModel = getOntologyModel();
        ParameterizedSparqlString pss = queryHelper.getParameterizedQuery(query);
        Query q = pss.asQuery();
        QueryExecution queryExecution = QueryExecutionFactory.create(q, ontologyModel);
        ResultSet resultSet = queryExecution.execSelect();
        while(resultSet.hasNext()) {
            QuerySolution querySolution = resultSet.next();
            if (querySolution.contains("prop")) {
                String prop = querySolution.getResource("prop").getURI();
                if (prop.startsWith(baseURI))
                    properties.add(prop.replace(baseURI, ""));
            }
        }
        return properties;
    }

    /**
     * Given a concept name and a lang it return a list of properties of that concept in that lang
     * @param className Name of the concept
     * @param lang Lang of properties' name
     * @return
     * @throws FileNotFoundException
     */
    public  List<Concept.Property> getConceptProperties(String className,String lang) throws FileNotFoundException {
        List<Concept.Property> properties = new ArrayList<>();
        OntModel ontModel = getOntologyModel();
        String baseURI = configuration.getString("ontomap.ontology.baseuri");
        Resource resource = ontModel.getOntResource(baseURI + className);
        if(resource== null)
            return new ArrayList<>();
        StmtIterator i = ontModel.listStatements(new SimpleSelector() {
            @Override
            public boolean test(Statement s) {
                if (s.getPredicate().equals(RDFS.domain)
                        || s.getPredicate().equals(RDFS.range)) {
                    return (s.getObject().equals(resource));
                }
                return false;
            }
        });
        while (i.hasNext()) {
            Statement s = i.next();
            Concept.Property property = new Concept.Property();
            property.setName(s.getSubject().getLocalName());
            property.setUri(resolveURI(s.getSubject().getURI()));
            property.setRange(getPropertyRange(s.getSubject().getURI()));

            Resource propertyResource =  ontModel.createResource(s.getSubject().getURI());
            List<Statement> statementList = ontModel.listStatements(propertyResource,ontModel.createProperty(RDFS_COMMENT),null,lang).toList();
            for(Statement statement : statementList){
                if(!Strings.isNullOrEmpty(lang)){
                    property.setDescription(statement.getObject().asLiteral().getString());
                }else
                 property.getDescriptions().put(statement.getLanguage(),statement.getObject().asLiteral().getString());
            }

            properties.add(property);
        }
        StmtIterator stmtIteratorSub = resource.listProperties(RDFS.subClassOf);
        while(stmtIteratorSub.hasNext()) {
            Statement statement = stmtIteratorSub.next();
            Resource subClass = (Resource) statement.getObject();
            properties.addAll(getConceptProperties(subClass.getLocalName(), lang));
        }
        return properties;
    }

    private String getPropertyRange(String uri) throws FileNotFoundException {
        OntModel ontModel = getOntologyModel();
        OntProperty ontProperty = ontModel.getOntProperty(uri);
        if(ontProperty==null){
            return null;
        }
        OntResource range = ontProperty.getRange();
        if(range == null)
            return null;
        else return range.getLocalName();
    }

    /**
     * Convert an Ontology Concept URI to a OnToMap API URL
     * @param conceptUri  Ontology Concept URI to convert
     * @return OnToMap API URL
     */
    private String resolveURI(String conceptUri){
        if(this.HOST == null)
            return conceptUri;
        return conceptUri.replace("ontomap.eu/ontology",this.HOST+"/api/v1/concepts");
    }




}
