package logger.services;

import com.mongodb.*;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigObject;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.pojo.PojoCodecProvider;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

/**
 * This class is used to provide a single instance of the database, to be used application-wide.
 */
@Singleton
public class MongoDBController {
    private MongoDatabase mongoDatabase;
    private MongoClient mongoClient;

    @Inject
    public MongoDBController(Config configuration) {
        String host = configuration.getString("mongo.host");
        int port = configuration.getInt("mongo.port");
        String db = configuration.getString("mongo.db_name");
        String dbUser = configuration.getString("mongo.user");
        String credentialSource = configuration.getString("mongo.auth_source");
        String dbPass = configuration.getString("mongo.password");
        MongoCredential credential = MongoCredential.createCredential(dbUser, credentialSource, dbPass.toCharArray());

        MongoClientSettings options = MongoClientSettings
                .builder()
                .applyToClusterSettings(builder -> {
                    builder.serverSelectionTimeout(configuration.getInt("mongo.timeout.server_selection"), TimeUnit.MILLISECONDS);
                    builder.hosts(Arrays.asList(
                            new ServerAddress(host, port)
                    ));
                })
                .applyToSocketSettings(builder -> {
                    builder.connectTimeout(configuration.getInt("mongo.timeout.connection"),TimeUnit.MILLISECONDS);
                })
                .credential(credential)
                .build();
        mongoClient = MongoClients.create(options);
        mongoDatabase = mongoClient.getDatabase(db); // .withCodecRegistry(
                // fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build())));
    }

    /**
     * Start a new session to start a new transaction
     *
     * @return a new session
     */
    public ClientSession getSession() {
        return mongoClient.startSession();
    }

    public MongoDatabase getMongoDatabase(){
        return mongoDatabase.withCodecRegistry(
                fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                        fromProviders(PojoCodecProvider.builder().automatic(true).build()))
        );
    }
}
