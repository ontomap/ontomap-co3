package logger.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.kevinsawicki.http.HttpRequest;
import com.mongodb.MongoTimeoutException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.*;
import com.mongodb.client.result.UpdateResult;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import org.bson.BsonDocument;
import org.bson.BsonString;
import org.bson.Document;

import javax.inject.Inject;
import java.io.IOException;
import java.util.Base64;
import java.util.Calendar;
import java.util.UUID;
import java.util.concurrent.TimeUnit;


/**
 * This class contains the internal methods interacting with MongoDB which concern the generation of access tokens for read-only use of the API.
 * Using this token an application get retrieve events without using certificate so the request can be done from a javascript client.
 */
public class AccessTokenMongoController {
    private final Config configuration;
    private MongoCollection<BsonDocument> accessTokenAppCollection;
    private MongoCollection<Document> accessTokenUserCollection;

    @Inject
    public AccessTokenMongoController(Config configuration, MongoDBController mongoDBController) {
        this.configuration = configuration;
        setAccessTokenAppCollection(mongoDBController);
        setAccessTokenUser(mongoDBController);
    }

    private void setAccessTokenAppCollection(MongoDBController mongoDBController){
        this.accessTokenAppCollection = mongoDBController.getMongoDatabase().getCollection(configuration.getString("mongo.collections.access_tokens_app"), BsonDocument.class);
        this.accessTokenAppCollection.createIndex(Indexes.ascending("application"),new IndexOptions().unique(true));
        this.accessTokenAppCollection.createIndex(Indexes.ascending("token"),new IndexOptions().unique(true));
    }

    private void setAccessTokenUser(MongoDBController mongoDBController){
        this.accessTokenUserCollection = mongoDBController.getMongoDatabase().getCollection(configuration.getString("mongo.collections.access_tokens_user"), Document.class);
        this.accessTokenUserCollection.createIndex(Indexes.ascending("access_token"),new IndexOptions().unique(true));
        this.accessTokenUserCollection.createIndex(Indexes.ascending("expires_date"), new IndexOptions().expireAfter( 1L, TimeUnit.SECONDS));
    }

    /**
     * Returns the access token associated with a specific application. If a token for the specified application
     * doesn't exist, it is generated and returned.
     * @param application The domain name of the application for which the token is requested
     * @return The access token associated with the application
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public String getToken(String application) throws MongoTimeoutException {
        String token;
        BsonDocument tokenEntry = accessTokenAppCollection.find(new Document().append("application", application)).first();
        if (tokenEntry == null) {
            token = generateToken(application);
        }
        else {
            token = tokenEntry.get("token").asString().getValue();
        }
        return token;
    }

    /**
     * Generates a new access token for a specific application and writes it to the database.
     * @param application The domain name of the application for which the new token is requested
     * @return The access token associated with the application
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public String generateToken(String application) throws MongoTimeoutException {
        BsonDocument tokenEntry = new BsonDocument();
        String newToken;
        BsonDocument oldEntry;
        // generate random token and check if it already exists in the DB; if so, repeat
        do {
            newToken = Base64.getUrlEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
            oldEntry = accessTokenAppCollection.find(new Document().append("token", newToken)).first();
        }
        while (oldEntry != null);
        tokenEntry.append("application", new BsonString(application));
        tokenEntry.append("token", new BsonString(newToken));
        UpdateResult res = accessTokenAppCollection.replaceOne(
                new Document().append("application", application),
                tokenEntry,
                new ReplaceOptions().upsert(true)
        );
        return newToken;
    }

    /**
     * Returns the domain name of the application associated with a specific token, if such association exists.
     * @param token The access token
     * @return The domain name of the application associated with the token if the token is valid, null otherwise
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public String verifyToken(String token) throws MongoTimeoutException {
        if (token == null) return null;
        BsonDocument tokenEntry = accessTokenAppCollection.find(new Document().append("token", token)).first();
        if (tokenEntry == null) return null;
        return tokenEntry.get("application").asString().getValue();
    }

    /**
     * Check if the user access_token is valid. The fuction check if the token is store in the cache on mongo, unless it check via validator and cache the result
     * @param token User access token to validate
     * @return if the token is valid
     * @throws MongoTimeoutException  If a problem occurred while connecting to the database
     */
    public int verifyUserToken(String instance,String token) throws MongoTimeoutException {
        token = token.replace("Bearer ","");
        if (token == null) return -1;
        int actor = -1;
        Document tokenEntry = accessTokenUserCollection.find(Filters.eq("access_token", token)).first();
        if (tokenEntry == null){
            ObjectMapper om = new ObjectMapper();
            String url = "";
            try {
                url = configuration.getConfig("liquidfeedback-endpoints.token-user.validator").getString(instance);
            }catch (ConfigException.Missing exception){
                return -1;
            }
            HttpRequest req = HttpRequest.post(url,true,"access_token",token);
            try {
                JsonNode res = om.readTree(req.body());
                if(!res.has("expires_in"))
                    return -1;
                int expires_in = res.get("expires_in").asInt();
                Document doc = new Document();
                doc.put("instance",instance);
                doc.put("access_token",token);
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.SECOND, expires_in);
                doc.put("expires_date",calendar.getTime());
                actor = res.get("member_id").asInt();
                doc.put("actor",actor);
                accessTokenUserCollection.replaceOne(Filters.eq("access_token", token),doc,new ReplaceOptions().upsert(true));
            } catch (IOException e) {
                e.printStackTrace();
                return -1;
            }
        } else {
            actor = tokenEntry.getInteger("actor");
        }
        return actor;
    }
}
