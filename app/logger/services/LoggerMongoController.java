package logger.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Strings;
import com.mongodb.*;
import com.mongodb.client.*;
import com.mongodb.client.ClientSession;
import com.mongodb.client.model.*;
import com.mongodb.client.result.DeleteResult;
import com.typesafe.config.Config;
import logger.controllers.ResponseController;
import gamification.services.GamificationService;
import logger.models.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.ext.com.google.common.collect.Streams;
import org.bson.*;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import play.Logger;
import play.Logger.ALogger;
import play.mvc.Result;
import logger.supports.BsonSerializer;
import logger.supports.CacheKey;
import logger.supports.GeoUtils;
import logger.supports.deserializers.FeatureDeserializer;
import logger.supports.deserializers.OTMEventDeserializer;
import logger.supports.exceptions.*;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

import static play.mvc.Results.ok;

/**
 * This class contains the methods used for inserting and retrieving data from the Logger.
 */
@Singleton
public class LoggerMongoController {

    public static final String APPLICATION_FIELD = "application";
    public static final String EVENT_LIST_FIELD = "event_list";
    public static final String ACTIVITY_OBJECTS_FIELD = "activity_objects";
    public static final String PROPERTIES_FIELD = "properties";
    public static final String EXTERNAL_URL_FIELD = "external_url";
    public static final String HIDDEN_FIELD = "hidden";
    public static final String VISIBILITY_DETAILS_FIELD = "visibility_details";
    public static final String HAS_TYPE_FIELD = "hasType";
    public static final String ADDITIONAL_PROPERTIES_FIELD = "additionalProperties";
    public static final String UNIT_FIELD = "unit";
    public static final String VALUE_FIELD = "value";
    public static final String REFERENCES_FIELD = "references";
    public static final String REFERENCES_FEATURE_TYPE = "type";
    public static final String TIMESTAMP_FIELD = "timestamp";
    public static final String DETAILS_FIELD = "details";
    public static final String ACTOR_FIELD = "actor";
    public static final String ACTIVITY_OBJECTS_GEOMETRY_FIELD = "activity_objects.geometry";
    public static final String GEO_OBJECTS_FEATURE_GEOMETRY_FIELD = "references.feature.geometry";
    public static final String ACTIVITY_TYPE_FIELD = "activity_type";
    public static final String ACTIVITY_OBJECTS_PROPERTIES_HAS_TYPE_FIELD = "activity_objects.properties.hasType";
    public static final String GEO_OBJECTS_FEATURE_PROPERTIES_HAS_TYPE_FIELD = "references.feature.properties.hasType";
    public static final String ACTIVITY_OBJECTS_PROPERTIES_EXTERNAL_URL_FIELD = "activity_objects.properties.external_url";
    public static final String GEO_OBJECTS_EXTERNAL_URL_FIELD = "references.external_url";
    public static final String HIDDEN_LIST_FIELD = "hidden_list";



    public static final String ACTIVITY_OBJECTS_CURRENT_VISIBILITY = "activity_objects.$.current_visibility";
    public static final String VISIBLE = "visible";
    public static final String HIDDEN = "hidden";

    public static final String PROPERTIES_EXTERNAL_URL_FIELD = "properties.external_url";
    public static final String VISIBILITY_LAST_UPDATE_TIMESTAMP_FIELD = "visibility_last_update_timestamp";
    public static final String GEOMETRY_FIELD = "geometry";
    public static final String PROPERTIES_HAS_TYPE_FIELD = "properties.hasType";
    public static final String INSTANCE_FIELD = "instance";
    public static final String ACA_FIELD = "aca";
    public static final String GROUP_FIELD = "group";

    private Config configuration;
    private ResponseController responseController;
    private MongoDBController mongoDBController;
    private FeatureController featureController;
    private MappingTranslationController mappingTranslationController;
    private OntologyController ontologyController;
    private ApplicationsController applicationsController;
    private SubscriptionsMongoController subscriptionsMongoController;
    private GamificationService gamificationController;

    // since Mongo doesn't have multi-collection transactions yet, a ReadWriteLock is used to ensure that write operations can run without interference
    private ReadWriteLock lock;
    private MongoCollection<OTMEvent> eventsCollection;
    private MongoCollection<IdempotencyKey> idempotencyCollection;
    private ALogger logger;
    private boolean validateHTML;

    @Inject
    public LoggerMongoController(Config configuration, ResponseController responseController,
                                 MongoDBController mongoDBController, FeatureController featureController, MappingTranslationController mappingTranslationController,
                                 OntologyController ontologyController, ApplicationsController applicationsController,
                                 SubscriptionsMongoController subscriptionsMongoController, GamificationService gamificationController) {
        this.configuration = configuration;
        this.responseController = responseController;
        this.mongoDBController = mongoDBController;
        this.mappingTranslationController = mappingTranslationController;
        this.ontologyController = ontologyController;
        this.applicationsController = applicationsController;
        this.subscriptionsMongoController = subscriptionsMongoController;
        this.gamificationController = gamificationController;
        this.featureController = featureController;
        this.eventsCollection = initEventsCollection(mongoDBController);
        this.idempotencyCollection = initIdempotencyCollection(mongoDBController);
        this.lock = new ReentrantReadWriteLock(true);
        this.logger = new Logger.ALogger(play.api.Logger.apply(getClass()));
        this.validateHTML = configuration.getBoolean("ontomap.application.html_snippet_validation");
    }

    /**
     * Initialize 'events' collection into the database
     *
     * @param mongoDBController
     * @return
     */
    private MongoCollection<IdempotencyKey> initIdempotencyCollection(MongoDBController mongoDBController) {
        MongoDatabase database = mongoDBController.getMongoDatabase();
        MongoCollection<IdempotencyKey> idempotencyCollection =  database.getCollection(configuration.getString("mongo.collections.idempotency"), IdempotencyKey.class);
        idempotencyCollection.createIndex(Indexes.compoundIndex(
                Indexes.ascending("application"),
                Indexes.ascending("instance"),
                Indexes.ascending("key")
        ), new IndexOptions().unique(true));
        idempotencyCollection.createIndex(Indexes.ascending("date"), new IndexOptions().expireAfter(86400L, TimeUnit.SECONDS));
        return idempotencyCollection;
    }

    /**
     * Initialize 'events' collection into the database
     *
     * @param mongoDBController
     * @return
     */
    private MongoCollection<OTMEvent> initEventsCollection(MongoDBController mongoDBController) {
        MongoDatabase database = mongoDBController.getMongoDatabase();
        MongoCollection<OTMEvent> eventsCollection = database.getCollection(this.configuration.getString("mongo.collections.events"), OTMEvent.class);
        eventsCollection.createIndex(Indexes.geo2dsphere("activity_objects.geometry"));
        return eventsCollection;
    }


    public Result insertEvents(String key, String application, String instance, long user_authenticated, List<AuthenticationType> authenticationTypes, JsonNode requestBody) throws MongoTimeoutException, UserNotValidException, NotSupportedActivityType, MalformedJsonException, InvalidGeometryException, MissingConceptMapping, MongoInvalidProperty, MissingPropertyMapping, JsonProcessingException {
        ObjectMapper om = new ObjectMapper();
        // event analysis
        List<OTMEvent> eventList = new ArrayList<>();

        Iterator<JsonNode> eventsIter = requestBody.withArray(EVENT_LIST_FIELD).elements();
        List<JsonNode> sortedEventList = Streams.stream(eventsIter).sorted((o1, o2) -> {
            long t1 = o1.get(TIMESTAMP_FIELD).longValue();
            long t2 = o2.get(TIMESTAMP_FIELD).longValue();
            return Long.compare(t1, t2);
        }).collect(Collectors.toList());

        lock.writeLock().lock();
        try (ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            try {
                // checking idempotent requests: if REPLAY or CONFLICT, don't actually write on DB
                switch (checkIdempotency(dbSession, key, application, instance, requestBody)) {
                    case REPLAY:
                        return responseController.successInsertEvents(sortedEventList.size());
                    case CONFLICT:
                        return responseController.idempotencyConflict();
                }

                SimpleModule simpleModule = new SimpleModule();
                simpleModule.addDeserializer(OTMEvent.class, new OTMEventDeserializer(instance, application));
                simpleModule.addDeserializer(Feature.class, new FeatureDeserializer(instance, application, this.mappingTranslationController, validateHTML));
                om.registerModule(simpleModule);


                for (JsonNode eventJson : sortedEventList) {
                    if (!authenticationTypes.contains(AuthenticationType.APPLICATION_CERTIFICATE) && (user_authenticated == -1 || eventJson.get("actor").asLong() != user_authenticated)) {
                        throw new UserNotValidException(user_authenticated + "");
                    }
                    OTMEvent event = null;
                    List<InstanceFeature> instanceFeatures = new ArrayList<>();
                    event = om.treeToValue(eventJson, OTMEvent.class);
                    eventList.add(event);

                    if (event.getActivityObjects() != null) {
                        // this set is used to check if an event contains non-unique external urls
                        Set<String> urls = new HashSet<>();
                        for (Feature feature : event.getActivityObjects()) {
                            if (!urls.add(feature.getExternalURL())) {
                                return responseController.duplicatedActivityObject();
                            }
                            InstanceFeature instanceFeature = new InstanceFeature(feature);
                            String visibility = featureController.isFeatureHidden(instance, application, feature.getExternalURL()) ? HIDDEN : VISIBLE;
                            feature.setCurrentVisibility(visibility);
                            instanceFeature.setCurrentVisibility(visibility);

                            instanceFeature.setApplication(application);
                            instanceFeature.setActor(event.getActor());
                            instanceFeature.setAca(event.getAca());
                            instanceFeature.setGroup(event.getGroup());
                            instanceFeature.addNumberProperty(TIMESTAMP_FIELD, event.getTimestamp());
                            instanceFeature.setVisibilityLastUpdate(event.getTimestamp());

                            if (event.getReferences() != null && event.getReferences().size() > 0) {
                                instanceFeature.setReferences(event.getReferences());
                            }

                            instanceFeatures.add(instanceFeature);

                        }
                    }
                    insertEvent(dbSession, event);
                    featureController.updateFeatures(dbSession, instance, instanceFeatures, false);

                    // applying visibility changes ("features" collection)
                    if (event.getVisibilityDetails() != null) {
                        for (OTMEvent.VisibilityDetail visibilityDetail : event.getVisibilityDetails()) {
                            setFeaturesVisibility(dbSession, instance, application, visibilityDetail.getExternalUrl(), event.getTimestamp(), visibilityDetail.isHidden());
                        }
                    }


                }

                // writing idempotency key
                writeIdempotency(key, application, instance, requestBody, dbSession);

                if (gamificationController.isActive()) {
                    gamificationController.checkEvents(dbSession, eventList);
                }
                dbSession.commitTransaction();

            } catch (Exception e) {
                dbSession.abortTransaction();
                throw e;
            } finally {
                dbSession.close();
            }
        } finally{
            lock.writeLock().unlock();
        }
        // checking subscriptions and send notifications if necessary
        subscriptionsMongoController.checkEvents(eventList);
//        notificationController.checkEvents(documentList);

        return responseController.successInsertEvents(eventList.size());
    }

    private void insertEvent(ClientSession clientSession, OTMEvent otmEvent) {
        eventsCollection.insertOne(clientSession, otmEvent);
    }

    public void insertGamificationEvent(ClientSession clientSession, OTMEvent otmEvent) {
        eventsCollection.insertOne(clientSession, otmEvent);
        subscriptionsMongoController.checkEvents(Arrays.asList(otmEvent));
    }
    public void insertGamificationEvents(ClientSession clientSession, Collection<OTMEvent> events) {
        eventsCollection.insertMany(clientSession, new ArrayList<>(events));
        subscriptionsMongoController.checkEvents(events);
    }

    /**
     * Retrieves a list of events based on the specified filters. The events returned are sorted by their timestamp, in
     * inverse chronological order.
     *
     * @param instance             The domain name of the OTM instance
     * @param actor                If not null, only events from this actor are retrieved
     * @param startTime            If not null, only events more recent than this timestamp are retrieved
     * @param endTime              If not null, only events more older than this timestamp are retrieved
     * @param application          If not null, only events from this application are retrieved
     * @param boundingBoxString    A bounding box string. If not null,
     *                             only events with activity_objects (or references to them) having geometries in this area are retrieved
     * @param activityType         If not null, only events having this activity type are retrieved
     * @param concept              If not null, only events with activity_objects which are instances of this concept are retrieved
     * @param externalUrl          If not null, only events with activity_objects with this external url are retrieved
     * @param referenceConcept     If not null, only events containing references to activity_objects which are instances of this concept are retrieved
     * @param referenceExternalUrl If not null, only events containing references to activity_objects with this external url are retrieved
     * @param limit                If > 0 (and with page > -1) sets the maximum number of results to return
     * @param page                 If > -1, sets the results page to return. The page size is set using the limit parameter
     * @param excludeFeatures      If true, the activity_objects field of each event is not returned
     * @param subConcepts          If true and a concept filter is specified, also the subconcepts of the specified concept are used to filter the events
     * @param aggregate            If true, returns a list of events having at most one activity_object; see {@link (List, AuthenticationType, int, int)}
     * @return The ObjectNode containing the list of events
     * @throws FileNotFoundException If the ontology file could not be found
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public ObjectNode retrieveEvents(String instance, boolean cleanEventList, long actor, long startTime, long endTime, String application, String boundingBoxString,
                                     String activityType, String concept, String externalUrl,
                                     String referenceConcept, String referenceExternalUrl,
                                     int limit, int page, boolean excludeFeatures, boolean subConcepts, boolean aggregate, String visibility, HashMap<String, String> additionalProperties)
            throws FileNotFoundException, MongoTimeoutException {

        List<Bson> aggregateList = new ArrayList<>();
        List<Bson> firstMatchFilters = new ArrayList<>();
        List<String> ignoredActionTypes = Arrays.asList("object_removed", "interest_removed", "support_removed", "initiator_removed", "delegation_removed", "contact_unpublished", "contribution_removed");

        firstMatchFilters.add(Filters.eq(INSTANCE_FIELD, instance));

        if (actor != -1) {
            firstMatchFilters.add(Filters.eq(ACTOR_FIELD, actor));
        }

        if (startTime != -1) {
            firstMatchFilters.add(Filters.gte(TIMESTAMP_FIELD, startTime));
        }

        if (endTime != -1) {
            firstMatchFilters.add(Filters.lte(TIMESTAMP_FIELD, endTime));
        }

        if (!Strings.isNullOrEmpty(application)) {
            firstMatchFilters.add(Filters.eq(APPLICATION_FIELD, application));
        }

        if (!Strings.isNullOrEmpty(boundingBoxString)) {
            List<Double> coordinates = GeoUtils.boundingBoxToCoords(boundingBoxString);
            if (coordinates == null) {
                throw new IllegalArgumentException("boundingbox");
            }
            firstMatchFilters.add(Filters.or(
                    Filters.geoWithin(ACTIVITY_OBJECTS_GEOMETRY_FIELD, GeoUtils.coordinatesToPolygon(coordinates)),
                    Filters.geoWithin(GEO_OBJECTS_FEATURE_GEOMETRY_FIELD, GeoUtils.coordinatesToPolygon(coordinates))
            ));
        }

        if (!Strings.isNullOrEmpty(activityType)) {
            firstMatchFilters.add(Filters.eq(ACTIVITY_TYPE_FIELD, activityType));
        }

        if (!Strings.isNullOrEmpty(concept)) {
            if (!subConcepts)
                firstMatchFilters.add(Filters.eq(ACTIVITY_OBJECTS_PROPERTIES_HAS_TYPE_FIELD, concept));
            else {
                Set<String> concepts = ontologyController.getSubConceptsSet(concept);
                List<Bson> conceptFilters = new ArrayList<>();
                for (String c : concepts) {
                    conceptFilters.add(Filters.eq(ACTIVITY_OBJECTS_PROPERTIES_HAS_TYPE_FIELD, c));
                }
                firstMatchFilters.add(Filters.or(conceptFilters));
            }
        }

        if (!Strings.isNullOrEmpty(externalUrl)) {
            firstMatchFilters.add(Filters.eq(ACTIVITY_OBJECTS_PROPERTIES_EXTERNAL_URL_FIELD, externalUrl));
        }

        if (!Strings.isNullOrEmpty(visibility)) {
            firstMatchFilters.add(Filters.eq("activity_objects.current_visibility", Boolean.parseBoolean(visibility) ? VISIBLE : HIDDEN));
        }

        if (additionalProperties.size() > 0) {
            List<Bson> additionalFilters = new ArrayList<>();
            for (String key : additionalProperties.keySet()) {
                String value = additionalProperties.get(key).toLowerCase();
                if (value.compareTo("true") == 0 || value.compareTo("false") == 0) {
                    additionalFilters.add(Filters.eq("activity_objects.properties.additionalProperties." + key, Boolean.parseBoolean(value)));
                } else if (StringUtils.isNumeric(value)) {
                    additionalFilters.add(Filters.eq("activity_objects.properties.additionalProperties." + key, Double.parseDouble(value)));
                }
                additionalFilters.add(Filters.eq("activity_objects.properties.additionalProperties." + key, additionalProperties.get(key)));
                firstMatchFilters.add(Filters.or(additionalFilters));
            }
        }

        if (firstMatchFilters.size() > 0) {
            aggregateList.add(Aggregates.match(Filters.and(firstMatchFilters)));
        } else {
            aggregateList.add(Aggregates.match(firstMatchFilters.get(0)));
        }
        if (!Strings.isNullOrEmpty(referenceConcept) || !Strings.isNullOrEmpty(referenceExternalUrl)) {
            UnwindOptions unwindOptions = new UnwindOptions();
            unwindOptions.preserveNullAndEmptyArrays(true);
            // aggregateList.add(Aggregates.unwind("$references", unwindOptions));

            aggregateList.add(Aggregates.lookup(
                    configuration.getString("mongo.collections.features"),
                    GEO_OBJECTS_EXTERNAL_URL_FIELD,
                    PROPERTIES_EXTERNAL_URL_FIELD,
                    "features"
            ));
            List<Bson> secondMatchAggregate = new ArrayList<>();
            if (!Strings.isNullOrEmpty(referenceConcept)) {
                if (!subConcepts)
                    secondMatchAggregate.add(Filters.eq(GEO_OBJECTS_FEATURE_PROPERTIES_HAS_TYPE_FIELD, referenceConcept));
                else {
                    Set<String> concepts = ontologyController.getSubConceptsSet(referenceConcept);
                    List<Bson> conceptFilters = new ArrayList<>();
                    for (String c : concepts) {
                        conceptFilters.add(Filters.eq(GEO_OBJECTS_FEATURE_PROPERTIES_HAS_TYPE_FIELD, c));
                    }
                    secondMatchAggregate.add(Filters.or(conceptFilters));
                }
            }
            if (!Strings.isNullOrEmpty(referenceExternalUrl)) {
                secondMatchAggregate.add(Filters.eq(GEO_OBJECTS_EXTERNAL_URL_FIELD, referenceExternalUrl));
            }

            if (secondMatchAggregate.size() > 0) {
                aggregateList.add(Aggregates.match(Filters.and(secondMatchAggregate)));
            } else {
                aggregateList.add(Aggregates.match(secondMatchAggregate.get(0)));
            }
        }

        aggregateList.add(Aggregates.sort(Sorts.descending(TIMESTAMP_FIELD)));

        if (aggregate) {
            aggregateList.add(Aggregates.unwind("$activity_objects"));

            // grouping events by
            aggregateList.add(new Document(
                    "$group",
                    new Document("_id", new Document().append("application", "$application").append("external_url", "$activity_objects.properties.external_url"))
                            .append("event", new Document("$first", "$$ROOT"))
            ));
//        pipeline.add(Aggregates.group("$activity_objects.properties.external_url $application", Accumulators.first("event", "$$ROOT")));
            aggregateList.add(Aggregates.replaceRoot("$event"));

            // excluding events with type "object_removed"
            aggregateList.add(Aggregates.match(Filters.not(Filters.in("activity_type", ignoredActionTypes))));
            aggregateList.add(Aggregates.sort(Sorts.descending("timestamp")));
        }


        if (excludeFeatures)
            aggregateList.add(Aggregates.project(Projections.exclude("features", ACTIVITY_OBJECTS_FIELD, INSTANCE_FIELD)));
        else aggregateList.add(Aggregates.project(Projections.exclude("features", INSTANCE_FIELD)));

        // pagination
        if (page > -1) {
            int defaultPageSize = configuration.getInt("ontomap.application.defaultpagesize");
            int pageSize = (limit > 0 && limit <= 500) ? limit : defaultPageSize;
            aggregateList.add(Aggregates.limit(pageSize));
            aggregateList.add(Aggregates.skip(pageSize * page));
        }

        ObjectMapper om = new ObjectMapper();
        SimpleModule sm = new SimpleModule();
        sm.addSerializer(BsonDocument.class, new BsonSerializer());
        om.registerModule(sm);
        ObjectNode result = om.createObjectNode();
        result.putArray(EVENT_LIST_FIELD);

        lock.readLock().lock();
        try {
            for (OTMEvent event : eventsCollection.aggregate(aggregateList)) {
                ObjectNode node = om.valueToTree(event);
//                node.putPOJO("id", node.get("_id"));
//                node.remove("_id");
                node.put("applicationName", applicationsController.getAppName(node.get("application").asText()));
                if (cleanEventList)
                    cleanEventForTokenRequest(node, aggregate);
                result.withArray(EVENT_LIST_FIELD).add(node);
            }
            return result;
        } finally {
            lock.readLock().unlock();
        }

    }

    /**
     * Given an event ID, returns the event having said ID, or null if it doesn't exist
     *
     * @param instance The domain name of the OTM instance
     * @param eventId  The ID of the event
     * @return An ObjectNode containing the event
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public ObjectNode retrieveSingleEvent(String instance, boolean cleanEventList, String eventId) throws MongoTimeoutException {
        ObjectMapper om = new ObjectMapper();
        SimpleModule sm = new SimpleModule();
        sm.addSerializer(BsonDocument.class, new BsonSerializer());
        om.registerModule(sm);
        lock.readLock().lock();
        try {
            // if id is not valid, IllegalArgumentException is thrown
            ObjectId objectId = new ObjectId(eventId);
            OTMEvent event = eventsCollection.find(new Document().append("_id", objectId).append(INSTANCE_FIELD, instance))
                    .projection(Projections.exclude("references.feature", INSTANCE_FIELD))
                    .first();
            if (event == null) return null;
            ObjectNode eventjson = om.valueToTree(event);
            if (cleanEventList)
                cleanEventForTokenRequest(eventjson, false);
            eventjson.put("applicationName", applicationsController.getAppName(event.getApplication()));
            return eventjson;
        } catch (IllegalArgumentException e) {
            // id not valid, returning null
            return null;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Deletes events from the logger. Events can be deleted based on ID (a single event), an actor (all events from that actor)
     * or source application (all events from that application). Only one filter can be specified. When an event containing an
     * activity_object is deleted from the "events" collection, the "features" collection is updated accordingly, with the
     * latest version of the features after the deletion of the events.
     *
     * @param instance          The domain name of the OTM instance
     * @param actor             If > -1, deletes all events from that actor
     * @param id                If not null, deletes the event having that ID
     * @param sourceApplication If not null, deletes the event sent by the specified application
     * @return A Play result representing a success/failure response
     * @throws JsonProcessingException  If a problem occurred while parsing JSON data
     * @throws IllegalArgumentException If not exactly one filter is specified
     */
    public Result deleteUserEvents(String instance, long actor, String id, String sourceApplication) throws JsonProcessingException, IllegalArgumentException {
        MongoDatabase database = mongoDBController.getMongoDatabase();

        // we need to delete documents from 2 collections
        MongoCollection<Document> featuresCollection = database.getCollection(configuration.getString("mongo.collections.features"));

        try (ClientSession dbSession = mongoDBController.getSession()) {

            dbSession.startTransaction();

            // iterable used to find which documents must be updated/deleted from the "features" collection
            FindIterable<OTMEvent> objectsToUpdateIterable = eventsCollection.find(dbSession).projection(Projections.include("activity_objects.properties.external_url", APPLICATION_FIELD));
            ObjectMapper om = new ObjectMapper();

            Document eventFilter = new Document();
            eventFilter.put(INSTANCE_FIELD, instance);
            if (actor != -1) eventFilter.put(ACTOR_FIELD, actor);
            if (!Strings.isNullOrEmpty(id)) eventFilter.put("_id", new ObjectId(id));
            if (!Strings.isNullOrEmpty(sourceApplication)) eventFilter.put(APPLICATION_FIELD, sourceApplication);

            // checking that only one filter is active before deleting events
            if (eventFilter.size() != 2) throw new IllegalArgumentException();

            objectsToUpdateIterable.filter(eventFilter);
            Set<Pair<String, String>> featureToRemove = new HashSet<>();
            long featuresDeletedCount;
            long eventsDeletedCount;
            lock.writeLock().lock();
            try {
                // getting app/extUrl of features to update
                for (OTMEvent event : objectsToUpdateIterable) {
                    String application = event.getApplication();
                    if (event.getActivityObjects() != null) {
                        featureToRemove.addAll(
                                event.getActivityObjects().stream() // using a stream to create a list of app/exturl pairs
                                        .map(val -> Pair.of(application, val.getExternalURL()))
                                        .collect(Collectors.toList()));
                    }
                }

                HashMap<CacheKey, Feature> updateMap = new HashMap<>();
                // for each feature to update, get the most recent version of that feature, excluding the event to delete
                for (Pair<String, String> p : featureToRemove) {
                    FindIterable<OTMEvent> featureIterable = eventsCollection.find(dbSession);
                    List<Bson> featFilters = new ArrayList<>();
                    featFilters.add(Filters.eq(INSTANCE_FIELD, instance));
                    featFilters.add(Filters.eq(APPLICATION_FIELD, p.getLeft()));
                    featFilters.add(Filters.eq(ACTIVITY_OBJECTS_PROPERTIES_EXTERNAL_URL_FIELD, p.getRight()));

                    // excluding the event to be deleted from the search
                    if (actor != -1) featFilters.add(Filters.not(Filters.eq(ACTOR_FIELD, actor)));
                    if (!Strings.isNullOrEmpty(id)) featFilters.add(Filters.not(Filters.eq("_id", new ObjectId(id))));
                    if (!Strings.isNullOrEmpty(sourceApplication))
                        featFilters.add(Filters.not(Filters.eq(APPLICATION_FIELD, sourceApplication)));

                    featureIterable.filter(Filters.and(featFilters));

                    // sorting by timestamp (most recent event first) and getting the first result (which will be the most recent)
                    featureIterable.sort(Sorts.descending(TIMESTAMP_FIELD));
                    OTMEvent event = featureIterable.first();
                    if (event != null) {
                        // using a stream to get the right activity_object (an event could contain more than one of them)
                        Feature feature = event.getActivityObjects().stream().filter(f -> f.getExternalURL().equals(p.getRight())).findFirst().get();

                        //adding some required fields to the feature
                        feature.setApplication(event.getApplication());
                        feature.addNumberProperty(TIMESTAMP_FIELD, event.getTimestamp());
                        updateMap.put(new CacheKey(p.getLeft(), p.getRight()), feature);
                    } else updateMap.put(new CacheKey(p.getLeft(), p.getRight()), null);
                }


                // deleting events
                DeleteResult deleteResult = eventsCollection.deleteMany(dbSession, eventFilter);
                eventsDeletedCount = deleteResult.getDeletedCount();

                List<InstanceFeature> updateList = new ArrayList<>();
                featuresDeletedCount = 0;

                // for each feature to update, either update it with the appropriate version
                for (Map.Entry<CacheKey, Feature> e : updateMap.entrySet()) {
                    if (e.getValue() == null) {
                        Document deleteFilter = new Document()
                                .append(INSTANCE_FIELD, instance)
                                .append(APPLICATION_FIELD, e.getKey().getKeys()[0])
                                .append(PROPERTIES_EXTERNAL_URL_FIELD, e.getKey().getKeys()[1]);
                        deleteResult = featuresCollection.deleteOne(dbSession, deleteFilter);
                        featuresDeletedCount += deleteResult.getDeletedCount();
                    } else {
                        updateList.add(new InstanceFeature(e.getValue()));
                    }
                }
                featureController.updateFeatures(dbSession, instance, updateList, true);

                dbSession.commitTransaction();

            } catch (Exception e) {
                dbSession.abortTransaction();
                throw e;
            } finally {
                dbSession.close();
            }
            return ok(om.createObjectNode()
                    .put("status", "ok")
                    .put("events_deleted", eventsDeletedCount)
                    .put("features_deleted", featuresDeletedCount));
        }
    }

    /**
     * Checks the status of a request including an idempotency key. If an idempotency key is not provided, the requested
     * operation can always be executed; if a key is provided, the request can be executed only if the same key hasn't been
     * used in the previous 24 hours. If a key is reused, the request is considered a replay if the request body is the same
     * as the previous one; if not, the request is considered in conflict with the previous one.
     *
     * @param key         The idempotency key
     * @param application The application performing the request
     * @param instance    The domain name of the OTM instance
     * @param requestBody The body of the request
     * @return {@link IdempotencyResult#SUCCESS} if the requested operation can be executed; {@link IdempotencyResult#REPLAY}
     * if the same request has already been sent; {@link IdempotencyResult#CONFLICT} if the requested operation can't be executed
     */
    private IdempotencyResult checkIdempotency(ClientSession clientSession, String key, String application, String instance, JsonNode requestBody) {
        if (key == null) return IdempotencyResult.SUCCESS;
        MongoDatabase database = mongoDBController.getMongoDatabase();
        Document filter = new Document()
                .append("key", key)
                .append("application", application)
                .append("instance", instance);
        IdempotencyKey entry = idempotencyCollection.find(clientSession).filter(filter).first();
        if (entry == null) return IdempotencyResult.SUCCESS;
        Document payload = entry.getPayload();
//        Logger.debug("{}, {}", payload.hashCode(), payload.toString());
//        Logger.debug("{}, {}", requestBody.hashCode(), requestBody.toString());
        if (payload.hashCode() == requestBody.hashCode()) return IdempotencyResult.REPLAY; // todo: equals?
        else return IdempotencyResult.CONFLICT;
    }

    /**
     * Writes on DB a record for storing a request performed with an idempotency key. Records older than 24 hours are automatically
     * purged from DB (index on date field with expire option)
     *
     * @param key         The idempotency key
     * @param application The application performing the request
     * @param instance    The domain name of the OTM instance
     * @param requestBody The body of the request
     * @throws MongoWriteException If a problem occurred while connecting to the database
     */
    private void writeIdempotency(String key, String application, String instance, JsonNode requestBody, ClientSession session) throws MongoWriteException {
        if (Strings.isNullOrEmpty(key)) return;
        IdempotencyKey idempotencyKey = new IdempotencyKey(key,application,instance,new Date(), Document.parse(requestBody.toString()));
        idempotencyCollection.insertOne(session, idempotencyKey);
    }

    /**
     * An enum representing the possible results of an idempotent request
     */
    public enum IdempotencyResult {
        SUCCESS, REPLAY, CONFLICT
    }

//    /**
//     * Updates the references for the newly inserted events.
//     * The reference of the feature is added to the event, why? In order to simplify the some API search like by "reference concept"
//     * @param instance The domain name of the OTM instance
//     * @throws MongoTimeoutException If a problem occurred while connecting to the database
//     */
//    private void updateReferences(String instance,ClientSession dbSession) throws MongoTimeoutException {
//        MongoDatabase database = mongoDBController.getDatabase();
//        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("mongo.collections.events"), BsonDocument.class);
//        List<Bson> filters = new ArrayList<>();
//        filters.add(Filters.eq(INSTANCE_FIELD, instance));
//        filters.add(Filters.exists(REFERENCES_FIELD));
//        // only the events without a feature in the references field are to be updated
//        filters.add(Filters.not(Filters.exists(REFERENCES_FEATURE_FIELD)));
//        FindIterable<BsonDocument> findIterable = collection.find(dbSession,Filters.and(filters));
//        List<WriteModel<BsonDocument>> writeList = new ArrayList<>();
//
//        // iterate over the events to update
//        try(MongoCursor<BsonDocument> cursor = findIterable.iterator()) {
//            while (cursor.hasNext()) {
//                BsonDocument event = cursor.next();
//                boolean edited = false;
//                if (event.isArray(REFERENCES_FIELD)) {
//
//                    // for each reference...
//                    for (BsonValue bsonValue : event.getArray(REFERENCES_FIELD)) {
//                        BsonDocument reference = bsonValue.asDocument();
//                        // ...check if it has to be updated...
//                        if (reference.get(FEATURE_FIELD) == null) {
//                            // ...and get the right version of the feature
//                            BsonDocument feature = featureController.getSingleFeature(dbSession,
//                                    instance,
//                                    reference.getString(APPLICATION_FIELD).getValue(),
//                                    reference.getString(EXTERNAL_URL_FIELD).getValue(), event.getNumber(TIMESTAMP_FIELD).longValue());
//
//                            if (feature != null) {
//                                reference.put(FEATURE_FIELD, feature);
//                                edited = true;
//                            }
//                        }
//                    }
//                    // if a change occurred, schedule the update of the event
//                    if (edited) {
//                        writeList.add(new ReplaceOneModel<>(Filters.eq("_id", event.get("_id")), event));
//                    }
//                }
//
//            }
//        }
//        // if there are events to update, perform the operation
//        if (!writeList.isEmpty()) collection.bulkWrite(dbSession,writeList);
//    }

    /**
     * Sets the hidden state of a specific feature.
     *
     * @param dbSession   The client session of the db transaction
     * @param instance    The domain name of the OTM instance
     * @param application The source application of the feature
     * @param externalUrl The external url of the feature
     * @param timestamp   The timestamp of the event
     * @param hidden      The hidden state to set
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    private void setFeaturesVisibility(ClientSession dbSession, String instance, String application, String externalUrl, long timestamp, boolean hidden) throws MongoTimeoutException {

        long featuresEditedCount = featureController.hideFeature(dbSession, instance, application, externalUrl, timestamp, hidden);

        //check if some row was modified: the application may not be the owner of the feature.
        // if the application it's the owner of the feature, I update all the events containing this feature
        if (featuresEditedCount > 0) {
            String visibility = hidden ? HIDDEN : VISIBLE;
            eventsCollection.updateMany(dbSession,
                    Filters.and(
                            Filters.eq(INSTANCE_FIELD, instance),
                            Filters.eq(ACTIVITY_OBJECTS_PROPERTIES_EXTERNAL_URL_FIELD, externalUrl)),
                    Updates.set(ACTIVITY_OBJECTS_CURRENT_VISIBILITY, visibility));
        }
    }

    /**
     * This metod is used to make the "visibility_status" update retroactive.
     * It was used to make consistent the db.
     */
    /*public void updateFeatureVisiblityIntoEvents(){
        MongoDatabase database = mongoDBController.getDatabase();
        MongoCollection<Document> featuresCollection = database.getCollection(configuration.getString("mongo.collections.features"));
        MongoCollection<Document> eventCollection = database.getCollection(configuration.getString("mongo.collections.events"));

        lock.writeLock().lock();
        try(ClientSession dbSession = mongoDBController.getSession()) {

            dbSession.startTransaction();
            List<Bson> filters = new ArrayList<>();

            FindIterable<Document> findIterable = featuresCollection.find(dbSession);

            try (MongoCursor<Document> cursor = findIterable.iterator()) {
                while (cursor.hasNext()) {
                    Document feature = cursor.next();

                    String visibility = (Boolean) feature.getOrDefault(HIDDEN, false) ? HIDDEN : VISIBLE;
                    String instance = feature.getString(INSTANCE_FIELD);
                    String externalUrl = ((Document) feature.get(PROPERTIES_FIELD)).getString(EXTERNAL_URL_FIELD);

                    filters.clear();
                    filters.add((Filters.eq(INSTANCE_FIELD, instance)));
                    filters.add(Filters.eq(ACTIVITY_OBJECTS_PROPERTIES_EXTERNAL_URL_FIELD, externalUrl));
                    UpdateResult res = eventCollection.updateMany(dbSession,Filters.and(filters), Updates.set(ACTIVITY_OBJECTS_CURRENT_VISIBILITY, visibility));
                }

                dbSession.commitTransaction();

            } catch (Exception e){
                dbSession.abortTransaction();
                throw e;
            }finally {
                dbSession.close();
            }
        } finally {
            lock.writeLock().unlock();
        }
    }*/


    /**
     * If an application wants events using APPLICATION_CERTIFICATE TOKEN instead of the certificate validation, the system will
     * "clean" all the events from the private data like the actor, description.
     *
     * @param event     the event to clean
     * @param aggregate true if it's an aggregate search
     */
    private void cleanEventForTokenRequest(ObjectNode event, boolean aggregate) {
        List<String> availableDetails = Arrays.asList("name");
        if (event.has("actor")) {
            event.put("actor", (String) null);
        }
        if (event.has(ACTIVITY_OBJECTS_FIELD)) {
            if (aggregate) {
                JsonNode act_obj = event.get(ACTIVITY_OBJECTS_FIELD);
                cleanFeatureForTokenRequest(act_obj);
            } else for (JsonNode act_obj : event.get(ACTIVITY_OBJECTS_FIELD)) {
                cleanFeatureForTokenRequest(act_obj);
            }
        }
        if (event.has(REFERENCES_FIELD)) {
            event.remove(REFERENCES_FIELD);
        }
        if (event.has(DETAILS_FIELD)) {
            for (Iterator<String> detail = event.get(DETAILS_FIELD).fieldNames(); detail.hasNext(); ) {
                String detailKey = detail.next();
                if (!availableDetails.contains(detailKey)) {
                    ((ObjectNode) event.get(DETAILS_FIELD)).put(detailKey, (String) null);
                }
            }
        }
        if (event.has(VISIBILITY_DETAILS_FIELD)) {
            event.remove(VISIBILITY_DETAILS_FIELD);
        }
    }

    /**
     * If an application wants events using APPLICATION_TOKEN instead of the certificate validation, the system will
     * "clean" all the events from the private data like the actor, description.
     *
     * @param act_obj the feature to change
     */
    private void cleanFeatureForTokenRequest(JsonNode act_obj) {
        List<String> availableProps = Arrays.asList(ADDITIONAL_PROPERTIES_FIELD, "hasType", "hasAddress", "hasName", "external_url");
        List<String> availableAdditionalProps = Arrays.asList("name", "address", "zoom_level");
        if (act_obj.has(PROPERTIES_FIELD)) {
            JsonNode props = act_obj.get(PROPERTIES_FIELD);
            for (Iterator<String> it = props.fieldNames(); it.hasNext(); ) {
                String key = it.next();
                if (!availableProps.contains(key)) {
                    ((ObjectNode) props).put(key, (String) null);
                }
            }
            if (props.has(ADDITIONAL_PROPERTIES_FIELD)) {
                for (Iterator<String> addProp = props.get(ADDITIONAL_PROPERTIES_FIELD).fieldNames(); addProp.hasNext(); ) {
                    String keyAdds = addProp.next();
                    if (!availableAdditionalProps.contains(keyAdds)) {
                        ((ObjectNode) props.get(ADDITIONAL_PROPERTIES_FIELD)).put(keyAdds, (String) null);
                    }
                }
            }
        }
    }
}
