package logger.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Strings;
import com.mongodb.MongoTimeoutException;
import com.mongodb.client.*;
import com.mongodb.client.model.*;
import com.mongodb.client.result.UpdateResult;
import com.typesafe.config.Config;
import logger.models.Feature;
import logger.models.InstanceFeature;
import logger.models.OTMEvent;
import org.apache.commons.lang3.tuple.Pair;
import org.bson.*;
import org.bson.conversions.Bson;
import logger.supports.BsonSerializer;
import logger.supports.GeoUtils;

import javax.inject.Inject;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.*;
import static logger.services.LoggerMongoController.*;

public class FeatureController {
    private Config config;
    private MongoCollection<InstanceFeature> featuresCollection;
    private ApplicationsController applicationsController;

    public static final String PROPERTIES_TIMESTAMP_FIELD = "properties.timestamp";

    @Inject
    public FeatureController(Config config, ApplicationsController applicationsController) {
        this.config = config;
        this.applicationsController = applicationsController;

    }


    /**
     * Initialize 'features' collection into the database
     *
     * @param mongoDBController
     */
    @Inject
    private void initFeaturesCollection(MongoDBController mongoDBController) {
        MongoDatabase database = mongoDBController.getMongoDatabase();
        MongoCollection<InstanceFeature> featuresCollection = database.getCollection(config.getString("mongo.collections.features"), InstanceFeature.class);
        featuresCollection.createIndex(Indexes.geo2dsphere("geometry"));
        //featuresCollection.createIndex(Indexes.compoundIndex(Indexes.ascending("application"),Indexes.ascending("properties.external_url")),new IndexOptions().unique(true));
        this.featuresCollection = featuresCollection;
    }


    /**
     * Returns the count of the concept instances from the Logger which satisfy the specified filters.
     *
     * @param instance          The domain name of the OTM instance
     * @param application       If not null, only concept instances from this application are retrieved
     * @param boundingBoxString A bounding box string. If not null,
     *                          only concept instances located in this area are retrieved
     * @param distanceString    A distance string If not null, only concept instances
     *                          located in the circle specified are retrieved
     * @param concepts          If not null, only instances of the specified concepts are retrieved
     * @return The count of the concept instances
     * @throws IllegalArgumentException If boundingBoxString or distanceString are in a wrong format
     * @throws MongoTimeoutException    If a problem occurred while connecting to the database
     */
    public int getFeaturesCount(ClientSession clientSession, String instance, String application, String boundingBoxString, String distanceString, Set<String> concepts)
            throws IllegalArgumentException, IOException, MongoTimeoutException {
        List<Feature> result = getFeatures(clientSession, instance, application, boundingBoxString, distanceString, concepts, true, true);
        return result.size();
    }

    public int getFeaturesCount(String instance, String application, String boundingBoxString, String distanceString, Set<String> concepts)
            throws IllegalArgumentException, IOException, MongoTimeoutException {
        List<Feature> result = getFeatures(null, instance, application, boundingBoxString, distanceString, concepts, true, true);
        return result.size();
    }

    /**
     * Returns a list of concept instances from the Logger, based on the specified filters.
     *
     * @param instance          The domain name of the OTM instance
     * @param application       If not null, only concept instances from this application are retrieved
     * @param boundingBoxString A bounding box string. If not null,
     *                          only concept instances located in this area are retrieved
     * @param distanceString    A distance string. If not null, only concept instances
     *                          located in the circle specified are retrieved
     * @param concepts          If not null, only instances of the specified concepts are retrieved
     * @param descriptions      If true, includes the complete set of properties for each concept instance
     * @param geometries        If true, includes the geometry for each concept instance
     * @return The list of the concept instances
     * @throws IllegalArgumentException If boundingBoxString or distanceString are in a wrong format
     * @throws MongoTimeoutException    If a problem occurred while connecting to the database
     */
    public List<Feature> getFeatures(ClientSession session, String instance, String application, String boundingBoxString, String distanceString, Set<String> concepts, boolean descriptions, boolean geometries)
            throws IllegalArgumentException, MongoTimeoutException {

        ObjectMapper om = new ObjectMapper();
        SimpleModule sm = new SimpleModule();
        sm.addSerializer(BsonDocument.class, new BsonSerializer());
        om.registerModule(sm);

        if (!config.getBoolean("ontomap.logger.enabled")) {
            return new ArrayList<>();
        }

        List<Bson> filters = new ArrayList<>();

        filters.add(eq(INSTANCE_FIELD, instance));
        filters.add(exists(HIDDEN_FIELD, false));

        if (!Strings.isNullOrEmpty(application)) {
            filters.add(eq(APPLICATION_FIELD, application));
        }

        if (!Strings.isNullOrEmpty(boundingBoxString)) {
            List<Double> coordinates = GeoUtils.boundingBoxToCoords(boundingBoxString);
            if (coordinates == null) {
                throw new IllegalArgumentException("boundingbox");
            }
            filters.add(geoWithin(GEOMETRY_FIELD, GeoUtils.coordinatesToPolygon(coordinates)));
            filters.add(ne(GEOMETRY_FIELD, null));
        }

        if (!Strings.isNullOrEmpty(distanceString)) {
            Pair<Bson, Double> distancePair = GeoUtils.distancePairFromString(distanceString);
            if (distancePair == null) {
                throw new IllegalArgumentException("distance");
            }
            filters.add(near(GEOMETRY_FIELD, distancePair.getLeft(), distancePair.getRight(), null));
            filters.add(ne(GEOMETRY_FIELD, null));
        }

        if (concepts != null && !concepts.isEmpty()) {
            filters.add(in(PROPERTIES_HAS_TYPE_FIELD, concepts));
        }

        FindIterable<InstanceFeature> findIterable = session != null ? featuresCollection.find(session) : featuresCollection.find();

        findIterable = findIterable.projection(Projections.exclude(HIDDEN_FIELD, VISIBILITY_LAST_UPDATE_TIMESTAMP_FIELD, INSTANCE_FIELD));

        if (!filters.isEmpty()) findIterable.filter(and(filters));

        List<Feature> result = StreamSupport.stream(findIterable.spliterator(), false).map(feature -> {
            feature.setApplicationName(applicationsController.getAppName(feature.getApplication()));
            if (!geometries) feature.setGeometry(null);
            if (!descriptions) {
                Document newProperties = new Document();
                newProperties.put(EXTERNAL_URL_FIELD, feature.getExternalURL());
                newProperties.put(HAS_TYPE_FIELD, feature.getHasType());
                feature.setProperties(newProperties);
            }
            return feature;
        }).collect(Collectors.toList());


        return result;
    }

    public List<Feature> getFeatures(String instance, String application, String boundingBoxString, String distanceString, Set<String> concepts, boolean descriptions, boolean geometries)
            throws IllegalArgumentException, MongoTimeoutException {
        return getFeatures(null, instance, application, boundingBoxString, distanceString, concepts, descriptions, geometries);
    }

    /**
     * Updates the collection "features" with the list provided. If forceUpdate is set to false, a feature is updated only if the replacement provided
     * is newer than the one currently in the collection.
     *
     * @param instance    The domain name of the OTM instance
     * @param features    A List of JsonNodes, each representing a feature
     * @param forceUpdate If true, the features are updated regardless of their timestamp.
     * @param dbSession   The client session of the db transaction
     * @throws MongoTimeoutException   If a problem occurred while connecting to the database
     * @throws JsonProcessingException If a problem occurred while parsing JSON data
     */
    public void updateFeatures(ClientSession dbSession, String instance, Collection<InstanceFeature> features, boolean forceUpdate) throws MongoTimeoutException {
        for(InstanceFeature feature: features){
            updateFeature(dbSession,instance,feature,forceUpdate);
        }
    }

    public void updateFeature(ClientSession dbSession, String instance, InstanceFeature featureToSave, boolean forceUpdate) throws MongoTimeoutException {
        featureToSave.setInstance(instance);
        List<Bson> filters = new ArrayList<>();
        filters.add(Filters.eq(INSTANCE_FIELD, instance));
        filters.add(Filters.eq(APPLICATION_FIELD, featureToSave.getApplication()));
        filters.add(Filters.eq(PROPERTIES_EXTERNAL_URL_FIELD, featureToSave.getExternalURL()));
        InstanceFeature featureToUpdate = featuresCollection.find(dbSession,Filters.and(filters)).first();
        if (featureToUpdate != null && (!forceUpdate || featureToUpdate.getNumberProperty(TIMESTAMP_FIELD).longValue() <= featureToSave.getNumberProperty(TIMESTAMP_FIELD).longValue())){
            featureToUpdate.setGeometry(featureToSave.getGeometry());
            featureToUpdate.setProperties(featureToSave.getProperties());
            if(featureToSave.getReferences()!= null){
                Set<OTMEvent.Reference> references = featureToUpdate.getReferences() != null ? featureToUpdate.getReferences() : new HashSet<>();
                references.addAll(featureToSave.getReferences());
                featureToUpdate.setReferences(references);
            }
            featuresCollection.replaceOne(dbSession,Filters.and(filters),featureToUpdate);
        }
        else {
            featuresCollection.insertOne(dbSession,featureToSave);
        }
    }


    /**
     * Returns the list of the hidden objects managed by a specific application.
     *
     * @param application The application managing the objects
     * @param instance    The domain name of the OTM instance
     * @return The list of the hidden objects
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    public ObjectNode getHiddenList(ClientSession clientSession, String application, String instance) throws MongoTimeoutException {

        Document filter = new Document();
        filter.put(APPLICATION_FIELD, application);
        filter.put(INSTANCE_FIELD, instance);
        filter.put(HIDDEN_FIELD, true);
        ObjectMapper om = new ObjectMapper();
        ObjectNode result = om.createObjectNode();
        result.putArray(HIDDEN_LIST_FIELD);

        FindIterable<InstanceFeature> findIterable = clientSession != null ? featuresCollection.find(clientSession, filter) : featuresCollection.find(filter);
        for (Feature feature : findIterable) {
            result.withArray(HIDDEN_LIST_FIELD).add(feature.getExternalURL());
        }
        return result;
    }

    public ObjectNode getHiddenList(String application, String instance) throws MongoTimeoutException {
        return getHiddenList(null, application, instance);
    }

    /**
     * Check in the feature is hidden or visible
     *
     * @param instance    The domain name of the OTM instance
     * @param application he source application of the feature
     * @param externalUrl the external url of the feature
     * @return <i>true</i> if the feature is hidden, <i>false</i> if not
     */
    boolean isFeatureHidden(String instance, String application, String externalUrl) throws MongoTimeoutException {
        List<Bson> filters = new ArrayList<>();
        filters.add((eq(INSTANCE_FIELD, instance)));
        filters.add(eq(APPLICATION_FIELD, application));
        filters.add(eq(PROPERTIES_EXTERNAL_URL_FIELD, externalUrl));
        FindIterable<BsonDocument> found = featuresCollection.find(and(filters), BsonDocument.class);
        BsonDocument feature = found.first();
        if (feature == null)
            return false;
        return feature.getBoolean("hidden", BsonBoolean.FALSE).getValue();
    }

    public long hideFeature(ClientSession clientSession, String instance, String application, String externalUrl, long hideTimestamp, boolean hide) throws MongoTimeoutException {
        Bson update = (hide) ? Updates.set(HIDDEN_FIELD, true) : Updates.unset(HIDDEN_FIELD);

        UpdateResult res = featuresCollection.updateMany(clientSession,
                and(
                        eq(INSTANCE_FIELD, instance),
                        eq(APPLICATION_FIELD, application),
                        eq(PROPERTIES_EXTERNAL_URL_FIELD, externalUrl),
                        lte(VISIBILITY_LAST_UPDATE_TIMESTAMP_FIELD, hideTimestamp)
                ),
                Updates.combine(update, Updates.set(VISIBILITY_LAST_UPDATE_TIMESTAMP_FIELD, hideTimestamp)));
        return res.getModifiedCount();
    }

    public InstanceFeature getFeature(ClientSession clientSession, String instance, String application, String external_url) {
        return featuresCollection
                .find(clientSession,
                        and(
                                eq(INSTANCE_FIELD, instance),
                                eq(APPLICATION_FIELD, application),
                                eq(PROPERTIES_EXTERNAL_URL_FIELD, external_url)))
                .first();
    }
}
