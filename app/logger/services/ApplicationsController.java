package logger.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.kevinsawicki.http.HttpRequest;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoTimeoutException;
import com.mongodb.client.*;
import com.mongodb.client.model.*;
import com.typesafe.config.Config;
import logger.models.Application;
import org.bson.codecs.pojo.PojoCodecProvider;
import play.Logger;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

/**
 * This class contains the internal methods used to manage the names linked to each application.
 */
@Singleton
public class ApplicationsController {
    private Config configuration;
    private MongoCollection<Application> applicationCollection;

    // ReadWriteLock used to grant write access to only one thread, the ApplicationsController is created as Singleton
    private ReadWriteLock lock;
    private Map<String, String> applicationsMap;

    private  Logger.ALogger logger;

    @Inject
    public ApplicationsController(Config configuration, MongoDBController mongoDBController) {
        this.configuration = configuration;
        logger = new  Logger.ALogger(play.api.Logger.apply(getClass()));
        initApplicationCollection(mongoDBController);
        lock = new ReentrantReadWriteLock();
        applicationsMap = new HashMap<>();
        retrieveAppNames();
    }

    /**
     * Initialize 'applications' collection into the database
     *
     * @param mongoDBController
     */
    private void initApplicationCollection(MongoDBController mongoDBController){
        MongoDatabase mongoDatabase =mongoDBController.getMongoDatabase().withCodecRegistry(
                fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                        fromProviders(PojoCodecProvider.builder().automatic(true).build()))
        );
        this.applicationCollection = mongoDatabase.getCollection(configuration.getString("mongo.collections.applications"),Application.class);
        this.applicationCollection.createIndex(Indexes.ascending("cert_common_name"),new IndexOptions().unique(true));
    }

    /**
     * Retrieves the application names from the URLs in application.conf and saves them in the DB and in memory, in order to grant fast access.
     */
    public void retrieveAppNames() {
        lock.writeLock().lock();
        initMap();
        Object b = configuration.getAnyRef("liquidfeedback-endpoints");
        List<String> urlList = configuration.getStringList("liquidfeedback-endpoints.appnames.domains");
        List<Application> docs = new ArrayList<>();
        logger.info("Retrieving application names...");
        try {
            for (String url : urlList) {
                ObjectMapper om = new ObjectMapper();
                HttpRequest req = HttpRequest.get(url);
                logger.info("{} {}: {}", req.method(), req.url().toString(), req.code()); // todo: error handling (only HTTP 200 responses should be accepted
                JsonNode res = om.readTree(req.body());
                for (JsonNode app : res.withArray("result")) {
                    if (app.has("cert_common_name")) {
                        Application application = new Application();
                        application.setCertCommonName(app.get("cert_common_name").asText());
                        application.setName( app.get("name").asText());
                        applicationsMap.put(app.get("cert_common_name").asText(), app.get("name").asText());
                        docs.add(application);
                    }
                }
            }
            saveOnDB(docs);
        }
        catch (MongoTimeoutException e) {
            logger.error("MongoTimeoutException in retrieveAppNames (writing on DB):", e);
        }
        catch (HttpRequest.HttpRequestException e) {
            logger.error("One or more endpoints unreachable, retrieving app names from DB");
            try {
                getFromDB();
            }
            catch (MongoTimeoutException e1) {
                logger.error("MongoTimeoutException in retrieveAppNames (retrieving from DB):", e);
            }
        }
        catch (IOException e) {
            logger.error("IOException in retrieveAppNames:", e);
        }
        finally {
            lock.writeLock().unlock();
        }
        logger.debug(applicationsMap.toString());
    }

    /**
     * Retrieves the name of the specified application from memory. If no name is present, returns the domain name of the application.
     * @param cn The domain name of the application whose name is requested
     * @return The name of the specified application
     */
    public String getAppName(String cn) {
        lock.readLock().lock();
        String name = applicationsMap.getOrDefault(cn, cn);
        lock.readLock().unlock();
        return name;
    }

    /**
     * Re-initializes the data structure used to store the application names in memory.
     */
    private void initMap() {
        applicationsMap.clear();
        applicationsMap.put(configuration.getString("ontomap.application.domain"), "OnToMap");
    }

    /**
     * Saves a list of MongoDB documents containing CN-name entries on DB. If an entry with a specific CN already exists,
     * it is overwritten.
     * @param apps A list of Application
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    private void saveOnDB(List<Application> apps) throws MongoTimeoutException {
        List<WriteModel<Application>> writeList = new ArrayList<>();
        for (Application app : apps) {
            writeList.add(new ReplaceOneModel(
                    Filters.eq("cert_common_name", app.getCertCommonName()),
                    app,
                    new ReplaceOptions().upsert(true)));
        }
        applicationCollection.bulkWrite(writeList);
    }

    /**
     * Retrieves the list of CN-name associations from DB and stores them in memory.
     * @throws MongoTimeoutException If a problem occurred while connecting to the database
     */
    private void getFromDB() throws MongoTimeoutException {
        initMap();
        for(Application app : applicationCollection.find()){
            applicationsMap.put(app.getCertCommonName(),app.getName());
        }
    }

}
