package logger.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.github.kevinsawicki.http.HttpRequest;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;
import logger.controllers.Attrs;
import logger.models.AuthenticationType;
import play.Environment;
import play.Logger;
import play.mvc.Http;

import javax.inject.Inject;
import javax.naming.InvalidNameException;
import javax.naming.ldap.Rdn;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * This class contains the internal methods used to verify the authentication status of the clients performing the requests.
 * In order to verify the client authentication, two request headers, which are set by the reverse proxy, are used.
 * The headers checked by this class are setted by nginx checking if the application has a certificate
 */
public class AuthenticationController {

    private Environment environment;
    private Config config;
    Logger.ALogger logger;
    private AccessTokenMongoController accessTokenMongoController;
    List<String> adminCommonNames;

    @Inject
    public AuthenticationController(Config config, Environment environment, AccessTokenMongoController accessTokenMongoController) {
        this.config = config;
        this.environment = environment;
        logger = new Logger.ALogger(play.api.Logger.apply(this.getClass()));
        this.accessTokenMongoController = accessTokenMongoController;
        this.adminCommonNames = config.getStringList("ontomap.admin.cn");
    }

    /**
     * Checks the authentication status of an HTTP request.
     *
     * @param request The HTTP request to verify
     * @return The CN of the client if successfully authenticated; null otherwise.
     */
    public Http.Request getAuthentication(Http.Request request) {
        String application = checkApplication(request);
        String instance = getOTMInstance(request);
        long actor = -1;
        List<AuthenticationType> authenticationTypes = new ArrayList<>();

        if (application != null) {
            request = request.addAttr(Attrs.APPLICATION, application);
            authenticationTypes.add(AuthenticationType.APPLICATION_CERTIFICATE);
            if (request.queryString("actor").isPresent()) {
                actor = Long.parseLong(request.queryString("actor").get());
            }
            if (environment.isDev() || adminCommonNames.contains(application)) {
                authenticationTypes.add(AuthenticationType.ADMIN);
            }
        }

        Optional<String> userToken = request.getHeaders().get("Authorization");
        if (userToken.isPresent() && actor==-1) {
            actor = accessTokenMongoController.verifyUserToken(instance, userToken.get());
            if (actor != -1) {
                authenticationTypes.add(AuthenticationType.USER_TOKEN);
            }
        }

        Optional<String> appToken = request.queryString("token");
        if (appToken.isPresent()) {
            application = accessTokenMongoController.verifyToken(appToken.get());
            if (application != null) {
                authenticationTypes.add(AuthenticationType.APPLICATION_TOKEN);
                request = request.addAttr(Attrs.APPLICATION, application);
            }
        }

        return request.addAttr(Attrs.AUTHENTICATIONS, authenticationTypes)
                .addAttr(Attrs.USER_AUTHENTICATED, actor)
                .addAttr(Attrs.INSTANCE, instance);

    }

    /**
     * Checks the authentication status of an HTTP request.
     *
     * @param request The HTTP request to verify
     * @return The CN of the client if successfully authenticated; null otherwise.
     */
    private String checkApplication(Http.Request request) {
        // if in dev environment, uses the X-Ontomap-Application to set the application. Defaults to ontomap.eu
        // this appened becouse in local dev you don't have nginx to set headers
//        if (environment.isDev())  {
//            return request.getHeaders().get("X-Ontomap-Application").orElse("ontomap.eu");
//        }
        if (environment.isDev())  {
            return request.getHeaders().get("X-Ontomap-Application").orElse(null);
        }
        // Auth status; "SUCCESS" if a valid client certificate is used.
        // this flag is the response of $ssl_client_verify: http://nginx.org/en/docs/http/ngx_http_ssl_module.html#var_ssl_client_verify
        Optional<String> authStatus = request.getHeaders().get("X-Ontomap-AuthStatus");
        // Distinguished Name from the client certificate
        // this flag is the response of $ssl_client_s_dn: http://nginx.org/en/docs/http/ngx_http_ssl_module.html#var_ssl_client_s_dn
        Optional<String> authDN = request.getHeaders().get("X-Ontomap-DN");
        if (!authStatus.isPresent() || !authStatus.get().equalsIgnoreCase("SUCCESS") || !authDN.isPresent())
            return null;

        // The CN field is extracted
        // this becouse X-Ontomap-DN is like ""/DC=Demo/OU=Demo/OU=Demo/CN=ontomap.eu"" but we need only the CN part
        List<String> stringList = Splitter.on(",").splitToList(authDN.get());
        try {
            for (String s : stringList) {
                Rdn rdn = new Rdn(s);
                if (rdn.size() > 0 && rdn.getType().equalsIgnoreCase("cn")) {
                    return (String) rdn.getValue();
                }
            }
            return null;
        } catch (InvalidNameException e) {
            logger.error("InvalidNameException in checkAuthentication: ", e);
            logger.error("RDN: ", authDN.get());
            logger.error("RDN LIST: ", stringList);
            return null;
        }
    }

    /**
     * Returns the domain name of the OTM instance managing the request.
     *
     * @param request The HTTP request to verify
     * @return The domain name of the OTM instance managing the request.
     */
    public String getOTMInstance(Http.RequestHeader request) {
        if (environment.isDev()) {
            Optional<String> instance = request.getHeaders().get("X-Ontomap-Instance");
            return instance.orElse("dev.co3.ontomap.eu");
        }
        return request.getHeaders().get("X-Instance").get();
    }

    public boolean isAcaManager(String instance, long idMember, String access_token) {
        if (idMember < 0 || Strings.isNullOrEmpty(access_token))
            return false;
        String url = "";
        try {
            url = config.getConfig("liquidfeedback-endpoints.user-role-checker").getString(instance);
        } catch (ConfigException.Missing exception) {
            return false;
        }
        ObjectMapper om = new ObjectMapper();
        HttpRequest req = HttpRequest.post(url, true,  "id", idMember, "include_roles", 1).authorization(access_token);
        try {
            JsonNode res = om.readTree(req.body());
            if (res.has("error"))
                return false;
            ArrayNode result = (ArrayNode) res.get("result");
            if (result.size() == 0) {
                return false;
            } else if (result.get(0).has("roles") && result.get(0).get("roles").has("gamification_manager")) {
                return result.get(0).get("roles").get("gamification_manager").asBoolean();

            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return false;
    }
}
