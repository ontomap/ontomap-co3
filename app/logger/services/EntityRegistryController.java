package logger.services;

import com.mongodb.MongoException;
import com.mongodb.MongoTimeoutException;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.*;
import com.mongodb.client.model.*;

import com.typesafe.config.Config;
import org.apache.commons.lang3.tuple.Pair;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonValue;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import logger.supports.CacheKey;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


/**
 * This class contains the internal methods used to manage the entity registry, used to define whether different objects
 * refer to the same entity.
 */
@Singleton
public class EntityRegistryController {

    private static final String LIST_FIELD = "list";
    private MongoDBController mongoDBController;
    private Config configuration;
    MongoCollection<Document> entityCollection;

    // for each concept instance, all the associated app/url pairs are cached
    private HashMap<CacheKey, Set<Pair<String, String>>> entityMap;
    private ReadWriteLock lock;

    final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Inject
    public EntityRegistryController(MongoDBController mongoDBController, Config configuration) {
        this.mongoDBController = mongoDBController;
        this.configuration = configuration;
        initEntityCollection(mongoDBController);
        entityMap = new HashMap<>();
        lock = new ReentrantReadWriteLock();
        cacheEntities();
    }

    /**
     * Initialize EntityRegistry collection into the database
     *
     * @param mongoDBController
     */
    private void initEntityCollection(MongoDBController mongoDBController){
        MongoDatabase database = mongoDBController.getMongoDatabase();
        this.entityCollection = database.getCollection(configuration.getString("mongo.collections.entity_registry"));
        this.entityCollection.createIndex(Indexes.compoundIndex(
                Indexes.ascending("list.application"),
                Indexes.ascending("list.external_url"),
                Indexes.ascending("list.instance")
        ), new IndexOptions().unique(true));
    }

    /**
     * Builds an in-memory cache containing all the existent associations between the concept instances, which are stored on MongoDB.
     */
    public void cacheEntities() {
        logger.info("(Re) building entity registry cache...");
        lock.writeLock().lock();
        entityMap.clear();
        MongoDatabase database = mongoDBController.getMongoDatabase();
        MongoCollection<BsonDocument> collection = database.getCollection(configuration.getString("mongo.collections.entity_registry"), BsonDocument.class);
        MongoIterable<BsonDocument> findIterable = collection.find();
        try(MongoCursor<BsonDocument> cursor = findIterable.iterator()) {
            while (cursor.hasNext()) {
                Set<Pair<String, String>> entitySet = new LinkedHashSet<>();
                BsonDocument registryEntry = cursor.next();
                BsonArray entityArray = registryEntry.getArray(LIST_FIELD);
                String instance = registryEntry.getString("instance").getValue();
                for (BsonValue individual : entityArray) {
                    String application = individual.asDocument().getString("application").getValue();
                    String externalUrl = individual.asDocument().getString("external_url").getValue();
                    entitySet.add(Pair.of(application, externalUrl));
                    entityMap.put(new CacheKey(instance, application, externalUrl), entitySet);
                }
            }
//            Logger.debug(entityMap.toString());
        }
        catch (MongoTimeoutException e) {
            logger.error("MongoTimeoutException in cacheEntities:", e);
            return;
        }
        finally {
            lock.writeLock().unlock();
        }
        logger.info("Entity registry cache (re)built");
    }

    /**
     * Given a application/external URL pair representing a concept instance, retrieves from cache a set of application/external URL pairs, representing each a concept instance associated with
     * the one specified.
     * @param instance The domain name of the OTM instance
     * @param application The source application of the specified instance
     * @param externalUrl The external URL of the specified instance
     * @return A set of application/external URL pairs, each representing an instance associated with the one specified.
     * If no associations exist for the specified instance, null is returned.
     */
    public Set<Pair<String, String>> getAssociations(String instance, String application, String externalUrl) {
        lock.readLock().lock();
        Set<Pair<String, String>> entitySet = entityMap.get(new CacheKey(instance, application, externalUrl));
        lock.readLock().unlock();
        return entitySet;
    }

    /**
     * Saves to the DB a new association between two concept instances and updates the cache. The specified instance (individualApplication/individualExternalUrl)
     * must not be already associated with another instance.
     * @param instance The domain name of the OTM instance
     * @param individualApplication The source application of the instance to associate with another one
     * @param individualExternalUrl The external URL of the instance to associate with another one
     * @param otherApplication The source application of the instance to associate the specified one with
     * @param otherExternalUrl The external URL of the instance to associate the specified one with
     * @return true if the association is saved, false otherwise
     */
    public boolean setAssociation(String instance, String individualApplication, String individualExternalUrl, String otherApplication, String otherExternalUrl) throws MongoException {
        try(ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            lock.readLock().lock();
            try {
                CacheKey key = new CacheKey(instance, individualApplication, individualExternalUrl);
                if (entityMap.containsKey(key)) return false;
                Document individual = new Document();
                individual.append("application", individualApplication);
                individual.append("external_url", individualExternalUrl);
                CacheKey otherKey = new CacheKey(instance, otherApplication, otherExternalUrl);
                if (entityMap.containsKey(otherKey)) {
                    Document filter = new Document();
                    filter.append("instance", instance);
                    filter.append("list.application", otherApplication);
                    filter.append("list.external_url", otherExternalUrl);
                    entityCollection.updateOne(dbSession,filter, Updates.addToSet("list", individual));
                } else {
                    Document equivalentTo = new Document();
                    equivalentTo.append("application", otherApplication);
                    equivalentTo.append("external_url", otherExternalUrl);
                    Document newEntry = new Document();
                    newEntry.append("list", Arrays.asList(equivalentTo, individual));
                    newEntry.append("instance", instance);
                    entityCollection.insertOne(dbSession,newEntry);
                }
                dbSession.commitTransaction();
            }catch(MongoException e){
                dbSession.abortTransaction();
                throw e;
            }finally {
                lock.readLock().unlock();
                dbSession.close();
            }
        }
        cacheEntities();
        return true;
    }

    /**
     * Deletes all existing associations concerning a specified concept instance.
     * @param instance The domain name of the OTM instance
     * @param individualApplication The source application of the instance to disassociate from others
     * @param individualExternalUrl The external URL of the instance to disassociate from others
     * @return true if an association has been removed, false otherwise
     */
    public boolean deleteAssociation(String instance, String individualApplication, String individualExternalUrl) {
        lock.readLock().lock();
        try (ClientSession dbSession = mongoDBController.getSession()) {
            try {
                dbSession.startTransaction();

                List<WriteModel<Document>> writeList = new ArrayList<>();
                Document filter = new Document();
                filter.append("instance", instance);

                Document condition = new Document();
                condition.append("application", individualApplication);
                condition.append("external_url", individualExternalUrl);
                writeList.add(new UpdateManyModel<>(filter, Updates.pull("list", condition)));
                writeList.add(new DeleteManyModel<>(Filters.exists("list.1", false)));
                BulkWriteResult res = entityCollection.bulkWrite(dbSession,writeList);
//            Logger.debug(res.toString());

                dbSession.commitTransaction();

                if (res.getModifiedCount() < 1) return false;

            } catch (Exception e) {
                dbSession.abortTransaction();
                return false;
            } finally {
                dbSession.close();
            }
        }finally {
            lock.readLock().unlock();
        }
        cacheEntities();
        return true;
    }
}
