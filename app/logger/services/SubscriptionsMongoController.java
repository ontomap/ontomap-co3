package logger.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.MongoClientSettings;
import com.mongodb.MongoException;
import com.mongodb.client.*;
import com.mongodb.client.model.IndexOptions;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.typesafe.config.Config;
import logger.models.OTMEvent;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.types.ObjectId;
import play.Logger.ALogger;
import play.libs.Json;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;
import logger.supports.BsonSerializer;

import javax.inject.Inject;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

public class SubscriptionsMongoController {

    private Config configuration;
    MongoDBController mongoDBController;
    private MongoCollection<BsonDocument> subscriptionCollection;
    @Inject
    private WSClient ws;
    private ALogger logger;

    @Inject
    public SubscriptionsMongoController(Config config, MongoDBController mongoDBController) {
        this.configuration = config;
        this.mongoDBController = mongoDBController;
        this.logger = new ALogger(play.api.Logger.apply(getClass()));
        initSubscriptionCollection(mongoDBController);
    }
    /**
     * Initialize 'mappings' collection into the database
     *
     * @param mongoDBController
     */
    private void initSubscriptionCollection(MongoDBController mongoDBController) {
        MongoDatabase database = mongoDBController.getMongoDatabase();
        this.subscriptionCollection = database.getCollection(configuration.getString("mongo.collections.subscriptions"), BsonDocument.class);
        subscriptionCollection.createIndex(Indexes.compoundIndex(
                Indexes.ascending("activity_type"),
                Indexes.ascending("application"),
                Indexes.ascending("source")
        ), new IndexOptions().unique(true));
    }

    /**
     * Verifies if one or more subscriptions for a specific event. A subscription is composed by an event source application
     * and activity type and an URL; when an event matching the subscription is submitted a POST request is performed to the
     * specified URL.
     *
     * @param instance     The OTM instance
     * @param application  The events source application
     * @param activityType The events activity type
     * @return A list of all URLs to wich a POST has to be sent. If no subscription for the requested event exists, an empty list is returned.
     */
    public List<String> verifySubscription(String instance, String application, String activityType) {
        List<String> result = new ArrayList<>();

        FindIterable<BsonDocument> findIterable = subscriptionCollection.find(new Document().append("instance", instance).append("application", application).append("activity_type", activityType));
        findIterable.projection(Projections.include("url"));
        try (MongoCursor<BsonDocument> cursor = findIterable.iterator()) {
            while (cursor.hasNext()) {
                BsonDocument subscription = cursor.next();
                result.add(subscription.getString("url").getValue());
            }
        }
        return result;
    }

    /**
     * For each event check if there is a subscription. If there is, the system will send a notification to the subscriber
     * @param events Event List to check
     */
    public void checkEvents(Collection<OTMEvent> events) {
        for (OTMEvent event : events) {
            List<String> urls = verifySubscription(event.getInstance(), event.getApplication(), event.getActivityType());
            for (String url : urls) {
                sendNotification(event, url);
            }
        }
    }

    /**
     * Send an event to and url as a POST Request
     * @param otmEvent
     * @param url
     */
    public void sendNotification(OTMEvent otmEvent, String url) {
        JsonNode event = Json.newObject()
                .put("id", otmEvent.getId().toHexString())
                .put("application", otmEvent.getApplication())
                .put("activity_type", otmEvent.getActivityType())
                .put("timestamp", otmEvent.getTimestamp()); // url
        CompletionStage<WSResponse> csResponse = ws.url(url).setRequestTimeout(Duration.ofSeconds(5)).post(event);
        try {
            int statusCode = csResponse.thenApply(WSResponse::getStatus).toCompletableFuture().get();
            if (statusCode == 200) {
                logger.debug("Request OK");
            } else {
                logger.debug("Request: NOT_OK -> Status Code: " + statusCode);
                logger.debug(event.toString());
            }
        } catch (InterruptedException e) {
            logger.error("InterruptedException", e);
            logger.debug(event.toString());
        } catch (ExecutionException e) {
            logger.error("ExecutionException", e);
            logger.debug(event.toString());
        }
    }

    /**
     * Returns the subscriptions associated with a specific application in a specific OTM instance.
     *
     * @param instance          The OTM instance
     * @param sourceApplication The source application
     * @return An ObjectNode containing an array of objects, each representing a subscription
     */
    public ObjectNode getSubscriptions(String instance, String sourceApplication) {
        ObjectMapper om = new ObjectMapper();
        SimpleModule sm = new SimpleModule();
        sm.addSerializer(BsonDocument.class, new BsonSerializer());
        om.registerModule(sm);
        FindIterable<BsonDocument> findIterable = subscriptionCollection.find(new Document().append("instance", instance).append("source_application", sourceApplication));
        findIterable.projection(Projections.exclude("instance", "source_application"));
        ObjectNode result = om.createObjectNode();
        result.withArray("subscriptions");
        try (MongoCursor<BsonDocument> cursor = findIterable.iterator()) {
            while (cursor.hasNext()) {
                ObjectNode sub = om.valueToTree(cursor.next());
                sub.set("id", sub.get("_id"));
                sub.remove("_id");
                result.withArray("subscriptions").add(sub);
            }
        }
        return result;
    }

    /**
     * Adds/updates a subscription from a specific application and relative to a specific OTM instance. At most one subscription
     * from an application to events coming from a specific app and with a specific type can exist. If a subscription is submitted
     * and one with the same application/activity type coming from the same app already exists, it is overwritten.
     *
     * @param instance          The OTM instance
     * @param application       The events application
     * @param activityType      The events activity type
     * @param sourceApplication The application which performs the subscription
     * @param url               The URL to which the notifications are to be sent
     * @return True if the subscription is successfully saved, false otherwise
     */
    public ObjectNode addSubscription(String instance, String application, String activityType, String sourceApplication, String url) {
        ObjectMapper om = new ObjectMapper();
        SimpleModule sm = new SimpleModule();
        sm.addSerializer(BsonDocument.class, new BsonSerializer());
        om.registerModule(sm);
        Document subscription = new Document()
                .append("instance", instance)
                .append("application", application)
                .append("activity_type", activityType)
                .append("source_application", sourceApplication)
                .append("url", url);
        try (ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            try {
                UpdateResult res = subscriptionCollection.withDocumentClass(Document.class).replaceOne(dbSession,
                        new Document()
                                .append("instance", instance)
                                .append("application", application)
                                .append("source_application", sourceApplication)
                                .append("activity_type", activityType),
                        subscription,
                        new ReplaceOptions().upsert(true)
                );
                dbSession.commitTransaction();
                if (res.getModifiedCount() < 1 && res.getUpsertedId() == null) return null;
                Document resDoc = subscriptionCollection.withDocumentClass(Document.class).find(subscription).first();
                BsonDocument resBson = resDoc.toBsonDocument(BsonDocument.class, MongoClientSettings.getDefaultCodecRegistry());
                ObjectNode resJson = om.valueToTree(resBson);
                resJson.set("id", resJson.get("_id"));
                resJson.remove("_id");
                resJson.remove("instance");
                resJson.remove("source_application");
                return resJson;
            } catch (MongoException e) {
                dbSession.abortTransaction();
                return null;
            } finally {
                dbSession.close();
            }
        }
    }

    /**
     * Deletes a subscription.
     *
     * @param instance    The OTM instance
     * @param application The events application
     * @param id          The event id
     * @return True if a subscription was deleted, false otherwise.
     */
    public boolean deleteSubscription(String instance, String application, String id) {
        ObjectId objectId;

        try {
            objectId = new ObjectId(id);
        } catch (IllegalArgumentException e) {
            logger.error("id problem");
            return false;
        }
        Document filter = new Document()
                .append("instance", instance)
                .append("source_application", application)
                .append("_id", objectId);

        try (ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            try {
                DeleteResult res = subscriptionCollection.deleteOne(dbSession, filter);
                dbSession.commitTransaction();
                return res.getDeletedCount() > 0;
            } catch (MongoException e) {
                dbSession.abortTransaction();
                return false;
            } finally {
                dbSession.close();
            }
        }
    }
}
