package logger.supports;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.primitives.Doubles;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import org.apache.commons.lang3.tuple.Pair;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonDouble;
import org.bson.BsonString;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

public class GeoUtils {

    public static List<Double> boundingBoxToCoords(String boundingBox) {
        if (Strings.isNullOrEmpty(boundingBox)) return null;
        List<String> coords = Splitter.on(",").splitToList(boundingBox);
        ArrayList<Double> coordsList = new ArrayList<>();
        Double temp;
        for (String coord : coords) {
            temp = Doubles.tryParse(coord);
            if (temp == null) return null;
            coordsList.add(temp);
        }
        if (coordsList.size() != 4) return null;
        return coordsList;
    }

    /**
     * Reads a distance string and returns a point/distance pair representing a circle.
     *
     * @param distanceString A string representing the center coordinates and radius (in meters) of a circle.
     *                       Example: "7.4,45.1,200"
     * @return A Pair composed by a Bson object (the circle center) and a Double (the circle radius)
     */
    public static Pair<Bson, Double> distancePairFromString(String distanceString) {
        if (Strings.isNullOrEmpty(distanceString)) return null;
        List<String> coords = Splitter.on(",").splitToList(distanceString);
        if (coords.size() != 3) return null;
        Double lng = Doubles.tryParse(coords.get(0));
        Double lat = Doubles.tryParse(coords.get(1));
        Double distance = Doubles.tryParse(coords.get(2));
        if (lng == null || lat == null || distance == null) return null;
        BsonDocument geometry = new BsonDocument();
        geometry.put("type", new BsonString("Point"));
        BsonArray coordsArray = new BsonArray();
        coordsArray.add(new BsonDouble(lng));
        coordsArray.add(new BsonDouble(lat));
        geometry.put("coordinates", coordsArray);
        return Pair.of(geometry, distance);
    }

    /**
     * Transforms a list of coordinates into a BasicDBObject representing a polygon.
     *
     * @param coordinates A list of coordinates;
     * @return A BasicDBObject representing a polygon
     */
    public static BasicDBObject coordinatesToPolygon(List<Double> coordinates) {
        BasicDBObject geometry = new BasicDBObject();
        geometry.put("type", "Polygon");

        BasicDBList coordsList = new BasicDBList();
        coordsList.add(new BasicDBList());
        coordsList.add(new BasicDBList());
        coordsList.add(new BasicDBList());
        coordsList.add(new BasicDBList());
        coordsList.add(new BasicDBList());

        ((BasicDBList) coordsList.get(0)).add(coordinates.get(2));
        ((BasicDBList) coordsList.get(0)).add(coordinates.get(1));

        ((BasicDBList) coordsList.get(1)).add(coordinates.get(0));
        ((BasicDBList) coordsList.get(1)).add(coordinates.get(1));

        ((BasicDBList) coordsList.get(2)).add(coordinates.get(0));
        ((BasicDBList) coordsList.get(2)).add(coordinates.get(3));

        ((BasicDBList) coordsList.get(3)).add(coordinates.get(2));
        ((BasicDBList) coordsList.get(3)).add(coordinates.get(3));

        ((BasicDBList) coordsList.get(4)).add(coordinates.get(2));
        ((BasicDBList) coordsList.get(4)).add(coordinates.get(1));

        geometry.put("coordinates", new BasicDBList());
        ((BasicDBList) geometry.get("coordinates")).add(coordsList);
        return geometry;
    }
}
