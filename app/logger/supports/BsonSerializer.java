package logger.supports;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.bson.BsonArray;
import org.bson.BsonDocument;
import org.bson.BsonType;
import org.bson.BsonValue;

import java.io.IOException;
import java.util.Map;

public class BsonSerializer extends JsonSerializer<BsonDocument> {

    @Override
    public void serialize(BsonDocument value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        serializeObject(value, gen);
    }

    private void serializeObject(BsonDocument value, JsonGenerator gen) throws IOException {
        gen.writeStartObject();
        for (Map.Entry<String, BsonValue> e : value.entrySet()) {
            gen.writeFieldName(e.getKey());
            serializeValue(e.getValue(), gen);
        }
        gen.writeEndObject();
    }

    private void serializeArray(BsonArray array, JsonGenerator gen) throws IOException {
        gen.writeStartArray();
        for(BsonValue value : array.getValues()) {
            serializeValue(value, gen);
        }
        gen.writeEndArray();
    }

    private void serializeValue(BsonValue value, JsonGenerator gen) throws IOException {
        BsonType type = value.getBsonType();
//        Logger.debug(type.toString());
        switch (type) {
            case DOCUMENT:
                serializeObject(value.asDocument(), gen);
                break;
            case ARRAY:
                serializeArray(value.asArray(), gen);
                break;
            case STRING:
                gen.writeString(value.asString().getValue());
                break;
            case INT32:
            case INT64:
                gen.writeNumber(value.asNumber().longValue());
                break;
            case DOUBLE:
                gen.writeNumber(value.asNumber().doubleValue());
                break;
            case BOOLEAN:
                gen.writeBoolean(value.asBoolean().getValue());
                break;
            case OBJECT_ID:
                gen.writeString(value.asObjectId().getValue().toString());
                break;
            case NULL:
            default:
                gen.writeNull();
                break;
        }
    }
}
