package logger.supports.actions;

import logger.controllers.Attrs;
import logger.controllers.ResponseController;
import logger.models.AuthenticationType;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import logger.services.AuthenticationController;

import javax.inject.Inject;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class ApplicationTokenAuthenticationAction extends play.mvc.Action.Simple {
    private AuthenticationController authenticationController;
    private ResponseController responseController;

    @Inject
    public ApplicationTokenAuthenticationAction(AuthenticationController authenticationController, ResponseController responseController) {
        super();
        this.authenticationController = authenticationController;
        this.responseController = responseController;
    }

    @Override
    public CompletionStage<Result> call(Http.Request request) {
        request = authenticationController.getAuthentication(request);
        List<AuthenticationType> authenticationTypes = request.attrs().get(Attrs.AUTHENTICATIONS);
        if (authenticationTypes.size() == 0) {
            return CompletableFuture.completedFuture(responseController.missingCertificate());
        }
        return delegate.call(request);
    }

    @With(ApplicationTokenAuthenticationAction.class)
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface  ApplicationTokenAuthentication {}
}
