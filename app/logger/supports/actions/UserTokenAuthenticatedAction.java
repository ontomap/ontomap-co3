package logger.supports.actions;

import logger.controllers.Attrs;
import logger.controllers.ResponseController;
import logger.models.AuthenticationType;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import logger.services.AccessTokenMongoController;
import logger.services.ApplicationsController;
import logger.services.AuthenticationController;

import javax.inject.Inject;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;


/**
 * This class implements a Play Action used to determine which API methods support read-only token authentication, useful for
 * situations in which using a client certificate is not feasible.
 */
public class UserTokenAuthenticatedAction extends Action<UserTokenAuthenticatedAction.UserTokenAuthentication> {

    private AuthenticationController authenticationController;
    private ResponseController responseController;

    @Inject
    public UserTokenAuthenticatedAction(AuthenticationController authenticationController, ResponseController responseController) {
        super();
        this.authenticationController = authenticationController;
        this.responseController = responseController;
    }

    @Override
    public CompletionStage<Result> call(Http.Request request) {
        request = authenticationController.getAuthentication(request);
        List<AuthenticationType> authenticationTypes = request.attrs().get(Attrs.AUTHENTICATIONS);
        if (authenticationTypes.contains(AuthenticationType.APPLICATION_CERTIFICATE)) {
            return delegate.call(request);
        }
        if (authenticationTypes.contains(AuthenticationType.USER_TOKEN)) {
            return delegate.call(request);
        }
        return CompletableFuture.completedFuture(responseController.noCertOrToken());
    }


    @With(UserTokenAuthenticatedAction.class)
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface UserTokenAuthentication {
        OperationType operationType() default OperationType.READ;
    }

    public enum OperationType {
        READ
        //WRITE
    }
}
