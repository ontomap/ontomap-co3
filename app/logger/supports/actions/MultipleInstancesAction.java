package logger.supports.actions;

import logger.controllers.ResponseController;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import logger.controllers.Attrs;
import logger.services.AuthenticationController;

import javax.inject.Inject;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * This class implements a Play Action used to determine which API methods support multiple OTM instances.
 */
public class MultipleInstancesAction extends play.mvc.Action.Simple {

    private AuthenticationController authenticationController;
    private ResponseController responseController;

    @Inject
    public MultipleInstancesAction(AuthenticationController authenticationController, ResponseController responseController) {
        super();
        this.authenticationController = authenticationController;
        this.responseController = responseController;
    }

    @Override
    public CompletionStage<Result> call(Http.Request request) {
        String instance = authenticationController.getOTMInstance(request);
        if (instance == null) {
            return CompletableFuture.completedFuture(responseController.internalServerError2(ResponseController.NO_INSTANCE));
        }
        request = request.addAttr(Attrs.INSTANCE, instance);
        return delegate.call(request);
    }

    @With(MultipleInstancesAction.class)
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface MultipleInstances {}
}
