package logger.supports.actions;

import com.typesafe.config.Config;
import logger.controllers.Attrs;
import logger.controllers.ResponseController;
import logger.models.AuthenticationType;
import play.Environment;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.With;
import logger.services.AuthenticationController;

import javax.inject.Inject;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;


/**
 * This class implements a Play Action used to determine which API methods require admin privileges; application.conf contains
 * the list of admin-enabled certificates.
 */
public class AdminAction extends play.mvc.Action.Simple {

    private AuthenticationController authenticationController;
    private ResponseController responseController;
    private Config configuration;
    private Environment environment;

    @Inject
    public AdminAction(AuthenticationController authenticationController, ResponseController responseController,
                       Config configuration, Environment environment) {
        super();
        this.authenticationController = authenticationController;
        this.responseController = responseController;
        this.configuration = configuration;
        this.environment = environment;
    }

    @Override
    public CompletionStage<Result> call(Http.Request request) {
        request = authenticationController.getAuthentication(request);
        List<AuthenticationType> authenticationTypes = request.attrs().get(Attrs.AUTHENTICATIONS);
        if(authenticationTypes.contains(AuthenticationType.ADMIN))
            return delegate.call(request);
        else
            return CompletableFuture.completedFuture(responseController.missingCertificate());
    }

    @With(AdminAction.class)
    @Target({ElementType.TYPE, ElementType.METHOD})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface AdminAuthentication {
    }
}
