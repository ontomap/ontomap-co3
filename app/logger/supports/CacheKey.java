package logger.supports;

import com.google.common.base.Joiner;

import java.util.Arrays;

public class CacheKey {
    private final String[] keys;

    public CacheKey(String... keys) {
        this.keys = keys;
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(keys);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof CacheKey)) return false;
        CacheKey otherKey = (CacheKey) obj;
        return Arrays.equals(this.keys, otherKey.keys);
    }

    @Override
    public String toString() {
        String res = "{ ";
        res += Joiner.on(", ").skipNulls().join(this.keys);
        res += " }";
        return res;
    }

    public String[] getKeys() {
        return keys;
    }
}
