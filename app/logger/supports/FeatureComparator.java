package logger.supports;

import logger.models.Feature;

import java.util.Comparator;

public class FeatureComparator implements Comparator<Feature> {
    @Override
    public int compare(Feature f1, Feature f2) {
        return f1.getId().compareTo(f2.getId());
    }
}
