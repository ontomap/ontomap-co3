package logger.supports.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.client.model.geojson.Geometry;
import com.mongodb.client.model.geojson.codecs.GeoJsonCodecProvider;
import org.bson.BsonType;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.configuration.CodecConfigurationException;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.json.JsonReader;

import java.io.IOException;
import java.io.Reader;

public class GeometryDeserializer extends JsonDeserializer<Geometry> {

    public GeometryDeserializer() {

    }

    @Override
    public Geometry deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        JsonNode geometry = p.getCodec().readTree(p);
        if (geometry.isNull())
            return null;
        JsonReader reader = new CustomJsonReader(geometry.toString());
        try {
            return CodecRegistries.fromProviders(new GeoJsonCodecProvider()).get(Geometry.class).decode(reader, DecoderContext.builder().build());
        }catch(CodecConfigurationException e){
            throw e;
        }
    }



    private class CustomJsonReader extends JsonReader {

        public CustomJsonReader(String json) {
            super(json);
        }

        public CustomJsonReader(Reader reader) {
            super(reader);
        }

        public BsonType getCurrentBsonType() {
            BsonType bsonType = super.getCurrentBsonType();
            if (bsonType != null && bsonType.equals(BsonType.INT32)) {
                return BsonType.DOUBLE;
            }
            return bsonType;
        }

        @Override
        public double readDouble() {
            try {
                checkPreconditions("readDouble", BsonType.DOUBLE);
                setState(getNextState());
                return doReadDouble();
            } catch (Exception e ) {
                checkPreconditions("readInt", BsonType.INT32);
                setState(getNextState());
                return doReadInt32() * 1.0;
            }
        }
    }
}

