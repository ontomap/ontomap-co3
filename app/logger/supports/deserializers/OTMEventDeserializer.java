package logger.supports.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Strings;
import logger.models.ActivityType;
import logger.models.Feature;
import logger.models.OTMEvent;
import org.bson.Document;
import logger.supports.exceptions.MalformedJsonException;
import logger.supports.exceptions.MongoInvalidProperty;
import logger.supports.exceptions.NotSupportedActivityType;

import java.io.IOException;
import java.util.*;

import static logger.services.LoggerMongoController.*;

public class OTMEventDeserializer extends JsonDeserializer<OTMEvent> {

    String instance;
    String application;


    public OTMEventDeserializer(String instance, String application) {
        this.instance = instance;
        this.application = application;
    }

    @Override
    public OTMEvent deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        JsonNode eventJson = p.getCodec().readTree(p);
        OTMEvent otmEvent = new OTMEvent();

        otmEvent.setInstance(instance);
        otmEvent.setApplication(application);

        Iterator<String> fieldNames = eventJson.fieldNames();
        while (fieldNames.hasNext()) {
            String key = fieldNames.next();
            JsonNode jsonValue = eventJson.get(key);
            switch (key) {
                case ACA_FIELD:
                    String aca = jsonValue.asText();
                    otmEvent.setAca(aca);
                    break;
                case GROUP_FIELD:
                    String group = jsonValue.asText();
                    otmEvent.setGroup(group);
                    break;
                case ACTOR_FIELD:
                    Long actor = jsonValue.asLong();
                    otmEvent.setActor(actor);
                    break;
                case TIMESTAMP_FIELD:
                    Long timestamp = jsonValue.asLong();
                    otmEvent.setTimestamp(timestamp);
                    break;
                case ACTIVITY_TYPE_FIELD:
                    String activityType = jsonValue.asText();
                    try {
                        activityType = ActivityType.valueOf(activityType).name();
                    } catch (IllegalArgumentException e) {
                        throw new NotSupportedActivityType(activityType);
                    }
                    otmEvent.setActivityType(activityType);
                    break;
                case ACTIVITY_OBJECTS_FIELD:
                    application = otmEvent.getApplication();
                    List<Feature> features = new ArrayList<>();
                    for (JsonNode activityObject : jsonValue) {
                        Feature feature = ctxt.readValue(activityObject.traverse(p.getCodec()), Feature.class);
                        features.add(feature);
                    }
                    otmEvent.setActivityObjects(features);
                    break;
                case REFERENCES_FIELD:
                    otmEvent.setReferences(new HashSet<>());
                    for (Iterator<JsonNode> hiddenListIterator = jsonValue.elements(); hiddenListIterator.hasNext(); ) {
                        JsonNode element = hiddenListIterator.next();
                        OTMEvent.Reference reference = new OTMEvent.Reference();
                        reference.setApplication(element.get(APPLICATION_FIELD).asText());
                        reference.setExternalUrl(element.get(EXTERNAL_URL_FIELD).asText());
                        if (element.has(REFERENCES_FEATURE_TYPE)) {
                            reference.setType(element.get(REFERENCES_FEATURE_TYPE).asText());
                        }
                        otmEvent.getReferences().add(reference);
                    }
                    break;
                case DETAILS_FIELD:
                    JsonNode detailsJson = jsonValue;
                    String invalid = checkPropertyNames(detailsJson);
                    if (!Strings.isNullOrEmpty(invalid))
                        throw new MongoInvalidProperty(invalid);
                    Document detailsDocument = Document.parse(jsonValue.toString());
                    otmEvent.setDetails(detailsDocument);
                    break;
                case VISIBILITY_DETAILS_FIELD:
                    for (Iterator<JsonNode> hiddenListIterator = jsonValue.elements(); hiddenListIterator.hasNext(); ) {
                        JsonNode element = hiddenListIterator.next();
                        OTMEvent.VisibilityDetail visibilityDetails = new OTMEvent.VisibilityDetail();
                        visibilityDetails.setExternalUrl(element.get(EXTERNAL_URL_FIELD).asText());
                        visibilityDetails.setHidden(element.get(HIDDEN_FIELD).asBoolean());
                    }
                    break;
                default:
                    throw new MalformedJsonException();
            }
        }
        return otmEvent;
    }


    /**
     * Checks if all the properties of a JsonNode or Array respect some requirements (for now, a property name must not start with
     * "$" and must not contain a dot). This is a recursive method.
     *
     * @param node The JsonNode to analyze
     * @return the name of the first invalid property name found, or null if all the property names are valid.
     */
    private String checkPropertyNames(JsonNode node) { // returns name of the first invalid property (null if all valid)
        if (node == null) return null;
        if (node.isObject()) {
            for (Iterator<Map.Entry<String, JsonNode>> iterator = node.fields(); iterator.hasNext(); ) {
                Map.Entry<String, JsonNode> entry = iterator.next();
                if (entry.getKey().startsWith("$") || entry.getKey().contains("."))
                    return entry.getKey();
                String invalid = checkPropertyNames(entry.getValue());
                if (!Strings.isNullOrEmpty(invalid)) return invalid;
            }
        } else if (node.isArray()) {
            for (Iterator<JsonNode> iterator = node.elements(); iterator.hasNext(); ) {
                String invalid = checkPropertyNames(iterator.next());
                if (!Strings.isNullOrEmpty(invalid)) return invalid;
            }
        }
        return null;
    }
}
