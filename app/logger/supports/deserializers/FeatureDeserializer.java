package logger.supports.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Strings;
import com.mongodb.client.model.geojson.Geometry;
import logger.models.Feature;
import org.bson.Document;
import org.bson.codecs.configuration.CodecConfigurationException;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import logger.services.MappingTranslationController;
import logger.supports.exceptions.InvalidGeometryException;
import logger.supports.exceptions.MissingConceptMapping;
import logger.supports.exceptions.MissingPropertyMapping;

import java.io.IOException;
import java.util.Iterator;

import static logger.services.LoggerMongoController.*;

public class FeatureDeserializer extends JsonDeserializer<Feature> {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    static {
        objectMapper.registerModule(new SimpleModule() {{
            addDeserializer(Geometry.class, new GeometryDeserializer());
        }});
    }

    MappingTranslationController mappingTranslationController;
    String instance;
    String application;
    boolean validateHTML;

    public FeatureDeserializer(String instance,String application,MappingTranslationController mappingTranslationController,boolean validateHTML){
        this.instance = instance;
        this.application = application;
        this.mappingTranslationController = mappingTranslationController;
        this.validateHTML = validateHTML;
    }


    @Override
    public Feature deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        JsonNode featureJson = p.getCodec().readTree(p);
        Feature feature = new Feature();

        Iterator<String> fieldNames = featureJson.fieldNames();
        while (fieldNames.hasNext()) {
            String key = fieldNames.next();
            JsonNode jsonValue = featureJson.get(key);
            switch (key) {
                case GEOMETRY_FIELD:
                    try {
                        Geometry geometry = objectMapper.treeToValue(jsonValue, Geometry.class);
                        feature.setGeometry(geometry);
                    } catch (CodecConfigurationException e) {
                        throw new InvalidGeometryException(jsonValue);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                    break;
                case PROPERTIES_FIELD:
                    Iterator<String> propertyFieldNames = jsonValue.fieldNames();
                    feature.setProperties(new Document());
                    while (propertyFieldNames.hasNext()) {
                        String propertyKey = propertyFieldNames.next();
                        JsonNode propertyValue = jsonValue.get(propertyKey);
                        switch (propertyKey) {
                            case HAS_TYPE_FIELD:
                                String applicationConcept = propertyValue.asText();
                                String translatedConcept = mappingTranslationController.getConceptMapping(instance, application, applicationConcept);
                                if (Strings.isNullOrEmpty(translatedConcept))
                                    throw new MissingConceptMapping(applicationConcept);
                                feature.addStringProperty(HAS_TYPE_FIELD, translatedConcept);
                                break;
                            case ADDITIONAL_PROPERTIES_FIELD: //these fields aren't translated
                                Document additionalPropertiesDocument = Document.parse(propertyValue.toString());
                                feature.putAdditionalProperties(additionalPropertiesDocument);
                                break;
                            case EXTERNAL_URL_FIELD:
                                feature.addStringProperty(EXTERNAL_URL_FIELD, propertyValue.asText());
                                break;
                            case "html_snippet": // sanitizing html if required
                                if (validateHTML) {
                                    String cleanHtml = Jsoup.clean(propertyValue.asText(), Whitelist.relaxed());
                                    feature.addStringProperty(propertyKey, cleanHtml);
                                } else {
                                    feature.getProperties().put(propertyKey, propertyValue);
                                }
                                break;
                            default:
                                applicationConcept = jsonValue.get(HAS_TYPE_FIELD).asText();
                                String translatedProperty = mappingTranslationController.getPropertyMapping(instance, application, applicationConcept, propertyKey);
                                ObjectNode property = objectMapper.createObjectNode();
                                if (Strings.isNullOrEmpty(translatedProperty))
                                    throw new MissingPropertyMapping(propertyKey);
                                String unit = mappingTranslationController.getPropertyUnit(instance, application, applicationConcept, propertyKey);
                                if (unit == null)
                                    property.putPOJO(translatedProperty, propertyValue);
                                else {
                                    property.putPOJO(translatedProperty, objectMapper.createObjectNode().put(UNIT_FIELD, unit).set(VALUE_FIELD, propertyValue));
                                }

                                feature.getProperties().putAll(Document.parse(property.toString()));
                                break;
                        }
                    }
            }
        }
        return feature;
    }
}
