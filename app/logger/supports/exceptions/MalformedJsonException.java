package logger.supports.exceptions;

import com.fasterxml.jackson.databind.JsonNode;

public class MalformedJsonException extends RuntimeException {

    private JsonNode node;

    public MalformedJsonException() {
        super();
    }

    public MalformedJsonException(String message) {
        super(message);
    }

    public MalformedJsonException(JsonNode node) {
        this();
        this.node = node;
    }

    public MalformedJsonException(String message, Throwable cause) {
        super(message, cause);
    }

    public MalformedJsonException(Throwable cause) {
        super(cause);
    }

    public JsonNode getNode() {
        return node;
    }

    public void setNode(JsonNode node) {
        this.node = node;
    }
}
