package logger.supports.exceptions;

public class MissingConceptMapping extends RuntimeException{

    public MissingConceptMapping () {
    }

    public MissingConceptMapping (String message) {
        super(message);
    }

    public MissingConceptMapping (String message, Throwable cause) {
        super(message, cause);
    }

    public MissingConceptMapping (Throwable cause) {
        super(cause);
    }

}

