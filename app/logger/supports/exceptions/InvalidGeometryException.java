package logger.supports.exceptions;

import com.fasterxml.jackson.databind.JsonNode;

public class InvalidGeometryException extends RuntimeException {
    private JsonNode node;

    public InvalidGeometryException() {
        super();
    }

    public InvalidGeometryException(String message) {
        super(message);
    }

    public InvalidGeometryException(JsonNode node) {
        this();
        this.node = node;
    }

    public InvalidGeometryException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidGeometryException(Throwable cause) {
        super(cause);
    }

    public JsonNode getNode() {
        return node;
    }

    public void setNode(JsonNode node) {
        this.node = node;
    }
}
