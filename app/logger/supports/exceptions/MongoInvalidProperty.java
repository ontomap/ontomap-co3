package logger.supports.exceptions;

public class MongoInvalidProperty  extends RuntimeException{

    public MongoInvalidProperty () {
    }

    public MongoInvalidProperty (String message) {
        super(message);
    }

    public MongoInvalidProperty (String message, Throwable cause) {
        super(message, cause);
    }

    public MongoInvalidProperty (Throwable cause) {
        super(cause);
    }

}
