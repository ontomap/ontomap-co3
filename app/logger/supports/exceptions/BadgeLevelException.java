package logger.supports.exceptions;

public class BadgeLevelException  extends RuntimeException {
    public BadgeLevelException() {
        super();
    }

    public BadgeLevelException(String message) {
        super(message);
    }

    public BadgeLevelException(String message, Throwable cause) {
        super(message, cause);
    }
}
