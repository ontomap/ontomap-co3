package logger.supports.exceptions;

import org.bson.types.ObjectId;

import java.util.List;

public class UndeactivableBadgeException extends  Exception {

    ObjectId badge;
    List<ObjectId> badgeList;

    public UndeactivableBadgeException() {
    }

    public UndeactivableBadgeException(ObjectId badge, List<ObjectId> badgeList) {
        this.badge = badge;
        this.badgeList = badgeList;
    }

    public UndeactivableBadgeException(String message) {
        super(message);
    }

    public UndeactivableBadgeException(String message, Throwable cause) {
        super(message, cause);
    }

    public UndeactivableBadgeException(Throwable cause) {
        super(cause);
    }

    public ObjectId getBadge() {
        return badge;
    }

    public List<ObjectId> getBadgeList() {
        return badgeList;
    }
}
