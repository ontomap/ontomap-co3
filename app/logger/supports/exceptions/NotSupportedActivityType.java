package logger.supports.exceptions;

public class NotSupportedActivityType  extends RuntimeException{

    public NotSupportedActivityType () {
    }

    public NotSupportedActivityType (String message) {
        super(message);
    }

    public NotSupportedActivityType (String message, Throwable cause) {
        super(message, cause);
    }

    public NotSupportedActivityType (Throwable cause) {
        super(cause);
    }

}
