package logger.supports.exceptions;

public class MissingPropertyMapping extends RuntimeException{

    public MissingPropertyMapping () {
    }

    public MissingPropertyMapping (String message) {
        super(message);
    }

    public MissingPropertyMapping (String message, Throwable cause) {
        super(message, cause);
    }

    public MissingPropertyMapping (Throwable cause) {
        super(cause);
    }
}
