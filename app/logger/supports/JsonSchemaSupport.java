package logger.supports;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.LogLevel;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import play.Environment;
import logger.supports.exceptions.MalformedJsonException;

import java.io.IOException;
import java.util.Iterator;

public class JsonSchemaSupport {


    public static void analyze(JsonNode requestBody, Environment environment, String filePath) throws MalformedJsonException,InternalError {
        if (requestBody == null || requestBody.isEmpty())throw new MalformedJsonException("missing body");
        ObjectMapper om = new ObjectMapper();
        ProcessingReport processingReport;
        try {
            JsonNode jsonSchemaNode = om.readTree(environment.getFile(filePath));
            JsonSchema jsonSchema = JsonSchemaFactory.byDefault().getJsonSchema(jsonSchemaNode);
            processingReport = jsonSchema.validate(requestBody);
        } catch (IOException | ProcessingException e) {
            throw new InternalError();
        }

        if (!processingReport.isSuccess()) { // request body not valid
            Iterator<ProcessingMessage> iter = processingReport.iterator();
            ArrayNode arrayNode = om.createArrayNode();
            StringBuilder errorMessage = new StringBuilder();
            while (iter.hasNext()) {
                ProcessingMessage next = iter.next();
                if (next.getLogLevel() == LogLevel.ERROR) {
                    errorMessage.append(next.toString());
                    arrayNode.add(next.asJson());
                }
            }
            throw new MalformedJsonException(arrayNode);
        }
    }
}
