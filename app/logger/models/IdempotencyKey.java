package logger.models;

import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.Date;

public class IdempotencyKey {

    private ObjectId id;

    private String key;

    private String application;

    private String instance;

    private Date date;

    private Document payload;

    public IdempotencyKey() {
    }

    public IdempotencyKey(String key,String application, String instance, Date date, Document payload) {
        this.key = key;
        this.application = application;
        this.instance = instance;
        this.date = date;
        this.payload = payload;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Document getPayload() {
        return payload;
    }

    public void setPayload(Document payload) {
        this.payload = payload;
    }
}
