package logger.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

public abstract class MultiInstanceObject extends MongoDBObject {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String application;
    @JsonIgnore
    String instance;

    public MultiInstanceObject() {
        super();
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }
}
