package logger.models;

public enum ActivityType {
    object_created,
    object_updated,
    object_removed,

    interest_added,
    interest_removed,

    support_added,
    support_removed,

    initiator_added,
    initiator_removed,

    suggestion_rated,
    support_updated,

    delegation_set,
    delegation_changed,
    delegation_removed,

    account_registered,
    screen_name_changed,
    profile_updated,
    avatar_changed,

    contact_published,
    contact_unpublished,

    issue_voted_on,
    issue_timeline_requested,
    issue_status_updated,

    contribution_added,
    contribution_updated,
    contribution_removed,

    participation_added,
    participation_removed,

    token_transferred,
    token_created,
//        coins_bought,
//        coins_sold,
//        coins_trade,
//        token_consumed,

    crowdfunding_started,
    participated_in_crowdfunding,
    object_discovered,

    membership_added,
    membership_removed,
    membership_requested,
    membership_rejected,
    membership_withdrawn,

    task_checkin,
    task_checkout
}
