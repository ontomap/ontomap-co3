package logger.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

public class Application {
    private ObjectId id;
    private String name;
    @BsonProperty(value = "cert_common_name")
    @JsonProperty("cert_common_name")
    private String certCommonName;

    public Application() {
    }

    public Application(String name, String certCommonName) {
        this.name = name;
        this.certCommonName = certCommonName;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCertCommonName() {
        return certCommonName;
    }

    public void setCertCommonName(String certCommonName) {
        this.certCommonName = certCommonName;
    }
}
