package logger.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.Document;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;

import java.util.Set;

public class InstanceFeature extends Feature{
    protected String applicationName;
    @BsonProperty(value = "visibility_last_update_timestamp")
    @JsonProperty("visibility_last_update_timestamp")
    protected long visibilityLastUpdate;
    protected long actor;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected String aca;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected String group;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Set<OTMEvent.Reference> references;

    public InstanceFeature() {
        super();
    }

    public InstanceFeature(Feature feature) {
        super();
        this.geometry = feature.geometry;
        this.properties = Document.parse(feature.properties.toJson());
        this.currentVisibility = feature.currentVisibility;
    }


    @BsonIgnore
    public String getApplicationName() {
        return applicationName;
    }

    @BsonIgnore
    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public long getActor() {
        return actor;
    }

    public void setActor(long actor) {
        this.actor = actor;
    }

    public String getAca() {
        return aca;
    }

    public void setAca(String aca) {
        this.aca = aca;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getCurrentVisibility() {
        return currentVisibility;
    }

    public void setCurrentVisibility(String currentVisibility) {
        this.currentVisibility = currentVisibility;
    }

    public long getVisibilityLastUpdate() {
        return visibilityLastUpdate;
    }

    public void setVisibilityLastUpdate(long visibilityLastUpdate) {
        this.visibilityLastUpdate = visibilityLastUpdate;
    }

    public Set<OTMEvent.Reference> getReferences() {
        return references;
    }

    public void setReferences(Set<OTMEvent.Reference> references) {
        this.references = references;
    }
}
