package logger.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.Document;
import org.bson.codecs.pojo.annotations.BsonProperty;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OTMEvent extends MultiInstanceObject {

    long actor;
    long timestamp;
    String aca;
    String group;

    @BsonProperty(value = "activity_type")
    @JsonProperty("activity_type")
    String activityType;

    @BsonProperty(value = "activity_objects")
    @JsonProperty("activity_objects")
    List<Feature> activityObjects;

    Document details;
    Set<Reference> references;

    @BsonProperty(value = "visibility_details")
    @JsonProperty("visibility_details")
    List<VisibilityDetail> visibilityDetails;

    public long getActor() {
        return actor;
    }

    public void setActor(long actor) {
        this.actor = actor;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getAca() {
        return aca;
    }

    public void setAca(String aca) {
        this.aca = aca;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public Map<String, Object> getDetails() {
        return details;
    }

    public void setDetails(Document details) {
        this.details = details;
    }

    public Set<Reference> getReferences() {
        return references;
    }

    public void setReferences(Set<Reference> references) {
        this.references = references;
    }

    public List<Feature> getActivityObjects() {
        return activityObjects;
    }

    public void setActivityObjects(List<Feature> activityObjects) {
        this.activityObjects = activityObjects;
    }

    public List<VisibilityDetail> getVisibilityDetails() {
        return visibilityDetails;
    }

    public void setVisibilityDetails(List<VisibilityDetail> visibilityDetails) {
        this.visibilityDetails = visibilityDetails;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Reference {
        @BsonProperty(value = "external_url")
        @JsonProperty("external_url")
        String externalUrl;
        String application;
        String type;

        public Reference() {
        }

        public String getExternalUrl() {
            return externalUrl;
        }

        public void setExternalUrl(String externalUrl) {
            this.externalUrl = externalUrl;
        }

        public String getApplication() {
            return application;
        }

        public void setApplication(String application) {
            this.application = application;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Reference reference = (Reference) o;
            return Objects.equals(externalUrl, reference.externalUrl);
        }

        @Override
        public int hashCode() {
            return Objects.hash(externalUrl);
        }
    }


    public static class VisibilityDetail {
        @BsonProperty(value = "external_url")
        @JsonProperty("external_url")
        String externalUrl;
        boolean hidden;

        public VisibilityDetail() {
        }

        public String getExternalUrl() {
            return externalUrl;
        }

        public void setExternalUrl(String externalUrl) {
            this.externalUrl = externalUrl;
        }

        public boolean isHidden() {
            return hidden;
        }

        public void setHidden(boolean hidden) {
            this.hidden = hidden;
        }
    }
}
