package logger.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.*;


public class Concept {
    private String id;
    private String uri;
    private Map<String,String> labels;
    private String label;
    private Map<String,String> descriptions;
    private String description;
    private List<Property> propertyList;
    private List<Relation> relations;

    public Concept(){
        this.propertyList = new ArrayList<>();
        this.relations = new ArrayList<>();
        this.descriptions = new HashMap<>();
        this.labels = new HashMap<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Map<String, String> getLabels() {
        return labels;
    }

    public void setLabels(Map<String, String> labels) {
        this.labels = labels;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Map<String, String> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(Map<String, String> descriptions) {
        this.descriptions = descriptions;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Relation> getRelations() {
        return relations;
    }

    public void setRelations(List<Relation> relations) {
        this.relations = relations;
    }

    public void addRelation(String subject,String object,String predicate){
        Relation relation = new Relation(subject,object,predicate);
        this.relations.add(relation);
    }

    public List<Property> getPropertyList() {
        return propertyList;
    }

    public void setPropertyList(List<Property> propertyList) {
        this.propertyList = propertyList;
    }




    public static class Property{
        private String name;
        @JsonIgnore
        private String uri;
        private String label;
        private Map<String,String> labels;
        private String description;
        private Map<String,String> descriptions;
        private String range;

        public Property(){
            this.descriptions = new HashMap<>();
            this.labels = new HashMap<>();
        }

        public String getUri() {
            return uri;
        }

        public void setUri(String uri) {
            this.uri = uri;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Map<String, String> getLabels() {
            return labels;
        }

        public void setLabels(Map<String, String> labels) {
            this.labels = labels;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public Map<String, String> getDescriptions() {
            return descriptions;
        }

        public void setDescriptions(Map<String, String> descriptions) {
            this.descriptions = descriptions;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRange() {
            return range;
        }

        public void setRange(String range) {
            this.range = range;
        }
    }

    public static class Relation{
        private String subject;
        private String object;
        private String predicate;

        public Relation(String subject, String object, String predicate) {
            this.subject = subject;
            this.object=object;
            this.predicate = predicate;
        }

        public String getSubject() {
            return subject;
        }

        public void setSubject(String subject) {
            this.subject = subject;
        }

        public String getObject() {
            return object;
        }

        public void setObject(String object) {
            this.object = object;
        }

        public String getPredicate() {
            return predicate;
        }

        public void setPredicate(String predicate) {
            this.predicate = predicate;
        }
    }
}
