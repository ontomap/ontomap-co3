package logger.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.mongodb.client.model.geojson.Geometry;
import logger.supports.serializer.GeometrySerializer;
import org.bson.Document;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

public class Feature extends MultiInstanceObject implements Cloneable {
    private final String type = "Feature";
    @JsonSerialize(using = GeometrySerializer.class)
    @JsonInclude(JsonInclude.Include.ALWAYS)
    protected Geometry geometry;
    protected Document properties;
    @BsonProperty(value = "current_visibility")
    @JsonProperty("current_visibility")
    protected String currentVisibility;

    public Feature() {
        properties = new Document();
    }

    public Feature(ObjectId id) {
        this();
        this.id = id;
    }

    public Feature(Geometry geometry, Document properties, ObjectId id) {
        this.geometry = geometry;
        this.properties = properties;
        this.id = id;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public Document getProperties() {
        return properties;
    }

    public void setProperties(Document properties) {
        this.properties = properties;
    }

    /**
     * Convenience method to add a String member.
     *
     * @param key   name of the member
     * @param value the String value associated with the member
     * @since 1.0.0
     */
    public void addStringProperty(String key, String value) {
        getProperties().put(key, value);
    }

    /**
     * Convenience method to add a Number member.
     *
     * @param key   name of the member
     * @param value the Number value associated with the member
     * @since 1.0.0
     */
    public void addNumberProperty(String key, Number value) {
        getProperties().put(key, value);
    }

    /**
     * Convenience method to add a Boolean member.
     *
     * @param key   name of the member
     * @param value the Boolean value associated with the member
     * @since 1.0.0
     */
    public void addBooleanProperty(String key, Boolean value) {
        getProperties().put(key, value);
    }


    public void putAdditionalProperties(Document addPro) {
        this.properties.put("additionalProperties", addPro);
    }

    public Document retrieveAdditionalProperties() {
        if (this.properties.get("additionalProperties") != null)
            return (Document) this.properties.get("additionalProperties");
        return null;
    }

    /**
     * Convenience method to get a String member.
     *
     * @param key name of the member
     * @return the value of the member, null if it doesn't exist
     * @since 1.0.0
     */
    @BsonIgnore
    public String getStringProperty(String key) {
        if (getProperties().get(key) != null)
            return getProperties().get(key).toString();
        else return null;
    }

    /**
     * Convenience method to get a Number member.
     *
     * @param key name of the member
     * @return the value of the member, null if it doesn't exist
     * @since 1.0.0
     */
    @BsonIgnore
    public Number getNumberProperty(String key) {
        return (Number) getProperties().get(key);
    }

    /**
     * Convenience method to get a Boolean member.
     *
     * @param key name of the member
     * @return the value of the member, null if it doesn't exist
     * @since 1.0.0
     */
    @BsonIgnore
    public Boolean getBooleanProperty(String key) {
        return (Boolean) getProperties().get(key);
    }

    @BsonIgnore
    public String getHasType() {
        return this.getStringProperty("hasType");
    }

    @BsonIgnore
    public String getExternalURL() {
        return this.getStringProperty("external_url");
    }

    public String getCurrentVisibility() {
        return currentVisibility;
    }

    public void setCurrentVisibility(String currentVisibility) {
        this.currentVisibility = currentVisibility;
    }
}
