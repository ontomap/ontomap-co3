package logger.models;

import java.util.Collection;

public class PaginatedResponse<T> {
    Collection<T> results;
    int totalCount;

    public Collection<T> getResults() {
        return results;
    }

    public void setResults(Collection<T> results) {
        this.results = results;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }
}
