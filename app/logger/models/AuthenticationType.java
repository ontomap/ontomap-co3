package logger.models;

public enum AuthenticationType {
    ADMIN,
    APPLICATION_CERTIFICATE,
    APPLICATION_TOKEN,
    USER_TOKEN,
    ACA_MANAGER
}
