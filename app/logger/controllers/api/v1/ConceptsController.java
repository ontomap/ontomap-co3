package logger.controllers.api.v1;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import logger.controllers.ResponseController;
import logger.models.Concept;
import play.Environment;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;
import logger.services.OntologyController;

import javax.inject.Inject;
import java.io.FileNotFoundException;

/**
 * This class contains the methods concerning the retrieval of concepts from the ontology.
 */
public class ConceptsController extends Controller {

    private ResponseController responseController;
    private Environment environment;
    private OntologyController ontologyController;

    private static final String JSON_CONTENT_TYPE = "application/json;charset=utf-8";
    private Logger.ALogger logger;

    @Inject
    public ConceptsController(Environment environment, ResponseController responseController, OntologyController ontologyController) {
        this.environment = environment;
        this.responseController = responseController;
        this.ontologyController = ontologyController;
        this.logger = new Logger.ALogger(play.api.Logger.apply(this.getClass()));
    }

    /**
     * @api {get} /concepts/root Get root concept of the ontology
     * @apiName GetRootConcept
     * @apiGroup Concepts APIs
     * @apiDescription Returns a JSON Object, representing complete information about the root concept of the CO3OTM Ontology.<br>
     * The field <code>relations</code> is a JSON Array of objects; each object represents a semantic relation in which the root concept is involved.
     *
     * @apiParamExample Example: Specify language
     * GET http://co3.dev.ontomap.eu/api/v1/concepts/root?lang=en
     * {
     *   "uri" : "http://co3.dev.ontomap.eu/api/v1/concepts/SchemaThing",
     *   "relations" : [
     *     {
     *       "subject" : "http://co3.dev.ontomap.eu/api/v1/concepts/AtomicThing",
     *       "object" : "http://co3.dev.ontomap.eu/api/v1/concepts/SchemaThing",
     *       "predicate" : "subClassOf"
     *     },
     *     {
     *       "subject" : "http://co3.dev.ontomap.eu/api/v1/concepts/Place",
     *       "object" : "http://co3.dev.ontomap.eu/api/v1/concepts/SchemaThing",
     *       "predicate" : "subClassOf"
     *     },
     *     {
     *       "subject" : "http://co3.dev.ontomap.eu/api/v1/concepts/Event",
     *       "object" : "http://co3.dev.ontomap.eu/api/v1/concepts/SchemaThing",
     *       "predicate" : "subClassOf"
     *     }
     *   ],
     *   "label" : "SchemaThing",
     *   "description" : "The root concept of the ontology."
     * }
     *
     * @apiParam (Information type) {String} [lang] A <a href="https://en.wikipedia.org/wiki/ISO_639-1" target="_blank">ISO 639-1 language code</a>.<br/>
     * If specified, the <code>label</code> field of the JSON Object will be a String containing the label of the root concept in the specified language, if present.<br/>
     * If not set, the <code>label</code> field of the JSON Object will be a JSON Object containing the labels of the root concept in all the available languages.
     *
     * @apiUse prettyPrint
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     *
     */

    public Result getRootConcept(String lang,boolean prettyPrint) {
        return getConcept("SchemaThing", lang,prettyPrint);
    }

    /**
     * @api {get} /concepts/<concept> Get ontology concept
     * @apiName GetOntologyConcept
     * @apiGroup Concepts APIs
     * @apiDescription Returns a JSON Object, representing complete information about the concept passed as parameter in the request.<br>
     * The field <code>relations</code> is a JSON Array of objects; each object represents a semantic relation in which <code>concept</code> is involved.
     *
     * @apiParamExample Example: Specify language
     * GET https://dev.co3.ontomap.eu/api/v1/concepts/School?lang=en
     * {
     *   "label" : "School",
     *   "description": "Institution designed for the teaching of students under the direction of teachers",
     *   "uri" : "http://co3.dev.ontomap.eu/api/v1/concepts/School",
     *   "relations": [
     *     {
     *       "subject": "http://co3.dev.ontomap.eu/api/v1/concepts/School",
     *       "object": "http://co3.dev.ontomap.eu/api/v1/concepts/HealthSocialService",
     *       "predicate": "subClassOf"
     *     }
     *   ],
     *   "propertyList": [
     *     {
     *       "name": "hasRector"
     *     },
     *     {
     *       "name": "hasOffice"
     *     },
     *     {
     *       "name": "hasCompany"
     *     },
     *     {
     *       "name": "isPublic"
     *     },
     *     {
     *       "name": "hasOpeningHours",
     *       "range": "string"
     *     },,
     *     ...
     *     ]
     * }
     *
     * @apiParam (required_parameters) {String} concept <strong>Required.</strong> The concept about which the information is requested.
     *
     * @apiParam (Information type) {String} [lang] A <a href="https://en.wikipedia.org/wiki/ISO_639-1" target="_blank">ISO 639-1 language code</a>.<br/>
     * If specified, the <code>label</code> field of the JSON Object will be a String containing the label of the requested concept in the specified language, if present.<br/>
     * If not set, the <code>label</code> field of the JSON Object will be a JSON Object containing the labels of the requested concept in all the available languages.
     *
     * @apiUse prettyPrint
     *
     * @apiError (Error 404 Not Found) instance_not_found The instance requested does not exist.
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     *
     */

    public Result getConcept(String conceptName, String lang, boolean prettyPrint) {
        ObjectMapper om = new ObjectMapper();
        ObjectNode result;
        om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        try {
            Concept concept = ontologyController.getOntologyConcept(conceptName,lang);
            if (concept == null) return responseController.missingConcept();
            result = om.valueToTree(concept);
            return (environment.isProd() && !prettyPrint)
                    ? ok(result)
                    : ok(om.writerWithDefaultPrettyPrinter().writeValueAsString(result)).as(JSON_CONTENT_TYPE);
        }
        catch (FileNotFoundException e) {
            logger.error("FileNotFoundException in getRootConcept:", e);
            return responseController.internalServerError2(ResponseController.ONTOLOGY_FILE_NOT_FOUND);
        }
        catch (JsonProcessingException e) {
            logger.error("JsonProcessingException in getRootConcept:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
    }

    /**
     * @api {get} /concepts Get all ontology concepts
     * @apiName GetAllConcepts
     * @apiGroup Concepts APIs
     * @apiDescription Returns a JSON Object, representing complete information about all the concepts of the CO3OTM Ontology.<br>
     * The field <code>concepts</code> is a JSON Array of objects, each representing an ontology concept.<br>
     * If <code>relations</code> is set to <code>true</code>, a JSON Array of objects named <code>relations</code> is included in the result;
     * each object of the Array represents a semantic relation among two concepts of the ontology. All the relations defined in the ontology are included.
     *
     * @apiParamExample Example: Concepts and relations
     * GET http://co3.dev.ontomap.eu/api/v1/concepts?relations=true
     * {
     *   "concepts": [
     *     {
     *       "label": {
     *         "en": "Event",
     *         "it": "Evento"
     *       },
     *       "description": {
     *         "en": "Event definition",
     *         "it": "Definizione di Evento"
     *       },
     *       "uri": "http://co3.dev.ontomap.eu/api/v1/concepts/Event"
     *     },
     *     {
     *       "label": {
     *         "en": "Place",
     *         "it": "Luogo"
     *       },
     *       "description": {
     *         "en": "Place definition",
     *         "it": "Definizione di Luogo"
     *       },
     *       "uri": "http://co3.dev.ontomap.eu/api/v1/concepts/Place"
     *     }
     *   ],
     *   "relations": [
     *     {
     *       "subject": "http://co3.dev.ontomap.eu/api/v1/concepts/Event",
     *       "object": "http://co3.dev.ontomap.eu/api/v1/concepts/Place",
     *       "predicate": "hasPlace"
     *     }
     *   ]
     * }
     *
     * @apiParam (Information type) {Boolean} [relations=false] If set to <code>true</code>, the relations among the concepts are included in the result.
     *
     * @apiParam (Information type) {String} [lang] A <a href="https://en.wikipedia.org/wiki/ISO_639-1" target="_blank">ISO 639-1 language code</a>.<br/>
     * If specified, the <code>label</code> field of the JSON Objects representing the concepts will be a String containing the label of the requested concept in the specified language, if present.<br/>
     * If not set, the <code>label</code> field of the JSON Objects representing the concepts will be a JSON Object containing the labels of each concept in all the available languages.
     *
     * @apiUse prettyPrint
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     *
     */

    public Result getAllConcepts(String lang, boolean relations,boolean prettyPrint) {
        ObjectMapper om = new ObjectMapper();
        ObjectNode result;
        try {
            result = ontologyController.getAllConcepts(lang, relations);
            return (environment.isProd() && !prettyPrint)
                    ? ok(result)
                    : ok(om.writerWithDefaultPrettyPrinter().writeValueAsString(result)).as(JSON_CONTENT_TYPE);
        } catch (FileNotFoundException e) {
            logger.error("FileNotFoundException in getRootConcept:", e);
            return responseController.internalServerError2(ResponseController.ONTOLOGY_FILE_NOT_FOUND);
        } catch (JsonProcessingException e) {
            logger.error("JsonProcessingException in getRootConcept:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
    }

}
