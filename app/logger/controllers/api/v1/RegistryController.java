package logger.controllers.api.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.MongoException;
import logger.controllers.Attrs;
import logger.controllers.ResponseController;
import org.apache.commons.lang3.tuple.Pair;
import play.Logger.ALogger;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import logger.supports.actions.CertAuthenticatedAction;
import logger.supports.actions.MultipleInstancesAction;
import logger.services.EntityRegistryController;

import javax.inject.Inject;
import java.util.Set;

/**
 * This class contains the methods concerning the insertion and retrieval of associations between different data objects.
 */
@CertAuthenticatedAction.CertificateAuthentication
@MultipleInstancesAction.MultipleInstances
public class RegistryController extends Controller {

    private ResponseController responseController;
    private EntityRegistryController entityRegistryController;
    private ALogger logger;

    @Inject
    public RegistryController(ResponseController responseController,
                              EntityRegistryController entityRegistryController) {
        this.responseController = responseController;
        this.entityRegistryController = entityRegistryController;
        this.logger = new ALogger(play.api.Logger.apply(getClass()));
    }

    public Result getAssociations(Http.Request request, String individualApplication, String individualExternalUrl) {
        String application = request.attrs().get(Attrs.APPLICATION);
        String instance = request.attrs().get(Attrs.INSTANCE);
        Set<Pair<String, String>> entitySet = entityRegistryController.getAssociations(instance, individualApplication, individualExternalUrl);
        if (entitySet == null) return responseController.missingInstance();
        ObjectMapper om = new ObjectMapper();
        ObjectNode result = om.createObjectNode();
//        result.putArray("list");
        for (Pair<String, String> entity : entitySet) {
            result.withArray("list").add(
                    om.createObjectNode().put("application", entity.getLeft()).put("external_url", entity.getRight())
            );
        }
        return ok(result);
    }

    public Result associateIndividual(Http.Request request, String individualExternalUrl, String otherApplication, String otherExternalUrl) {
        // todo: log these requests, applications can associate only owned individuals
        String application = request.attrs().get(Attrs.APPLICATION);
        String instance = request.attrs().get(Attrs.INSTANCE);
        try {
            boolean res = entityRegistryController.setAssociation(instance, application, individualExternalUrl, otherApplication, otherExternalUrl);
            if (res) return getAssociations(request,application,individualExternalUrl);
            else return responseController.alreadyAssociatedInstance();
        }catch(MongoException e){
            logger.error("MongoException in associateIndividual:", e);
            return responseController.internalServerError2(ResponseController.MONGODB_CONNECTION_FAILED);
        }
    }

    public Result deleteAssociation(Http.Request request,String individualExternalUrl) {
        // todo: log these requests, applications can delete only owned individuals
        String application = request.attrs().get(Attrs.APPLICATION);
        String instance = request.attrs().get(Attrs.INSTANCE);
        boolean res = entityRegistryController.deleteAssociation(instance, application, individualExternalUrl);
        if (res) return ok("deleted association"); //todo: message
        else return responseController.missingInstance();
    }
}
