package logger.controllers.api.v1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Strings;
import logger.controllers.Attrs;
import logger.controllers.ResponseController;
import logger.services.ApplicationsController;
import logger.services.EntityRegistryController;
import logger.services.LoggerMongoController;
import logger.services.MappingTranslationController;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import logger.supports.actions.AdminAction;
import logger.supports.actions.MultipleInstancesAction;

import javax.inject.Inject;

/**
 * This class contains the methods concerning the actions used to administer the system, such as refreshing the caches
 * or deleting events from the logger.
 * These actions can be performed only by using a "admin" client certificate when in production (see the AdminAction class).
 */
public class AdminController extends Controller {

    //private CacheController cacheController;
    private EntityRegistryController entityRegistryController;
    private MappingTranslationController mappingTranslationController;
    private ApplicationsController applicationsController;
    private LoggerMongoController loggerMongoController;
    private ResponseController responseController;
    private Logger.ALogger logger;


    @Inject
    public AdminController(
                           EntityRegistryController entityRegistryController, MappingTranslationController mappingTranslationController,
                           ApplicationsController applicationsController, LoggerMongoController loggerMongoController,
                           ResponseController responseController) {
        this.entityRegistryController = entityRegistryController;
        this.mappingTranslationController = mappingTranslationController;
        this.applicationsController = applicationsController;
        this.loggerMongoController = loggerMongoController;
        this.responseController = responseController;
    }

    @AdminAction.AdminAuthentication
    public Result refreshCaches() {
        applicationsController.retrieveAppNames();
        entityRegistryController.cacheEntities();
        mappingTranslationController.cacheMappings();
        //cacheController.buildCache();

        logger = new  Logger.ALogger(play.api.Logger.apply(getClass()));
        return ok("Caches updated"); // todo: message
    }

    @MultipleInstancesAction.MultipleInstances
    @AdminAction.AdminAuthentication
    public Result deleteUserEvents(Http.Request request,long actor, String id, String application) {
        int paramCount = 0;
        paramCount += (actor == -1) ? 0 : 1;
        paramCount += (Strings.isNullOrEmpty(id)) ? 0 : 1;
        paramCount += (Strings.isNullOrEmpty(application)) ? 0 : 1;
        if (paramCount != 1) return badRequest("Only one parameter accepted"); // todo: message

        String instance = request.attrs().get(Attrs.INSTANCE);
        // TODO CHECK if WORKS
        try {
            return loggerMongoController.deleteUserEvents(instance, actor, id, application);
        }
        catch (JsonProcessingException e) {
            logger.error("JsonProcessingException in deleteUserEvents:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
    }

    /*
    @MultipleInstancesAction.MultipleInstances
    @AdminAction.AdminAuthentication
    public Result updateFeatureVisiblityIntoEvents() {
        try {
            loggerMongoController.updateFeatureVisiblityIntoEvents();
            return ok("Events updated with feature's visibility status");
        }catch (MongoException e){
            return responseController.internalServerError2(ResponseController.MONGODB_CONNECTION_FAILED);
        }

    }
    */
}
