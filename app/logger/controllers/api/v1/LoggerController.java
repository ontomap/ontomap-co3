package logger.controllers.api.v1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.mongodb.MongoBulkWriteException;
import com.mongodb.MongoException;
import com.mongodb.MongoTimeoutException;
import com.typesafe.config.Config;
import logger.controllers.Attrs;
import logger.controllers.ResponseController;
import logger.models.ActivityType;
import logger.models.AuthenticationType;
import play.Environment;
import play.Logger;
import play.http.HttpErrorHandler;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import logger.supports.actions.ApplicationTokenAuthenticationAction;
import logger.supports.actions.CertAuthenticatedAction;
import logger.supports.actions.MultipleInstancesAction;
import logger.services.LoggerMongoController;
import logger.supports.exceptions.*;

import javax.inject.Inject;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

import static logger.controllers.Attrs.USER_AUTHENTICATED;

/**
 * This class contains the methods concerning the insertion and retrieval of events from the logger.
 */
public class LoggerController extends Controller {

    private Config configuration;
    private Environment environment;
    private ResponseController responseController;
    private LoggerMongoController loggerMongoController;
    private Logger.ALogger logger;

    @Inject
    public LoggerController(Config configuration, Environment environment, ResponseController responseController,
                            LoggerMongoController loggerMongoController) {
        this.configuration = configuration;
        this.environment = environment;
        this.responseController = responseController;
        this.loggerMongoController = loggerMongoController;
        this.logger = new Logger.ALogger(play.api.Logger.apply(this.getClass()));
    }

    /**
     * @apiDefine wrong_content_type
     * @apiError (Error 415 Unsupported Media Type) wrong_content_type The request has an unexpected Content-Type.<br/>
     * The Content-Type for this request must be <code>application/json</code>.
     */

    /**
     * @apiDefine malformed_json
     * @apiError (Error 400 Bad Request) malformed_json The payload sent is not a valid JSON string or it doesn't adhere to the JSON Schema.
     */

    /**
     * @apiDefine too_many_requests
     * @apiError (Error 429 Too Many Requests) too_many_requests The API has received too many requests in a short time.
     * Exponential backoff is suggested for the retrying of the requests.
     */

    /**
     * @apiDefine invalid_certificate
     * @apiError (Error 401 Unauthorized) invalid_certificate The client certificate was not provided or it is invalid.
     */

    /**
     *
     * @api {post} /logger/events Post user activity events
     * @apiName InsertEvents
     * @apiGroup Logger APIs
     * @apiDescription Creates and inserts into CO3OTM one or more user activity events.<br/>
     * The request must have a <code>application/json</code> Content-Type and must include the JSON representation of the event(s) in its body (see provided example).<br/>
     *
     * The events are saved in the same format used for their posting, with the addition of an "application" (String) field. The application field contains the name of the application that posted the event, extracted from the certificate used to perform the HTTP POST Request.
     *
     * In order to post events to CO3OTM, every application must insert its mapping rules into the logger
     * (see the <a href="#api-Mapping_APIs-InsertMappingRules">Insert mapping rules API</a>).

     * @apiPermission Client Certificate.
     * @apiHeader (Request Headers) Idempotency-Key If specified, the request is considered idempotent, so it can be safely
     * retried in case of network errors without saving duplicated events, as long as the request is resubmitted using the same key.<br/>
     * The key is a client-generated random string; good examples of possible keys are
     * <a href="https://en.wikipedia.org/wiki/Universally_unique_identifier#Version_4_.28random.29" target="_blank">V4 UUIDs</a>.<br/>
     * Keys expire after 24 hours.<br/>
     * It is strongly advised to always include an Idempotency Key, so that the events can be resubmitted safely in case of network problems.
     *
     * @apiParamExample Request body (example)
     * POST https://dev.co3.ontomap.eu/api/v1/logger/events
     * {
     *   "event_list": [
     *     {
     *       "actor": 12345,
     *       "timestamp": 1485338648883,
     *       "activity_type": "object_created",
     *       "activity_objects": [
     *         {
     *           "type": "Feature",
     *           "geometry": {
     *             "type": "Point",
     *             "coordinates": [
     *               7.643340826034546,
     *               45.07558142970864
     *             ]
     *           },
     *           "properties": {
     *             "hasType": "Asili",
     *             "external_url": "http://co3.app1.eu/Asili/1",
     *              "id": "1",
     *              "aca": "https://co3.app3.eu/ACA/5",,
     *             "nome": "Asilo 1"
     *           }
     *         }
     *       ],
     *       "references": [
     *         {
     *           "application": "co3.app1.eu",
     *           "external_url": "http://co3.app1.eu/Asili/22"
     *         }
     *       ],
     *       "visibility_details": [
     *         {
     *           "external_url": "http://co3.app1.eu/Asili/56",
     *           "hidden": true
     *         }
     *       ],
     *       "details": { ... }
     *     },
     *     { ... }
     *   ]
     * }
     *
     * @apiParam (Required attributes) {Int} actor <strong>Required.</strong> The CO3UUM ID of the user performing the action.
     * @apiParam (Required attributes) {Long} timestamp <strong>Required.</strong> The Unix timestamp of the action (in ms).
     * @apiParam (Required attributes) {String} activity_type <strong>Required.</strong> The user activity type.
     * @apiParam (Attributes) {Array} [activity_objects] An Array of <a href="http://geojson.org/geojson-spec.html#feature-objects" target="_blank">GeoJSON Features</a>.<br/>
     * The feature properties must include a <code>hasType</code> field (the concept to which the feature belongs) and a
     * <code>external_url</code> field (deep link to the feature).
     * The remaining properties (as well as the feature concept) are expressed in the terminology of the client application,
     * and must be included in the mapping previously sent to the CO3OTM by the application.<br/>
     * The properties can contain a <code>additionalProperties</code> object: every field contained in this object will not be translated and it is stored as-is.<br/>
     * The same event must not contain multiple activity_objects having the same external_url.
     *
     * @apiParam (Attributes) {Array} [references] An Array of objects.<br/>
     * Every object contains three fields. The first two are mandatory: <code>external_url</code> (String) and <code>application</code> (String). The third one is optional: <code>type</code> (String)
     * referring to a specific feature managed by a specific application and the type of reference.
     *
     * @apiParam (Attributes) {Array} [visibility_details] An Array of objects.<br/>
     * Every object contains two fields: <code>external_url</code> (String) and <code>hidden</code> (Boolean).<br/>
     * When <code>visibility_details</code> is included in a JSON event, the feature corresponding to each <code>external_url</code>
     * is hidden/shown from the data retrieval results, based on the corresponding <code>hidden</code> value.<br/>
     * <code>visibility_details</code> is used when the action to be logged implies a change of visibility for some features
     * (for example, if a feature is deleted or made private/public).<br/>
     * The visibility change of a feature is applied only if the timestamp of the event is greater than or equal to the timestamp
     * of the last logged event containing a visibility change for same feature.
     *
     * @apiParam (Attributes) {Object} [details] An Object that can include further details regarding the user action to be logged.
     * This field is stored as-is.
     *
     * @apiUse malformed_json
     *
     * @apiError (Error 400 Bad Request) duplicated_activity_object An event contains two or more activity_objects with the same external_url.
     *
     * @apiUse missing_concept_mapping
     *
     * @apiUse missing_property_mapping
     *
     * @apiError  (Error 400 Bad Request) not_valid_geometry One or more activity_objects have an invalid geometry.
     *
     * @apiUse invalid_certificate
     *
     * @apiError (Error 409 Conflict) idempotency_conflict A different request using the same Idempotency Key was sent previously.
     *
     * @apiUse wrong_content_type
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     */
    //@UserTokenAuthenticatedAction.UserTokenAuthentication(operationType = UserTokenAuthenticatedAction.OperationType.WRITE)
    @CertAuthenticatedAction.CertificateAuthentication
    @BodyParser.Of(Json100Mb.class)
    public Result insertEvents(Http.Request request) {
        if (!configuration.getBoolean("ontomap.logger.enabled")) return forbidden("Logger disabled"); // todo: message
        String application = request.attrs().get(Attrs.APPLICATION);
        String instance = request.attrs().get(Attrs.INSTANCE);

        String idempotencyKey = request.getHeaders().get("Idempotency-Key").orElse(null);

        if (!request.contentType().orElse("").equals(Http.MimeTypes.JSON)) {
            return responseController.wrongContentType();
        }

        JsonNode requestBody = request.body().asJson();
        if (requestBody == null) return responseController.malformedJsonRequest("");
        ObjectMapper om = new ObjectMapper();

        // json schema validation
        ProcessingReport processingReport;
        try {
            JsonNode jsonSchemaNode = om.readTree(environment.getFile(configuration.getString("ontomap.logger.json_schema")));
            JsonSchema jsonSchema = JsonSchemaFactory.byDefault().getJsonSchema(jsonSchemaNode);
            processingReport = jsonSchema.validate(requestBody);
        }
        catch (IOException | ProcessingException e) {
            logger.error("Exception in insertEvents:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
        if (!processingReport.isSuccess()) { // request body not valid
            Iterator<ProcessingMessage> iter = processingReport.iterator();
            ArrayNode arrayNode = om.createArrayNode();
            StringBuilder errorMessage = new StringBuilder();
            while (iter.hasNext()) {
                ProcessingMessage next = iter.next();
                errorMessage.append(next.toString());
                arrayNode.add(next.asJson());
            }
            return responseController.malformedJsonRequest(arrayNode);
        }

        try {
            return loggerMongoController.insertEvents(idempotencyKey, application, instance, request.attrs().get(USER_AUTHENTICATED),request.attrs().get(Attrs.AUTHENTICATIONS), requestBody);
        }
        catch (MongoTimeoutException e) {
            logger.error("MongoTimeoutException in insertEvents:", e);
            logger.info(requestBody.toString());
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
        catch (MongoException e) {
            logger.error("MongoException in insertEvents:", e);
            logger.info(requestBody.toString());
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
        catch (UserNotValidException e){
            logger.error("UserNotValid in insertEvents:", e);
            logger.info(requestBody.toString());
            return responseController.notValidUser(e);
        } catch (NotSupportedActivityType e){
            logger.error("NotSupportedActivityType in insertEvents:", e);
            logger.info(requestBody.toString());
            return responseController.notSupportedActivityType(e.getMessage());
        } catch (MongoInvalidProperty e) {
            logger.error("MongoInvalidProperty in insertEvents:", e);
            logger.info(requestBody.toString());
            return responseController.invalidProperty(e.getMessage());
        } catch (MalformedJsonException e) {
            logger.error("MalformedJsonException in insertEvents:", e);
            logger.info(requestBody.toString());
            return responseController.malformedJsonRequest(e.getNode());
        } catch (MissingConceptMapping e) {
            logger.error("MissingConceptMapping in insertEvents:", e);
            logger.info(requestBody.toString());
            return responseController.missingConceptMapping(e.getMessage());
        } catch (MissingPropertyMapping e) {
            logger.error("MissingPropertyMapping in insertEvents:", e);
            logger.info(requestBody.toString());
            return responseController.missingPropertyMapping(e.getMessage());
        } catch (InvalidGeometryException e) {
            logger.error("InvalidGeometryException in insertEvents:", e);
            logger.info(requestBody.toString());
            return responseController.malformedFeatureGeometry(e.getNode());
        } catch (Exception e){
            logger.error("Exception in insertEvents:", e);
            logger.info(requestBody.toString());
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
    }

    /**
     * @api {get} /logger/events Retrieve user activity events
     * @apiName RetrieveEvents
     * @apiGroup Logger APIs
     * @apiDescription Returns a JSON Object containing a list of all the logged events, sorted from the most recent to the least recent one.<br/>
     * The events are returned in the same format used for their posting, with the addition of an "application" (String) field.
     * The application field contains the name of the application that posted the event.<br/>
     * It is possible to combine multiple filters in a single request.<br/>
     *
     * @apiParam (Filters) {Int} [actor] Returns only the events related to actions performed by the user having the specified CO3UUM ID.
     * @apiParam (Filters) {String} [application] Returns only the events related to the specified application.
     * @apiParam (Filters) {String} [activity_type] Returns only the events related to actions of the specified type.
     * @apiParam (Filters) {String} [concept] Returns only the events which contain activity_objects belonging to the specified concept (CO3OTM Ontology).
     * @apiParam (Filters) {String} [external_url] Returns only the events which contain the activity_object having the specified external_url.
     * @apiParam (Filters) {String} [reference_concept] Returns only the events which refer to activity_objects belonging to the specified concept (CO3OTM Ontology).
     * @apiParam (Filters) {String} [reference_external_url] Returns only the events which refer to the activity_object having the specified external_url.
     * @apiParam (Filters) {Boolean} [subconcepts=false] If set to <code>true</code> and <code>concept</code> or
     * <code>reference_concept</code> are specified, returns only the events which contain or refer to activity_objects
     * belonging to the specified concept and its subconcepts. Has effect only if <code>concept</code> or
     * <code>reference_concept</code> are specified.
     * @apiParam (Filters) {Long} [start_time] Returns only the events having a timestamp greater than or equal to the specified timestamp (Unix timestamp in ms).
     * @apiParam (Filters) {Long} [end_time] Returns only the events having a timestamp less than or equal to the specified timestamp (Unix timestamp in ms).
     * @apiParam (Filters) {String} [boundingbox] A string representing the coordinates of the North-East and South-West
     * points of a bounding box (using EPSG:4326 as CRS).<br/>
     * Returns only the events including or referring to activity_objects located in the specified bounding box.<br/>
     * String format: <code>SW_lng,SW_lat,NE_lng,NE_lat</code>
     * @apiParam (Filters) {Boolean} [aggregate]  If set to <code>true</code>, each event of the returned list will have at most one activity_object.
     * If an activity_object (identified by application/external_url) appears in more than one event, only the most recent version of it is returned.
     * @apiParam (Filters) {Boolean} [visibility]  If set to <code>true</code>, returns only the events which contain visible activity_objects (current_visibility = true).
     * @apiParam (Filters) {String} [additionalProperty_X]  Returns only the events containing activity_objects whose X <code>additionalProperty</code> has the specified value (Int/Double, String and Boolean only supported).
     *
     * E.g.: <code>additionalProperty_moderation = true</code>) only returns the events which contain activity_objects have that additionalProperty <code>moderation = true</code>.
     * @apiUse access_token
     *
     *
     * @apiParamExample Retrieve events
     * GET https://dev.co3.ontomap.eu/api/v1/logger/events?actor=67890
     * {
     *   "event_list": [
     *     {
     *       "application": "co3.app1.eu",
     *       "actor": 67890,
     *       "timestamp": 14967425487654,
     *       "activity_type": "profile_updated",
     *     },
     *     {
     *       "application": "co3.app2.eu",
     *       "actor": 67890,
     *       "timestamp": 1485338648883,
     *       "activity_type": "object_created",
     *       "activity_objects": [
     *         {
     *           "type": "Feature",
     *           "geometry": {
     *             "type": "Point",
     *             "coordinates": [
     *               7.643340826034546,
     *               45.07558142970864
     *             ]
     *           },
     *           "properties": {
     *             "hasType": "Asili",
     *             "external_url": "http://co3.app1.eu/Asili/1",
     *             "id": "1",
     *             "aca": "https://co3.app3.eu/ACA/5",,
     *             "nome": "Asilo 1"
     *           }
     *         }
     *       ],
     *       "references": [
     *         {
     *           "application": "co3.app1.eu",
     *           "external_url": "http://co3.app1.eu/Asili/22"
     *         }
     *       ],
     *       "visibility_details": [
     *         {
     *           "external_url": "http://co3.app1.eu/Asili/56",
     *           "hidden": true
     *         }
     *       ],
     *       "details": {
     *           "action_id": "76b66b1a-d846-4a56-8bfb-5aa5b4a3227f"
     *       }
     *     }
     *   ]
     * }
     *
     * @apiParam (Filters) {String} [reference_concept] If specified, returns only the events referring to activity_objects
     * belonging to the specified concept (OnToMap terminology).
     *
     * @apiParam (Filters) {String} [reference_external_url] If specified, returns only the events referring to the activity_object having the specified external_url.
     *
     * @apiUse pagination
     *
     * @apiUse prettyPrint
     *
     * @apiUse malformed_parameter
     *
     * @apiUse invalid_certificate
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     * @apiPermission Client Certificate, access_token, application token.
     */
    @ApplicationTokenAuthenticationAction.ApplicationTokenAuthentication
    public Result retrieveEvents(Http.Request request,long actor, long startTime, long endTime, String application, String boundingBoxString,
                                 String activityType, String concept, String externalUrl,
                                 boolean excludeFeatures,
                                 boolean prettyPrint,
                                 boolean subConcepts,
                                 boolean aggregate,
                                 String visibility,
                                 String referenceConcept, String referenceExternalUrl,
                                 int limit, int page) {
        if (!configuration.getBoolean("ontomap.logger.enabled")) return forbidden("Logger disabled"); // todo: message

        String instance = request.attrs().get(Attrs.INSTANCE);
//        boolean excludeFeatures = Boolean.parseBoolean(request.queryString("exclude_features").get());
//        boolean prettyPrint = Boolean.parseBoolean(request.queryString("prettyprint").get());
//        boolean subConcepts = Boolean.parseBoolean(request.queryString("subconcepts").get());
//        boolean aggregate = Boolean.parseBoolean(request.queryString("aggregate").get());
//        String visibility = request.queryString("visibility").get();

        HashMap<String,String> additionalProperties = new HashMap<>();
        for(String key :  request.queryString().keySet()){
            if(key.startsWith("additionalProperty_"))
            additionalProperties.put(key.replace("additionalProperty_",""), request.queryString(key).get());
        }
        List<AuthenticationType> authenticationTypes = request.attrs().get(Attrs.AUTHENTICATIONS);
        boolean clean = authenticationTypes.size() ==1 && authenticationTypes.contains(AuthenticationType.APPLICATION_TOKEN);
        ObjectMapper om = new ObjectMapper();
        try {
            ObjectNode result = loggerMongoController.retrieveEvents(instance,clean,actor, startTime, endTime, application, boundingBoxString,
                    activityType, concept, externalUrl, referenceConcept, referenceExternalUrl, limit,
                    page, excludeFeatures, subConcepts, aggregate,visibility,additionalProperties);
            return (environment.isProd() && !prettyPrint)
                    ? ok(result)
                    : ok(om.writerWithDefaultPrettyPrinter().writeValueAsString(result)).as(InstancesController.JSON_CONTENT_TYPE);
        }
        catch (FileNotFoundException e) {
            logger.error("FileNotFoundException in retrieveEvents:", e);
            return responseController.internalServerError2(ResponseController.ONTOLOGY_FILE_NOT_FOUND);
        }
        catch (MongoTimeoutException e) {
            logger.error("MongoTimeoutException in retrieveEvents:", e);
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
        catch (JsonProcessingException e) {
            logger.error("JsonProcessingException in retrieveEvents:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
        catch (IllegalArgumentException | MongoBulkWriteException e) {
            return responseController.malformedParameterError(e.getMessage());
        }
    }


    /**
     * @api {get} /api/v1/logger/events/<id> Retrieve a specific user activity event
     * @apiName RetrieveSingleEvent
     * @apiGroup Logger APIs
     * @apiDescription Returns a JSON Object containing the event from the logger having the specified <i>id</i>.
     * The event is returned in the same format used for its posting, with the addition of an "application" (String)
     * field containing the name of the application that posted the event.
     * The name of the application is extracted from the certificate sent by the application
     * which inserted the event.<br/>
     * Every request made to this endpoint must include a client certificate signed by LiquidFeedback or contain the token as param.
     * @apiParamExample Retrieve a single event
     * GET https://dev.co3.ontomap.eu/api/v1/logger/events/5b103de8a1d1cd2e98cf05ed
     * {
     *      "actor": 12345,
     *      "timestamp": 1485338648883,
     *      "activity_type": "object_created",
     *      "references": [
     *        {
     *           "application": "co3.app1.eu",
     *           "external_url": "http://co3.app1.eu/Asili/22"
     *        }
     *       ],
     *       "details": {},
     *       "application": "ontomap.eu",
     *       "id": "5b103de8a1d1cd2e98cf05ed",
     *       "applicationName": "OnToMap"
     * }
     *
     * @apiParam (Required parameter) {String} id <strong>Required.</strong> The id of the event that is requested.
     *
     * @apiUse prettyPrint
     *
     * @apiUse invalid_certificate
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     *
     * @apiPermission Client Certificate, access_token, application token.
     */

    @ApplicationTokenAuthenticationAction.ApplicationTokenAuthentication
    @MultipleInstancesAction.MultipleInstances
    public Result retrieveSingleEvent(Http.Request request,String eventId, boolean prettyPrint) {
        if (!configuration.getBoolean("ontomap.logger.enabled")) return forbidden("Logger disabled"); // todo: message
        String instance = request.attrs().get(Attrs.INSTANCE);
        List<AuthenticationType> authenticationTypes = request.attrs().get(Attrs.AUTHENTICATIONS);
        boolean clean = authenticationTypes.size() ==1 && authenticationTypes.contains(AuthenticationType.APPLICATION_TOKEN);
        ObjectMapper om = new ObjectMapper();
        try {
            ObjectNode result = loggerMongoController.retrieveSingleEvent(instance,clean,eventId);
            if (result == null) return responseController.eventNotFound();
            return (environment.isProd() && !prettyPrint)
                    ? ok(result)
                    : ok(om.writerWithDefaultPrettyPrinter().writeValueAsString(result)).as(InstancesController.JSON_CONTENT_TYPE);
        }
        catch (MongoTimeoutException e) {
            logger.error("MongoTimeoutException in retrieveSingleEvent:", e);
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
        catch (JsonProcessingException e) {
            logger.error("JsonProcessingException in retrieveSingleEvent:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
    }

    public Result getActivityTypesList(){
        ObjectMapper om = new ObjectMapper();
        return ok((JsonNode) om.valueToTree(ActivityType.values()));
    }


    public static class Json100Mb extends BodyParser.Json {
        @Inject
        public Json100Mb(HttpErrorHandler errorHandler) {
            super(100 * 1024 * 1024, errorHandler);
        }
    }
}
