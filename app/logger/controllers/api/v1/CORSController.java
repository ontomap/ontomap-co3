package logger.controllers.api.v1;

import play.mvc.Controller;
import play.mvc.Result;

public class CORSController extends Controller {

    public Result preflight(String all){
        return  ok()
                .withHeader("Allow", "*")
                .withHeader("Access-Control-Allow-Methods", "POST, GET, PUT, PATCH, DELETE, OPTIONS")
                .withHeader("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept, Referrer, User-Agent, Authorization");
    }
}
