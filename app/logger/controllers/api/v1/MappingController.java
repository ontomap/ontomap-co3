package logger.controllers.api.v1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingMessage;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.mongodb.MongoTimeoutException;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.client.result.UpdateResult;
import com.typesafe.config.Config;
import logger.controllers.Attrs;
import logger.controllers.ResponseController;
import org.bson.Document;
import play.Environment;
import play.Logger.ALogger;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import logger.supports.actions.CertAuthenticatedAction.CertificateAuthentication;
import logger.supports.actions.MultipleInstancesAction.MultipleInstances;
import logger.services.MappingTranslationController;
import logger.services.MongoDBController;
import logger.services.OntologyController;

import javax.inject.Inject;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

/**
 * This class contains the methods concerning the insertion and retrieval of mapping rules pertaining to each application.
 */
public class MappingController extends Controller {

    private Environment environment;
    private ResponseController responseController;
    private MongoDBController mongoDBController;
    private Config configuration;
    private MappingTranslationController mappingTranslationController;
    private OntologyController ontologyController;
    private ALogger logger;

    @Inject
    public MappingController(Environment environment, ResponseController responseController,
                             MongoDBController mongoDBController, Config configuration,
                             MappingTranslationController mappingTranslationController, OntologyController ontologyController) {
        this.environment = environment;
        this.responseController = responseController;
        this.mongoDBController = mongoDBController;
        this.configuration = configuration;
        this.mappingTranslationController = mappingTranslationController;
        this.ontologyController = ontologyController;
        this.logger = new ALogger(play.api.Logger.apply(getClass()));
    }

    /**
     * @apiDefine missing_concept_mapping
     * @apiError (Error 400 Bad Request) missing_concept_mapping There is no corresponding mapping for the concept indicated.
     */

    /**
     * @apiDefine missing_property_mapping
     * @apiError (Error 400 Bad Request) missing_property_mapping There is no corresponding mapping for the property indicated.
     */

    /**
     * @api {post} /logger/mappings Insert mapping rules
     * @apiName InsertMappingRules
     * @apiGroup Mapping APIs
     * @apiDescription Publishes to CO3OTM the mapping rules for translating concepts and properties to the CO3OTM Ontology.<br/>
     * The request must have a <code>application/json</code> Content-Type and must include the JSON representation of the mapping rules in its body (see provided example).<br/>
     * Every request made to this endpoint must include a client certificate signed by LiquidFeedback.<br/>
     * The mapping rules consist in a list of concept mappings, which associate a concept in the application terminology
     * to one in the OnToMap terminology. For each concept it is possible to include a list of property mappings,
     * each one associating a property in the app terminology to one in the OTM terminology.<br/>
     * All the concept and property mappings must refer to concepts and properties in the  CO3OTM Ontology.<br/>
     * When a property represents a measure, it is possible to include its measure unit, in order to avoid ambiguities.<br/>
     * When new mapping rules are submitted, any previous rules associated to the application performing the request are overwritten.
     *
     * @apiParamExample Insert mapping rules
     * POST https://dev.co3.ontomap.eu/api/v1/logger/mappings
     * {
     *   "mappings": [
     *     {
     *       "app_concept": "ChildCare",
     *       "ontomap_concept": "Kindergarten",
     *       "properties": [
     *         {
     *           "app_property": "denominazione",
     *           "ontomap_property": "hasName"
     *         },
     *         {
     *           "app_property": "id",
     *           "ontomap_property": "hasID"
     *         },
     *         {
     *           "app_property": "aca",
     *           "ontomap_property": "hasAca"
     *         },
     *         {
     *           "app_property": "indirizzo",
     *           "ontomap_property": "hasAddress"
     *         },
     *         {
     *           "app_property": "telefono",
     *           "ontomap_property": "hasPhoneNumber"
     *         },
     *         {
     *           "app_property": "retta_mensile",
     *           "ontomap_property": "hasMonthlyRate",
     *           "unit": "EUR"
     *         }
     *       ]
     *     }
     *   ]
     * }
     *
     * @apiUse malformed_json
     *
     * @apiUse wrong_content_type
     *
     * @apiUse invalid_certificate
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     *
     * @apiUse missing_concept_mapping
     *
     * @apiUse missing_property_mapping
     * @apiPermission Client Certificate.
     */

    @CertificateAuthentication
    @MultipleInstances
    public Result insertMappingRules(Http.Request request) { // todo: move to MappingTranslationController
        String application = request.attrs().get(Attrs.APPLICATION);
        String instance = request.attrs().get(Attrs.INSTANCE);
        if (!request.contentType().orElse("").equals(Http.MimeTypes.JSON)) {
            return responseController.wrongContentType();
        }
        MongoDatabase database = mongoDBController.getMongoDatabase();
        MongoCollection<Document> collection = database.getCollection(configuration.getString("mongo.collections.mappings"));
        JsonNode requestBody = request.body().asJson();
        if (requestBody == null) return responseController.malformedJsonRequest("");
        ObjectMapper om = new ObjectMapper();
        ProcessingReport processingReport;
        //TODO: check null request body
        try {
            JsonNode jsonSchemaNode = om.readTree(environment.getFile(configuration.getString("ontomap.logger.mappings.json_schema")));
            JsonSchema jsonSchema = JsonSchemaFactory.byDefault().getJsonSchema(jsonSchemaNode);
            processingReport = jsonSchema.validate(requestBody);
        }
        catch (IOException | ProcessingException e) {
            logger.error("Exception in insertMappingRules:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
        if (!processingReport.isSuccess()) { // request body not valid
            Iterator<ProcessingMessage> iter = processingReport.iterator();
            StringBuilder errorMessage = new StringBuilder();
            while (iter.hasNext()) {
                errorMessage.append(iter.next().toString());
            }
            return responseController.malformedJsonRequest(errorMessage.toString());
        }

        Set<String> conceptsSet;
        Set<String> propertySet;

        try {
            conceptsSet = ontologyController.getSubConceptsSet("SchemaThing");
            propertySet = ontologyController.getPropertiesSet();
        }
        catch (FileNotFoundException e) {
            logger.error("FileNotFoundException in insertMappingRules:", e);
            return responseController.internalServerError2(ResponseController.ONTOLOGY_FILE_NOT_FOUND);
        }

        for(Iterator<JsonNode> mappingsIterator = requestBody.withArray("mappings").elements(); mappingsIterator.hasNext();) {
            JsonNode mapping = mappingsIterator.next();

            if (!conceptsSet.contains(mapping.get("ontomap_concept").asText()))
                return responseController.invalidConceptMapping(mapping.get("ontomap_concept").asText());

            if (mapping.get("properties") != null) {
                for (Iterator<JsonNode> propertiesIterator = mapping.withArray("properties").elements(); propertiesIterator.hasNext();) {
                    JsonNode property = propertiesIterator.next();
                    String otmProperty = property.get("ontomap_property").asText();
                    if (otmProperty.startsWith("$") || otmProperty.contains("."))
                        return responseController.invalidProperty(otmProperty);
                    if (!propertySet.contains(otmProperty))
                        return responseController.invalidPropertyMapping(otmProperty);
                }
            }
        }

            try {
                Document rulesBson = Document.parse(om.writeValueAsString(requestBody));
                rulesBson.put("application", application);
                rulesBson.put("instance", instance);
                UpdateResult res = collection.replaceOne(
                        Filters.and(Filters.eq("application", application), Filters.eq("instance", instance)),
                        rulesBson, new ReplaceOptions().upsert(true));
                mappingTranslationController.cacheMappings(); // todo: check exceptions?
                return ok(getMappingRules(instance, application));
            } catch (JsonProcessingException e) {
                logger.error("JsonProcessingException in insertMappingRules:", e);
                return responseController.internalServerError2(ResponseController.PARSING_ERROR);
            } catch (MongoTimeoutException e) {
                logger.error("MongoTimeoutException in insertMappingRules:", e);
                return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
            } catch (IOException e) {
                logger.error("IOException in insertMappingRules:", e);
                return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }

    }

    /**
     *
     * @api {get} /logger/mappings Retrieve mapping rules
     * @apiName RetrieveMappingRules
     * @apiGroup Mapping APIs
     * @apiDescription Returns a JSON Object containing a list of all the concept mappings associated with the application performing the request.<br/>
     * @apiParamExample Retrieve mapping rules
     * GET https://dev.co3.ontomap.eu/api/v1/logger/mappings
     * {
     *   "application": "co3.app1.eu",
     *   "mappings": [
     *     {
     *       "app_concept": "ChildCare",
     *       "ontomap_concept": "Kindergarten",
     *       "properties": [
     *         {
     *           "app_property": "denominazione",
     *           "ontomap_property": "hasName"
     *         },
     *         {
     *           "app_property": "indirizzo",
     *           "ontomap_property": "hasAddress"
     *         },
     *         {
     *           "app_property": "telefono",
     *           "ontomap_property": "hasPhoneNumber"
     *         },
     *         {
     *           "app_property": "retta_mensile",
     *           "ontomap_property": "hasMonthlyRate",
     *           "unit": "EUR"
     *         }
     *       ]
     *     }
     *   ]
     * }
     *
     *
     * @apiUse invalid_certificate
     *
     * @apiError (Error 404 Not Found) mapping_not_found There is no mapping associated with the application yet.
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     * @apiPermission Client Certificate.
     */
    @CertificateAuthentication
    @MultipleInstances
    public Result retrieveMappingRules(Http.Request request) {
        String application = request.attrs().get(Attrs.APPLICATION);
        String instance =  request.attrs().get(Attrs.INSTANCE);
        try {
            JsonNode mappingRules = getMappingRules(instance, application);
            if (mappingRules == null) return responseController.missingMapping();
            return ok(mappingRules);
        }
        catch (MongoTimeoutException e) {
            logger.error("MongoTimeoutException in retrieveMappingRules:", e);
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
        catch (IOException e) {
            logger.error("IOException in getMappingRules:", e);
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
    }

    protected JsonNode getMappingRules(String instance, String application) throws MongoTimeoutException, IOException {
        MongoDatabase database = mongoDBController.getMongoDatabase();
        MongoCollection<Document> collection = database.getCollection(configuration.getString("mongo.collections.mappings"));
        ObjectMapper om = new ObjectMapper();
        Document mapping  = collection.find()
                .filter(Filters.and(Filters.eq("application", application), Filters.eq("instance", instance)))
                .projection(Projections.exclude("_id", "instance"))
                .first();
        if (mapping == null) return null;
        return om.readTree(mapping.toJson());
    }

}
