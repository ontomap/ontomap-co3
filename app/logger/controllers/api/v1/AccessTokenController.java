package logger.controllers.api.v1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoTimeoutException;
import logger.controllers.Attrs;
import logger.controllers.ResponseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import logger.supports.actions.CertAuthenticatedAction;
import logger.services.AccessTokenMongoController;

import javax.inject.Inject;

/**
 * This class contains the methods concerning the generation of access tokens for read-only use of the API.
 */

public class AccessTokenController extends Controller {

    private final AccessTokenMongoController accessTokenMongoController;
    private final ResponseController responseController;
    final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Inject
    public AccessTokenController(AccessTokenMongoController accessTokenMongoController, ResponseController responseController) {
        this.accessTokenMongoController = accessTokenMongoController;
        this.responseController = responseController;
    }


    /**
     * @api {get} /access_token  Get the access token
     * @apiName GetToken
     * @apiGroup Token APIs
     * @apiDescription Returns a JSON Object containing an access token for read-only use of the API. This token can be used when it is not possible or practical to use a client certificate (e.g. AJAX requests).
     * This token can be used within a GET HTTP Request to retrieve an event list. However every event will only contain non-sensitive information (e.g.: id, name, position of the feature).
     * @apiParamExample Example
     * GET https://dev.co3.ontomap.eu/api/v1/access_token
     * {
     *   "token" : "ZGNmZDk0MTYtZjQ5YS00YTQ4LTg4NzUtMDE5M2EyMDg0M2Yy"
     * }
     *
     * @apiUse invalid_certificate
     * @apiPermission Client Certificate
     */
    @CertAuthenticatedAction.CertificateAuthentication
    public Result getToken(Http.Request request) { // todo:exceptions
        String application = request.attrs().get(Attrs.APPLICATION);
        try {
            String token = accessTokenMongoController.getToken(application);
            ObjectMapper om = new ObjectMapper();
            return ok(om.createObjectNode().put("token", token));
        }
        catch (MongoTimeoutException e) {
            logger.error("MongoTimeoutException in getToken:", e);
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
    }

    /**
     * @api {post} /access_token/renew  Renew the access token
     * @apiName RenewToken
     * @apiGroup Token APIs
     * @apiDescription Returns a JSON Object containing a new access token for read-only use of the API. This token can be used when it is not possible or practical to use a client certificate (e.g. AJAX requests).
     * This token can be used within a GET HTTP Request to retrieve an event list. However every event will only contain non-sensitive information (e.g.: id, name, position of the feature).
     *
     * @apiUse invalid_certificate
     * @apiPermission Client Certificate
     */
    @CertAuthenticatedAction.CertificateAuthentication
    public Result renewToken(Http.Request request) { // todo:exceptions
        String application = request.attrs().get(Attrs.APPLICATION);
        try {
            String newToken = accessTokenMongoController.generateToken(application);
            ObjectMapper om = new ObjectMapper();
            return ok(om.createObjectNode().put("token", newToken));
        }
        catch (MongoTimeoutException e) {
            logger.error("MongoTimeoutException in renewToken:", e);
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
    }
}
