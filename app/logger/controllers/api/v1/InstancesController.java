package logger.controllers.api.v1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.mongodb.MongoTimeoutException;
import com.typesafe.config.Config;
import logger.controllers.Attrs;
import logger.controllers.ResponseController;
import logger.models.Feature;
import logger.services.EntityRegistryController;
import logger.services.FeatureController;
import logger.services.MappingTranslationController;
import logger.services.OntologyController;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.jena.query.QueryException;
import play.Environment;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import logger.supports.FeatureComparator;
import logger.supports.actions.ApplicationTokenAuthenticationAction;
import logger.supports.actions.CertAuthenticatedAction;
import logger.supports.actions.MultipleInstancesAction;

import javax.inject.Inject;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class contains the methods concerning the retrieval of concept instances from the Triple Store or the Logger.
 */
@MultipleInstancesAction.MultipleInstances
public class InstancesController extends Controller {

    private ResponseController responseController;
    private Config configuration;
    private Environment environment;
    private FeatureController featureController;
    private MappingTranslationController mappingTranslationController;
    private OntologyController ontologyController;
    private EntityRegistryController entityRegistryController;

    public static final String JSON_CONTENT_TYPE = "application/json;charset=utf-8";
    private static final String CACHE_HEADER = "OnToMap-Cached-Result";

    private Logger.ALogger logger;

    @Inject
    public InstancesController(Config configuration, Environment environment,
                               ResponseController responseController,
                                 FeatureController featureController,
                               MappingTranslationController mappingTranslationController,
                               OntologyController ontologyController, EntityRegistryController entityRegistryController) {
        this.configuration = configuration;
        this.environment = environment;
        this.responseController = responseController;
        this.featureController = featureController;
        this.mappingTranslationController = mappingTranslationController;
        this.ontologyController = ontologyController;
        this.entityRegistryController = entityRegistryController;
        this.logger = new Logger.ALogger(play.api.Logger.apply(this.getClass()));
    }

    /**
     * @apiDefine prettyPrint
     * @apiParam (Presentation) {Boolean} [prettyprint=false] If set to <code>true</code>, the JSON string served is indented.
     *
     */

    /**
     * @apiDefine access_token
     * @apiHeader (Security Header) {String} [access_token]  Allows to override the client certificate validation.
     *
     */

    /**
     * @apiDefine required_parameters Required parameters
     */

    /**
     * @apiDefine pagination
     * @apiParam (Pagination) {Number{>=0}} [page] If specified, the results are divided in pages and the requested page is returned.<br/>
     * The page size is set using the <code>limit</code> parameter (default: 200 instances).
     * @apiParam (Pagination) {Number{1-500}} [limit=200] Sets the page size. Has effect only if <code>page</code> is set.
     */

    /**
     * @apiDefine missing_parameter
     * @apiError (Error 400 Bad Request) missing_parameter The parameter indicated is missing.
     */

    /**
     * @apiDefine malformed_parameter
     * @apiError (Error 400 Bad Request) malformed_parameter The parameter indicated is malformed.
     */

    /**
     * @apiDefine internal_server_error
     * @apiError (Error 5xx Internal Server Error) internal_server_error There was an error during the elaboration of the request.
     */

    /**
     * @api {get} /instances/<concept> Get concept instances
     * @apiName GetConceptInstances
     * @apiGroup Instances APIs
     * @apiDescription Returns a <a href="http://geojson.org/geojson-spec.html#feature-collection-objects" target="_blank">GeoJSON FeatureCollection</a>,
     * containing a list of <a href="http://geojson.org/geojson-spec.html#feature-objects" target="_blank">Features</a>. <br/>
     * Each <a href="http://geojson.org/geojson-spec.html#feature-objects" target="_blank">Feature</a> of the collection represents an instance of the specified concept.<br/>
     * The <code>provenance</code> field of each Feature, if present, is a URL that can be invoked to retrieve the description of the provenance of the Feature, in JSON format.<br/>
     * The <code>application</code> field of each Feature, if present, is a string containing the domain name of the application managing the Feature.<br/>
     * By default, only the labels and direct URLs to the instances are included in the <code>properties</code> field of each Feature.<br/>
     *
     *
     * @apiParamExample Example: Descriptions, Geometries, Source
     * GET https://dev.co3.ontomap.eu/api/v1/instances/Park?descriptions=true&geometries=true&source=ontomap.eu,co3.app1.eu
     * {
     *  "type": "FeatureCollection",
     *  "features": [
     *  {
     *    "type": "Feature",
     *    "geometry": {"type": "Point", "coordinates": [45.5, 7]},
     *    "id": "http://ontomap.eu/ontology/Park/1",
     *    "application": "ontomap.eu",
     *    "properties": {
     *      "external_url": "https://co3.app1.eu/Parks/1",
     *      "hasType": "Park",
     *      "hasID": "1",
     *      "hasAca": "https://co3.app3.eu/ACA/5",,
     *      "hasAddress": "Address for Park 1",
     *    }
     *  },
     *  {
     *    "type": "Feature",
     *    "geometry": {"type": "Point", "coordinates": [45, 7.5]},
     *    "id": "https://co3.app1.eu/Parks/5",
     *    "application": "co3.app1.eu",
     *    "properties": {
     *      "external_url": "https://co3.app1.eu/Parks/5",
     *      "hasType": "Park",
     *      "hasID": "5",
     *      "hasAca": "https://co3.app3.eu/ACA/5",,
     *      "hasAddress": "Address for Park 5",
     *    }
     *  }]
     * }
     *
     * @apiParamExample Example: Count
     * GET https://dev.co3.ontomap.eu/api/v1/instances/Park?count=true
     * {"count": 1}
     *
     * @apiParamExample Example: Bounding box, language
     * GET https://dev.co3.ontomap.eu/api/v1/instances/Park?lang=en&boundingbox=7.4,45.1,7.6,44.9
     * {
     *  "type": "FeatureCollection",
     *  "features": [
     *  {
     *    "type": "Feature",
     *    "geometry": null,
     *    "id": "http://ontomap.eu/ontology/Park/2",
     *    "application": "ontomap.eu",
     *    "properties": {
     *      "external_url": "https://dev.co3.ontomap.eu/api/v1/instances/Park/2",
     *      "hasID": "2",
     *      "hasAca": "https://co3.app3.eu/ACA/5",,
     *      "hasType": "Park"
     *    }
     *  }]
     * }
     *
     * @apiParamExample Example: Distance
     * GET https://dev.co3.ontomap.eu/api/v1/instances/Park?distance=7.4,45.1,200
     * {
     *  "type": "FeatureCollection",
     *  "features": [
     *  {
     *    "type": "Feature",
     *    "geometry": null,
     *    "id": "http://ontomap.eu/ontology/Park/2",
     *    "application": "ontomap.eu",
     *    "provenance": "http://ontomap.eu/ontology/Provenance/1",
     *    "properties": {
     *      "external_url": "https://dev.co3.ontomap.eu/api/v1/instances/Park/2",
     *      "hasID": "2",
     *      "hasAca": "https://co3.app3.eu/ACA/5",,
     *      "hasType": "Park"
     *    }
     *  }]
     * }
     *
     * @apiParam (required_parameters) {String} concept <strong>Required.</strong> The concept whose instances are requested.
     *
     * @apiParam (Information type) {Boolean} [count=false] If set to <code>true</code>, returns the number of instances of the selected concept.
     *
     * @apiParam (Information type) {Boolean} [descriptions=false] If set to <code>true</code>, all the properties of the instances are included in the result.
     *
     * @apiParam (Information type) {Boolean} [geometries=false] If set to <code>true</code>, the <a href="http://geojson.org/geojson-spec.html#geometry-objects" target="_blank">geometries</a>
     * of the instances (using EPSG:4326 as CRS) are included in the result.
     *
     * @apiParam (Filters) {Boolean} [subconcepts=false] If set to <code>true</code>, all instances of the specified concept and
     * its subconcepts are returned.
     *
     * @apiParam (Filters) {String} [boundingbox] A string representing the coordinates of the North-East and South-West points of a bounding box (using EPSG:4326 as CRS).<br/>
     * If specified, only the instances within the bounding box are returned.<br/>
     * String format: <code>NE_lng,NE_lat,SW_lng,SW_lat</code> (see example below).
     *
     * @apiParam (Filters) {String} [distance] A string representing the center coordinates (using EPSG:4326 as CRS) and radius (in meters) of a circle.<br/>
     * If specified, only the instances within the circle are returned.<br/>
     * String format: <code>lng,lat,distance</code> (see example below).
     *
     * @apiParam (Filters) {String} [applications="All applications"] A string containing one or more application domain names, separated by commas.<br/>
     * If specified, only the instances managed by the specified applications are returned.<br/>
     *
     * @apiParam (Filters) {String} [validFrom] A <a href="https://en.wikipedia.org/wiki/ISO_8601" target="_blank">ISO 8601 date/time string</a>.<br/>
     * If specified, only the instances created/valid after the specified instant are returned.<br/>
     * Warning: this feature is not yet implemented.
     *
     * @apiParam (Filters) {String} [validTo] A <a href="https://en.wikipedia.org/wiki/ISO_8601" target="_blank">ISO 8601 date/time string</a>.<br/>
     * If specified, only the instances created/valid before the specified instant are returned.<br/>
     * Warning: this feature is not yet implemented.
     *
     *
     * @apiUse pagination
     *
     * @apiUse prettyPrint
     *
     * @apiUse access_token
     *
     * @apiUse missing_parameter
     *
     * @apiUse malformed_parameter
     *
     * @apiUse missing_concept_mapping
     *
     * @apiUse invalid_certificate
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error

     * @apiPermission Client Certificate, access_token, application token.
     */

    @ApplicationTokenAuthenticationAction.ApplicationTokenAuthentication
    public Result getConceptInstances(Http.Request request, String applications, String concept, boolean descriptions, boolean geometries, boolean count,
                                      String boundingBoxString, String distanceString, String lang, String validFrom, String validTo, int limit, int page,boolean prettyPrint, boolean translate, boolean subConcepts) {
        if (Strings.isNullOrEmpty(concept)) return responseController.missingParameterError("concept");

        String instance = request.attrs().get(Attrs.INSTANCE);
        String queryConcept;

        if (translate && request.attrs().containsKey(Attrs.APPLICATION)) {
            String application = request.attrs().get(Attrs.APPLICATION);
            queryConcept = mappingTranslationController.getConceptMapping(instance, application, concept);
            if (Strings.isNullOrEmpty(queryConcept)) return responseController.missingConceptMapping(concept);
        }
        else queryConcept = concept;

        String ontomapDomain = configuration.getString("ontomap.application.domain");

        List<String> sourceApplications = new ArrayList<>();

        if (!Strings.isNullOrEmpty(applications)) sourceApplications = Splitter.on(",").splitToList(applications);
        else {
            //sourceApplications.add(ontomapDomain);
            sourceApplications.add(null);
        }

        ObjectMapper om = new ObjectMapper();

        Set<String> conceptsSet;

        try {
            if (subConcepts) {
                conceptsSet = ontologyController.getSubConceptsSet(queryConcept);
            }
            else {
                conceptsSet = new HashSet<>();
                conceptsSet.add(queryConcept);
            }
        }
        catch (FileNotFoundException e) {
            logger.error("FileNotFoundException in getConceptInstances:", e);
            return responseController.internalServerError2(ResponseController.ONTOLOGY_FILE_NOT_FOUND);
        }

        if (count) {
            try {
                int countInstances = 0;
                boolean cacheFlag = false;
                for (String app : sourceApplications) {
                    countInstances += featureController.getFeaturesCount(instance, app, boundingBoxString, distanceString, conceptsSet);
                }
                if (cacheFlag)
                    return ok(om.createObjectNode().put("count", countInstances)).withHeader(CACHE_HEADER, "true");
                else
                    return ok(om.createObjectNode().put("count", countInstances));

            }
            catch (IllegalArgumentException e) {
                return responseController.malformedParameterError(e.getMessage());
            }
            catch (QueryException e) {
                logger.error("QueryException in getConceptInstances:", e);
                return responseController.badGateway(ResponseController.TRIPLESTORE_CONNECTION_FAILED);
            }
            catch (JsonProcessingException e) {
                logger.error("JSONException in getConceptInstances:", e);
                return responseController.internalServerError2(ResponseController.PARSING_ERROR);
            }
            catch (FileNotFoundException e) {
                logger.error("FileNotFoundException in getConceptInstances:", e);
                return responseController.internalServerError2(ResponseController.ONTOLOGY_FILE_NOT_FOUND);
            }
            catch (IOException e) { // todo: verify
                logger.error("IOException in getConceptInstances:", e);
                return responseController.internalServerError2(ResponseController.PARSING_ERROR);
            }
            catch (MongoTimeoutException e) {
                logger.error("MongoTimeoutException in getConceptInstances:", e);
                return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
            }
        }
        List<Feature> features = new ArrayList<>();
        List<ObjectNode> featuresObj = new ArrayList<>();
        boolean resultsFromCache = false;
        try {
            for (String app : sourceApplications) {
                List<Feature> appResult = featureController.getFeatures(instance, app, boundingBoxString, distanceString, conceptsSet, descriptions, geometries);
                features.addAll(appResult);
            }
            features.sort(new FeatureComparator());
            if (page > -1) { // pagination
                int defaultPageSize = configuration.getInt("ontomap.application.defaultpagesize");
                int pageSize  = (limit > 0 && limit <= 500) ? limit : defaultPageSize;
                List<List<Feature>> pages = Lists.partition(features, pageSize);
                if (page >= pages.size()) features.clear();
                else features = pages.get(page);
            }

            // entity registry
            // todo: check if everything ok
            for (Feature f : features) {
                String indApplication = f.getApplication();
                String indExtUrl = f.getExternalURL();
                Set<Pair<String, String>> entitySet = entityRegistryController.getAssociations(instance, indApplication, indExtUrl);
                ObjectNode objectNode = om.valueToTree(f);
                if (entitySet != null) {
                    ArrayNode entitiesArray =objectNode.putArray("entities");
                    for (Pair<String, String> p : entitySet) {
                        entitiesArray.add(om.createObjectNode().put("application", p.getLeft()).put("external_url", p.getRight()));
                    }
                }
                featuresObj.add(objectNode);
            }

            ObjectNode result = om.createObjectNode();
            result.put("type", "FeatureCollection");
            result.withArray("features").addAll(featuresObj);
            Result res;
            res = (environment.isProd() && !prettyPrint)
                    ? ok(result)
                    : ok(om.writerWithDefaultPrettyPrinter().writeValueAsString(result)).as(JSON_CONTENT_TYPE);
            return (resultsFromCache) ? res.withHeader(CACHE_HEADER, "true") : res;
        }

        catch (IllegalArgumentException e) {
            return responseController.malformedParameterError(e.getMessage());
        }
        catch (QueryException e) {
            logger.error("QueryException in getConceptInstances:", e);
            return responseController.badGateway(ResponseController.TRIPLESTORE_CONNECTION_FAILED);
        }
        catch (IOException e) {
            logger.error(ExceptionUtils.getStackTrace(e));
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        }
        catch (MongoTimeoutException e) {
            logger.error("MongoTimeoutException in getConceptInstances:", e);
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
    }


//    /**
//     * @apiIgnore Not finished Method
//     * @api {get} /instances/<concept>/<id> Get instance
//     * @apiName GetInstanceByID
//     * @apiGroup Instances APIs
//     * @apiDescription Returns a <a href="http://geojson.org/geojson-spec.html#feature-objects" target="_blank">GeoJSON Feature</a>,
//     * representing the requested instance.<br/>
//     * All the properties of the instance, including its
//     * <a href="http://geojson.org/geojson-spec.html#geometry-objects" target="_blank">geometry</a> (using EPSG:4326 as CRS), are returned by default
//     * in the <code>instance_properties</code> field.<br/>
//     * The <code>provenance</code> field is a URL that can be invoked to retrieve the description of the provenance of the instance, in JSON format.<br/>
//     * The property <code>relations</code> is a JSON Array of objects; each object represents a semantic or spatial relation in which the selected instance is involved.
//     *
//     * @apiUser access_token
//     *
//     * @apiParamExample Example: Specify language
//     * GET https://dev.co3.ontomap.eu/api/v1/instances/Park/1?lang=en
//     * {
//     *   "type": "Feature",
//     *   "geometry": {"type": "Point", "coordinates": [45, 7.5]},
//     *   "id": "http://ontomap.eu/ontology/Park/1",
//     *   "properties": {
//     *     "external_url": "https://dev.co3.ontomap.eu/api/v1/instances/Park/1",
//     *     "address": "Address for Park 1",
//     *     "label": "Park 1",
//     *     "relations": [
//     *       {
//     *         "subject": "http://ontomap.eu/ontology/Park/1",
//     *         "object": "http://ontomap.eu/ontology/Store/1",
//     *         "predicate": "contains"
//     *       }
//     *     ]
//     *   }
//     * }
//     * @apiParamExample Example: Exclude geometry
//     * GET https://dev.co3.ontomap.eu/api/v1/instances/Park?geometry=false
//     * {
//     *   "type": "Feature",
//     *   "geometry": null,
//     *   "id": "http://ontomap.eu/ontology/Park/1",
//     *   "properties": {
//     *     "external_url": "https://dev.co3.ontomap.eu/api/v1/instances/Park/1",
//     *     "address": "Address for Park 1",
//     *     "label": {
//     *         "en": "Park 1",
//     *         "it": "Parco 1"
//     *     }
//     *     "relations": [
//     *       {
//     *         "subject": "http://ontomap.eu/ontology/Park/1",
//     *         "object": "http://ontomap.eu/ontology/Store/1",
//     *         "predicate": "contains"
//     *       }
//     *     ]
//     *   }
//     * }
//     *
//     * @apiParam (Required parameters) {String} concept <strong>Required.</strong> The concept of the instance requested.
//     *
//     * @apiParam (Required parameters) {String} id <strong>Required.</strong> The ID of the instance requested.
//     *
//     * @apiParam (Information type) {Boolean} [geometry=true] If set to <code>false</code>, the <a href="http://geojson.org/geojson-spec.html#geometry-objects" target="_blank">geometry</a>
//     * of the instance is excluded from the result.
//     *
//     * @apiParam (Information type) {String} [lang] A <a href="https://en.wikipedia.org/wiki/ISO_639-1" target="_blank">ISO 639-1 language code</a>.<br/>
//     * If specified, the <code>label</code> property of the Feature will be a String containing the label of the corresponding instance in the specified language, if present.<br/>
//     * If not set, the <code>label</code> property of the Feature will be a JSON Object containing the labels of the corresponding instance in all the available languages.
//     *
//     * @apiUse prettyPrint
//     *
//     * @apiUse access_token
//     *
//     * @apiError (Error 404 Not Found) concept_not_found The concept requested does not exist.
//     *
//     * @apiUse invalid_certificate
//     *
//     * @apiUse too_many_requests
//     *
//     * @apiUse internal_server_error
//     *
//     */

//    public Result getInstanceByID(String concept, String id, String lang) {
//
//        String instance = (String) ctx().args.get("instance");
//        boolean prettyPrint = Boolean.parseBoolean(request().getQueryString("prettyprint"));
//        try {
//            ObjectNode feature = tripleStoreController.getInstanceByID(concept, id, lang);
//            if (feature == null) return responseController.missingInstance();
//            ObjectMapper om = new ObjectMapper();
//            if (environment.isProd() && !prettyPrint) return ok(feature);
//            else return ok(om.writerWithDefaultPrettyPrinter().writeValueAsString(feature)).as(JSON_CONTENT_TYPE);
//        }
//        catch (IOException e) {
//            Logger.error("IOException in method getInstanceById:", e);
//            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
//        }
//    }

//    public Result serveOntology() {
////        Logger.debug(environment.resource("conf/WeGovNowOntology.owl").toString());
//        return redirect(environment.resource("conf/WeGovNowOntology.owl").toString());
//    }

//    public Result provenanceRedirect(String id) {
//        return redirect(controllers.api.v1.routes.InstancesController.getProvenanceByID(id));
//    }
//
//    public Result instanceRedirect(String concept, String id) {
//        return redirect(controllers.api.v1.routes.InstancesController.getInstanceByID(concept, id,null));
//    }

//    public Result getProvenanceByID(String id) {
//        boolean prettyPrint = Boolean.parseBoolean(request().getQueryString("prettyprint"));
//        ObjectMapper om = new ObjectMapper();
//        JsonNode resultJson = tripleStoreController.getProvenanceByID(id);
//        if (resultJson == null) return responseController.missingProvenance();
//        try {
//            if (environment.isProd() && !prettyPrint) return ok(resultJson);
//            return ok(om.writerWithDefaultPrettyPrinter().writeValueAsString(resultJson)).as(JSON_CONTENT_TYPE);
//        }
//        catch (JsonProcessingException e) {
//            Logger.error("IOException in method getInstanceById:", e);
//            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
//        }
//    }


    /**
     * @api {get} /logger/hiddenlist Retrieve hidden data objects
     * @apiName RetrieveHiddenList
     * @apiGroup Logger APIs
     * @apiDescription Returns a list of all of the data objects (activity_objects) managed by the application making the request which are currently hidden from the
     * <a href="#api-Instances_APIs-GetConceptInstances">data retrieval API</a> results.<br/>
     *
     * @apiParamExample Retrieve list of hidden data objects
     * GET https://dev.co3.ontomap.eu/api/v1/logger/hiddenlist
     * {
     *   "hidden_list": [
     *     "http://co3.app1.eu/Asili/1",
     *     "http://co3.app1.eu/Asili/2"
     *   ]
     * }
     *
     * @apiUse invalid_certificate
     *
     * @apiUse too_many_requests
     *
     * @apiUse internal_server_error
     * @apiPermission Client Certificate.
     */
    @CertAuthenticatedAction.CertificateAuthentication
    public Result getHiddenList(Http.Request request) {
        if (!configuration.getBoolean("ontomap.logger.enabled")) return forbidden("Logger disabled"); // todo: message
        String instance = request.attrs().get(Attrs.INSTANCE);
        String application = request.attrs().get(Attrs.APPLICATION);

        try {
            ObjectNode result = featureController.getHiddenList(application, instance);
            return ok(result);
        }
        catch (MongoTimeoutException e) {
            logger.error("MongoTimeoutException in getHiddenList:", e);
            return responseController.badGateway(ResponseController.MONGODB_CONNECTION_FAILED);
        }
    }
}
