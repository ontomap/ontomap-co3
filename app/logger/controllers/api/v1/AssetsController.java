package logger.controllers.api.v1;

import com.typesafe.config.Config;
import play.Environment;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.File;

public class AssetsController extends Controller {

    private Config configuration;
    private Environment environment;

    @Inject
    public AssetsController(Config configuration, Environment environment) {
        this.configuration = configuration;
        this.environment = environment;
    }

    public Result getBadgesSchema() {
        File f =  environment.getFile(configuration.getString("gamification.json_schemas.badges"));
        return ok(f);
    }

    public Result getRulesSchema() {
        File f =  environment.getFile(configuration.getString("gamification.json_schemas.rules"));
        return ok(f);
    }

    public Result getOntologyFile(){
        File f =  environment.getFile(configuration.getString("ontomap.ontology.path"));
        return ok(f);
    }

    public Result getLoggingSchemaFile(){
        File f =  environment.getFile(configuration.getString("ontomap.logger.json_schema"));
        return ok(f);
    }

    public Result getMappingSchemaFile(){
        File f =  environment.getFile(configuration.getString("ontomap.logger.mappings.json_schema"));
        return ok(f);
    }
}
