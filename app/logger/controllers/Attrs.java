package logger.controllers;

import logger.models.AuthenticationType;
import play.libs.typedmap.TypedKey;

import java.util.List;

public class Attrs {
    public static final TypedKey<String> INSTANCE = TypedKey.create("instance");
    public static final TypedKey<String> APPLICATION = TypedKey.create("application");
    public static final TypedKey<List<AuthenticationType>> AUTHENTICATIONS = TypedKey.create("authentications");
    public static final TypedKey<Long> USER_AUTHENTICATED = TypedKey.create("user_authenticated");
}
