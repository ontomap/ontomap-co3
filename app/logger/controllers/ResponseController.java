package logger.controllers;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import logger.models.ActivityType;
import play.mvc.Controller;
import play.mvc.Result;
import logger.supports.exceptions.UserNotValidException;

import javax.inject.Inject;

/*
 * This class contains all the error codes and strings returned by the API, as well as the methods which generate the appropriate HTTP responses.
 */
public class ResponseController extends Controller {

    public static final String ERROR = "error";
    public static final String PARAM = "param";
    public static final String MESSAGE = "message";
    public static final String ERROR_ID = "error_id";

    public static final String ACTION_NOT_FOUND = "action_not_found";
    public static final String NO_CN_IN_CERTIFICATE = "no_cn_in_client_certificate";
    public static final String NO_CERT_OR_TOKEN = "no_cert_or_token";
    public static final String MISSING_PARAMETER = "missing_parameter";
    public static final String ERROR_CODE = "error_code";
    public static final String MALFORMED_PARAMETER = "malformed_parameter";
    public static final String MISSING_INSTANCE = "instance_not_found";
    public static final String MISSING_CONCEPT = "concept_not_found";
    public static final String MISSING_PROVENANCE = "provenance_not_found";
    public static final String MALFORMED_JSON = "malformed_json";
    public static final String NOT_VALID_GEOMETRY = "not_valid_geometry";
    public static final String DUPLICATED_ACTIVITY_OBJECT = "duplicated_activity_object";
    public static final String INVALID_REFERENCE = "invalid_reference";
    public static final String MISSING_MAPPING = "mapping_not_found";
    public static final String MISSING_CONCEPT_MAPPING = "concept_mapping_not_found";
    public static final String MISSING_PROPERTY_MAPPING = "property_mapping_not_found";
    public static final String INVALID_CONCEPT_MAPPING = "invalid_ontology_concept_mapping";
    public static final String INVALID_PROPERTY_MAPPING = "invalid_ontology_property_mapping";
    public static final String INTERNAL_SERVER_ERROR = "internal_server_error";
    public static final String BAD_GATEWAY = "bad_gateway";
    public static final String WRONG_CONTENT_TYPE = "wrong_content_type";
    public static final String ALREADY_ASSOCIATED = "istance_already_associated";
    public static final String IDEMPOTENCY_CONFLICT = "idempotency_conflict";
    public static final String INVALID_CERT = "invalid_certificate";
    public static final String TOO_MANY_REQUESTS = "too_many_requests";
    public static final String EVENT_NOT_FOUND = "event_not_found";
    public static final String SUBSCRIPTION_NOT_FOUND = "subscription_not_found";
    public static final String USER_NOT_VALID = "user_not_valid";
    public static final String USER_NOT_FOUND = "user_not_found";
    public static final String NOT_VALID_NOTIFICATION_CHANNEL = "not_valid_notification_channel";
    public static final String ACTIVITY_OBJECT_ALREADY_EXIST = "activity_object_already_exist";
    public static final String NO_TOKEN = "no_access_token";
    public static final String NOT_VALID_ID = "not_valid_id";


    public static final String ACTION_NOT_FOUND_MESSAGE = "Action not found: ";
    public static final String NO_CN_IN_CERTIFICATE_MESSAGE = "No Common Name in the client certificate provided";
    public static final String NO_CERT_OR_TOKEN_MESSAGE = "No valid certificate or access token provided";
    public static final String NO_TOKEN_MESSAGE = "No valid access token provided";
    public static final String MISSING_PARAMETER_MESSAGE = "Missing parameter";
    public static final String MALFORMED_PARAMETER_MESSAGE = "Malformed parameter";
    public static final String MISSING_INSTANCE_MESSAGE = "Instance not found";
    public static final String MISSING_CONCEPT_MESSAGE = "Concept not found";
    public static final String MISSING_PROVENANCE_MESSAGE = "Provenance not found";
    public static final String DUPLICATED_ACTIVITY_OBJECT_MESSAGE = "Duplicated Activity Object in a single event";
    public static final String ACTIVITY_OBJECT_ALREADY_EXIST_MESSAGE = "Activity Object already exist";
    public static final String INVALID_REFERENCE_MESSAGE = "Referenced item not present in the logger: ";
    public static final String INVALID_PROPERTY_MESSAGE = "Property name containing invalid character: ";
    public static final String MISSING_MAPPING_MESSAGE = "Mapping rules not found";
    public static final String MISSING_CONCEPT_MAPPING_MESSAGE = "Concept mapping not found: ";
    public static final String MISSING_PROPERTY_MAPPING_MESSAGE = "Property mapping not found: ";
    public static final String INVALID_CONCEPT_MAPPING_MESSAGE = "Ontology concept mapping not valid: ";
    public static final String INVALID_PROPERTY_MAPPING_MESSAGE = "Ontology property mapping not valid: ";
    public static final String INTERNAL_SERVER_ERROR_MESSAGE = "Internal server error";
    public static final String BAD_GATEWAY_MESSAGE = "Bad gateway";
    public static final String WRONG_CONTENT_TYPE_MESSAGE = "Wrong Content-Type. Expected application/json";
    public static final String ALREADY_ASSOCIATED_MESSAGE = "Instance already associated with one or more other instances";
    public static final String IDEMPOTENCY_CONFLICT_MESSAGE = "A different request with the same Idempotency Key was sent previously";
    public static final String INVALID_CERT_MESSAGE = "The client certificate provided is invalid";
    public static final String MISSING_CERT_MESSAGE = "No client certificate was provided";
    public static final String TOO_MANY_REQUESTS_MESSAGE = "The API has received too many requests in a short time";
    public static final String EVENT_NOT_FOUND_MESSAGE = "No event with provided ID was found";
    public static final String SUBSCRIPTION_NOT_FOUND_MESSAGE = "No subscription with specified details was found";
    public static final String USER_NOT_VALID_MESSAGE = "Events done by another user";
    public static final String USER_NOT_FOUND_MESSAGE = "access_token not provided or not valid";
    public static final String NOT_VALID_NOTIFICATION_CHANNEL_MESSAGE = "Chooses are 'logger' or 'gamification'";
    public static final String NOT_VALID_ID_MESSAGE = "The provided id is not valid.";

    public static final int UNKNOWN_EXCEPTION = 100;
    public static final int ONTOLOGY_FILE_NOT_FOUND = 101;
    public static final int TRIPLESTORE_CONNECTION_FAILED = 102;
    public static final int PARSING_ERROR = 103;
    public static final int MONGODB_CONNECTION_FAILED = 104;
    public static final int MONGODB_EXCEPTION = 106;
    public static final int NO_INSTANCE = 105;

    private Config config;

    @Inject
    public ResponseController(Config config){
        this.config = config;
    }

    public Result successInsertEvents(int num) {
        ObjectMapper om = new ObjectMapper();
        return ok(om.createObjectNode().put("status", "ok").put("events", num));
    }

    public Result genericClientError(int statusCode, String message) {
        ObjectMapper om = new ObjectMapper();
        return status(statusCode, om.createObjectNode().put(MESSAGE, message));
    }

    public Result pathNotFound(String path) {
        ObjectMapper om = new ObjectMapper();
        return notFound(om.createObjectNode().put(ERROR, ACTION_NOT_FOUND).put(MESSAGE, ACTION_NOT_FOUND_MESSAGE + path));
    }

    public Result missingParameterError(String param) {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, MISSING_PARAMETER).put(MESSAGE, MISSING_PARAMETER_MESSAGE).put(PARAM, param));
    }

    public Result malformedParameterError(String param) {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, MALFORMED_PARAMETER).put(MESSAGE, MALFORMED_PARAMETER_MESSAGE).put(PARAM, param));
    }

    public Result genericMalformedParameterError(String message) {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, MALFORMED_PARAMETER).put(MESSAGE, message));
    }

    public Result internalServerError2(int errorCode) {
        ObjectMapper om = new ObjectMapper();
        return internalServerError(om.createObjectNode().put(ERROR, INTERNAL_SERVER_ERROR).put(MESSAGE, INTERNAL_SERVER_ERROR_MESSAGE).put(ERROR_CODE, errorCode));
    }

    public Result unknownInternalServerError(String exceptionId) {
        ObjectMapper om = new ObjectMapper();
        return internalServerError(om.createObjectNode().put(ERROR, INTERNAL_SERVER_ERROR).put(MESSAGE, INTERNAL_SERVER_ERROR_MESSAGE).put(ERROR_CODE, UNKNOWN_EXCEPTION).put(ERROR_ID, exceptionId));
    }

    public Result badGateway(int errorCode) {
        ObjectMapper om = new ObjectMapper();
        return status(502, om.createObjectNode().put(ERROR, BAD_GATEWAY).put(MESSAGE, BAD_GATEWAY_MESSAGE).put(ERROR_CODE, errorCode));
    }

    public Result missingInstance() {
        ObjectMapper om = new ObjectMapper();
        return notFound(om.createObjectNode().put(ERROR, MISSING_INSTANCE).put(MESSAGE, MISSING_INSTANCE_MESSAGE));
    }

    public Result missingConcept() {
        ObjectMapper om = new ObjectMapper();
        return notFound(om.createObjectNode().put(ERROR, MISSING_CONCEPT).put(MESSAGE, MISSING_CONCEPT_MESSAGE));
    }

    public Result missingProvenance() {
        ObjectMapper om = new ObjectMapper();
        return notFound(om.createObjectNode().put(ERROR, MISSING_PROVENANCE).put(MESSAGE, MISSING_PROVENANCE_MESSAGE));
    }

    public Result malformedJsonRequest(String message) {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, MALFORMED_JSON).put(MESSAGE, message));
    }

    public Result malformedJsonRequest(JsonNode message) {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, MALFORMED_JSON).putPOJO(MESSAGE, message));
    }

    public Result malformedFeatureGeometry(JsonNode message) {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, NOT_VALID_GEOMETRY).putPOJO(MESSAGE, message));
    }

    public Result noCNInCertificate() {
        ObjectMapper om = new ObjectMapper();
        return forbidden(om.createObjectNode().put(ERROR, NO_CN_IN_CERTIFICATE).put(MESSAGE, NO_CN_IN_CERTIFICATE_MESSAGE));
    }

    public Result noCertOrToken() {
        ObjectMapper om = new ObjectMapper();
        return forbidden(om.createObjectNode().put(ERROR, NO_CERT_OR_TOKEN).put(MESSAGE, NO_CERT_OR_TOKEN_MESSAGE));
    }

    public Result noToken() {
        ObjectMapper om = new ObjectMapper();
        return forbidden(om.createObjectNode().put(ERROR, NO_TOKEN).put(MESSAGE, NO_TOKEN_MESSAGE));
    }

    public Result duplicatedActivityObject() {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, DUPLICATED_ACTIVITY_OBJECT).put(MESSAGE, DUPLICATED_ACTIVITY_OBJECT_MESSAGE));
    }

    public Result activityObjectAlreadyExist() {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, ACTIVITY_OBJECT_ALREADY_EXIST).put(MESSAGE, ACTIVITY_OBJECT_ALREADY_EXIST_MESSAGE));
    }

    public Result invalidReference(String url) {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, INVALID_REFERENCE).put(MESSAGE, INVALID_REFERENCE_MESSAGE + url));
    }

    public Result missingMapping() {
        ObjectMapper om = new ObjectMapper();
        return notFound(om.createObjectNode().put(ERROR, MISSING_MAPPING).put(MESSAGE, MISSING_MAPPING_MESSAGE));
    }

    public Result invalidProperty(String property) {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, MALFORMED_JSON).put(MESSAGE, INVALID_PROPERTY_MESSAGE + property));
    }

    public Result missingConceptMapping(String concept) {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, MISSING_CONCEPT_MAPPING).put(MESSAGE, MISSING_CONCEPT_MAPPING_MESSAGE + concept));
    }

    public Result missingPropertyMapping(String property) {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, MISSING_PROPERTY_MAPPING).put(MESSAGE, MISSING_PROPERTY_MAPPING_MESSAGE + property));
    }

    public Result invalidConceptMapping(String concept) {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, INVALID_CONCEPT_MAPPING).put(MESSAGE, INVALID_CONCEPT_MAPPING_MESSAGE + concept));
    }

    public Result invalidPropertyMapping(String property) {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, INVALID_PROPERTY_MAPPING).put(MESSAGE, INVALID_PROPERTY_MAPPING_MESSAGE + property));
    }

    public Result wrongContentType() {
        ObjectMapper om = new ObjectMapper();
        return status(415, om.createObjectNode().put(ERROR, WRONG_CONTENT_TYPE).put(MESSAGE, WRONG_CONTENT_TYPE_MESSAGE));
    }

    public Result alreadyAssociatedInstance() {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, ALREADY_ASSOCIATED).put(MESSAGE, ALREADY_ASSOCIATED_MESSAGE));
    }

    public Result idempotencyConflict() {
        ObjectMapper om = new ObjectMapper();
        return status(409, om.createObjectNode().put(ERROR, IDEMPOTENCY_CONFLICT).put(MESSAGE, IDEMPOTENCY_CONFLICT_MESSAGE));
    }

    public Result invalidCertificate() {
        ObjectMapper om = new ObjectMapper();
        return unauthorized(om.createObjectNode().put(ERROR, INVALID_CERT).put(MESSAGE, INVALID_CERT_MESSAGE));
    }

    public Result missingCertificate() {
        ObjectMapper om = new ObjectMapper();
        return unauthorized(om.createObjectNode().put(ERROR, INVALID_CERT).put(MESSAGE, MISSING_CERT_MESSAGE));
    }

    public Result tooManyRequestsResult() {
        ObjectMapper om = new ObjectMapper();
        return status(429, om.createObjectNode().put(ERROR, TOO_MANY_REQUESTS).put(MESSAGE, TOO_MANY_REQUESTS_MESSAGE));
    }

    public Result eventNotFound() {
        ObjectMapper om = new ObjectMapper();
        return notFound(om.createObjectNode().put(ERROR, EVENT_NOT_FOUND).put(MESSAGE, EVENT_NOT_FOUND_MESSAGE));
    }

    public Result subscriptionNotFound() {
        ObjectMapper om = new ObjectMapper();
        return notFound(om.createObjectNode().put(ERROR, SUBSCRIPTION_NOT_FOUND).put(MESSAGE, SUBSCRIPTION_NOT_FOUND_MESSAGE));
    }

    public Result notValidUser(UserNotValidException e) {
        ObjectMapper om = new ObjectMapper();
        return unauthorized(om.createObjectNode().put(ERROR, USER_NOT_VALID).put(MESSAGE, USER_NOT_VALID_MESSAGE));
    }

    public Result userNotFound() {
        ObjectMapper om = new ObjectMapper();
        return unauthorized(om.createObjectNode().put(ERROR, USER_NOT_FOUND).put(MESSAGE, USER_NOT_FOUND_MESSAGE));
    }

    public Result notValidNotificationChannel() {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, NOT_VALID_NOTIFICATION_CHANNEL).put(MESSAGE, NOT_VALID_NOTIFICATION_CHANNEL_MESSAGE));
    }

    public Result acaNotFound() {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, "aca_not_found").put(MESSAGE, "Event that contain activity object must be contain an ACA."));
    }

    public Result notValidId() {
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, NOT_VALID_ID).put(MESSAGE,NOT_VALID_ID_MESSAGE));
    }

    public static Result notFound(String error,String message) {
        ObjectMapper om = new ObjectMapper();
        return notFound(om.createObjectNode().put(ERROR, error).put(MESSAGE,message));
    }

    public Result notSupportedActivityType(String actionType) {
        ObjectMapper om = new ObjectMapper();
        ObjectNode objectNode = om.createObjectNode();
        objectNode.put(ERROR,"not_supported_action_type");
        objectNode.put(MESSAGE,"'"+actionType+"' is not a supported activity_type");
        objectNode.putPOJO("supported_activity_types", ActivityType.values());
        return badRequest(objectNode);
    }

    public static Result badRequest(String error, String message){
        ObjectMapper om = new ObjectMapper();
        return badRequest(om.createObjectNode().put(ERROR, error).put(MESSAGE,message));
    }
}
