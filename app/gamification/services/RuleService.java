package gamification.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.common.base.Strings;
import com.google.common.collect.Sets;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.Updates;
import com.typesafe.config.Config;
import gamification.models.LogRule;
import gamification.models.Rule;
import gamification.supports.deserializers.RuleDeserializer;
import logger.models.ActivityType;
import logger.models.Feature;
import logger.models.InstanceFeature;
import logger.models.OTMEvent;
import org.bson.Document;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;
import org.bson.json.JsonReader;
import org.bson.types.ObjectId;
import play.Logger;
import logger.services.FeatureController;
import logger.services.LoggerMongoController;
import logger.services.MongoDBController;
import logger.services.OntologyController;

import javax.inject.Inject;
import javax.inject.Provider;

import java.io.IOException;
import java.lang.reflect.MalformedParametersException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.*;
import static logger.services.LoggerMongoController.*;

public class RuleService {

    private final MongoCollection<Rule> rulesCollection;
    private Provider<GamificationLoggerService> gamificationLoggerServiceProvider;
    @Inject
    Provider<LoggerMongoController> loggerMongoControllerProvider;
    @Inject
    Provider<FeatureController> featureControllerProvider;
    MongoDBController mongoDBController;
    Logger.ALogger aLogger;
    RuleDeserializer ruleDeserializer;
    Set<String> activityObjectPropertyNameList;

    @Inject
    public RuleService(Config config, MongoDBController mongoDBController, OntologyController ontologyController, Provider<GamificationLoggerService> gamificationLoggerServiceProvider) {
        MongoDatabase mongoDatabase = mongoDBController.getMongoDatabase();
        this.mongoDBController = mongoDBController;
        this.rulesCollection = this.initRulesCollection(config, mongoDatabase);
        this.gamificationLoggerServiceProvider = gamificationLoggerServiceProvider;
        aLogger = new Logger.ALogger(play.api.Logger.apply("RuleController"));
        this.ruleDeserializer = new RuleDeserializer(ontologyController);
        this.activityObjectPropertyNameList = this.getRuleProperties(config.getString("ontomap.instance.url"));
    }

    private MongoCollection<Rule> initRulesCollection(Config config, MongoDatabase mongoDatabase) {
        MongoCollection<Rule> rulesCollection = mongoDatabase.getCollection(config.getString("mongo.collections.gamification.rules"), Rule.class);
        rulesCollection.createIndex(Indexes.hashed("instance"));
        rulesCollection.createIndex(Indexes.hashed("activityType"));
        rulesCollection.createIndex(Indexes.ascending("activityObjectParams.paramName"));
        rulesCollection.createIndex(Indexes.ascending("variable.name"));
        return rulesCollection;
    }

    public Set<String> getRuleProperties(){
        return this.activityObjectPropertyNameList;
    }

    /**
     * This function analyzes a given  OTMEvent and determine the which Gamification Rules can be applied
     * @param clientSession
     * @param event
     * @return
     */
    public List<Rule> getApplicableRules(ClientSession clientSession, OTMEvent event) {
        String activityType = event.getActivityType();
        List<Bson> filters = new ArrayList<>();
        filters.add(eq("instance", event.getInstance()));
        filters.add(
                or(
//                        Filters.elemMatch("acas", Filters.eq(aca)),
                        in("acas", event.getAca()),
                        exists("acas", false)
                ));
        filters.add(eq("active", true));
        Date now = new Date();
        filters.add(or(gt("validFrom", now), exists("validFrom", false)));
        filters.add(or(lte("validTo", now), exists("validTo", false)));

        filters.add(eq("activityType", activityType));

        if (event.getActivityObjects() != null) {
            for (Feature feature : event.getActivityObjects()) {
                Set<Bson> propertiesFilter = new HashSet<>();
                for (Map.Entry<String, Object> prop : feature.getProperties().entrySet()) {
                    if (this.activityObjectPropertyNameList.contains(prop.getKey()))
                        propertiesFilter.add(and(
                                eq("activityObjectProperties.propertyName", prop.getKey()),
                                eq("activityObjectProperties.propertyValue", prop.getValue())));
                }

                List<Bson> subSets = Sets.powerSet(propertiesFilter).stream().filter(perm -> perm.size() > 0).map(perm -> {
                    Set<Bson> p = new HashSet<>(perm);
                    p.add(size("activityObjectProperties", p.size()));
                    return and(p);
                }).collect(Collectors.toList());
                subSets.add(exists("activityObjectProperties", false));
                filters.add(or(subSets));
            }
        } else
            filters.add(exists("activityObjectProperties", false));


        if (event.getReferences() != null) {
            for (OTMEvent.Reference reference : event.getReferences()) {
                Set<Bson> propertiesFilter = new HashSet<>();
                InstanceFeature referenceFeature = featureControllerProvider.get().getFeature(clientSession, event.getInstance(), reference.getApplication(), reference.getExternalUrl());
                if (referenceFeature == null) {
                    // It happens only in dev where a feature may has been  dropped
                    continue;
                }
                for (Map.Entry<String, Object> prop : referenceFeature.getProperties().entrySet()) {
                    propertiesFilter.add(and(
                            eq("referenceObjectProperties.propertyName", prop.getKey()),
                            eq("referenceObjectProperties.propertyValue", prop.getValue())));
                }
                List<Bson> subSets = Sets.powerSet(propertiesFilter).stream().filter(perm -> perm.size() > 0).map(perm -> {
                    Set<Bson> p = new HashSet<>(perm);
                    p.add(size("referenceObjectProperties", p.size()));
                    return and(p);
                }).collect(Collectors.toList());
                subSets.add(exists("referenceObjectProperties", false));
                filters.add(or(subSets));
            }

        } else
            filters.add(exists("referenceObjectProperties", false));

        return StreamSupport
                .stream(rulesCollection.find(clientSession, and(filters)).spliterator(), false)
                .filter(rule -> gamificationLoggerServiceProvider.get()
                        .canRuleBeAssigned(event.getInstance(),event.getActor(),event.getAca(),event.getTimestamp(), rule))
                .collect(Collectors.toList());
    }

    private Set<String> getRuleProperties (String instance) {
        return this.getRules(instance).stream()
                .flatMap(rule ->{
            Set<String> ret = new HashSet<>();
            if (rule.getActivityObjectProperties() != null){
                ret.addAll(rule.getActivityObjectProperties().stream().map(Rule.ActivityObjectProperty::getPropertyName).collect(Collectors.toSet()));
            }
            if (rule.getReferenceObjectProperties() != null){
                ret.addAll(rule.getReferenceObjectProperties().stream().map(Rule.ActivityObjectProperty::getPropertyName).collect(Collectors.toSet()));
            }
            return ret.stream();
            }
        ).collect(Collectors.toSet());
    }


    /**
     * Retrieve a Gamification Rul give its ID
     * @param clientSession
     * @param id
     * @return
     */
    public Rule getRuleById(ClientSession clientSession, ObjectId id) {
        return rulesCollection.find(clientSession, eq("_id", id)).first();
    }

    public Rule getRuleById(ObjectId id) {
        return rulesCollection.find(eq("_id", id)).first();
    }

    /**
     * Retrieve the list of all Rules
     * @param instance
     * @return
     */
    public List<Rule> getRules(String instance) {
        return StreamSupport
                .stream(
                        rulesCollection.find(eq("instance", instance))
                                .spliterator(), false)
                .collect(Collectors.toList());
    }

    /**
     * Retrieve the list of all Gamification Rules, filtered by variable
     * @param instance
     * @param variable
     * @return
     */
    public List<Rule> getRules(String instance, String variable) {
        if (Strings.isNullOrEmpty(variable))
            return getRules(instance);
        return StreamSupport
                .stream(
                        rulesCollection.find(
                                and(
                                        eq("instance", instance),
                                        eq("rewards.variables.name", variable.toLowerCase())
                                ))
                                        .spliterator(), false)
                                .collect(Collectors.toList());
    }

    /**
     * Enable or disable one rule. If disabled this rule cannot be used in Gamification calculations
     * @param id
     * @param actor
     * @param active
     */
    public void enableRule(ObjectId id, long actor, boolean active) {
        try (ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            try {
                rulesCollection.updateOne(eq("_id", id), Updates.set("active", active));
                Rule rule = getRuleById(dbSession, id);
                OTMEvent otmEvent = this.buildRuleEvent(actor, rule, ActivityType.object_updated);
                loggerMongoControllerProvider.get().insertGamificationEvent(dbSession, otmEvent);
                dbSession.commitTransaction();
            } catch (Exception ex) {
                dbSession.abortTransaction();
                throw ex;
            } finally {
                dbSession.close();
            }
        }
    }

    /**
     * Create one rule.
     * @param instance
     * @param actor     Who made the action. It's used to generate an OTMEvent
     * @param ruleJson
     * @return
     * @throws MalformedParametersException
     * @throws IOException
     */
    public Rule createRule(String instance, long actor, JsonNode ruleJson) throws MalformedParametersException, IOException {
        Rule rule;
        try (ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                SimpleModule simpleModule = new SimpleModule();
                simpleModule.addDeserializer(Rule.class, this.ruleDeserializer);
                objectMapper.registerModule(simpleModule);
                try {
                    rule = objectMapper.treeToValue(ruleJson, Rule.class);
                } catch (MalformedParametersException e) {
                    aLogger.error("MalformedParametersException in createRule:", e);
                    throw e;
                }
                rule.setInstance(instance);
                rulesCollection.insertOne(dbSession, rule);
                OTMEvent otmEvent = this.buildRuleEvent(actor, rule, ActivityType.object_created);
                loggerMongoControllerProvider.get().insertGamificationEvent(dbSession, otmEvent);
                dbSession.commitTransaction();
            } catch (Exception ex) {
                dbSession.abortTransaction();
                throw ex;
            } finally {
                this.activityObjectPropertyNameList = this.getRuleProperties(instance);
                dbSession.close();
            }
        }
        return rule;
    }


    private OTMEvent buildRuleEvent(long actor, Rule rule, ActivityType activityType) {
        OTMEvent event = new OTMEvent();
        event.setInstance(rule.getInstance());
        event.setApplication(rule.getInstance());
        event.setActivityType(activityType.name());
        event.setActor(actor);
        event.setTimestamp(new Date().getTime());
        Feature feature = new Feature();
        feature.addStringProperty(EXTERNAL_URL_FIELD, "https://" + rule.getInstance() + "/api/v1/gamification/rules/" + rule.getId().toString());
        feature.setGeometry(null);
        feature.addStringProperty(HAS_TYPE_FIELD, "Rule");
        feature.addBooleanProperty("IsActive", rule.isActive());
        event.setActivityObjects(Collections.singletonList(feature));
        return event;
    }
}
