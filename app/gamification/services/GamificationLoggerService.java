package gamification.services;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.*;
import com.typesafe.config.Config;
import gamification.controllers.ViewMode;
import gamification.models.LogBadge;
import gamification.models.LogRule;
import gamification.models.badges.Badge;
import gamification.models.LogAssignment;
import gamification.models.Rule;
import gamification.models.badges.BadgeContainer;
import gamification.models.profiles.users.MainPersona;
import gamification.supports.serializer.ObjectIdSerializer;
import joptsimple.internal.Strings;
import logger.models.OTMEvent;
import logger.models.PaginatedResponse;
import org.bson.Document;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;
import org.bson.json.JsonReader;
import org.bson.types.ObjectId;
import logger.services.MongoDBController;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.*;
import java.util.stream.Collectors;

@Singleton
public class GamificationLoggerService {

    private final MongoCollection<LogAssignment> logsCollection;
    private final String rulesCollectionName;
    private final String badgesCollectionName;
    private final BadgeService badgeController;

    private final MongoDatabase db;

    @Inject
    public GamificationLoggerService(Config config, MongoDBController mongoDBController) {
        MongoDatabase mongoDatabase = mongoDBController.getMongoDatabase();
        this.db = mongoDatabase;
        logsCollection = this.initLogsCollection(config, mongoDatabase);
        rulesCollectionName = config.getString("mongo.collections.gamification.rules");
        badgesCollectionName = config.getString("mongo.collections.gamification.badges");
        badgeController = new BadgeService(config, mongoDBController);
    }

    private MongoCollection<LogAssignment> initLogsCollection(Config config, MongoDatabase mongoDatabase) {
        MongoCollection<LogAssignment> logsCollection = mongoDatabase.getCollection(config.getString("mongo.collections.gamification.logs"), LogAssignment.class);
        logsCollection.createIndex(Indexes.ascending("instance", "actor", "idRule"));
        logsCollection.createIndex(Indexes.ascending("instance", "actor", "idBadge"));
        logsCollection.createIndex(Indexes.descending("date"));
        return logsCollection;
    }

    /**
     * Save a list of Logs
     * @param logs
     */
    public void saveLogs(List<LogAssignment> logs) {
        if (logs.size() > 0)
            logsCollection.insertMany(logs);
    }

    public void saveLogs(ClientSession dbSession, List<? extends LogAssignment> logs) {
        if (logs.size() > 0) {
            logsCollection.insertMany(dbSession, logs);
        }
    }

    /**
     * Check if a rule has a day/ever count limit and is applicable another time
     * @param event
     * @param rule
     * @return
     */
    public boolean canRuleBeAssigned(String instance, long actor, String aca, long timestamp, Rule rule) {
        boolean ever = true;
        boolean day = true;
        List<Bson> filters = new ArrayList<>(Arrays.asList(
                Filters.eq("instance", instance),
                Filters.eq("actor", actor),
                Filters.eq("rule._id", rule.getId()),
                !Strings.isNullOrEmpty(aca) ? Filters.eq("aca",aca) : Filters.exists("aca", false)
        ));

        if (rule.getMaxEvEver() > 0) {
            Document everDoc = logsCollection.aggregate(
                    Arrays.asList(
                            Aggregates.match(
                                    Filters.and(
                                            filters
                                    )),
                            Aggregates.group("$rule.id", Accumulators.sum("count", 1))
                    ), Document.class
            ).first();
            ever = everDoc == null || everDoc.getInteger("count") < rule.getMaxEvEver();
        }

        if (rule.getMaxEvDay() > 0) {
            Calendar today = Calendar.getInstance();
            today.setTimeInMillis(timestamp);
            today.set(Calendar.HOUR_OF_DAY, 0);
            today.set(Calendar.MINUTE, 0);
            today.set(Calendar.SECOND, 1);
            filters.add(Filters.gte("date", today.getTime()));
            Document dayDoc = logsCollection.aggregate(
                    Arrays.asList(
                            Aggregates.match(Filters.and(filters)),
                            Aggregates.group("$rule.id", Accumulators.sum("count", 1))
                    ), Document.class
            ).first();
            day = dayDoc == null || dayDoc.getInteger("count") < rule.getMaxEvDay();
        }
        return day && ever;
    }

    /**
     * Get a filtered list of Logs
     *
     * @param instance
     * @param viewMode
     * @param actor
     * @param aca
     * @param group
     * @param startTime
     * @param endTime
     * @param variable
     * @param rules
     * @param badges
     * @param resolve
     * @param page
     * @param pageSize
     * @return
     */
    public PaginatedResponse<ObjectNode> getLogs(String instance, ViewMode viewMode, long actor, String aca, String group, Date startTime, Date endTime,String variable, boolean rules, boolean badges, boolean resolve, int page, int pageSize) {
        PaginatedResponse<ObjectNode> ret = new PaginatedResponse<>();
        int count = 0;
        List<ObjectNode> logs = new ArrayList<>();
        if (badges || rules) {
            List<Bson> matchFilter = new ArrayList<>();
            List<Bson> aggregates = new ArrayList<>();
            if (startTime != null) matchFilter.add(Filters.gte("date", startTime));
            if (endTime != null) matchFilter.add(Filters.lte("date", endTime));
            matchFilter.add(Filters.eq("instance", instance));

            if (actor > -1)
                matchFilter.add(Filters.eq("actor", actor));
            switch (viewMode) {
                case SINGLE:
                    matchFilter.add(aca != null ? Filters.eq("aca", aca) : Filters.exists("aca", false));
                    matchFilter.add(group != null ? Filters.eq("group", group) : Filters.exists("group", false));
                    break;
                default:
                    if (aca != null) {
                        matchFilter.add(Filters.eq("aca", aca));
                    }
                    if (group != null) {
                        matchFilter.add(Filters.eq("group", aca));
                    }
                    break;
            }


            if(variable != null){
                matchFilter.add(Filters.or(
                        Filters.eq("rule.rewards.ACTOR.name", variable),
                        Filters.eq("rule.rewards.REFERENCE_OWNER.name", variable)
                ));
            }

            matchFilter.add(Filters.or(
                    Filters.exists("rule", rules),
                    Filters.exists("idBadge", badges)
            ));

            aggregates.add(Aggregates.match(Filters.and(matchFilter)));

            if (resolve) {
                if (badges) {
                    aggregates.add(Aggregates.lookup(badgesCollectionName, "idBadge", "badgeList._id", "badgeContainer"));
                    aggregates.add(Aggregates.unwind("$badgeContainer", new UnwindOptions().preserveNullAndEmptyArrays(true)));
                }
            }
            List<Facet> facets = new ArrayList<>();
            facets.add(new Facet("metadata", Arrays.asList(Aggregates.count())));
            List<Bson> results = new ArrayList<>();
            results.add(Aggregates.sort(Sorts.descending("date")));
            if (page > -1 && pageSize > 0) {
                int skipped = page * pageSize;
                results.add(Aggregates.skip(skipped));
                results.add(Aggregates.limit(pageSize));
            }
            facets.add(new Facet("results", results));
            aggregates.add(Aggregates.facet(facets));
            Document result = logsCollection.aggregate(aggregates, Document.class).first();

            if (!result.isEmpty()) {
                List<Document> metadata = result.getList("metadata", Document.class);
                if (metadata.size() > 0) {
                    count = result.getList("metadata", Document.class).get(0).getInteger("count");
                    logs = resolveDocumentLogs(resolve, result.getList("results", Document.class));
                }
            }
        }
        ret.setTotalCount(count);
        ret.setResults(logs);
        return ret;
    }

    public PaginatedResponse<ObjectNode> getLogs(String instance, ViewMode viewMode, long actor, Date startTime, Date endTime,String variable, boolean rules, boolean badges, boolean resolve, int page, int pageSize) {
        return this.getLogs(instance, viewMode, actor, null, null, startTime, endTime,variable, rules, badges, resolve, page, pageSize);
    }

    public PaginatedResponse<ObjectNode> getLogs(String instance, ViewMode viewMode, long actor, String aca, Date startTime, Date endTime,String variable, boolean rules, boolean badges, boolean resolve, int page, int pageSize) {
        return this.getLogs(instance, viewMode, actor, aca, null, startTime, endTime,variable, rules, badges, resolve, page, pageSize);
    }

    /**
     * Convert a list of documents ina list of Logs
     * @param resolveRules
     * @param documents
     * @return
     */
    private List<ObjectNode> resolveDocumentLogs(boolean resolveRules, List<Document> documents) {
        ObjectMapper om = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(ObjectId.class, new ObjectIdSerializer());
        om.registerModule(simpleModule);
        om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        CodecRegistry registry = CodecRegistries.fromRegistries(logsCollection.getCodecRegistry());
        return documents.stream().map(document -> {
            ObjectNode node = null;
            if (document.containsKey("rule")) {
                JsonReader reader = new JsonReader(document.toJson());
                LogRule logAssignment = registry.get(LogRule.class).decode(reader, DecoderContext.builder().build());
                node = om.valueToTree(logAssignment);
                if (!resolveRules) {
                    ObjectId idRule = logAssignment.getRule().getId();
                    ObjectId idEvent = logAssignment.getEvent().getId();
                    node.remove("rule");
                    node.remove("event");
                    node.put("idRule", idRule.toString());
                    node.put("idEvent", idEvent.toString());
                }
            } else {
                Document badgeContainerDoc = document.get("badgeContainer", Document.class);
                document.remove("ruleList");
                document.remove("badgeContainer");
                JsonReader reader = new JsonReader(document.toJson());
                LogBadge logAssignment = registry.get(LogBadge.class).decode(reader, DecoderContext.builder().build());
                node = om.valueToTree(logAssignment);
                if (badgeContainerDoc != null) {
                    BadgeContainer badgeContainer = badgeController.docToBadgeContainer(badgeContainerDoc);
                    ObjectId idBadge = document.getObjectId("idBadge");
                    Badge badge = badgeController.getBadge(badgeContainer, idBadge);

                    node.remove("idBadge");
                    node.putPOJO("badge", om.valueToTree(badge));
                }
            }
            return node;
        })
                .sorted((o1, o2) -> {
                    // Custom SORT: Default sort by timestamp, but Badges comes after rule application if the timestamp if equal
                    long t1 = o1.get("timestamp").asLong();
                    long t2 = o2.get("timestamp").asLong();
                    if (t1 == t2) {
                        if (o1.has("rule") && o2.has("idBadge")) {
                            return 1;
                        } else if (o1.has("idBadge") && o2.has("rule")) {
                            return -1;
                        }
                    }
                    return Long.compare(t2, t1);
                }).collect(Collectors.toList());
    }
}
