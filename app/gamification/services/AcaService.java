package gamification.services;

import com.mongodb.WriteConcern;
import com.mongodb.client.ClientSession;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.*;
import com.typesafe.config.Config;
import gamification.models.badges.AcaBadgeContainer;
import gamification.models.profiles.AcaProfile;
import gamification.models.Rule;
import gamification.models.profiles.TemporalVariableContainer;
import gamification.models.profiles.users.MainPersona;
import org.bson.conversions.Bson;
import logger.services.MongoDBController;

import javax.inject.Inject;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class AcaService {

    private final MongoCollection<AcaProfile> acaProfileMongoCollection;
    private final String pilotName;

    @Inject
    public AcaService(Config config, MongoDBController mongoDBController) {
        acaProfileMongoCollection = mongoDBController.getMongoDatabase().getCollection(config.getString("mongo.collections.gamification.aca_profiles"), AcaProfile.class);

        List<Bson> indexes = new ArrayList<>();
        indexes.add(Indexes.ascending("instance"));
        indexes.add(Indexes.ascending("url"));
        acaProfileMongoCollection.createIndex(Indexes.compoundIndex(indexes), new IndexOptions().name("unique_fields").unique(true));

        pilotName = config.getString("gamification.allACAProfile");
    }

    /**
     * Retrieve a specific AcaProfile
     *
     * @param clientSession
     * @param instance
     * @param url
     * @param upsert if true and the AcaProfile not exist, the AcaProfile will be created
     * @return
     */
    public AcaProfile getAcaProfile(ClientSession clientSession, String instance, String url, boolean upsert) {
        if (url == null)
            return this.getPilot(clientSession,instance);
        Bson filters = Filters.and(
                Filters.eq("instance", instance),
                Filters.eq("url", url)
        );
        AcaProfile acaProfile;
        if (clientSession != null) {
            acaProfile = acaProfileMongoCollection.find(clientSession, filters).first();
        } else {
            acaProfile = acaProfileMongoCollection.find(filters).first();
        }
        if (acaProfile == null && upsert) {
            acaProfile = createAcaProfile(clientSession, instance, url);
        }
        return acaProfile;
    }

    /**
     * Retrieve the AllAcaProfile
     * @param clientSession
     * @param instance
     * @return
     */
    public AcaProfile getPilot(ClientSession clientSession, String instance) {
        AcaProfile acaProfile = this.getAcaProfile(clientSession,instance,this.pilotName,true);
        acaProfile.setPilotProfile(true);
        return acaProfile;
    }

    /**
     * Retrieve a specific AcaProfile
     *
     * @param instance
     * @param url
     * @param upsert if true and the AcaProfile not exist, the AcaProfile will be created
     * @return
     */
    public AcaProfile getAcaProfile(String instance, String url, boolean upsert) {
        return this.getAcaProfile(null, instance, url, upsert);
    }

    /**
     * Get all AcaProfile in a specifica instance
     *
     * @param instance
     * @return
     */
    public List<AcaProfile> getAcaProfile(String instance) {
        return StreamSupport
                .stream(acaProfileMongoCollection.find(Filters.eq("instance", instance))
                        .spliterator(),false)
                .collect(Collectors.toList());
    }

    /**
     * Create an AcaProfile
     * @param clientSession
     * @param instance
     * @param url
     * @return
     */
    private AcaProfile createAcaProfile(ClientSession clientSession, String instance, String url) {
        if (url == null)
            return null;
        AcaProfile acaProfile = new AcaProfile();
        acaProfile.setInstance(instance);
        acaProfile.setUrl(url);
        save(clientSession, acaProfile);
        return acaProfile;
    }

    /**
     * Add Variables to that AcaProfile
     *
     * @param acaProfile
     * @param variable
     * @param date
     */
    public void applyRuleVariableCooperative(AcaProfile acaProfile, Rule.Variable variable, Date date) {
        if (acaProfile == null)
            return;
        for (AcaBadgeContainer.Timeframe timeframe : AcaBadgeContainer.Timeframe.values()) {
            TemporalVariableContainer temporalVariableContainer = acaProfile.getTemporalVariableContainer(timeframe, date);
            if (temporalVariableContainer == null) {
                temporalVariableContainer = acaProfile.addTemporalVariableContainer(timeframe, date);
            }
            temporalVariableContainer.getVariables().merge(variable.getName(), variable.getAssignment(), Double::sum);
        }
    }

    /**
     * Save an AcaProfile
     *
     * @param clientSession
     * @param acaProfile
     */
    public void save(ClientSession clientSession, AcaProfile acaProfile) {
        if (acaProfile == null) {
            return;
        }
        if (clientSession != null) {
            if (acaProfile.getId() == null) {
                acaProfileMongoCollection.insertOne(clientSession, acaProfile);
            } else {
                acaProfileMongoCollection.replaceOne(clientSession, Filters.eq("_id", acaProfile.getId()), acaProfile);
            }
        } else {
            if (acaProfile.getId() == null) {
                acaProfileMongoCollection.insertOne(acaProfile);
            } else {
                acaProfileMongoCollection.replaceOne(Filters.eq("_id", acaProfile.getId()), acaProfile);
            }
        }
    }

    /**
     * Save a list of AcaProfile
     *
     * @param clientSession
     * @param acaProfiles
     */
    public void saveMany(ClientSession clientSession, Set<AcaProfile> acaProfiles) {
        List<WriteModel<AcaProfile>> writeList = acaProfiles.stream().map(acaProfile ->
                new ReplaceOneModel<>(Filters.eq("_id", acaProfile.getId()), acaProfile, new ReplaceOptions().upsert(true))
        ).collect(Collectors.toList());
        acaProfileMongoCollection.bulkWrite(clientSession, writeList,new BulkWriteOptions());
    }

    /**
     * Return all AcaProfiles of the instance
     *
     * @param clientSession
     * @return
     */
    public FindIterable<AcaProfile> getAll(ClientSession clientSession) {
        return acaProfileMongoCollection.find(clientSession);
    }
}
