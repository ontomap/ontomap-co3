package gamification.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.client.ClientSession;
import com.mongodb.client.FindIterable;
import com.typesafe.config.Config;
import gamification.controllers.NotificationController;
import gamification.models.LogAssignment;
import gamification.models.LogBadge;
import gamification.models.LogRule;
import gamification.models.Ranking;
import gamification.models.badges.*;
import gamification.models.profiles.AcaProfile;
import gamification.models.profiles.users.AcaPersona;
import gamification.models.profiles.users.MainPersona;
import gamification.models.profiles.users.UserProfile;
import gamification.models.Rule;
import logger.models.InstanceFeature;
import logger.models.OTMEvent;
import org.bson.types.ObjectId;
import logger.supports.TimeUtils;
import logger.services.FeatureController;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class GamificationService {

    UserProfileService userProfileService;
    GamificationLoggerService loggerAssignmentController;
    AcaService acaService;
    RuleService ruleService;
    Provider<BadgeService> badgeServiceProvider;
    FeatureController featureController;
    NotificationController notificationController;

    private final List<String> mainVariables;
    private final boolean active;

    @Inject
    public GamificationService(Config config,UserProfileService userProfileService,  GamificationLoggerService loggerAssignmentController,
                               AcaService acaService, RuleService ruleService,
                               Provider<BadgeService> badgeServiceProvider, FeatureController featureController, NotificationController notificationController) {
        this.userProfileService = userProfileService;
        this.loggerAssignmentController = loggerAssignmentController;
        this.acaService = acaService;
        this.ruleService = ruleService;
        this.featureController = featureController;
        this.badgeServiceProvider = badgeServiceProvider;
        this.notificationController = notificationController;
        this.active = config.getBoolean("gamification.active");
        this.mainVariables = config.getStringList("gamification.main_variables");
    }

    public boolean isActive() {
        return active;
    }

    /**
     * It's the main Gamification method. Given a list of events, it elaborate and apply rules and badges to users
     * @param clientSession
     * @param events
     */
    public void checkEvents(ClientSession clientSession, List<OTMEvent> events) {

        GameResult gameResult = new GameResult();
        for (OTMEvent event : events) {
            String aca = event.getAca();
            String instance = event.getInstance();
            long timestamp = event.getTimestamp();
            long actor = event.getActor();

            List<Rule> rules = ruleService.getApplicableRules(clientSession, event);

            for (Rule rule : rules) {
                List<RuleTarget> ruleTargets = new ArrayList<>();
                for (Rule.Reward reward : rule.getRewards()) {
                    Rule.Role role = reward.getRole();
                    switch (role) {
                        case REFERENCE_OWNER:
                            if (event.getReferences() != null) {
                                for (OTMEvent.Reference reference : event.getReferences()) {
                                    InstanceFeature ref = featureController.getFeature(clientSession, instance, reference.getApplication(), reference.getExternalUrl());
                                    if (ref != null) {
                                        long actorRef = ref.getActor();
                                        String acaRef = ref.getAca();
                                        String groupRef = ref.getGroup();
                                        ruleTargets.add(new RuleTarget(actorRef, acaRef, groupRef, role));
                                    }
                                }
                            }
                            break;
                        case ACTOR:
                            String group = event.getGroup();
                            ruleTargets.add(new RuleTarget(actor, aca, group, role));
                            break;
                    }
                }

                for (RuleTarget target : ruleTargets) {
                    MainPersona mainPersona = userProfileService.getUserMainProfile(clientSession, instance, target.getActor());
                    AcaProfile acaProfile = null;
                    List<AcaProfile> cooperativeProfiles = new ArrayList<>();
                    AcaProfile pilotProfile = acaService.getPilot(clientSession,instance);
                    cooperativeProfiles.add(pilotProfile);
                    if(target.getAca() != null){
                        acaProfile = acaService.getAcaProfile(clientSession, mainPersona.getInstance(), aca,true);
                        cooperativeProfiles.add(acaProfile);
                    }


                    RuleResult ruleResult = applyRule(mainPersona,aca,cooperativeProfiles, target.getGroup(),rule,target.getRole(), event);
                    gameResult.getLogs().add(ruleResult.getLogAssignment());

                    List<LogBadge> acaBadges = checkExistingCooperativeBadges( mainPersona, acaProfile, timestamp);
                    acaBadges.addAll(checkExistingCooperativeBadges(mainPersona, pilotProfile, timestamp));

                    List<LogBadge> bl = checkBadges(clientSession, mainPersona, cooperativeProfiles, ruleResult, timestamp);
                    userProfileService.save(clientSession, mainPersona);
                    gameResult.getLogs().addAll(acaBadges);
                    gameResult.getLogs().addAll(bl);
                    gameResult.getNotifications().addAll(acaBadges);
                    gameResult.getNotifications().addAll(bl);
                    acaService.save(clientSession, acaProfile);
                    acaService.save(clientSession, pilotProfile);
                }
            }
        }
        saveLogs(clientSession,gameResult.getLogs());
        sendLogs(clientSession,gameResult.getLogs());
    }

    /**
     * This function is used in 'checkEvents' to apply a rule to a specific persona increasing its variables
     * @param mainPersona
     * @param acaUrl
     * @param cooperativeProfiles
     * @param group
     * @param rule
     * @param role
     * @param event
     * @return
     */
    private RuleResult applyRule(MainPersona mainPersona, String acaUrl ,List<AcaProfile> cooperativeProfiles, String group, Rule rule, Rule.Role role, OTMEvent event) {
        RuleResult ruleResult = new RuleResult();
        long timestamp = event.getTimestamp();
        Date eventDate = new Date(timestamp);
        for (Rule.Variable variable : rule.getRewards(role)) {
            userProfileService.getPersona(mainPersona, acaUrl, group).applyRuleRankingVariable(variable, eventDate);
            ruleResult.getModifiedVariables().add(variable.getName());
            userProfileService.getPersona(mainPersona, acaUrl, group).applyVariable(variable);

            for(AcaProfile acaProfile: cooperativeProfiles){
                acaService.applyRuleVariableCooperative(acaProfile, variable,eventDate);
            }
            ruleResult.getModifiedVariablesAca().add(variable.getName());


//            userProfileService.applyRuleRankingVariable(mainPersona, acaUrl, null, variable,eventDate);
            if (mainVariables.contains(variable.getName())) {
                userProfileService.getPersona(mainPersona, acaUrl, group).applyRuleMaxVariable(variable);
            }
        }
        ruleResult.setLogAssignment(new LogRule(event, rule, role, mainPersona.getInstance(), mainPersona.getActor(), acaUrl, group));
        return ruleResult;
    }

    /**
     * When a user make a action in a new ACA, he will receive all active badges gained by that aca.
     * @param mainPersona
     * @param acaProfile
     * @param timestamp
     * @return
     */
    private List<LogBadge> checkExistingCooperativeBadges(MainPersona mainPersona, AcaProfile acaProfile, long timestamp) {
        List<LogBadge> notifications = new ArrayList<>();
        if (acaProfile != null) {
            AcaPersona acaPersona = (AcaPersona) mainPersona.getPersona(acaProfile.getUrl());
            if (acaPersona == null) {
                //If not exist I'll create it in order to add all existent aca badges
                acaPersona = (AcaPersona) userProfileService.getPersona(mainPersona, acaProfile.getUrl(), null);
                for (UserProfile.AssignedBadge assignedBadge : acaProfile.getBadges().getCurrentBadges()) {
                    acaPersona.getBadges().getCurrentBadges().add(assignedBadge);
                    LogBadge logAssignment = new LogBadge(timestamp, assignedBadge.getIdBadge(), mainPersona, acaProfile.getUrl());
                    notifications.add(logAssignment);
                }
            }
        }
        return notifications;
    }

    /**
     * This function is called when new badge is created/edited. It check if that badge it's already assignable to some users
     * @param clientSession
     * @param instance
     * @param timestamp
     * @return
     */
    public List<LogBadge> checkNewBadges(ClientSession clientSession,String instance, long timestamp) {
        List<LogBadge> logBadges = new ArrayList<>();
        for(MainPersona mainPersona : userProfileService.getAllUserMainProfile(clientSession,instance)){
            logBadges.addAll(checkBadges(clientSession,mainPersona,new ArrayList<>(),new RuleResult(),timestamp));
        }
        return logBadges;
    }

    /**
     * Check if a Persona has one o more badges that can be assignable to him self
     * @param clientSession
     * @param mainPersona
     * @param cooperativeProfiles
     * @param ruleResult
     * @param timestamp
     * @return
     */
    private List<LogBadge> checkBadges(ClientSession clientSession, MainPersona mainPersona, List<AcaProfile> cooperativeProfiles, RuleResult ruleResult, long timestamp) {
        boolean badgeAdded = true;
        List<LogBadge> logBadges = new ArrayList<>();
        Date eventTime = new Date(timestamp);
        while (badgeAdded) {
            badgeAdded = false;
            List<Badge> badges = badgeServiceProvider.get().getApplicableIndividualBadges(clientSession, mainPersona, ruleResult.getModifiedVariables());
            for (Badge badge : badges) {
                badgeAdded = true;
                LogBadge logAssignment = new LogBadge(timestamp, badge.getId(), mainPersona, null);
                UserProfile.AssignedBadge assignedBadge = new UserProfile.AssignedBadge(badge);
                assignedBadge.setAssignedDate(eventTime);
                mainPersona.getBadges().getCurrentBadges().add(assignedBadge);
                logBadges.add(logAssignment);
            }
            for( AcaProfile cooperativeProfile: cooperativeProfiles){
                String profileUrl = cooperativeProfile.getUrl();
                for (TemporalBadgeContainer.Timeframe timeframe : TemporalBadgeContainer.Timeframe.values()) {
                    List<Badge> profileApplicableBadges = badgeServiceProvider.get().getApplicableAcaBadges(clientSession, cooperativeProfile, timeframe, ruleResult.getModifiedVariablesAca());

                    for (Badge badge : profileApplicableBadges) {
                        UserProfile.AssignedBadge assignedBadge = new UserProfile.AssignedBadge(badge);
                        assignedBadge.setAssignedDate(eventTime);
                        assignedBadge.setTimeframe(timeframe.name());
                        Calendar expireDate = Calendar.getInstance();
                        if (timeframe.equals(TemporalBadgeContainer.Timeframe.WEEK)) {
                            expireDate.setTime(TimeUtils.nextFirstOfTheWeek(eventTime));
                            expireDate.add(Calendar.DATE, 7);
                            assignedBadge.setExpiredDate(expireDate.getTime());
                        } else {
                            expireDate.setTime(TimeUtils.nextFirstOfTheMonth(eventTime));
                            expireDate.add(Calendar.MONTH, 1);
                            assignedBadge.setExpiredDate(expireDate.getTime());
                        }
                        //If an application push an old event, maybe the assignedBadge is already expired
                        if (assignedBadge.isExpired())
                            cooperativeProfile.getBadges().getRemovedBadges().add(assignedBadge);
                        else
                            cooperativeProfile.getBadges().getCurrentBadges().add(assignedBadge);

                        for (MainPersona m : getUserMainProfileByAca(clientSession, cooperativeProfile)) {
                            LogBadge logAssignment;
                            if (cooperativeProfile.isPilotProfile()){
                                logAssignment = new LogBadge(timestamp, badge.getId(), m, null);
                                m.getBadges().getCurrentBadges().add(assignedBadge);
                            } else {
                                logAssignment = new LogBadge(timestamp, badge.getId(), m, profileUrl);
                                m.getPersona(profileUrl).getBadges().getCurrentBadges().add(assignedBadge);
                            }

                            logBadges.add(logAssignment);
                        }
                    }
                }

            }
        }
        return logBadges;
    }

    /**
     * Retrieve the list of all User that made at list one action in the specified ACA
     * @param clientSession
     * @param acaProfile
     * @return
     */
    private List<MainPersona> getUserMainProfileByAca(ClientSession clientSession, AcaProfile acaProfile) {
        FindIterable<MainPersona> mainPersonaFindIterable;
        if (acaProfile.isPilotProfile()){
            mainPersonaFindIterable = userProfileService.getAllUserMainProfile(clientSession, acaProfile.getInstance());
        } else {
            mainPersonaFindIterable = userProfileService.getUserMainProfileByAca(clientSession, acaProfile);
        }
        return StreamSupport.stream(mainPersonaFindIterable.spliterator(), false).collect(Collectors.toList());
    }

    /**
     * This function is called by Weekly/Monthly task and reset all aca badges for that period (next week or next month)
     * @param clientSession
     */
    public void resetAcaBadges(ClientSession clientSession) {
        Set<AcaProfile> acaProfiles = StreamSupport.stream(acaService.getAll(clientSession).spliterator(), false).collect(Collectors.toSet());

        for (AcaProfile acaProfile : acaProfiles) {
            Set<UserProfile.AssignedBadge> toBeRemoved = new HashSet<>();
            Set<UserProfile.AssignedBadge> newCurrent = new HashSet<>();
            for (UserProfile.AssignedBadge assignedBadge : acaProfile.getBadges().getCurrentBadges()) {
                if (assignedBadge.isExpired()) {
                    toBeRemoved.add(assignedBadge);
                } else {
                    newCurrent.add(assignedBadge);
                }
            }
            acaProfile.getBadges().setCurrentBadges(newCurrent);
            acaProfile.getBadges().getRemovedBadges().addAll(toBeRemoved);
        }

        acaService.saveMany(clientSession, acaProfiles);
    }

    /**
     * This function is used to calculate the final Weekly/Monthly ranking and assign badges to respective users
     *
     * @param dbSession
     * @param timeframe
     * @param mainPersonaCache
     * @param date
     * @return
     */
    public List<JsonNode> calculateRankingBadges(ClientSession dbSession, TemporalBadgeContainer.Timeframe timeframe, Map<ObjectId, MainPersona> mainPersonaCache, Date date) {
        List<JsonNode> notifications = new ArrayList<>();
        for (RankingBadgeContainer rankingBadgeContainer : badgeServiceProvider.get().getRankingBadges(dbSession)) {
            Ranking ranking = userProfileService.getRanking(rankingBadgeContainer.getInstance(), mainPersonaCache.values(), rankingBadgeContainer.getAca(), timeframe, date, rankingBadgeContainer.getVariable());
            for (BadgePosition badgePosition : rankingBadgeContainer.getBadgeList()) {
                int position = badgePosition.getPosition();
                List<Ranking.Rank> ranks = ranking.getRankByPosition(position);
                for (Ranking.Rank rank : ranks) {
                    if (rank.getValue() == 0)
                        continue;
                    MainPersona mainPersona = userProfileService.getUserMainProfile(dbSession, rankingBadgeContainer.getInstance(), rank.getActor());
                    UserProfile userProfile = mainPersona.getPersona(rankingBadgeContainer.getAca());
                    UserProfile.AssignedBadge assignedBadge = new UserProfile.AssignedBadge(badgePosition, ranking.getEndingDate(), ranking.getTimeframe());
                    userProfile.getBadges().getCurrentBadges().add(assignedBadge);
                    LogBadge logAssignment = new LogBadge(new Date().getTime(), badgePosition.getId(), mainPersona, rankingBadgeContainer.getAca());
                    notifications.add(buildNotification(dbSession, logAssignment));
                }
            }
        }
        return notifications;
    }

    /**
     * Save in Database a list of Logs
     * @param clientSession
     * @param logs
     */
    public void saveLogs(ClientSession clientSession,List<? extends LogAssignment> logs){
        loggerAssignmentController.saveLogs(clientSession,logs);
    }

    /**
     * Sent logs as notification (throw WebSocket) to users
     * @param clientSession
     * @param logs
     */
    public void sendLogs(ClientSession clientSession,List<? extends LogAssignment> logs){
        for (LogAssignment assignment : logs) {
            JsonNode n = buildNotification(clientSession, assignment);
            notificationController.sendNotification(assignment.getActor(), NotificationController.Channel.GAMIFICATION, n);
        }
    }

    /**
     * Build a notification starting from a LogAssignment
     * @param clientSession
     * @param logAssignment
     * @return
     */
    private JsonNode buildNotification(ClientSession clientSession, LogAssignment logAssignment) {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode logNode = objectMapper.valueToTree(logAssignment);
        if (logAssignment instanceof LogBadge) {
            Badge badge = badgeServiceProvider.get().getBadge(clientSession, ((LogBadge) logAssignment).getIdBadge());
            logNode.putPOJO("badge", badge);
            logNode.remove("idBadge");
        }
        return logNode;
    }

    private static class RuleTarget {
        long actor;
        String aca;
        String group;
        Rule.Role role;

        public RuleTarget(long actor, String aca, String group, Rule.Role role) {
            this.actor = actor;
            this.aca = aca;
            this.group = group;
            this.role = role;
        }

        public long getActor() {
            return actor;
        }

        public void setActor(long actor) {
            this.actor = actor;
        }

        public String getAca() {
            return aca;
        }

        public void setAca(String aca) {
            this.aca = aca;
        }

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public Rule.Role getRole() {
            return role;
        }

        public void setRole(Rule.Role role) {
            this.role = role;
        }
    }

    private static class RuleResult {
        Set<String> modifiedVariables;
        Set<String> modifiedVariablesAca;
        LogRule logAssignment;
//        Map<String, Document> featureCache;

        public LogRule getLogAssignment() {
            return logAssignment;
        }

        public void setLogAssignment(LogRule logAssignment) {
            this.logAssignment = logAssignment;
        }

        public RuleResult() {
            // this.featureCache = featureCache;
            this.modifiedVariables = new HashSet<>();
            this.modifiedVariablesAca = new HashSet<>();

        }

        public Set<String> getModifiedVariables() {
            return modifiedVariables;
        }

        public void setModifiedVariables(Set<String> modifiedVariables) {
            this.modifiedVariables = modifiedVariables;
        }

        public Set<String> getModifiedVariablesAca() {
            return modifiedVariablesAca;
        }

    }

    private static class GameResult {
        private List<LogAssignment> logs;
        private List<LogBadge> notifications;

        public GameResult(List<LogAssignment> logs, List<LogBadge> notifications) {
            this.logs = logs;
            this.notifications = notifications;
        }

        public GameResult() {
            this.logs = new ArrayList<>();
            this.notifications = new ArrayList<>();
        }

        public List<LogAssignment> getLogs() {
            return logs;
        }

        public void setLogs(List<LogAssignment> logs) {
            this.logs = logs;
        }

        public List<LogBadge> getNotifications() {
            return notifications;
        }

        public void setNotifications(List<LogBadge> notifications) {
            this.notifications = notifications;
        }
    }
}
