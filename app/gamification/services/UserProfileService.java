package gamification.services;

import com.mongodb.client.ClientSession;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.*;
import com.typesafe.config.Config;
import gamification.models.badges.Badge;
import gamification.models.badges.BadgeStep;
import gamification.models.badges.IndividualBadgeContainer;
import gamification.models.profiles.AcaProfile;
import gamification.models.Ranking;
import gamification.models.Rule;
import gamification.models.badges.TemporalBadgeContainer;
import gamification.models.profiles.TemporalVariableContainer;
import gamification.models.profiles.users.GroupPersona;
import gamification.models.profiles.users.MainPersona;
import gamification.models.profiles.users.AcaPersona;
import gamification.models.profiles.users.UserProfile;
import logger.services.LoggerMongoController;
import org.bson.*;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import logger.supports.TimeUtils;
import logger.services.MongoDBController;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.*;

public class UserProfileService {
    private MongoCollection<MainPersona> userProfileMongoCollection;

    @Inject
    public UserProfileService(Config config, MongoDBController mongoDBController) {
        MongoDatabase mongoDatabase = mongoDBController.getMongoDatabase();
        userProfileMongoCollection = mongoDatabase.getCollection(config.getString("mongo.collections.gamification.users_profiles"), MainPersona.class);
        userProfileMongoCollection.createIndex(Indexes.ascending("variables.**"));
        userProfileMongoCollection.createIndex(Indexes.ascending("acaPersonas.variables.**"));
        userProfileMongoCollection.createIndex(Indexes.ascending("acaPersonas.groupPersonas.variables.**"));
    }

    /**
     * Retrieve
     * @param clientSession
     * @param instance
     * @param actor
     * @return
     */
    public MainPersona getUserMainProfile(ClientSession clientSession, String instance, long actor) {
        List<Bson> filters = new ArrayList<>();
        filters.add(Filters.eq("instance", instance));
        filters.add(Filters.eq("actor", actor));
        MainPersona mainPersona = clientSession == null ? userProfileMongoCollection.find(and(filters)).first() : userProfileMongoCollection.find(clientSession, and(filters)).first();
        if (mainPersona == null) {
            mainPersona = new MainPersona();
            mainPersona.setInstance(instance);
            mainPersona.setActor(actor);
            if (clientSession != null)
                userProfileMongoCollection.insertOne(clientSession, mainPersona);
            else
                userProfileMongoCollection.insertOne(mainPersona);
        }
        return mainPersona;
    }

    public MainPersona getUserMainProfile(String instance, long actor) {
        return this.getUserMainProfile(null,instance,actor);
    }

    /**
     * Give a mainPersona (root of persona tree) retrieve a persona (node of the tree)
     * @param mainPersona
     * @param aca
     * @param group
     * @return
     */
    public UserProfile getPersona(MainPersona mainPersona, String aca, String group) {
        if (aca != null) {
            AcaPersona acaPersona = (AcaPersona) mainPersona.getPersona(aca);
            if (acaPersona == null) {
                acaPersona = new AcaPersona();
                acaPersona.setAca(aca);
                mainPersona.getAcaPersonas().add(acaPersona);
            }
            if (group != null) {
                GroupPersona groupPersona = (GroupPersona) acaPersona.getPersona(aca, group);
                if (groupPersona == null) {
                    groupPersona = new GroupPersona();
                    groupPersona.setGroup(group);
                    acaPersona.getGroupPersonas().add(groupPersona);
                }
                return groupPersona;
            } else {
                return acaPersona;
            }
        } else {
            return mainPersona;
        }
    }

    public void save(ClientSession clientSession, MainPersona mainPersona) {
        if (mainPersona.getId() == null) {
            userProfileMongoCollection.insertOne(clientSession, mainPersona);
        } else {
            userProfileMongoCollection.replaceOne(clientSession, Filters.eq("_id", mainPersona.getId()), mainPersona);
        }
    }

    public void saveMany(ClientSession clientSession, Collection<MainPersona> mainPersonas) {
        List<WriteModel<MainPersona>> writeList = mainPersonas.stream().map(mainPersona ->
                new ReplaceOneModel<>(Filters.eq("_id", mainPersona.getId()), mainPersona, new ReplaceOptions().upsert(true))
        ).collect(Collectors.toList());
        userProfileMongoCollection.bulkWrite(clientSession, writeList);
        // mainPersonas.forEach(mainPersona -> save(clientSession, mainPersona));
    }

    /**
     * Get all Profile that made at least one action in that ACA
     * @param session
     * @param acaProfile
     * @return
     */
    public FindIterable<MainPersona> getUserMainProfileByAca(ClientSession session, AcaProfile acaProfile) {
        return userProfileMongoCollection.find(session,
                and(
                        eq("instance", acaProfile.getInstance()),
                        eq("acaPersonas.aca", acaProfile.getUrl())
                )
        );
    }

    /**
     * Get the list of all Users
     * @param session
     * @param instance
     * @return
     */
    public FindIterable<MainPersona> getAllUserMainProfile(ClientSession session, String instance) {
        return userProfileMongoCollection.find(session,
                and(
                        eq("instance", instance)
                )
        );
    }

    /**
     * Calculate  the ranking of a variable
     * @param clientSession
     * @param instance
     * @param aca
     * @param timeframe
     * @param variable
     * @return
     */
    public Ranking getRanking(ClientSession clientSession, String instance, String aca, TemporalBadgeContainer.Timeframe timeframe, String variable) {
        Ranking ranking = new Ranking(instance, aca, variable, timeframe, new Date());
        Document filterDocument = new Document();
        BsonArray array = new BsonArray();
        if (aca != null) {
            array.add(new BsonString("acaPersonas.$fullTemporalVariables." + timeframe.name() + "." + variable));
        } else {
            array.add(new BsonString("$fullTemporalVariables." + timeframe.name() + "." + variable));
        }
        array.add(new BsonDouble(0));
        filterDocument.put("$ifNull", array);

        Bson matchFilter;
        if (aca != null) {
            matchFilter = and(
                    eq("instance", instance),
                    eq("acaPersonas.aca", aca)
            );
        } else {
            matchFilter = eq("instance", instance);
        }

        for (Document document : userProfileMongoCollection.withDocumentClass(Document.class).aggregate(clientSession, Arrays.asList(
                Aggregates.match(
                        matchFilter
                ),
                Aggregates.project(Projections.fields(
                        Projections.computed("actor", 1),
                        Projections.computed(variable, filterDocument)
                )),
                Aggregates.sort(Sorts.descending(variable))
        ))) {
            ranking.addRank(document.getLong("actor"), document.getDouble(variable));
        }
        ranking.calculatePositions();
        ranking.setTotalCount(ranking.getRankList().size());
        return ranking;
    }


    public Ranking getRanking(String instance, Collection<MainPersona> mainPersonas, String aca, TemporalBadgeContainer.Timeframe timeframe, Date rankingDate, String variable) {
        Ranking ranking = new Ranking(instance, aca, variable, timeframe, rankingDate);
        for (MainPersona mainPersona : mainPersonas) {
            if (mainPersona.getInstance().equals(instance)) {
                UserProfile userProfile = aca == null ? mainPersona : mainPersona.getPersona(aca);
                if (userProfile != null) {
                    ranking.addRank(mainPersona.getActor(), userProfile.getFullTemporalVariables(timeframe, rankingDate).getOrDefault(variable, 0.0));
                } else {
                    ranking.addRank(mainPersona.getActor(), 0.0);
                }
            }
        }
        ranking.calculatePositions();
        ranking.setTotalCount(ranking.getRankList().size());
        return ranking;
    }

    public Map<ObjectId, MainPersona> removeAllExpiredBadges(ClientSession dbSession) {
        Map<ObjectId, MainPersona> mainPersonaCache = new HashMap<>();
        for (MainPersona mainPersona : userProfileMongoCollection.find(dbSession)) {
            removeAllExpiredBadges(mainPersona);
            for (AcaPersona acaPersona : mainPersona.getAcaPersonas()) {
                removeAllExpiredBadges(acaPersona);
            }
            mainPersonaCache.put(mainPersona.getId(), mainPersona);
        }
        return mainPersonaCache;
    }

    public void removeAllExpiredBadges(UserProfile userProfile) {
        Set<UserProfile.AssignedBadge> assignedBadges = userProfile.getBadges().getCurrentBadges();
        Set<UserProfile.AssignedBadge> removedBadges = new HashSet<>();
        for (UserProfile.AssignedBadge assignedBadge : assignedBadges) {
            if (assignedBadge.isExpired()) {
                removedBadges.add(assignedBadge);
            }
        }
        assignedBadges.removeAll(removedBadges);
        userProfile.getBadges().getRemovedBadges().addAll(removedBadges);
    }

    public void resetRankingVariables(Map<ObjectId, MainPersona> mainPersonaCache) {
        for (MainPersona mainPersona : mainPersonaCache.values()) {
            mainPersona.removeOldTemporalContainers();
            for (AcaPersona acaPersona : mainPersona.getAcaPersonas()) {
                acaPersona.removeOldTemporalContainers();
            }
            mainPersonaCache.put(mainPersona.getId(), mainPersona);
        }
    }
}
