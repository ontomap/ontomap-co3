package gamification.services;

import akka.japi.Pair;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Indexes;
import com.mongodb.client.model.Projections;
import com.typesafe.config.Config;
import gamification.models.Ranking;
import gamification.models.badges.TemporalBadgeContainer;
import org.bson.Document;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.conversions.Bson;
import org.bson.json.JsonReader;
import org.bson.types.ObjectId;
import logger.services.MongoDBController;
import play.Logger;

import javax.inject.Inject;
import java.util.*;

import static com.mongodb.client.model.Filters.*;

public class RankingService {
    private int cacheTime;
    MongoDBController mongoDBController;
    UserProfileService userProfileController;
    private MongoCollection<Ranking> rankingCacheCollection;
    private final Logger.ALogger logger;

    @Inject
    public RankingService(Config config, MongoDBController mongoDBController, UserProfileService userProfileController) {
        this.mongoDBController = mongoDBController;
        this.userProfileController = userProfileController;
        MongoDatabase mongoDatabase = mongoDBController.getMongoDatabase();
        rankingCacheCollection = mongoDatabase.getCollection(config.getString("mongo.collections.gamification.ranking_cache"), Ranking.class);
        rankingCacheCollection.createIndex(Indexes.ascending("instance"));
        rankingCacheCollection.createIndex(Indexes.ascending("variable"));
        logger = new Logger.ALogger(play.api.Logger.apply(getClass()));
        cacheTime = config.getInt("gamification.ranking.caching_time");
    }

    /**
     * Calculate a ranking for the specified variable. This ranking will be cached.
     * @param instance
     * @param aca
     * @param timeframe
     * @param variable
     * @param page
     * @param pageSize
     * @return
     */
    public Ranking getRanking(String instance, String aca,TemporalBadgeContainer.Timeframe timeframe, String variable, int page, int pageSize) {
        int from = pageSize * (page);
        int to = pageSize * (page +1);
        try (ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            Ranking ranking = null;
            try {
                ranking = getCacheRanking(dbSession, instance,aca, timeframe, variable, from, pageSize);
                if (ranking != null) {
                    if (!isRankingCacheExpired(ranking)) {
                        return ranking;
                    }
                    rankingCacheCollection.deleteOne(Filters.eq("_id", ranking.getId()));
                }
                ranking = userProfileController.getRanking(dbSession, instance,aca, timeframe, variable);
                ranking.setId(ObjectId.get());
                rankingCacheCollection.insertOne(dbSession, ranking);
                if (to < ranking.getRankList().size()) {
                    ranking.setRankList(ranking.getRankList().subList(from, to));
                } else if (from < ranking.getRankList().size()) {
                    ranking.setRankList(ranking.getRankList().subList(from, ranking.getRankList().size()));
                } else {
                    ranking.setRankList(new ArrayList<>());
                }
                dbSession.commitTransaction();
            } catch (Exception ex) {
                dbSession.abortTransaction();
                logger.error("getRanking",ex.getMessage());
                throw ex;
            } finally {
                dbSession.close();
                return ranking;
            }
        }
    }

    /**
     * Function that retrieve the cached ranking
     *
     * @param clientSession
     * @param instance
     * @param aca
     * @param timeframe
     * @param variable
     * @param from
     * @param to
     * @return
     */
    private Ranking getCacheRanking(ClientSession clientSession, String instance,String aca, TemporalBadgeContainer.Timeframe timeframe, String variable, int from, int to) {
        List<Bson> filters = Arrays.asList(
                Filters.eq("instance", instance),
                Filters.eq("timeframe", timeframe.name()),
                Filters.eq("variable", variable)
        );
        if(aca != null){
            filters.add(Filters.eq("aca",aca));
        }
        return rankingCacheCollection.find(clientSession,and(filters)).projection(Projections.slice("rankList", from, to)).first();
    }

    private Pair<Ranking, Ranking.Rank> getSingleCacheRanking(ClientSession clientSession, String instance, String aca, TemporalBadgeContainer.Timeframe timeframe, String variable, long actor) {
        List<Bson> filters = Arrays.asList(
                Filters.eq("instance", instance),
                Filters.eq("timeframe", timeframe.name()),
                Filters.eq("variable", variable)
        );
        if(aca != null){
            filters.add(Filters.eq("aca",aca));
        }

        Document doc = rankingCacheCollection.withDocumentClass(Document.class).aggregate(clientSession,
                Arrays.asList(
                        Aggregates.match(
                                and(filters)
                        ),
                        Aggregates.unwind("$rankList"),
                        Aggregates.match(eq("rankList.actor", actor))
                )
        ).first();
        if(doc == null){
            Ranking ranking = rankingCacheCollection.find(clientSession,
                    and(
                            Filters.eq("instance", instance),
                            Filters.eq("timeframe", timeframe.name()),
                            Filters.eq("variable", variable)
                    )).projection(Projections.exclude("rankList")).first();
            if(ranking == null)
                return null;
            Ranking.Rank rank = new Ranking.Rank();
            rank.setPosition(1 +ranking.getTotalCount());
            rank.setActor(actor);
            rank.setValue(0);
            return new Pair<>(ranking,rank);
        }else {
            Document rankDoc = doc.get("rankList", Document.class);
            JsonReader reader = new JsonReader(rankDoc.toJson());
            Ranking.Rank rank = CodecRegistries.fromRegistries(rankingCacheCollection.getCodecRegistry()).get(Ranking.Rank.class).decode(reader, DecoderContext.builder().build());
            doc.remove("rankList");
            reader = new JsonReader(doc.toJson());
            Ranking ranking = CodecRegistries.fromRegistries(rankingCacheCollection.getCodecRegistry()).get(Ranking.class).decode(reader, DecoderContext.builder().build());
            return new Pair<>(ranking,rank);
        }
    }

    /**
     * Retrieve the position of a single user in a Ranking
     *
     * @param instance
     * @param aca
     * @param actor
     * @param variable
     * @param timeframe
     * @return
     */
    public Pair<Ranking, Ranking.Rank> getSingleRank(String instance, String aca,long actor, String variable, TemporalBadgeContainer.Timeframe timeframe) {
        try (ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            Pair<Ranking, Ranking.Rank> rankingRankPair = null;
            try {
                rankingRankPair = getSingleCacheRanking(dbSession, instance, aca, timeframe, variable, actor);
                if (rankingRankPair != null) {
                    Ranking ranking = rankingRankPair.first();
                    if (!isRankingCacheExpired(ranking)) {
                        return rankingRankPair;
                    }
                    rankingCacheCollection.deleteOne(Filters.eq("_id", ranking.getId()));
                } else {
                    Ranking ranking = userProfileController.getRanking(dbSession, instance,aca, timeframe, variable) ;
                    ranking.setId(ObjectId.get());
                    rankingCacheCollection.insertOne(dbSession, ranking);
                    Ranking.Rank rank = null;
                    Optional<Ranking.Rank> rankOptional = ranking.getRankList().stream().filter(r -> r.getActor().equals(actor)).findAny();
                    if(!rankOptional.isPresent()){
                        rank = new Ranking.Rank();
                        rank.setPosition(1 +ranking.getTotalCount());
                        rank.setActor(actor);
                        rank.setValue(0);
                    } else {
                        rank = rankOptional.get();
                    }
                    rankingRankPair = new Pair<>(ranking,rank);
                    dbSession.commitTransaction();
                }
            } catch (Exception ex) {
                dbSession.abortTransaction();
                throw ex;
            } finally {
                dbSession.close();
                return rankingRankPair;
            }
        }
    }

    private boolean isRankingCacheExpired(Ranking ranking){
        Date lastUpdate = ranking.getId().getDate();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(lastUpdate);
        calendar.add(Calendar.SECOND, cacheTime);
        return new Date().after(calendar.getTime());
    }
}
