package gamification.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.mongodb.MongoException;
import com.mongodb.MongoWriteException;
import com.mongodb.client.ClientSession;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.*;
import com.typesafe.config.Config;
import gamification.models.LogBadge;
import gamification.models.badges.*;
import gamification.models.profiles.AcaProfile;
import gamification.models.profiles.users.MainPersona;
import gamification.models.profiles.users.UserProfile;
import gamification.supports.deserializers.BadgeContainerDeserializer;
import logger.models.ActivityType;
import logger.models.Feature;
import logger.models.OTMEvent;
import logger.supports.exceptions.BadgeLevelException;
import org.bson.Document;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.conversions.Bson;
import org.bson.json.JsonReader;
import org.bson.types.ObjectId;
import logger.supports.TimeUtils;
import logger.services.LoggerMongoController;
import logger.services.MongoDBController;
import logger.supports.exceptions.UndeactivableBadgeException;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

import java.lang.reflect.MalformedParametersException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.*;
import static logger.services.LoggerMongoController.EXTERNAL_URL_FIELD;
import static logger.services.LoggerMongoController.HAS_TYPE_FIELD;

@Singleton
public class BadgeService {
    private MongoCollection<Document> badgesCollection;
    private final MongoDBController mongoDBController;
    @Inject
    Provider<LoggerMongoController> loggerMongoControllerProvider;
    @Inject
    Provider<GamificationService> gamificationServiceProvider;
    private IndividualBadgeContainer levels;

    @Inject
    public BadgeService(Config config, MongoDBController mongoDBController) {
        MongoDatabase mongoDatabase = mongoDBController.getMongoDatabase();
        this.mongoDBController = mongoDBController;
        this.initBadgesCollection(config, mongoDatabase);
        String instance = config.getString("ontomap.instance.url");
        this.buildBadgeLevels(instance);
    }

    /**
     * Init the Badge Containers Collection
     * @param config
     * @param mongoDatabase
     */
    private void initBadgesCollection(Config config, MongoDatabase mongoDatabase) {
        this.badgesCollection = mongoDatabase.getCollection(config.getString("mongo.collections.gamification.badges"), Document.class);
        this.badgesCollection.createIndex(Indexes.ascending("badgeList.**"));
        this.badgesCollection.createIndex(Indexes.ascending("instance"));
        this.badgesCollection.createIndex(Indexes.ascending("type"));
        this.badgesCollection.createIndex(Indexes.ascending("badgeList.preconditions.variables.variable"));
    }

    /**
     * Create the Badge Level Container and cache it
     *
     * @param instance
     */
    private void buildBadgeLevels(String instance) {
        IndividualBadgeContainer levels =
                this.badgesCollection.withDocumentClass(IndividualBadgeContainer.class)
                        .find(Filters.and(
                                Filters.eq("levelsContainer", true),
                                Filters.eq("instance", instance)))
                        .first();
        this.setBadgeLevels(levels);

    }

    /**
     * Retrieve the Badge Level Container
     * @return
     */
    public IndividualBadgeContainer getBadgeLevels() {
        return this.levels;
    }

    private void setBadgeLevels(IndividualBadgeContainer individualBadgeContainer) {
        this.levels = individualBadgeContainer;
    }

    /**
     * Retrieve a badge container by specific ObjectId
     * @param objectId
     * @return
     */
    public BadgeContainer<?> getBadgeContainerById(ObjectId objectId) {
        return getBadgeContainerById(null, objectId);
    }

    public BadgeContainer<?> getBadgeContainerById(ClientSession clientSession, ObjectId objectId) {
        Document document = clientSession == null ?
                badgesCollection.find(eq("_id", objectId), Document.class).first() :
                badgesCollection.find(clientSession, eq("_id", objectId), Document.class).first();
        return document != null ? docToBadgeContainer(document) : null;
    }

    /**
     * Retrieve a badge container by specific badge ObjectId (that is contained in its BadgeList)
     * @param objectId
     * @return
     */
    public BadgeContainer<?> getBadgeContainerByBadgeId(ObjectId objectId) {
        return getBadgeContainerByBadgeId(null, objectId);
    }

    public BadgeContainer<? extends Badge> getBadgeContainerByBadgeId(ClientSession clientSession, ObjectId objectId) {
        Document document = clientSession == null ?
                badgesCollection.find(eq("badgeList._id", objectId), Document.class).first() :
                badgesCollection.find(clientSession, eq("badgeList._id", objectId), Document.class).first();

        return document != null ? docToBadgeContainer(document) : null;
    }

    /**
     * Retrieve a badge by specific ObjectId
     * @param dbSession
     * @param objectId
     * @return
     */
    public Badge getBadge(ClientSession dbSession, String objectId) {
        return ObjectId.isValid(objectId) ? getBadge(dbSession, new ObjectId(objectId)) : null;
    }

    public Badge getBadge(ClientSession dbSession, ObjectId objectId) {
        BadgeContainer<? extends Badge> badgeContainer = getBadgeContainerByBadgeId(dbSession, objectId);
        Badge badge = getBadge(badgeContainer, objectId);
        if (badge == null)
            return null;
        badge.setContainerReference(new Badge.ContainerReference(badgeContainer));
        return badge;
    }

    /**
     * Given an ObjectId belonging to a badge, and a BadgeContainer, this function retrieve the badge
     * @param badgeContainer
     * @param idBadge
     * @return
     */
    public Badge getBadge(BadgeContainer<? extends Badge> badgeContainer, ObjectId idBadge) {
        if (badgeContainer == null) return null;
        List<? extends Badge> badgeList = badgeContainer.getBadgeList();
        Optional<? extends Badge> optionalBadge = badgeList.stream().filter(badge -> badge.getId().equals(idBadge)).findAny();
        if (optionalBadge.isPresent()) {
            Badge badge = optionalBadge.get();
            badge.setContainerReference(new Badge.ContainerReference(badgeContainer));
            return badge;
        }
        return null;
    }

    /**
     * Retrieve all BadgeContainer filtered by type
     * @param instance
     * @param type
     * @return
     */
    public List<BadgeContainer> getAllBadges(String instance, String type) {
        Bson filter;
        if (type != null) {
            filter = and(eq("instance", instance), eq("type", type.toLowerCase()));
        } else {
            filter = eq("instance", instance);
        }
        return StreamSupport
                .stream(badgesCollection.find(filter).spliterator(), false)
                .map(this::docToBadgeContainer)
                .collect(Collectors.toList());
    }

    /**
     * Enable or disable a BadgeContainer
     * @param badgeContainer
     * @param actor             Who made the action. It's used to create and OTMEvent
     * @param enable
     * @throws UndeactivableBadgeException
     */
    public void enableBadgeContainer(BadgeContainer<Badge> badgeContainer, long actor, boolean enable) throws UndeactivableBadgeException {
        try (ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            try {
                enableBadges(dbSession, badgeContainer, badgeContainer.getBadgeList(), enable);
                List<OTMEvent> events = buildBadgeEvent(actor, badgeContainer, badgeContainer.getBadgeList(), ActivityType.object_updated);
                this.loggerMongoControllerProvider.get().insertGamificationEvents(dbSession, events);
                dbSession.commitTransaction();
            } catch (MongoException | UndeactivableBadgeException ex) {
                dbSession.abortTransaction();
                throw ex;
            } finally {
                dbSession.close();
            }
        }
    }

    /**
     * Enable or disable a badge
     * @param badgeContainer
     * @param badge
     * @param actor            Who made the action. It's used to create and OTMEvent
     * @param enable
     * @throws UndeactivableBadgeException
     */
    public void enableBadge(BadgeContainer badgeContainer, Badge badge, long actor, boolean enable) throws UndeactivableBadgeException {
        try (ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            try {
                enableBadges(dbSession, badgeContainer, Collections.singletonList(badge), enable);
                List<OTMEvent> events = buildBadgeEvent(actor, badgeContainer, Collections.singletonList(badge), ActivityType.object_updated);
                this.loggerMongoControllerProvider.get().insertGamificationEvents(dbSession, events);
                dbSession.commitTransaction();
            } catch (MongoException | UndeactivableBadgeException ex) {
                dbSession.abortTransaction();
                throw ex;
            } finally {
                dbSession.close();
            }
        }
    }

    /**
     * Enable or disable a badgeList
     * @param clientSession
     * @param badgeContainer
     * @param badges
     * @param enable
     * @throws UndeactivableBadgeException
     */
    public void enableBadges(ClientSession clientSession, BadgeContainer<?> badgeContainer, List<Badge> badges, boolean enable) throws UndeactivableBadgeException {

        for (Badge badge : badges) {
            if (enable) {
                if (badge instanceof BadgeStep) {
                    BadgeStep badgeStep = (BadgeStep) badge;
                    List<ObjectId> preconditions = badgeStep.getPreconditions().getBadgeList()
                            .stream()
                            //Convert all BadgePrecondition ID to BadgeLevels
                            .map(id -> getBadge(clientSession, id))
                            // Get only no-active of these
                            .filter(prec -> !prec.isActive()).map(Badge::getId)
                            .collect(Collectors.toList());
                    // Is the list is not empty there is at least one badgeLevel precondition that is not active.
                    if (preconditions.size() > 0) {
                        throw new UndeactivableBadgeException(badge.getId(), preconditions);
                    }
                }
            } else {
                List<ObjectId> precs = isPreconditionOf(clientSession, badgeContainer.getInstance(), badge.getId());
                if (precs.size() > 0) {
                    throw new UndeactivableBadgeException(badge.getId(), precs);
                }
            }
            badges.forEach(b -> b.setActive(enable));
            badgesCollection.updateOne(clientSession, eq("badgeList._id", badge.getId()), Updates.set("badgeList.$.active", enable));
        }
    }

    /**
     * Create a badge
     * @param instance
     * @param actor         Who made the action. It's used to create and OTMEvent
     * @param badgeJson
     * @return
     * @throws MalformedParametersException
     * @throws MongoWriteException
     * @throws JsonProcessingException
     */
    public BadgeContainer<?> createBadge(String instance, long actor, JsonNode badgeJson) throws MalformedParametersException, MongoWriteException, JsonProcessingException {
        BadgeContainer<? extends Badge> badgeContainer = null;
        boolean isNewLevelContainer = false;
        try (ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                SimpleModule simpleModule = new SimpleModule();
                simpleModule.addDeserializer(BadgeContainer.class, new BadgeContainerDeserializer(this));
                objectMapper.registerModule(simpleModule);
                badgeContainer = objectMapper.treeToValue(badgeJson, BadgeContainer.class);
                badgeContainer.setInstance(instance);

                switch (badgeJson.get("type").asText().toLowerCase()) {
                    case "individual":
                        IndividualBadgeContainer individualBadgeContainer = (IndividualBadgeContainer) badgeContainer;
                        if (individualBadgeContainer.isLevelsContainer()) {
                            if (this.levels != null)
                                throw new BadgeLevelException();
                            else {
                                isNewLevelContainer = true;
                            }
                        }
                        badgesCollection.withDocumentClass(IndividualBadgeContainer.class).insertOne(dbSession, individualBadgeContainer);
                        break;
                    case "aca":
                        badgesCollection.withDocumentClass(AcaBadgeContainer.class).insertOne(dbSession, (AcaBadgeContainer) badgeContainer);
                        break;
                    case "ranking":
                        badgesCollection.withDocumentClass(RankingBadgeContainer.class).insertOne(dbSession, (RankingBadgeContainer) badgeContainer);
                        break;
                }
                List<OTMEvent> events = buildBadgeEvent(actor, badgeContainer, (Collection<Badge>) badgeContainer.getBadgeList(), ActivityType.object_created);
                this.loggerMongoControllerProvider.get().insertGamificationEvents(dbSession, events);

                if(badgeJson.get("type").asText().toLowerCase().compareTo("individual") == 0){
                    List<LogBadge> logBadges =gamificationServiceProvider.get().checkNewBadges(dbSession,instance,events.get(0).getTimestamp());

                    gamificationServiceProvider.get().saveLogs(dbSession,logBadges);
                    gamificationServiceProvider.get().sendLogs(dbSession,logBadges);
                }

                dbSession.commitTransaction();
                if (isNewLevelContainer){
                    setBadgeLevels((IndividualBadgeContainer) badgeContainer);
                }
                return badgeContainer;
            } catch (MongoWriteException ex) {
                dbSession.abortTransaction();
                throw new MongoException("Badge name already exist: '" + badgeJson.get("name").asText() + "'");
            } finally {
                dbSession.close();
            }
        }
    }

    /**
     * Edit a badge container
     * @param instance
     * @param actor            Who made the action. It's used to create and OTMEvent
     * @param badgeJson
     * @return
     */
    public BadgeContainer<?> editBadgeContainer(String instance, long actor, JsonNode badgeJson) throws Exception {
        if (!badgeJson.has("id") || !ObjectId.isValid(badgeJson.get("id").asText())) {
            return null; // ID not Valid
        }
        try (ClientSession dbSession = mongoDBController.getSession()) {
            dbSession.startTransaction();
            try {
                BadgeContainer<?> olDBadgeContainer = getBadgeContainerById(new ObjectId(badgeJson.get("id").asText()));
                if (olDBadgeContainer == null) {
                    throw new Exception("Badge Not EXIST");
                }
                if (!badgeJson.get("type").asText().toLowerCase().equals(olDBadgeContainer.getType())) {
                    throw new Exception("BadgeContainer non dello stesso tipo");
                }

                ObjectMapper objectMapper = new ObjectMapper();
                SimpleModule simpleModule = new SimpleModule();
                simpleModule.addDeserializer(BadgeContainer.class, new BadgeContainerDeserializer(this));
                objectMapper.registerModule(simpleModule);
                BadgeContainer<?> badgeContainer = objectMapper.treeToValue(badgeJson, BadgeContainer.class);
                badgeContainer.setInstance(instance);
                badgeContainer.setId(olDBadgeContainer.getId());
                Set<Badge> editedBadges;
                OTMEvent containerEvent = null;
                if (!Objects.equals(olDBadgeContainer.getName(), badgeContainer.getName())) {
                    containerEvent = buildBadgeContainerEvent(actor, badgeContainer, ActivityType.object_updated);
                }

                switch (badgeJson.get("type").asText().toLowerCase()) {
                    default:
                        IndividualBadgeContainer individualBadgeContainer = (IndividualBadgeContainer) badgeContainer;
                        if (individualBadgeContainer.isLevelsContainer() && !individualBadgeContainer.equals(levels)) {
                            throw new BadgeLevelException();
                        }
                        editedBadges = editBadgeContainer(olDBadgeContainer, individualBadgeContainer);
                        badgesCollection.withDocumentClass(IndividualBadgeContainer.class)
                                .replaceOne(dbSession, Filters.eq("_id", olDBadgeContainer.getId()), (IndividualBadgeContainer) olDBadgeContainer);
                        break;
                    case "aca":
                        AcaBadgeContainer acaBadgeContainer = (AcaBadgeContainer) badgeContainer;
                        editedBadges = editBadgeContainer(olDBadgeContainer, acaBadgeContainer);
                        badgesCollection.withDocumentClass(AcaBadgeContainer.class)
                                .replaceOne(dbSession, Filters.eq("_id", olDBadgeContainer.getId()), (AcaBadgeContainer) olDBadgeContainer);
                        break;
                    case "ranking":
                        RankingBadgeContainer rankingBadgeContainer = (RankingBadgeContainer) badgeContainer;
                        editedBadges = editBadgeContainer(olDBadgeContainer, rankingBadgeContainer);
                        badgesCollection.withDocumentClass(RankingBadgeContainer.class)
                                .replaceOne(dbSession, Filters.eq("_id", olDBadgeContainer.getId()), (RankingBadgeContainer) olDBadgeContainer);
                        break;
                }
                List<OTMEvent> events = buildBadgeEvent(actor, badgeContainer, editedBadges, ActivityType.object_updated);
                if (containerEvent != null) {
                    events.add(0, containerEvent);
                }
                this.loggerMongoControllerProvider.get().insertGamificationEvents(dbSession, events);

                if(badgeJson.get("type").asText().toLowerCase().compareTo("individual") == 0){
                    List<LogBadge> logBadges =gamificationServiceProvider.get().checkNewBadges(dbSession,instance,events.get(0).getTimestamp());

                    gamificationServiceProvider.get().saveLogs(dbSession,logBadges);
                    gamificationServiceProvider.get().sendLogs(dbSession,logBadges);
                }

                dbSession.commitTransaction();
                return olDBadgeContainer;
            } catch (MongoWriteException | JsonProcessingException ex) {
                dbSession.abortTransaction();
                throw ex;
            } catch (Exception ex) {
                dbSession.abortTransaction();
                throw ex;
            } finally {
                dbSession.close();
            }
        }
    }

    /**
     * Apply changes from new to old BadgeContainer
     *
     * @param oldB
     * @param newB
     * @return
     * @throws Exception
     */
    private Set<Badge> editBadgeContainer(BadgeContainer oldB, BadgeContainer newB) throws Exception {
        Set<Badge> editedBadges = new HashSet<>();
        oldB.setName(newB.getName());
        if (oldB.getBadgeList().size() > newB.getBadgeList().size()) {
            throw new Exception("Ci stanno badge");
        }
        int i = 0;
        while (i < oldB.getBadgeList().size()) {
            Badge oldBadgeStep = (Badge) oldB.getBadgeList().get(i);
            Badge newBadgeStep = (Badge) newB.getBadgeList().get(i);
            if (!oldBadgeStep.equals(newBadgeStep)) {
                throw new Exception("L'ordine non coincide");
            }
            if (!Objects.equals(oldBadgeStep.getName(), newBadgeStep.getName())) {
                oldBadgeStep.setName(newBadgeStep.getName());
                editedBadges.add(newBadgeStep);
            }
            if (!Objects.equals(oldBadgeStep.getLogo(), newBadgeStep.getLogo())) {
                oldBadgeStep.setLogo(newBadgeStep.getLogo());
                editedBadges.add(newBadgeStep);
            }
            i++;
        }
        while (i < newB.getBadgeList().size()) {
            BadgeStep newBadgeStep = (BadgeStep) newB.getBadgeList().get(i);
            oldB.getBadgeList().add(newBadgeStep);
            editedBadges.add(newBadgeStep);
            i++;
        }

        return editedBadges;
    }

    /**
     * Check if a Badge is precondition of another one
     * @param clientSession
     * @param instance
     * @param idBadge
     * @return          The list of all badges id
     */
    private List<ObjectId> isPreconditionOf(ClientSession clientSession, String instance, ObjectId idBadge) {
        return StreamSupport
                .stream(
                        badgesCollection.aggregate(clientSession,
                                Arrays.asList(
                                        Aggregates.match(eq("instance", instance)),
                                        Aggregates.unwind("$badgeList"),
                                        Aggregates.match(and(
                                                eq("badgeList.preconditions.badgeList", idBadge),
                                                eq("badgeList.active", true))
                                        ),
                                        Aggregates.project(Projections.computed("badgeid", "$badgeList._id"))
                                ), Document.class)
                                .spliterator(), false)
                .map(document -> document.getObjectId("badgeid"))
                .collect(Collectors.toList());
    }

    /**
     * Convert a Document (MongoDB) to a BadgeContainer
     * @param document
     * @return
     */
    public BadgeContainer<? extends Badge> docToBadgeContainer(Document document) {
        JsonReader reader = new JsonReader(document.toJson());
        switch (document.getString("type")) {
            case "individual":
                return CodecRegistries.fromRegistries(badgesCollection.getCodecRegistry()).get(IndividualBadgeContainer.class).decode(reader, DecoderContext.builder().build());
            case "aca":
                return CodecRegistries.fromRegistries(badgesCollection.getCodecRegistry()).get(AcaBadgeContainer.class).decode(reader, DecoderContext.builder().build());
            default:
                return CodecRegistries.fromRegistries(badgesCollection.getCodecRegistry()).get(RankingBadgeContainer.class).decode(reader, DecoderContext.builder().build());
        }
    }

    /**
     * Retrieve the List of all Badges (Individual) that have one (or more) variables as precondition
     * @param clientSession
     * @param mainPersona       Use to check if one o more badges are already applicable
     * @param modifiedVariables
     * @return
     */
    public List<Badge> getApplicableIndividualBadges(ClientSession clientSession, MainPersona mainPersona, Set<String> modifiedVariables) {
        if (modifiedVariables.size() == 0)
            return new ArrayList<>();
        List<ObjectId> excludedBadges = mainPersona.getBadges().getCurrentBadges().stream().map(UserProfile.AssignedBadge::getIdBadge).collect(Collectors.toList());
        List<Bson> variableFilters = new ArrayList<>();
        variableFilters.add(eq("badgeList.preconditions.variables.threshold", 0));
        for (String variable : modifiedVariables) {
            variableFilters.add(
                    and(
                            eq("badgeList.preconditions.variables.variable", variable),
                            lte("badgeList.preconditions.variables.threshold", mainPersona.getFullVariables().getOrDefault(variable, 0.0))
                    ));
        }
        return StreamSupport.stream(badgesCollection.aggregate(clientSession,
                Arrays.asList(
                        Aggregates.match(
                                and(
                                        eq("instance", mainPersona.getInstance()),
                                        eq("type", "individual"))
                        ),
                        Aggregates.unwind("$badgeList"),
                        Aggregates.match(
                                and(
                                        nin("badgeList._id", excludedBadges),
                                        or(variableFilters),
                                        or(
                                                size("badgeList.preconditions.badgeList", 0),
                                                in("badgeList.preconditions.badgeList", excludedBadges)),
                                        ne("badgeList.preconditions.active", false))
                        )
                ), Document.class)
                .spliterator(), false)
                .filter(badge -> {
                    //Make a filter base on precondition variables satisfied by profile variables
                    Document document = badge.get("badgeList", Document.class).get("preconditions", Document.class);
                    JsonReader reader = new JsonReader(document.toJson());
                    BadgeStep.Preconditions preconditions = CodecRegistries.fromRegistries(badgesCollection.getCodecRegistry()).get(BadgeStep.Preconditions.class).decode(reader, DecoderContext.builder().build());
                    boolean preconditionOk = true;
                    if (preconditions.getBadgeList() != null) {
                        for (ObjectId idBadge : preconditions.getBadgeList()) {
                            preconditionOk = preconditionOk && excludedBadges.contains(idBadge);
                        }
                    }
                    preconditionOk = preconditionOk && preconditions.getVariables()
                            .stream()
                            .filter(variableCondition ->
                                    mainPersona.getFullVariables().getOrDefault(variableCondition.getVariable(), 0.0) < variableCondition.getThreshold())
                            .map(BadgeStep.VariableCondition::getVariable)
                            .count() == 0;
                    return preconditionOk;
                }).map(document -> {
                    Document levelDoc = document.get("badgeList", Document.class);
                    JsonReader reader = new JsonReader(levelDoc.toJson());
                    return CodecRegistries.fromRegistries(badgesCollection.getCodecRegistry()).get(BadgeStep.class).decode(reader, DecoderContext.builder().build());
                })
                .collect(Collectors.toList());

    }

    /**
     * Retrieve the List of all Badges (ACA) that have one (or more) variables as precondition
     * @param clientSession
     * @param acaProfile           Use to check if one o more badges are already applicable
     * @param timeframe
     * @param modifiedVariablesAca
     * @return
     */
    public List<Badge> getApplicableAcaBadges(ClientSession clientSession, AcaProfile acaProfile, TemporalBadgeContainer.Timeframe timeframe, Set<String> modifiedVariablesAca) {
        if (modifiedVariablesAca.size() == 0)
            return new ArrayList<>();
        List<ObjectId> gottenBadges = acaProfile.getBadges().getCurrentBadges().stream().map(UserProfile.AssignedBadge::getIdBadge).collect(Collectors.toList());
        List<ObjectId> excludedBadges = acaProfile.getBadges().getCurrentBadges().stream()
                .filter(assignedBadge -> {
                    Date date = assignedBadge.getExpiredDate();
                    Calendar expireCalendar = Calendar.getInstance();
                    Calendar nowCalendar = Calendar.getInstance();
                    expireCalendar.setTime(date);
                    if (timeframe.equals(TemporalBadgeContainer.Timeframe.MONTH)) {
                        return expireCalendar.get(Calendar.MONTH) != nowCalendar.get(Calendar.MONTH);
                    } else if (timeframe.equals(TemporalBadgeContainer.Timeframe.WEEK)) {
                        return expireCalendar.get(Calendar.WEEK_OF_YEAR) != nowCalendar.get(Calendar.WEEK_OF_YEAR);
                    }
                    return false;
                })
                .map(UserProfile.AssignedBadge::getIdBadge).collect(Collectors.toList());
        List<Bson> variableFilters = new ArrayList<>();
        for (String variable : modifiedVariablesAca) {
            variableFilters.add(and(
                    eq("badgeList.preconditions.variables.variable", variable),
                    lte("badgeList.preconditions.variables.threshold", acaProfile.getTemporalVariables(timeframe).getOrDefault(variable, 0.0))
            ));
        }
        return StreamSupport.stream(badgesCollection.aggregate(clientSession,
                Arrays.asList(
                        Aggregates.match(
                                and(
                                        eq("instance", acaProfile.getInstance()),
                                        eq("type", "aca"),
                                        or(
                                                exists("aca", false),
                                                eq("aca", acaProfile.getUrl())
                                        ))
                        ),
                        Aggregates.unwind("$badgeList"),
                        Aggregates.match(
                                and(
                                        nin("badgeList._id", excludedBadges),
                                        eq("timeframe", timeframe.name()),
                                        or(variableFilters),
                                        or(
                                                size("badgeList.preconditions.badgeList", 0),
                                                in("badgeList.preconditions.badgeList", gottenBadges)),
                                        ne("badgeList.preconditions.active", false))
                        )
                ), Document.class)
                .spliterator(), false)
                .filter(badge -> {
                    //Make a filter base on precondition variables satisfied by profile variables
                    Document document = badge.get("badgeList", Document.class).get("preconditions", Document.class);
                    JsonReader reader = new JsonReader(document.toJson());
                    BadgeStep.Preconditions preconditions = CodecRegistries.fromRegistries(badgesCollection.getCodecRegistry()).get(BadgeStep.Preconditions.class).decode(reader, DecoderContext.builder().build());
                    return preconditions.getVariables()
                            .stream()
                            .filter(variableCondition ->
                                    acaProfile.getTemporalVariables(timeframe).getOrDefault(variableCondition.getVariable(), 0.0) < variableCondition.getThreshold())
                            .map(BadgeStep.VariableCondition::getVariable)
                            .count() == 0;
                }).map(document -> {
                    Document levelDoc = document.get("badgeList", Document.class);
                    JsonReader reader = new JsonReader(levelDoc.toJson());
                    return CodecRegistries.fromRegistries(badgesCollection.getCodecRegistry()).get(BadgeStep.class).decode(reader, DecoderContext.builder().build());
                })
                .collect(Collectors.toList());

    }

    /**
     * Get all Ranking BadgeContainer
     * @param dbSession
     * @return
     */
    public List<RankingBadgeContainer> getRankingBadges(ClientSession dbSession) {
        return StreamSupport
                .stream(badgesCollection.find(dbSession, Filters.eq("type", "ranking")).spliterator(), false)
                .map(this::docToBadgeContainer)
                .map(badgeContainer -> (RankingBadgeContainer) badgeContainer)
                .collect(Collectors.toList());
    }

    /**
     * Calculate del level of a user
     * @param mainPersona
     * @return
     */
    public BadgeStep getUserLevel (MainPersona mainPersona) {
        BadgeStep level = null;
        IndividualBadgeContainer levels = this.getBadgeLevels();
        Set<ObjectId> currentBadges = mainPersona.getBadges()
                .getCurrentBadges().stream()
                .map(UserProfile.AssignedBadge::getIdBadge)
                .collect(Collectors.toSet());
        if(levels != null && levels.getBadgeList().size() > 0) {
            //Get the last element
            level = levels.getBadgeList().stream()
                    .filter(badgeStep -> currentBadges.contains(badgeStep.getId()))
                    .reduce((first,second) -> second).orElse(null);
            if (level == null){
                level = levels.getBadgeList().get(0);
            }
            level.setContainerReference(new Badge.ContainerReference(levels));
        }
        return level;
    }

    /**
     * Create a List of OTMEvents (one per Badge) when a Badge is created/modified
     * @param actor
     * @param badgeContainer
     * @param badges
     * @param activityType
     * @return
     */
    private List<OTMEvent> buildBadgeEvent(long actor, BadgeContainer<?> badgeContainer, Collection<Badge> badges, ActivityType activityType) {
        List<OTMEvent> events = new ArrayList<>();
        for (Badge badge : badges) {
            OTMEvent event = new OTMEvent();
            event.setInstance(badgeContainer.getInstance());
            event.setApplication(badgeContainer.getInstance());
            event.setActivityType(activityType.name());
            event.setActor(actor);
            event.setTimestamp(new Date().getTime());
            Feature feature = new Feature();
            feature.addStringProperty(EXTERNAL_URL_FIELD, "https://" + badgeContainer.getInstance() + "/api/v1/gamification/badges/" + badge.getId().toString());
            feature.setGeometry(null);
            feature.addStringProperty(HAS_TYPE_FIELD, "Badge");
            feature.addStringProperty("hasBadgeContainer", "https://" + badgeContainer.getInstance() + "/api/v1/gamification/badgeContainers/" + badgeContainer.getId().toString());
            feature.addBooleanProperty("IsActive", badge.isActive());
            feature.addStringProperty("hasName", badge.getName());
            feature.addStringProperty("hasLogo", badge.getLogo());
            event.setActivityObjects(Collections.singletonList(feature));
            events.add(event);
        }
        return events;
    }

    /**
     * Create an OTMEvent when a BadgeContainer is created/modified
     * @param actor
     * @param badgeContainer
     * @param activityType
     * @return
     */
    private OTMEvent buildBadgeContainerEvent(long actor, BadgeContainer<?> badgeContainer, ActivityType activityType) {
        OTMEvent event = new OTMEvent();
        event.setInstance(badgeContainer.getInstance());
        event.setApplication(badgeContainer.getInstance());
        event.setActivityType(activityType.name());
        event.setActor(actor);
        event.setTimestamp(new Date().getTime());
        Feature feature = new Feature();
        feature.addStringProperty(EXTERNAL_URL_FIELD, "https://" + badgeContainer.getInstance() + "/api/v1/gamification/badgeContainers/" + badgeContainer.getId().toString());
        feature.setGeometry(null);
        feature.addStringProperty(HAS_TYPE_FIELD, "BadgeContainer");
        feature.addStringProperty("hasName", badgeContainer.getName());
        event.setActivityObjects(Collections.singletonList(feature));
        return event;
    }
}
