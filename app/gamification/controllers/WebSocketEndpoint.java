package gamification.controllers;

import akka.stream.javadsl.Flow;
import com.fasterxml.jackson.databind.JsonNode;
import logger.controllers.ResponseController;
import play.libs.F;
import play.libs.streams.ActorFlow;
import play.mvc.*;
import akka.actor.*;
import akka.stream.*;
import logger.services.AccessTokenMongoController;
import logger.services.AuthenticationController;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;


public class WebSocketEndpoint extends Controller {
    private final ActorSystem actorSystem;
    private final Materializer materializer;
    private ResponseController responseController;
    private AccessTokenMongoController accessTokenMongoController;
    private AuthenticationController authenticationController;
    private NotificationController notificationController;


    @Inject
    public WebSocketEndpoint(ActorSystem actorSystem, Materializer materializer, ResponseController responseController,
                             AccessTokenMongoController accessTokenMongoController, AuthenticationController authenticationController, NotificationController notificationController) {
        this.actorSystem = actorSystem;
        this.materializer = materializer;
        this.responseController = responseController;
        this.accessTokenMongoController = accessTokenMongoController;
        this.authenticationController = authenticationController;
        this.notificationController = notificationController;
    }

    /**
     * @api {ws} /gamification/notifications/ws Receive Gamification Notifications
     * @apiName Receive Gamification Notifications
     * @apiGroup Gamification Logs APIs
     * @apiDescription WebSocket endpoint connection to receive notifications (concerning a user) when a badge is assigned or when a rule belonging to a dependency chain is triggered.
     * Notifications have the same structure as a Gamification Entry Log with "resolve" set to <i>true</i> (see the <a href="#api-Gamification_Logs_APIs-Get_User_Logs">Insert Gamification Logs APIs</a>).
     *
     * The Play framework used within CO3OTM does not support the management of permanent WebSockets. In order to address this issue, CO3OTM will send through the socket a ping message every 10 seconds in order to keep the socket “alive” - please ignore it:
     * <code>{
     *      "info": "keep-alive-ping"
     * }</code>
     * @apiError (Error 403 Forbidden) no_access_token No valid access token provided
     * @apiPermission authorization.
     */
    public WebSocket socket(String channelString) {
        return WebSocket.Json.acceptOrResult(
                request -> CompletableFuture.completedFuture(request.queryString("Authorization").map(token -> {
                    String instance = authenticationController.getOTMInstance(request);
                    long actor = accessTokenMongoController.verifyUserToken(instance,token);
                    if (actor== -1) {
                        return F.Either.<Result, Flow<JsonNode, JsonNode, ?>>Left(responseController.noToken());
                    }
                    NotificationController.Channel  channel;
                    switch (channelString.toUpperCase()){
                        case "LOGGER":
                            channel = NotificationController.Channel.LOGGER;
                            break;
                        case "GAMIFICATION":
                            channel = NotificationController.Channel.GAMIFICATION;
                            break;
                        default:
                            return F.Either.<Result, Flow<JsonNode, JsonNode, ?>>Left(responseController.notValidNotificationChannel());
                    }
                    NotificationController.Channel finalChannel = channel;
                    return F.Either.<Result, Flow<JsonNode, JsonNode, ?>>Right(ActorFlow.actorRef(out -> notificationController.addActor(actor, finalChannel,out), actorSystem, materializer));
                }).orElseGet(() ->  F.Either.<Result, Flow<JsonNode, JsonNode, ?>>Left(responseController.noToken())))
        );
    }
}
