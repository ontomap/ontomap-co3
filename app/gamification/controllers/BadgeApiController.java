package gamification.controllers;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mongodb.MongoException;
import com.typesafe.config.Config;
import gamification.models.badges.IndividualBadgeContainer;
import logger.controllers.Attrs;
import logger.controllers.ResponseController;
import gamification.services.BadgeService;
import gamification.models.badges.Badge;
import gamification.models.badges.BadgeContainer;
import gamification.supports.serializer.ObjectIdSerializer;
import logger.supports.exceptions.BadgeLevelException;
import org.bson.types.ObjectId;
import play.Environment;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import logger.supports.JsonSchemaSupport;
import logger.supports.actions.AcaManagerAction;
import logger.supports.actions.MultipleInstancesAction;
import logger.supports.exceptions.MalformedJsonException;
import logger.supports.exceptions.UndeactivableBadgeException;

import javax.inject.Inject;
import java.lang.reflect.MalformedParametersException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static logger.controllers.ResponseController.*;

/**
 * This class contains the methods concerning  the insertion and retrieval of Gamification module's badges.
 */
@MultipleInstancesAction.MultipleInstances
public class BadgeApiController extends Controller {
    private final BadgeService badgeController;
    private final Config config;
    private final Environment environment;
    private final ResponseController responseController;
    private final ObjectMapper om;
    private final Logger.ALogger logger;

    @Inject
    public BadgeApiController(Config config, Environment environment, BadgeService badgeController, ResponseController responseController) {
        this.config = config;
        this.environment = environment;
        this.badgeController = badgeController;
        this.responseController = responseController;
        this.om = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(ObjectId.class, new ObjectIdSerializer());
        om.registerModule(simpleModule);
        om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        logger = new Logger.ALogger(play.api.Logger.apply("BadgeApiController"));
    }


    /**
     * @apiDefine invalid_timeframe
     * @apiError (Error 400 Bad Request) invalid_timeframe The <i>timeframe</i> selected is not valid. Value can be <code>WEEK or MONTH</code>
     */

    /**
     * @apiDefine gamification_disabled
     * @apiError (Error 403 Forbidden) gamification_disabled The CO3OTM Achievements Manager is not active in this CO3 instance.
     */

    /**
     * @apiDefine not_valid_id
     * @apiError (Error 400 BadRequest) not_valid_id The provided <i>id</i> is not valid.
     */

    /**
     * @api {post} /gamification/badgeContainers Create new badges
     * @apiName Create Badge Container
     * @apiGroup Badge APIs
     * @apiDescription Creates and inserts into the CO3OTM Achievements Manager, one or more Badge Containers having at least one included badge.<br/>
     * The request must have a <code>application/json</code> Content-Type and must include the JSON representation of the Badge Container(s) in its body (see provided example).<br/>
     * Badge Container JSON Schema can be found at <a  target="_blan" href="./api/v1/gamification/badgeContainers/schema/">Schema Link</a>
     *
     * @apiPermission Client Certificate.
     * @apiParamExample Creation of Individual Badges (example)
     * POST https://dev.co3.ontomap.eu/api/v1/gamification/badgeContainers
     * [
     *    {
     *         "name": "Document Creator",
     *         "type":"<b>individual</b>",
     *         "badgeList": [
     *             {
     *                 "name": "Bronze",
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 10
     *                         }
     *                     ]
     *                 }
     *             },
     *             {
     *                 "name": "Silver",
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 20
     *                         }
     *                     ]
     *                 }
     *             },
     *             {
     *                 "name": "Gold",
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 30
     *                         }
     *                     ]
     *                 }
     *             }
     *         ]
     *     }
     * ]
     *
     * @apiParamExample Creation of Weekly ACA Badges (example)
     * POST https://dev.co3.ontomap.eu/api/v1/gamification/badgeContainers
     * [
     *     {
     *         "name": "Document Creator ACA",
     *         "type":"<b>aca</b>",
     *         "timeframe":"WEEK",
     *         "badgeList": [
     *             {
     *                 "name": "Bronze",
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 10
     *                         }
     *                     ]
     *                 }
     *             },
     *             {
     *                 "name": "Silver",
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 20
     *                         }
     *                     ]
     *                 }
     *             },
     *             {
     *                 "name": "Gold",
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 30
     *                         }
     *                     ]
     *                 }
     *             }
     *         ]
     *     }
     *
     * @apiParamExample Creation of Weekly Ranking Badges (example)
     * POST https://dev.co3.ontomap.eu/api/v1/gamification/badgeContainers
     * [
     *     {
     *         "name": "Document Creator Ranking",
     *         "type": "<b>ranking</b>",
     *         "variable": "documentCreated",
     *         "timeframe": "WEEK",
     *         "badgeList": [
     *             {
     *                 "name": "Bronze",
     *                 "position": 2
     *             },
     *             {
     *                 "name": "Silver",
     *                 "position": 1
     *             }
     *         ]
     *     }
     * ]
     *
     * @apiUse malformed_json
     * @apiUse invalid_certificate
     * @apiUse gamification_disabled
     * @apiUse wrong_content_type
     * @apiUse internal_server_error
     *
     *
     */
    @AcaManagerAction.AcaManagerAuthentication
    public Result createBadges(Http.Request request) {
        if (!config.getBoolean("gamification.active")) return forbidden("Gamification disabled");
        String instance = request.attrs().get(Attrs.INSTANCE);
        if (!request.contentType().orElse("").equals(Http.MimeTypes.JSON)) {
            return responseController.wrongContentType();
        }
        long actor = request.attrs().get(Attrs.USER_AUTHENTICATED);
        if (actor == -1){
            return responseController.userNotFound();
        }
        JsonNode requestBody = request.body().asJson();
        try {
            JsonSchemaSupport.analyze(requestBody, environment, config.getString("gamification.json_schemas.badges"));
        } catch (InternalError error) {
            logger.error("Exception in createBadges:", error.getMessage());
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        } catch (MalformedJsonException e) {
            if (e.getNode() != null)
                return responseController.malformedJsonRequest(e.getNode());
            else
                return responseController.malformedJsonRequest(e.getMessage());
        }
        ArrayNode badgesArray = (ArrayNode) requestBody;
        List<BadgeContainer<?>> badgeContainers = new ArrayList<>();
        for (JsonNode badgeJson : badgesArray) {
            try {
                BadgeContainer<?> badgeContainer = badgeController.createBadge(instance,actor, badgeJson);
                badgeContainers.add(badgeContainer);
            } catch (JsonProcessingException | MalformedParametersException | MongoException e) {
                return responseController.malformedJsonRequest(e.getMessage());
            } catch (BadgeLevelException e){
                return responseController.malformedJsonRequest("Levels Badge Container already Exist");
            }
        }
        return ok((JsonNode) om.valueToTree(badgeContainers));
    }


    /**
     * @api {get} /gamification/badges/<idBadge> Get a Badge
     * @apiName Get a Badge
     * @apiGroup Badge APIs
     * @apiDescription Retrieves information about a specified badge. The response includes some information about the container of that badge.
     *
     * @apiParam (Field Attributes) {String} idBadge <strong>Required.</strong> The <i>id</i> of the badge to be retrieved.
     *
     * @apiParamExample Get a badge
     * GET https://dev.co3.ontomap.eu/api/v1/gamification/badges/5eba9f0fc6502572e0db2fc3
     * {
     *     "id": "5eba9f0fc6502572e0db2fc3",
     *     "name": "Silver",
     *     "active": true,
     *     "logo":"https://co3.app.com/document_creator_aca_silver.jpg"
     *     "containerReference": {
     *         "id": "5eba9f0fc6502572e0db2fc1",
     *         "name": "Document Creator ACA",
     *         "logo":"https://co3.app.com/document_creator_aca.jpg"
     *         "type": "aca"
     *     },
     *     "preconditions": {
     *         "variables": [
     *             {
     *                 "variable": "documentCreated",
     *                 "threshold": 20.0
     *             }
     *         ],
     *         "badgeList": [
     *             "5eba9f0fc6502572e0db2fc2"
     *         ]
     *     }
     * }
     *
     * @apiError (Error 400 BadRequest) not_valid_id The provided  <i>id</i> is not valid.
     * @apiError (Error 404 NotFound) badge_not_found The provided  <i>id</i> does not belong to an existing badge.
     *
     */
    public Result getBadgeInfo(String id) {
        if (!config.getBoolean("gamification.active")) return forbidden("gamification_disabled");
        if (!ObjectId.isValid(id))
            return badRequest("not_valid_id");
        ObjectId idBadge= new ObjectId(id);
        BadgeContainer<Badge> badgeContainer = (BadgeContainer<Badge>) badgeController.getBadgeContainerByBadgeId(idBadge);
        if(badgeContainer == null)
            return responseController.notFound("badge_not_found","The id provided is not belonging to a badge.");
        Badge badge = badgeController.getBadge(badgeContainer,idBadge);
        ObjectNode badgeNode = om.valueToTree(badge);
        return ok(badgeNode);
    }

    /**
     * @api {get} /gamification/badgeContainers/<idBadgeContainer> Get a Badge Container
     * @apiName Get a Badge Container
     * @apiGroup Badge APIs
     * @apiDescription Retrieves the information about a specific badge container.

     * @apiParam (Field Attributes) {String} idBadgeContainer <strong>Required.</strong> The <i>id</i> of the badge container to be retrieved.

     * @apiParamExample Get a badge Container
     * GET https://dev.co3.ontomap.eu/api/v1/gamification/badgeContainers/5eaf20506b644c21b3bb5d63
     * {
     *         "id":"5eaf20506b644c21b3bb5d63",
     *         "name": "Document Creator",
     *         "type":"individual",
     *         "badgeList": [
     *             {
     *                 "name": "Bronze",
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 10
     *                         }
     *                     ]
     *                 }
     *             },
     *             {
     *                 "name": "Silver",
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 20
     *                         }
     *                     ]
     *                 }
     *             },
     *             {
     *                 "name": "Gold",
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 30
     *                         }
     *                     ]
     *                 }
     *             }
     *         ]
     *     }
     *
     * @apiError (Error 400 BadRequest) not_valid_id The provided <i>id</i> is not valid.
     * @apiError (Error 404 NotFound) badge_container_not_found The provided <i>id</i> does not belong to an existing badge container.
     * @apiUse gamification_disabled
     *
     */
    public Result getBadgeContainerInfo(String id) {
        if (!config.getBoolean("gamification.active")) return forbidden("gamification_disabled");
        if (!ObjectId.isValid(id)) {
            return responseController.notValidId();
        }
        BadgeContainer<Badge> badgeContainer = (BadgeContainer<Badge>) badgeController.getBadgeContainerById(new ObjectId(id));
        if (badgeContainer == null)
            return responseController.notFound("badge_container_not_found","The provided id is not belonging to a badge container.");
        return ok((JsonNode) om.valueToTree(badgeContainer));
    }

    /**
     * @api {get} /gamification/badgeContainers Get All Badge Containers
     * @apiName Get All Badge Containers
     * @apiGroup Badge APIs
     * @apiDescription Returns a list of all Badge Container. This list can be filtered by type of badge container.
     *
     * @apiParamExample Get full badge container List
     * GET https://dev.co3.ontomap.eu/api/v1/gamification/badgeContainers
     *[
     *     {
     *         "id": "5eba7ee4c6502572e0db2fbc",
     *         "name": "Document Creator Ranking",
     *         "badgeList": [
     *             {
     *                 "id": "5eba7ee4c6502572e0db2fbd",
     *                 "name": "Bronze",
     *                 "active": true,
     *                 "position": 2
     *             },
     *             {
     *                 "id": "5eba7ee4c6502572e0db2fbe",
     *                 "name": "Silver",
     *                 "active": true,
     *                 "position": 1
     *             }
     *         ],
     *         "timeframe": "WEEK",
     *         "type": "ranking",
     *         "variable": "documentCreated"
     *     },
     *     {
     *         "id": "5eba9f0fc6502572e0db2fc1",
     *         "name": "Document Creator ACA",
     *         "badgeList": [
     *             {
     *                 "id": "5eba9f0fc6502572e0db2fc2",
     *                 "name": "Bronze",
     *                 "active": true,
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 1.0
     *                         }
     *                     ]
     *                 }
     *             },
     *             {
     *                 "id": "5eba9f0fc6502572e0db2fc3",
     *                 "name": "Silver",
     *                 "active": true,
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 20.0
     *                         }
     *                     ],
     *                     "badgeList": [
     *                         "5eba9f0fc6502572e0db2fc2"
     *                     ]
     *                 }
     *             },
     *             {
     *                 "id": "5eba9f0fc6502572e0db2fc4",
     *                 "name": "Gold",
     *                 "active": true,
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 30.0
     *                         }
     *                     ],
     *                     "badgeList": [
     *                         "5eba9f0fc6502572e0db2fc3"
     *                     ]
     *                 }
     *             }
     *         ],
     *         "timeframe": "WEEK",
     *         "type": "aca"
     *     }
     * ]
     *
     * @apiParam (Attributes) {String} [type]  The type of Badge Container. Can be set to <code>individual, aca or ranking</code>.
     */
    public Result getAllBadges(Http.Request request,String type) {
        String instance = request.attrs().get(Attrs.INSTANCE);
        List<BadgeContainer> badges = badgeController.getAllBadges(instance,type);
        ObjectMapper om = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(ObjectId.class, new ObjectIdSerializer());
        om.registerModule(simpleModule);
        om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        return ok((JsonNode) om.valueToTree(badges));
    }

    /*
     * @api {get} /gamification/levels Get Badge Levels Container
     * @apiName Get Badge Levels Container
     * @apiGroup Badge APIs
     * @apiDescription Returns a Badge Container that contains all badge 'Levels'.
     *
     * GET https://dev.co3.ontomap.eu/api/v1/gamification/levels
     */
    public Result getLevelsContainer(){
        IndividualBadgeContainer levels = badgeController.getBadgeLevels();
        ObjectMapper om = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(ObjectId.class, new ObjectIdSerializer());
        om.registerModule(simpleModule);
        om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        return ok((JsonNode) om.valueToTree(levels));

    }

    /**
     * @api {post} /gamification/badge/<idBadge> Enable or Disable a Badge
     * @apiName Enable or Disable a Badge
     * @apiGroup Badge APIs
     * @apiDescription Changes the <i>active</i> flag of a badge. A badge can not be disabled if it's a badge precondition of another badge.
     * A badge can not be enabled if one of its badge preconditions is disabled.
     * @apiPermission Client Certificate.
     * @apiParam (Field Attributes) {String} idBadge <strong>Required.</strong> The <i>id</i> of the badge to be enabled/disabled.
     *
     * @apiParam (Required Attributes) {Boolean} enable <strong>Required.</strong> Enable or disable the selected badge.
     *
     * @apiError (Error 400 BadRequest) badge_enable_exception The selected badge can not be enabled.
     *
     * @apiError (Error 400 BadRequest) badge_disable_exception The selected badge can not be disabled.
     *
     * @apiError (Error 400 BadRequest) not_valid_id The provided <i>id</i> is not valid.
     * @apiError (Error 404 NotFound) badge_not_found The provided <i>id</i> does not belong to an existing badge.
     *
     * @apiUse invalid_certificate
     * @apiUse gamification_disabled
     *
     */
    @AcaManagerAction.AcaManagerAuthentication
    public Result enableBadge(Http.Request request,String id,boolean enable) {
        if (!config.getBoolean("gamification.active")) return forbidden("gamification_disabled");
        if (!ObjectId.isValid(id))
            return responseController.notValidId();
        long actor = request.attrs().get(Attrs.USER_AUTHENTICATED);
        if (actor == -1){
            return responseController.userNotFound();
        }
        try {
            ObjectId idBadge = new ObjectId(id);
            BadgeContainer badgeContainer = badgeController.getBadgeContainerByBadgeId(idBadge);
            if(badgeContainer == null)
                return responseController.notFound("badge_not_found","The id provided is not belonging to a badge.");
            Badge badge = badgeController.getBadge(badgeContainer,new ObjectId(id));
            badgeController.enableBadge(badgeContainer,badge,actor, enable);
            return ok((JsonNode) om.valueToTree(badge));
        } catch (UndeactivableBadgeException e) {
            ObjectMapper objectMapper = new ObjectMapper();
            if (enable)
                return badRequest(objectMapper.createObjectNode().put(ERROR, "badge_enable_exception").put(MESSAGE, "Cannot enable this Badge due to these dependencies (BadgeLevels): " + e.getBadgeList().stream().map(ObjectId::toString).collect(Collectors.joining(", "))));
            else
                return badRequest(objectMapper.createObjectNode().put(ERROR, "badge_disable_exception").put(MESSAGE, "Cannot disable this Badge due to these dependencies (BadgeLevels): " + e.getBadgeList().stream().map(ObjectId::toString).collect(Collectors.joining(", "))));
        } catch (MongoException e) {
            return responseController.internalServerError2(MONGODB_EXCEPTION);
        }
    }

    /**
     * @api {post} /gamification/badgeContainers/<idBadgeContainer> Enable or Disable a Badge Container
     * @apiName Enable or Disable a Badge Container
     * @apiGroup Badge APIs
     * @apiDescription Changes the <i>active</i> flag of all badges inside the container. A badge can not be disabled if it's a badge precondition of another badge.
     * A badge can not be enabled if one of its badge preconditions is disabled.
     *
     * @apiParam (Field Attributes) {String} idBadgeContainer <strong>Required.</strong> The <i>id</i> of the badge container.
     *
     * @apiParam (Required Attributes) {Boolean} enable <strong>Required.</strong> Enable or disable the selected badge container.
     *
     * @apiError (Error 400 BadRequest) not_valid_id The provided <i>id</i> is not valid.
     *
     * @apiError (Error 400 BadRequest) badge_enable_exception The selected badge container can not be enabled.
     *
     * @apiError (Error 400 BadRequest) badge_disable_exception The selected badge container can not be disabled.
     *
     * @apiError (Error 404 NotFound) badge_container_not_found The provided <i>id</i> does not belong to an existing badge container.
     *
     * @apiUse invalid_certificate
     * @apiUse gamification_disabled
     * @apiPermission Client Certificate.
     */
    @AcaManagerAction.AcaManagerAuthentication
    public Result enableBadgeContainer(Http.Request request,String id,boolean enable) {
        if (!config.getBoolean("gamification.active")) return forbidden("gamification_disabled");
        if (!ObjectId.isValid(id))
            return responseController.notValidId();
        long actor = request.attrs().get(Attrs.USER_AUTHENTICATED);
        if (actor == -1){
            return responseController.userNotFound();
        }
        try {
            BadgeContainer badgeContainer = badgeController.getBadgeContainerById(new ObjectId(id));
            if(badgeContainer==null)
                return responseController.notFound("badge_container_not_found","The id provided is not belonging to a badge container.");
            badgeController.enableBadgeContainer(badgeContainer,actor, enable);
            return ok((JsonNode) om.valueToTree(badgeContainer));
        } catch (UndeactivableBadgeException e) {
            ObjectMapper objectMapper = new ObjectMapper();
            if (enable)
                return badRequest(objectMapper.createObjectNode().put(ERROR, "badge_enable_exception").put(MESSAGE, "Cannot enable this Badge due to these dependencies (BadgeList): " + e.getBadgeList().stream().map(ObjectId::toString).collect(Collectors.joining(", "))));
            else
                return badRequest(objectMapper.createObjectNode().put(ERROR, "badge_disable_exception").put(MESSAGE, "Cannot disable this Badge due to these dependencies (BadgeList): " + e.getBadgeList().stream().map(ObjectId::toString).collect(Collectors.joining(", "))));
        } catch (MongoException e) {
            return responseController.internalServerError2(MONGODB_EXCEPTION);
        }
    }


    /**
     * @api {patch} /gamification/badgeContainers Edit Badge Containers
     * @apiName Edit Badge Containers
     * @apiGroup Badge APIs
     * @apiDescription Changes the name of the BadgeContainer, the names of the badges included in the container and their logos.
     *
     * @apiParamExample Edit a Badge Container
     * PATCH https://dev.co3.ontomap.eu/api/v1/gamification/badgeContainers
     * [
     *    {
     *         "name": "Document Creator",
     *         "type":"<b>individual</b>",
     *         "badgeList": [
     *             {
     *                 "name": "Bronze",
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 10
     *                         }
     *                     ]
     *                 }
     *             },
     *             {
     *                 "name": "Silver",
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 20
     *                         }
     *                     ]
     *                 }
     *             },
     *             {
     *                 "name": "Gold",
     *                 "preconditions": {
     *                     "variables": [
     *                         {
     *                             "variable": "documentCreated",
     *                             "threshold": 30
     *                         }
     *                     ]
     *                 }
     *             }
     *         ]
     *     }
     * ]
     * @apiUse malformed_json
     * @apiUse invalid_certificate
     * @apiUse gamification_disabled
     * @apiUse wrong_content_type
     * @apiUse internal_server_error
     * @apiPermission Client Certificate.
     *
     */
    @AcaManagerAction.AcaManagerAuthentication
    public Result editBadgeContainer(Http.Request request) {
        if (!config.getBoolean("gamification.active")) return forbidden("gamification_disabled");
        String instance = request.attrs().get(Attrs.INSTANCE);
        long actor = request.attrs().get(Attrs.USER_AUTHENTICATED);
        if (actor == -1){
            return responseController.userNotFound();
        }
        try {
            JsonNode requestBody = request.body().asJson();
            try {
                JsonSchemaSupport.analyze(requestBody, environment, config.getString("gamification.json_schemas.badges"));
            } catch (InternalError error) {
                logger.error("Exception in createBadges:", error.getMessage());
                return responseController.internalServerError2(ResponseController.PARSING_ERROR);
            } catch (MalformedJsonException e) {
                if (e.getNode() != null)
                    return responseController.malformedJsonRequest(e.getNode());
                else
                    return responseController.malformedJsonRequest(e.getMessage());
            }
            ArrayNode badgesArray = (ArrayNode) requestBody;
            List<BadgeContainer<?>> badgeContainers = new ArrayList<>();
            for (JsonNode badgeJson : badgesArray) {
                try {
                    badgeContainers.add(badgeController.editBadgeContainer(instance,actor,badgeJson));
                } catch (MalformedParametersException | MongoException e) {
                    return responseController.malformedJsonRequest(e.getMessage());
                } catch (Exception e){
                    return responseController.malformedJsonRequest("Levels Badge Container already Exist");
                }
            }
            return ok((JsonNode) om.valueToTree(badgeContainers));
        } catch (MongoException e) {
            return responseController.internalServerError2(MONGODB_EXCEPTION);
        }
    }
}
