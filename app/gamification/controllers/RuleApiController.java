package gamification.controllers;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.common.base.Strings;
import com.typesafe.config.Config;
import logger.controllers.Attrs;
import logger.controllers.ResponseController;
import logger.controllers.api.v1.LoggerController;
import gamification.services.RuleService;
import gamification.models.Rule;
import gamification.supports.serializer.ObjectIdSerializer;
import logger.supports.actions.AdminAction;
import org.bson.types.ObjectId;
import play.Environment;
import play.Logger;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import logger.supports.JsonSchemaSupport;
import logger.supports.actions.AcaManagerAction;
import logger.supports.actions.MultipleInstancesAction;
import logger.supports.exceptions.MalformedJsonException;

import javax.inject.Inject;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.MalformedParametersException;
import java.util.ArrayList;
import java.util.List;

public class RuleApiController extends Controller {
    RuleService ruleController;
    Config config;
    Environment environment;
    ResponseController responseController;
    private Logger.ALogger logger;

    @Inject
    public RuleApiController(Config config, Environment environment, RuleService ruleController, ResponseController responseController){
        this.config = config;
        this.environment= environment;
        this.ruleController = ruleController;
        this.responseController= responseController;
        this.logger = new Logger.ALogger(play.api.Logger.apply(this.getClass()));
    }

    /**
     * @api {post} /gamification/rules Insert new rules
     * @apiName Insert new rules
     * @apiGroup Gamification Rules APIs
     * @apiDescription Creates and inserts into the CO3OTM Achievements Manager one or more rules.<br/>
     * The request must have a <code>application/json</code> Content-Type and must include the JSON representation of the rule(s) in its body (see provided example).<br/>
     * Rules JSON Schema can be found at <a  target="_blan" href="./api/v1/gamification/rules/schema/">Schema Link</a>
     *
     * @apiHeader (Headers){String} [access_token] It's used to retrieve the actor ID and validate the request and must belong to an ACA Manager.
     * @apiParam (Filters) {String} [actor] The CO3UUM ID of the user (ACA Manager) who is creating these rules. It is  <strong>Required</strong> if the request is performed using a valid Client Certificate.
     * @apiPermission Client Certificate.
     * @apiParamExample Creation of rules
     * POST https://dev.co3.ontomap.eu/api/v1/gamification/rules
     *
     * [
     *     {
     *         "id": "5ef1c97630d4e95532c9be7d",
     *         "activity_type": "contribution_added",
     *         "activityObjectProperties": [
     *             {
     *                 "propertyName": "hasType",
     *                 "propertyValue": "Comment"
     *             }
     *         ],
     *         "rewards": {
     *             "ACTOR": [
     *                 {
     *                     "name": "points",
     *                     "assignment": 1.0
     *                 }
     *             ]
     *         },
     *         "active": true
     *     },
     *     {
     *         "id": "5ef1c97730d4e95532c9be7e",
     *         "activity_type": "contribution_added",
     *         "activityObjectProperties": [
     *             {
     *                 "propertyName": "hasType",
     *                 "propertyValue": "Comment"
     *             }
     *         ],
     *         "rewards": {
     *             "ACTOR": [
     *                 {
     *                     "name": "Contents",
     *                     "assignment": 1.0
     *                 }
     *             ]
     *         },
     *         "active": true
     *     }
     * ]
     *
     * @apiUse malformed_json
     * @apiUse invalid_certificate
     * @apiUse wrong_content_type
     * @apiUse internal_server_error
     * @apiUse gamification_disabled
     */
    @AcaManagerAction.AcaManagerAuthentication
    @MultipleInstancesAction.MultipleInstances
    @BodyParser.Of(LoggerController.Json100Mb.class)
    public Result createRules(Http.Request request){
        if (!config.getBoolean("gamification.active")) return forbidden("Gamification disabled");
        String instance = request.attrs().get(Attrs.INSTANCE);
        long actor = request.attrs().get(Attrs.USER_AUTHENTICATED);
        if (actor == -1){
            return responseController.userNotFound();
        }
        if (!request.contentType().orElse("").equals(Http.MimeTypes.JSON)) {
            return responseController.wrongContentType();
        }
        JsonNode requestBody = request.body().asJson();
        try{
            JsonSchemaSupport.analyze(requestBody,environment,config.getString("gamification.json_schemas.rules"));
        } catch (InternalError error){
            logger.error("Exception in createRules:", error.getMessage());
            return responseController.internalServerError2(ResponseController.PARSING_ERROR);
        } catch (MalformedJsonException e) {
            if (e.getNode() != null)
                return responseController.malformedJsonRequest(e.getNode());
            else
                return responseController.malformedJsonRequest(e.getMessage());
        }
        ArrayNode rulesArray = (ArrayNode)requestBody;
        List<Rule> rules = new ArrayList<>();
        for(JsonNode ruleJson : rulesArray){
            try {
                Rule createdRule =ruleController.createRule(instance,actor,ruleJson);
                rules.add(createdRule);
            } catch (JsonProcessingException | MalformedParametersException e) {
                return responseController.malformedJsonRequest(e.getMessage());
            } catch (FileNotFoundException e){
                return responseController.internalServerError2(ResponseController.ONTOLOGY_FILE_NOT_FOUND);
            } catch (IOException e) {
                return responseController.internalServerError2(ResponseController.PARSING_ERROR);
            }
        }

        return  ok((JsonNode) new ObjectMapper().valueToTree(rules));
    }

    /**
     * @api {GET} /gamification/rules/<idRule> Get a rule
     * @apiName Get a rule
     * @apiGroup Gamification Rules APIs
     * @apiDescription Retrieves information about a rule.
     *
     * @apiParam (Field Attributes) {String} idRule <strong>Required.</strong> The <i>id</i> of the rule to be retrieved.
     *
     * @apiParamExample Get a specific rule
     * GET https://dev.co3.ontomap.eu/api/v1/gamification/rules/5ebaa075c6502572e0db2fc5
     *
     * {
     *     "id": "5ef1c97730d4e95532c9be7e",
     *     "activity_type": "contribution_added",
     *     "activityObjectProperties": [
     *         {
     *             "propertyName": "hasType",
     *             "propertyValue": "Comment"
     *         }
     *     ],
     *     "rewards": {
     *         "ACTOR": [
     *             {
     *                 "name": "Contents",
     *                 "assignment": 1.0
     *             }
     *         ]
     *     },
     *     "active": true
     * }
     *
     * @apiError (Error 400 BadRequest) not_valid_id The provided <i>id</i> is not valid.
     * @apiError (Error 404 NotFound) rule_not_found The provided <i>id</i> does not belong to an existing rule.
     * @apiUse gamification_disabled
     */
    public Result getRuleInfo(String id){
        if (!config.getBoolean("gamification.active")) return forbidden("gamification_disabled");
        if(!ObjectId.isValid(id))
            return badRequest("not_valid_id");
        Rule rule = ruleController.getRuleById(new ObjectId(id));
        if(rule == null){
            return notFound("rule_not_found");
        }
        ObjectMapper objectMapper = new ObjectMapper();
        return ok((JsonNode) objectMapper.valueToTree(rule));
    }

    /**
     * @api {GET} /gamification/rules Get all rules
     * @apiName Get all rules
     * @apiGroup Gamification Rules APIs
     * @apiDescription Retrieves all rules in the CO3OTM Achievements Manager.
     *
     * @apiParam (Filters) {String} [variable] Filter rules by variable reward.
     *
     * @apiParamExample Get all Rules
     * GET https://dev.co3.ontomap.eu/api/v1/gamification/rules
     * [
     *     {
     *         "id": "5ef1c97630d4e95532c9be7d",
     *         "activity_type": "contribution_added",
     *         "activityObjectProperties": [
     *             {
     *                 "propertyName": "hasType",
     *                 "propertyValue": "Comment"
     *             }
     *         ],
     *         "rewards": {
     *             "ACTOR": [
     *                 {
     *                     "name": "points",
     *                     "assignment": 1.0
     *                 },
     *                 {
     *                     "name": "contents",
     *                     "assignment": 1.0
     *                  }
     *             ]
     *         },
     *         "active": true
     *     },
     *     {
     *         "id": "5ef1c97730d4e95532c9be7e",
     *         "activity_type": "contribution_added",
     *         "activityObjectProperties": [
     *             {
     *                 "propertyName": "hasType",
     *                 "propertyValue": "Photo"
     *             }
     *         ],
     *         "rewards": {
     *             "ACTOR": [
     *                 {
     *                     "name": "photos",
     *                     "assignment": 1.0
     *                 }
     *             ]
     *         },
     *         "active": true
     *     }
     * ]
     */
    @MultipleInstancesAction.MultipleInstances
    public Result getAllRules(Http.Request request, String variable){
        String instance = request.attrs().get(Attrs.INSTANCE);
        List<Rule> rules = Strings.isNullOrEmpty(variable) ? ruleController.getRules(instance) : ruleController.getRules(instance,variable);
        ObjectMapper om = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(ObjectId.class, new ObjectIdSerializer());
        om.registerModule(simpleModule);
        om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        return ok((JsonNode) om.valueToTree(rules));
    }

    /**
     * @api {POST} /gamification/rules/<idRule> Enables or disables a rule
     * @apiName Enables or disables a rule
     * @apiGroup Gamification Rules APIs
     * @apiDescription Enables or disables a Gamification rule.
     *
     * @apiHeader (Headers){String} [access_token] It's used to retrieve the actor ID and validate the request and must belong to an ACA Manager.
     * @apiParam (Field Attributes) {String} [actor] The CO3UUM ID of the user (ACA Manager) who is updating the rule. It is  <strong>Required</strong> if the request is performed using a valid Client Certificate.
     * @apiParam (Field Attributes) {String} idRule <strong>Required.</strong> The <i>id</i> of the rule.
     * @apiParam (Required Attributes) {Boolean} enable <strong>Required.</strong> Enable or disable the selected rule.
     *
     * @apiUse not_valid_id
     * @apiUse gamification_disabled
     * @apiUse invalid_certificate
     * @apiPermission Client Certificate.
     */
    @AcaManagerAction.AcaManagerAuthentication
    public Result enableRule(Http.Request request,String id,boolean enable){
        if (!config.getBoolean("gamification.active")) return forbidden("gamification_disabled");
        if(!ObjectId.isValid(id))
            return badRequest("not_valid_id");
        long actor = request.attrs().get(Attrs.USER_AUTHENTICATED);
        if (actor == -1){
            return responseController.userNotFound();
        }
        ruleController.enableRule(new ObjectId(id),actor,enable);
        return ok();
    }


    @AdminAction.AdminAuthentication
    public Result getProperties(){
        ObjectMapper om = new ObjectMapper();
        return ok((JsonNode) om.valueToTree(this.ruleController.getRuleProperties()));
    }
}
