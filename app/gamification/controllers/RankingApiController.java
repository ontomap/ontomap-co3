package gamification.controllers;

import akka.japi.Pair;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import logger.controllers.Attrs;
import logger.controllers.ResponseController;
import gamification.services.RankingService;
import gamification.models.Ranking;
import gamification.models.badges.TemporalBadgeContainer;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import logger.supports.actions.MultipleInstancesAction;
import logger.supports.actions.UserTokenAuthenticatedAction;

import javax.inject.Inject;

@MultipleInstancesAction.MultipleInstances
public class RankingApiController extends Controller {
    private RankingService rankingController;
    private ResponseController responseController;

    @Inject
    public RankingApiController(RankingService rankingController, ResponseController responseController){
        this.responseController = responseController;
        this.rankingController = rankingController;
    }

    /**
     * @api {get} /gamification/ranking Get Variable Ranking
     * @apiName Get Variable Ranking
     * @apiGroup Gamification Ranking APIs
     * @apiDescription Returns the Gamification Ranking on the specified variable. A ranking can be weekly or monthly.
     *
     * The result is an object that contains:
     *  * The users' current positions in the ranking - paginated by 30 entries per page and cached for 5 minutes.
     *  * The time of the ranking update.
     *  * The date when this ranking closes. The closure date is the upcoming Monday for weekly rankings while it is the first day of the upcoming month for monthly rankings.
     *
     * @apiParam (Filters) {String} variable <strong>Required</strong> It specifies the variable of the ranking.
     * @apiParam (Filters) {String} [timeframe=WEEK] It can be <code>WEEK</code> or <code>MONTH</code>. It's used to select the weekly or monthly ranking.
     * @apiParam (Filters) {String} [aca] The ranking is calculated only for events done in that ACA.
     * @apiParam (Filters) {String} [page] The page of the ranking entries to be returned.
     *
     * @apiParamExample Get "points" weekly ranking
     * https://dev.co3.ontomap.eu/api/v1/gamification/ranking?variable=points
     *{
     *     "variable": "points",
     *     "timeframe": "WEEK",
     *     "rankList": [
     *         {
     *             "actor": 92,
     *             "value": 50,
     *             "position": 1
     *         },
     *         {
     *             "actor": 12,
     *             "value": 20,
     *             "position": 2
     *         },
     *         {
     *             "actor": 30,
     *             "value": 20,
     *             "position": 2
     *         },
     *         {
     *             "actor": 36,
     *             "value": 10,
     *             "position": 4
     *         },
     *         {
     *             "actor": 37,
     *             "value": 0,
     *             "position": 5
     *         }
     *     ],
     *     "totalCount": 6,
     *     "endingDate": 1589148000000,
     *     "lastUpdate": 1588695982000,
     *     "page": 1,
     *     "pageSize": 30
     * }
     *
     * @apiUse invalid_timeframe
     */
    public Result getRanking(Http.Request request,String variable,String timeframeName,String aca,int page, int pageSize){
        String instance = request.attrs().get(Attrs.INSTANCE);
        TemporalBadgeContainer.Timeframe timeframe;
        try{
            timeframe = TemporalBadgeContainer.Timeframe.valueOf(timeframeName.toUpperCase());
        } catch (Exception e){
            return badRequest("invalid_timeframe");
        }
        Ranking ranking = rankingController.getRanking(instance,aca,timeframe,variable,page,pageSize);
        ObjectMapper om = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        om.registerModule(simpleModule);
        om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        ObjectNode rankingJson = om.valueToTree(ranking);
        rankingJson.put("lastUpdate",ranking.getId().getDate().getTime());
        if (page > -1  && pageSize > -1) {
            rankingJson.put("page", page);
            rankingJson.put("pageSize", pageSize);
        }
        return ok(rankingJson);
    }

    /**
     * @api {get} /gamification/rank Get a User Rank
     * @apiName Get a User Rank
     * @apiGroup Gamification Ranking APIs
     * @apiDescription Returns the Gamification Rank of a specific user on the specified variable. A ranking can be weekly or monthly.
     *
     * The result is an object that contains:
     *  * The users' current positions in the ranking - paginated by 30 entries per page and cached for 5 minutes.
     *  * The time of the ranking update.
     *  * The date when this ranking closes. The closure date is the upcoming Monday for weekly rankings while it is the first day of the upcoming month for monthly rankings.
     *
     * @apiHeader (Headers){String} [access_token] It's used to retrieve the actor ID and validate the request.
     *
     * @apiParam (Filters) {String} [actor] The CO3UUM ID of the user. It is  <strong>Required</strong> if the request is performed using a valid Client Certificate.
     * @apiParam (Filters) {String} variable <strong>Required</strong> It specifies the variable of the ranking.
     * @apiParam (Filters) {String} [timeframe=WEEK] It can be <code>WEEK</code> or <code>MONTH</code>. It's used to select the weekly or monthly ranking.
     * @apiParam (Filters) {String} [aca] The ranking is calculated only for events done in that ACA.
     *
     * @apiParamExample Get "points" weekly ranking
     * https://dev.co3.ontomap.eu/api/v1/gamification/rank?variable=points
     *{
     *     "variable": "points",
     *     "timeframe": "WEEK",
     *     "endingDate": 1589148000000,
     *     "lastUpdate": 1588695982000,
     *     "rank": {
     *         "actor": 92,
     *         "value": 50,
     *         "position": 1
     *     }
     * }
     * @apiUse invalid_timeframe
     * @apiPermission Client Certificate, access_token.
     */
    @UserTokenAuthenticatedAction.UserTokenAuthentication(operationType = UserTokenAuthenticatedAction.OperationType.READ)
    public Result getSingleRank(Http.Request request,String aca,String variable,String timeframeName){
        ObjectMapper om = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        om.registerModule(simpleModule);
        om.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        long actor = request.attrs().get(Attrs.USER_AUTHENTICATED);
        String instance = request.attrs().get(Attrs.INSTANCE);
        if(actor == -1)
            return responseController.userNotFound();
        TemporalBadgeContainer.Timeframe timeframe = TemporalBadgeContainer.Timeframe.WEEK;
        try{
            timeframe = TemporalBadgeContainer.Timeframe.valueOf(timeframeName);
        } catch (Exception e){

        }
        Pair<Ranking, Ranking.Rank> rankingRankPair = rankingController.getSingleRank(instance,aca,actor,variable,timeframe);
        ObjectNode objectNode = om.valueToTree(rankingRankPair.first());
        objectNode.remove("rankList");
        objectNode.remove("size");
        objectNode.put("lastUpdate",rankingRankPair.first().getId().getDate().getTime());
        objectNode.putPOJO("rank",rankingRankPair.second());
        return ok(objectNode);
    }
}
