package gamification.controllers;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import logger.controllers.Attrs;
import logger.controllers.ResponseController;
import gamification.services.GamificationLoggerService;
import gamification.supports.serializer.ObjectIdSerializer;
import logger.models.PaginatedResponse;
import logger.supports.actions.AdminAction;
import org.bson.types.ObjectId;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import logger.supports.actions.AcaManagerAction;
import logger.supports.actions.UserTokenAuthenticatedAction;


import javax.inject.Inject;
import java.util.Date;

import static play.mvc.Results.ok;

public class LoggerAssignmentApiController extends Controller {
    @Inject
    GamificationLoggerService loggerAssignmentController;
    @Inject
    ResponseController responseController;
    private final String pilotName;

    @Inject
    public LoggerAssignmentApiController(Config config) {
        this.pilotName = config.getString("gamification.allACAProfile");
    }

    /**
     * @api {get} /gamification/logs Get User Logs
     * @apiName Get User Logs
     * @apiGroup Gamification Logs APIs
     * @apiDescription Returns the Gamification log list of a User Achievements Profile.
     * @apiHeader (Headers){String} [access_token] It's used to retrieve the actor ID and validate the request.
     * @apiParam (Filters) {String} [actor] The CO3UUM ID of the user owner of these gamification logs. It is  <strong>Required</strong> if the request is performed using a valid Client Certificate.
     * @apiParam (Filters) {String} [aca] Returns only the logs concerning the actor's events occurred in that aca.
     * @apiParam (Filters) {String} [group] Returns only the logs concerning the actor's events occurred in that group (aca Required in this case).
     * @apiParam (Filters) {long} [startTime] Returns only the logs concerning the actor's events having a timestamp greater than or equal to the specified timestamp (Unix timestamp in ms).
     * @apiParam (Filters) {long} [endTime] Returns only the logs concerning the actor's events having a timestamp less than or equal to the specified timestamp (Unix timestamp in ms).
     * @apiParam (Filters) {String} [viewMode=AGGREGATE] Can be <code>SINGLE</code> or <code>AGGREGATE</code>.
     * * If <code>viewMode=SINGLE</code> retrieves the logs of the specified user achievements profile.
     * * If <code>viewMode=AGGREGATE</code> retrieves the fusion of the logs concerning the hierarchy of user achievements profiles having the specified one as its root.
     * @apiParam (Filters) {Boolean} [rules=true] Specifies whether the retrieved logs must contain the entries about applied rules.
     * @apiParam (Filters) {Boolean} [badges=true]Specifies whether the retrieved logs must contain the entries about assigned badges.
     * @apiParam (Filters) {Boolean} [resolve=false] If <code>true</code> the system returns the description (structure) of the badge/rule instead of the badge/rule id
     * @apiError (Error 400 BadRequest) not_valid_view_mode The <code>viewMode</code> selected as filter is not valid .
     * @apiError (Error 400 BadRequest) no_aca_specified Group specified without an ACA.
     * @apiUse invalid_certificate
     * @apiParamExample Get Gamification Logs of a User
     * GET https://dev.co3.ontomap.eu/api/v1/gamification/logs?page=1&pageSize=1&rules=false
     * {
     * "results": [
     * {
     * "id": "5fb3a7063471d2759ca976af",
     * "actor": 1,
     * "aca": "https://api.co3-dev.firstlife.org/v6/fl/Things/5eb133d99ccf2b2e55ae1ebc",
     * "idBadge": "5ed76e90dbe6fc58300f8ec1",
     * "timestamp": 1605609222407
     * }
     * ],
     * "totalCount": 714,
     * "page": 1,
     * "pagesize": 1
     * }
     * @apiParamExample Get Gamification Logs "Resolved" of a User
     * GET https://dev.co3.ontomap.eu/api/v1/gamification/logs?page=1&pageSize=1&resolve=true&rules=false
     * {
     * "results": [
     * {
     * "id": "5fb3a7063471d2759ca976af",
     * "actor": 1,
     * "aca": "https://api.co3-dev.firstlife.org/v6/fl/Things/5eb133d99ccf2b2e55ae1ebc",
     * "timestamp": 1605609222407,
     * "badge": {
     * "id": "5ed76e90dbe6fc58300f8ec1",
     * "name": "Silver",
     * "active": true,
     * "containerReference": {
     * "id": "5ed76e90dbe6fc58300f8ebf",
     * "name": "CO.management",
     * "type": "aca"
     * },
     * "preconditions": {
     * "variables": [
     * {
     * "variable": "Activisms",
     * "threshold": 5.0
     * }
     * ],
     * "badgeList": [
     * "5ed76e90dbe6fc58300f8ec0"
     * ]
     * }
     * }
     * }
     * ],
     * "totalCount": 714,
     * "page": 1,
     * "pagesize": 1
     * }
     * @apiPermission Client Certificate, access_token.
     */
    @UserTokenAuthenticatedAction.UserTokenAuthentication
    public Result getLogList(Http.Request request, String aca, String group, long startTime, long endTime, String variable, String viewModeString, boolean rules, boolean badges, boolean resolve, int page, int pagesize) {
        long actor = request.attrs().get(Attrs.USER_AUTHENTICATED);
        if (actor == -1)
            return responseController.userNotFound();
        return this.getLogListAdmin(request, actor, aca, group, startTime, endTime, variable, viewModeString, rules, badges, resolve, page, pagesize);

    }

    /**
     * @api {get} /gamification/admin/logs Get Users Logs
     * @apiName Get Users Logs
     * @apiGroup Gamification Logs APIs
     * @apiDescription Returns the Gamification log list of all User Achievements Profile.
     * @apiHeader (Headers){String} [access_token] It's used to retrieve the actor ID and validate the request.
     * @apiParam (Filters) {String} [actor] The CO3UUM ID of the user owner of these gamification logs. It is  <strong>Required</strong> if the request is performed using a valid Client Certificate.
     * @apiParam (Filters) {String} [aca] Returns only the logs concerning the actor's events occurred in that aca.
     * @apiParam (Filters) {String} [group] Returns only the logs concerning the actor's events occurred in that group (aca Required in this case).
     * @apiParam (Filters) {long} [startTime] Returns only the logs concerning the actor's events having a timestamp greater than or equal to the specified timestamp (Unix timestamp in ms).
     * @apiParam (Filters) {long} [endTime] Returns only the logs concerning the actor's events having a timestamp less than or equal to the specified timestamp (Unix timestamp in ms).
     * @apiParam (Filters) {String} [variable] Returns only the logs concerning rules have that variable as reward.
     * @apiParam (Filters) {String} [viewMode=AGGREGATE] Can be <code>SINGLE</code> or <code>AGGREGATE</code>.
     * * If <code>viewMode=SINGLE</code> retrieves the logs of the specified user achievements profile.
     * * If <code>viewMode=AGGREGATE</code> retrieves the fusion of the logs concerning the hierarchy of user achievements profiles having the specified one as its root.
     * @apiParam (Filters) {Boolean} [rules=true] Specifies whether the retrieved logs must contain the entries about applied rules.
     * @apiParam (Filters) {Boolean} [badges=true]Specifies whether the retrieved logs must contain the entries about assigned badges.
     * @apiParam (Filters) {Boolean} [resolve=false] If <code>true</code> the system returns the description (structure) of the badge/rule instead of the badge/rule id
     * @apiError (Error 400 BadRequest) not_valid_view_mode The <code>viewMode</code> selected as filter is not valid .
     * @apiError (Error 400 BadRequest) no_aca_specified Group specified without an ACA.
     * @apiUse invalid_certificate
     * @apiParamExample Get Gamification Logs of a User
     * GET https://dev.co3.ontomap.eu/api/v1/gamification/admin/logs?page=0&pageSize=1&resolved=true
     * [
     *     "results": [
     *         {
     *             "id": "5fb3b1523471d2759ca976e3",
     *             "actor": 93,
     *             "aca": "https://api.co3-dev.firstlife.org/v6/fl/Things/5eb133d99ccf2b2e55ae1ebc",
     *             "event": {
     *                 "id": "5fb3b1523471d2759ca976e1",
     *                 "application": "co3.firstlife.org",
     *                 "instance": "dev.co3.ontomap.eu",
     *                 "actor": 93,
     *                 "timestamp": 1605611858415,
     *                 "aca": "https://api.co3-dev.firstlife.org/v6/fl/Things/5eb133d99ccf2b2e55ae1ebc",
     *                 "details": {},
     *                 "references": [
     *                     {
     *                         "application": "co3.firstlife.org",
     *                         "external_url": "https://api.co3-dev.firstlife.org/v6/fl/Things/5fb3b151fc3c08384f650f7c"
     *                     }
     *                 ],
     *                 "activity_type": "contribution_added",
     *                 "activity_objects": [
     *                     {
     *                         "geometry": {
     *                             "type": "Point",
     *                             "coordinates": [
     *                                 7.742615222930908,
     *                                 45.11617660522461
     *                             ]
     *                         },
     *                         "properties": {
     *                             "hasType": "Photo",
     *                             "external_url": "https://api.co3-dev.firstlife.org/v6/fl/flresources/5fb3b152c38696634800d250",
     *                             "hasName": "name",
     *                             "hasFileId": "5f7725a73543064179920592",
     *                             "hasFileType": "image/jpeg"
     *                         },
     *                         "externalURL": "https://api.co3-dev.firstlife.org/v6/fl/flresources/5fb3b152c38696634800d250",
     *                         "hasType": "Photo",
     *                         "current_visibility": "visible"
     *                     }
     *                 ]
     *             },
     *             "rule": {
     *                 "id": "5ef1c97730d4e95532c9be7e",
     *                 "activity_type": "contribution_added",
     *                 "activityObjectProperties": [
     *                     {
     *                         "propertyName": "hasType",
     *                         "propertyValue": "Photo"
     *                     }
     *                 ],
     *                 "rewards": {
     *                     "ACTOR": [
     *                         {
     *                             "name": "Contents",
     *                             "assignment": 1.0
     *                         }
     *                     ]
     *                 },
     *                 "active": true
     *             },
     *             "role": "ACTOR",
     *             "timestamp": 1605611858415
     *         }
     *     ],
     *     "totalCount": 7504,
     *     "page": 1,
     *     "pagesize": 1
     * ]
     * @apiPermission Client Certificate, access_token (belong to an ACA Manager).
     */
    @AcaManagerAction.AcaManagerAuthentication
    public Result getLogListAdmin(Http.Request request, long actor, String aca, String group, long startTime, long endTime, String variable, String viewModeString, boolean rules, boolean badges, boolean resolve, int page, int pagesize) {
        String instance = request.attrs().get(Attrs.INSTANCE);
        return this.getLogList(instance, actor, aca, group, startTime, endTime, variable, viewModeString, rules, badges, resolve, page, pagesize);
    }

    public Result getLogList(String instance, long actor, String aca, String group, long startTime, long endTime, String variable, String viewModeString, boolean rules, boolean badges, boolean resolve, int page, int pagesize) {
        if (aca != null && (aca.equals("null") || aca.toLowerCase().equals(this.pilotName))) {
            aca = null;
        }
        if (group != null && group.equals("null")) {
            group = null;
        }
        if (variable != null && variable.equals("null")) {
            variable = null;
        }
        if (aca == null && group != null) {
            ObjectNode r = new ObjectMapper().createObjectNode();
            r.put("error", "no_aca_specified");
            return badRequest(r);
        }
        ObjectMapper om = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(ObjectId.class, new ObjectIdSerializer());
        om.registerModule(simpleModule);
        om.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        Date dateStart = startTime < 0 ? null : new Date(startTime);
        Date dateEnd = endTime < 0 ? null : new Date(endTime);
        ViewMode viewMode;
        try {
            viewMode = ViewMode.valueOf(viewModeString.toUpperCase());
        } catch (IllegalArgumentException e) {
            ObjectNode r = new ObjectMapper().createObjectNode();
            r.put("error", "not_valid_view_mode");
            return badRequest(r);
        }
        PaginatedResponse searchResults = loggerAssignmentController.getLogs(instance, viewMode, actor, aca, group, dateStart, dateEnd, variable, rules, badges, resolve, page, pagesize);
        ObjectNode objectNode = om.valueToTree(searchResults);
        if (page > -1 && pagesize > -1) {
            objectNode.put("page", page);
            objectNode.put("pagesize", pagesize);
        }
        return ok(objectNode);
    }
}
