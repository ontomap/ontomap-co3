package gamification.controllers;

import akka.actor.ActorRef;
import akka.actor.Props;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import gamification.models.WebSocketActor;

import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class NotificationController{
    private Map<Long, Map<Channel,ActorRef>> usersRef;

    public NotificationController(){
        this.usersRef = new HashMap<>();
    }

    public void removeActor(long actor){
        this.usersRef.remove(actor);
    }

    /**
     * Create a Channel that allows to send notification to certain user using WebSocket
     * @param actor
     * @param channel
     * @param actorRef
     * @return
     */
    public Props addActor(long actor, Channel channel, ActorRef actorRef){
        Props props = WebSocketActor.props(actorRef,actor,this);
        if(!usersRef.containsKey(actor)){
            usersRef.put(actor, new HashMap<>());
        }
        Map<Channel,ActorRef> channels = usersRef.get(actor);
        channels.put(channel,actorRef);
        return props;
    }

    /**
     * Send a notification (msg) to an actor if he subscribed that channel
     * @param actor
     * @param channel
     * @param msg
     */
    public void sendNotification(long actor,Channel channel, JsonNode msg){
        ActorRef out = getRef(actor,channel);
        if(out!= null) {
            out.tell(msg, ActorRef.noSender());
        }
    }

    private ActorRef getRef(long actor, Channel channel){
        Map<Channel,ActorRef> channels = usersRef.get(actor);
        return  channels == null ? null : channels.get(channel);
    }

    /**
     * Send a message to all user;
     * @param objectNode
     */
    public void sendBroadcast(ObjectNode objectNode) {
        for(Map<Channel,ActorRef> channelActorRefMap : usersRef.values()){
            for(ActorRef ref : channelActorRefMap.values()){
                if(ref!=null){
                    ref.tell(objectNode, ActorRef.noSender());
                }
            }
        }
    }


    public enum Channel{
        LOGGER,
        GAMIFICATION
    }


}
