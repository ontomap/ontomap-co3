package gamification.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import gamification.models.badges.BadgeStep;
import gamification.models.profiles.users.MainPersona;
import gamification.services.BadgeService;
import logger.controllers.Attrs;
import logger.controllers.ResponseController;
import gamification.services.UserProfileService;
import gamification.models.profiles.users.UserProfile;
import gamification.supports.serializer.UserProfileSerializer;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import logger.supports.actions.UserTokenAuthenticatedAction;

import javax.inject.Inject;

public class UserProfileApiController extends Controller {
    @Inject
    UserProfileService userProfileController;

    @Inject
    BadgeService badgeService;

    @Inject
    ResponseController responseController;


    /**
     * @api {GET} /gamification/profile Retrieves User Achievements Profiles
     * @apiName Retrieves User Achievements Profiles
     * @apiGroup Gamification Achievements Profiles APIs
     * @apiDescription This API offers three possible uses:
     *  * HIERARCHY: Retrieves the subtree of the specified user achievements profile.
     *  * SINGLE: Retrieves the specified user achievements profile.
     *  * AGGREGATE: Retrieves the aggregated information for the specified user achievements profile (in the following denoted as U). This is the sum of achievements stored in the subtree of the hierarchy having U as root.
     * U can be a user main profile (actor = CO3UUM ID, aca= null, group = null), an ACA (actor = CO3UUM ID, aca= ACA_NAME, group = null) or a group (actor = CO3UUM ID, aca= ACA_NAME, group = GROUP_NAME).
     *
     * @apiParamExample Get user profile HIERARCHY
     * GET https://dev.co3.ontomap.eu/api/v1/gamification/profile/user?actor=92&viewmode=HIERARCHY
     *{
     *     "acaPersonas": [
     *         {
     *             "aca": "aca3",
     *             "groupPersonas": [],
     *             "badges": {
     *                 "currentBadges": [
     *                     {
     *                         "idBadge": "5eba9f0fc6502572e0db2fc2",
     *                         "assignedDate": 1589378722000,
     *                         "expiredDate": 1590357600000
     *                     }
     *                 ],
     *                 "removedBadges": []
     *             },
     *             "variables": {
     *                 "points": 40.0
     *             },
     *             "variablesMaxValue": {
     *                 "points": 40.0
     *             },
     *             "temporalVariables": {
     *                 "MONTH": {
     *                     "documentCreated": 4.0,
     *                     "points": 40.0
     *                 },
     *                 "WEEK": {
     *                     "documentCreated": 4.0,
     *                     "points": 40.0
     *                 }
     *             }
     *         }
     *     ],
     *     "badges": {
     *         "currentBadges": [],
     *         "removedBadges": []
     *     },
     *     "variables": {
     *         "points": 30.0
     *     },
     *     "variablesMaxValue": {
     *         "points": 30.0
     *     },
     *     "temporalVariables": {
     *         "MONTH": {
     *             "documentCreated": 3.0,
     *             "points": 30.0
     *         },
     *         "WEEK": {
     *             "documentCreated": 3.0,
     *             "points": 30.0
     *         }
     *     },
     *     "actor": 92,
     *     "view_mode": "full"
     * }
     *
     * @apiParamExample Get SINGLE user profile
     * GET https://dev.co3.ontomap.eu/api/v1/gamification/profile/user?actor=92&viewmode=single
     * {
     *     "badges": {
     *         "currentBadges": [],
     *         "removedBadges": []
     *     },
     *     "variables": {
     *         "points": 30.0
     *     },
     *     "variablesMaxValue": {
     *         "points": 30.0
     *     },
     *     "temporalVariables": {
     *         "MONTH": {
     *             "documentCreated": 3.0,
     *             "points": 30.0
     *         },
     *         "WEEK": {
     *             "documentCreated": 3.0,
     *             "points": 30.0
     *         }
     *     },
     *     "actor": 92,
     *     "view_mode": "single"
     * }
     *
     * @apiParamExample Get AGGREGATE user profile
     * GET https://dev.co3.ontomap.eu/api/v1/gamification/profile/user?actor=92&viewmode=aggregate
     *{
     *     "badges": {
     *         "currentBadges": [
     *             {
     *                 "idBadge": "5eba9f0fc6502572e0db2fc2",
     *                 "assignedDate": 1589378722000,
     *                 "expiredDate": 1590357600000
     *             }
     *         ],
     *         "removedBadges": []
     *     },
     *     "variables": {
     *         "points": 70.0
     *     },
     *     "variablesMaxValue": {
     *         "points": 70.0
     *     },
     *     "temporalVariables": {
     *         "MONTH": {
     *             "documentCreated": 7.0,
     *             "points": 70.0
     *         },
     *         "WEEK": {
     *             "documentCreated": 7.0,
     *             "points": 70.0
     *         }
     *     },
     *     "actor": 92,
     *     "view_mode": "aggregate"
     * }
     * @apiError (Error 403 Forbidden) no_access_token No valid access token provided
     * @apiPermission Client Certificate, access_token.
     */
    @UserTokenAuthenticatedAction.UserTokenAuthentication(operationType = UserTokenAuthenticatedAction.OperationType.READ)
    public Result getUserProfile(Http.Request request,String aca,String group,String mode){
        ObjectMapper om = new ObjectMapper();
        long actor = request.attrs().get(Attrs.USER_AUTHENTICATED);
        String instance = request.attrs().get(Attrs.INSTANCE);
        if(actor == -1)
            return responseController.userNotFound();
        MainPersona mainPersona = userProfileController.getUserMainProfile(instance,actor);
        BadgeStep level = badgeService.getUserLevel(mainPersona);
        mainPersona.setBadgeLevel(level);
        UserProfile userProfile = mainPersona.getPersona(aca,group);
        if(userProfile==null) {
            ObjectNode r = om.createObjectNode();
            r.put("error", "user_persona_not_found");
            return notFound(r);
        }
        ViewMode viewMode;
        try{
            viewMode = ViewMode.valueOf(mode.toUpperCase());
        } catch (IllegalArgumentException e){
            ObjectNode r = om.createObjectNode();
            r.put("error","not_valid_view_mode");
            return badRequest(r);
        }
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addSerializer(UserProfile.class,new UserProfileSerializer(viewMode));
        om.registerModule(simpleModule);
        ObjectNode objectNode =  om.valueToTree(userProfile);
        objectNode.put("actor",actor);
        if(aca!=null)
            objectNode.put("aca",aca);
        if(group!=null)
            objectNode.put("group",group);
        objectNode.put("view_mode",viewMode.name().toLowerCase());
        return ok(objectNode);
    }


}
