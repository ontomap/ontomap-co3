package gamification.controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import logger.controllers.Attrs;
import gamification.models.profiles.AcaProfile;
import gamification.services.AcaService;
import play.mvc.Http;
import play.mvc.Result;
import logger.supports.actions.UserTokenAuthenticatedAction;

import javax.inject.Inject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static play.mvc.Results.ok;

@UserTokenAuthenticatedAction.UserTokenAuthentication
public class AcaController {

    @Inject
    AcaService acaService;

    /**
     * @api {GET} /gamification/profile/aca Retrieves ACA Achievements Profiles
     * @apiName Retrieves ACA Achievements Profiles
     * @apiGroup Gamification Achievements Profiles APIs
     * @apiDescription Retrieves information about a specified ACA Profile.
     * @apiParamExample Get ACA profile
     * GET https://dev.co3.ontomap.eu/api/v1/gamification/profile/aca?aca=https://api.co3-dev.firstlife.org/v6/fl/Things/5eb17fec6bd1ca539013a507
     * [
     *     {
     *         "temporalVariableContainers": [
     *             {
     *                 "timeframe": "MONTH",
     *                 "start": 1604185200000,
     *                 "end": 1606777199999,
     *                 "variables": {
     *                     "Initiatives": 4.0,
     *                     "Groupjoins": 1.0,
     *                     "Contents": 3.0,
     *                     "Activisms": 4.0,
     *                     "Acticreas": 4.0,
     *                     "points": 164.0
     *                 }
     *             }
     *         ],
     *         "instance": "dev.co3.ontomap.eu",
     *         "url": "https://api.co3-dev.firstlife.org/v6/fl/Things/5eb17fec6bd1ca539013a507",
     *         "badges": {
     *             "currentBadges": [],
     *             "removedBadges": [
     *                 {
     *                     "idBadge": "5ed76e90dbe6fc58300f8ec0",
     *                     "assignedDate": 1599226951894,
     *                     "expiredDate": 1600034400000
     *                 },
     *                 {
     *                     "idBadge": "5ed76e90dbe6fc58300f8ec0",
     *                     "assignedDate": 1604226976071,
     *                     "expiredDate": 1604876400000
     *                 }
     *             ]
     *         }
     *     }
     * ]
     *
     * @apiError (Error 403 Forbidden) no_access_token No valid access token provided
     * @apiPermission Client Certificate, access_token.
     */
    public Result getAcaProfile(Http.Request request, String aca, long startTime, long endTime) {
        String instance = request.attrs().get(Attrs.INSTANCE);
        List<AcaProfile> acaProfileList = new ArrayList<>();
        if(aca != null)
            acaProfileList.add(acaService.getAcaProfile(instance, aca, false));
        else
            acaProfileList = acaService.getAcaProfile(instance);
        Date startDate = null, endDate = null;
        if (startTime > -1) {
            startDate = new Date(startTime);
        }
        if (endTime > -1) {
            endDate = new Date(endTime);
        }
        for(AcaProfile acaProfile: acaProfileList){
            if (startDate != null || endDate != null) {
                acaProfile.setTemporalVariableContainers(acaProfile.getTemporalVariableContainers(startDate, endDate));
            }
        }
        ObjectMapper om = new ObjectMapper();
        return ok((JsonNode) om.valueToTree(acaProfileList));
    }

}
