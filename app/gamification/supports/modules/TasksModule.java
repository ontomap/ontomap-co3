package gamification.supports.modules;

import com.google.inject.AbstractModule;
import gamification.supports.tasks.MonthlyTasks;
import gamification.supports.tasks.TestTask;
import gamification.supports.tasks.WebSocketKeepAliveTask;
import gamification.supports.tasks.WeeklyTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TasksModule extends AbstractModule {
    private Logger logger;

    public TasksModule() {
        logger = LoggerFactory.getLogger(this.getClass());
    }

    @Override
    protected void configure() {
        logger.debug("Binding Tasks...");
        bind(MonthlyTasks.class).asEagerSingleton();
        bind(WeeklyTask.class).asEagerSingleton();
        bind(WebSocketKeepAliveTask.class).asEagerSingleton();

//       bind(TestTask.class).asEagerSingleton();//TODO DELETE
    }
}
