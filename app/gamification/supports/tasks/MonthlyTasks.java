package gamification.supports.tasks;

import akka.actor.ActorSystem;
import gamification.controllers.NotificationController;
import gamification.services.GamificationService;
import gamification.services.UserProfileService;
import gamification.models.badges.TemporalBadgeContainer;
import scala.concurrent.ExecutionContext;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;
import logger.supports.TimeUtils;
import logger.services.MongoDBController;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MonthlyTasks extends TimeframeTask {


    @Inject
    public MonthlyTasks(ActorSystem actorSystem, ExecutionContext executionContext, GamificationService gamificationController, UserProfileService userProfileController, MongoDBController mongoDBController, NotificationController notificationController) {
        super(actorSystem, executionContext, gamificationController, userProfileController, mongoDBController, notificationController, TemporalBadgeContainer.Timeframe.MONTH);
    }


    protected void initialize() {
        actorSystem
                .scheduler()
                .scheduleOnce(
                        nextFirstOftheMonth(), // initialDelay
                        new Runnable() {
                            @Override
                            public void run() {
                                logger.info("MONTHLY TASK STARTED");
                                taskJob();
                                actorSystem
                                        .scheduler()
                                        .scheduleOnce(
                                                nextFirstOftheMonth(), // initialDelay
                                                this,
                                                executionContext);
                            }
                        },
                        executionContext);
    }

    public FiniteDuration nextFirstOftheMonth(){
        Date now = new Date();
        Date next = TimeUtils.nextFirstOfTheMonth(now);

        // This task Start ah 4.00 AM
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(next);
        calendar.add(Calendar.HOUR,4);

        long time = (calendar.getTimeInMillis() - now.getTime()) / (1000 * 60);
        return  Duration.create(time, TimeUnit.MINUTES);
    }
}
