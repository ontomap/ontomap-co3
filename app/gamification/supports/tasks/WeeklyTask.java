package gamification.supports.tasks;

import akka.actor.ActorSystem;
import gamification.controllers.NotificationController;
import gamification.services.GamificationService;
import gamification.services.UserProfileService;
import gamification.models.badges.TemporalBadgeContainer;
import scala.concurrent.ExecutionContext;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;
import logger.supports.TimeUtils;
import logger.services.MongoDBController;

import javax.inject.Inject;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class WeeklyTask extends TimeframeTask {

    @Inject
    public WeeklyTask(ActorSystem actorSystem, ExecutionContext executionContext, GamificationService gamificationController, UserProfileService userProfileController, MongoDBController mongoDBController, NotificationController notificationController) {
        super(actorSystem, executionContext, gamificationController, userProfileController, mongoDBController, notificationController,TemporalBadgeContainer.Timeframe.WEEK);
    }


    protected void initialize() {
        actorSystem
                .scheduler()
                .scheduleAtFixedRate(
                        nextFirstOftheWeek(), // initialDelay
                        Duration.create(7, TimeUnit.DAYS), // interval
                        () -> {
                            logger.info("WEEKLY TASK STARTED");
                            taskJob();
                        },
                        executionContext);
    }

    public FiniteDuration nextFirstOftheWeek() {
        Date now = new Date();
        Date next = TimeUtils.nextFirstOfTheWeek(now);

        // This task Start ah 3.00 AM
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(next);
        calendar.add(Calendar.HOUR,3);

        long time = (calendar.getTimeInMillis() - now.getTime()) / (1000 * 60);
        return Duration.create(time, TimeUnit.MINUTES);
    }
}
