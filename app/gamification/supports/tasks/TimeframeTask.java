package gamification.supports.tasks;

import akka.actor.ActorSystem;
import akka.japi.Pair;
import com.fasterxml.jackson.databind.JsonNode;
import com.mongodb.*;
import com.mongodb.client.ClientSession;
import com.mongodb.client.TransactionBody;
import gamification.controllers.NotificationController;
import gamification.services.GamificationService;
import gamification.services.UserProfileService;
import gamification.models.badges.TemporalBadgeContainer;
import gamification.models.profiles.users.MainPersona;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.ExecutionContext;
import logger.supports.TimeUtils;
import logger.services.MongoDBController;

import javax.inject.Inject;
import java.util.*;

public abstract class TimeframeTask {
    final ActorSystem actorSystem;
    final ExecutionContext executionContext;
    GamificationService gamificationController;
    UserProfileService userProfileController;
    MongoDBController mongoDBController;
    NotificationController notificationController;
    TemporalBadgeContainer.Timeframe timeframe;
    protected Logger logger;

    @Inject
    public TimeframeTask(ActorSystem actorSystem,
                         ExecutionContext executionContext,
                         GamificationService gamificationController,
                         UserProfileService userProfileController,
                         MongoDBController mongoDBController,
                         NotificationController notificationController,
                         TemporalBadgeContainer.Timeframe timeframe) {
        this.actorSystem = actorSystem;
        this.executionContext = executionContext;
        this.gamificationController = gamificationController;
        this.userProfileController = userProfileController;
        this.mongoDBController = mongoDBController;
        this.notificationController = notificationController;
        this.timeframe = timeframe;
        this.logger = LoggerFactory.getLogger(this.getClass());
        this.initialize();
    }

    protected abstract void initialize();

    protected void taskJob() {
        ClientSession dbSession = mongoDBController.getSession();
        TransactionOptions txnOptions = TransactionOptions.builder()
                .readPreference(ReadPreference.primary())
                .readConcern(ReadConcern.LOCAL)
                .writeConcern(WriteConcern.MAJORITY)
                .build();

        List<JsonNode> logs = new ArrayList<>();

        TransactionBody txnBody = (TransactionBody<String>) () -> {

            Map<ObjectId, MainPersona> mainPersonaCache = userProfileController.removeAllExpiredBadges(dbSession);
            gamificationController.resetAcaBadges(dbSession);

            Pair<Date, Date> lastWeek = TimeUtils.getPreviousWeek();

            logs.addAll(gamificationController.calculateRankingBadges(dbSession, timeframe, mainPersonaCache, lastWeek.first()));
            userProfileController.resetRankingVariables(mainPersonaCache);
            userProfileController.saveMany(dbSession, mainPersonaCache.values());
            return "Inserted into collections in different databases";
        };

        try {
            dbSession.withTransaction(txnBody, txnOptions);
        } catch (RuntimeException e) {
            this.logger.info(e.toString());
        } finally {
            dbSession.close();
            for (JsonNode log : logs) {
                long actor = log.get("actor").asLong();
                notificationController.sendNotification(actor, NotificationController.Channel.GAMIFICATION, log);
            }
        }
    }
}
