package gamification.supports.tasks;

import akka.actor.ActorSystem;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.typesafe.config.Config;
import gamification.controllers.NotificationController;
import play.Logger;
import scala.concurrent.ExecutionContext;
import scala.concurrent.duration.Duration;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

public class WebSocketKeepAliveTask{

    final ActorSystem actorSystem;
    final ExecutionContext executionContext;
    NotificationController notificationController;
    Logger.ALogger logger;
    int seconds;
    @Inject
    public WebSocketKeepAliveTask(ActorSystem actorSystem,
                                  ExecutionContext executionContext, NotificationController notificationController, Config config) {
        this.actorSystem = actorSystem;
        this.executionContext = executionContext;
        this.notificationController= notificationController;
        logger = new  Logger.ALogger(play.api.Logger.apply("WeeklyTask"));
        seconds = config.getInt("websocket.keepalive.ping.time ");
        this.initialize();
    }

    protected void initialize() {
        ObjectNode objectNode = new ObjectMapper().createObjectNode();
        objectNode.put("info","keep-alive-ping");

        actorSystem
                .scheduler()
                .schedule(
                        Duration.Zero(), // initialDelay
                        Duration.create(seconds, TimeUnit.SECONDS), // interval
                        () -> notificationController.sendBroadcast(objectNode),
                        executionContext);
    }
}

