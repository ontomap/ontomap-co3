package gamification.supports.tasks;

import akka.actor.ActorSystem;
import gamification.controllers.NotificationController;
import gamification.services.GamificationService;
import gamification.services.UserProfileService;
import gamification.models.badges.TemporalBadgeContainer;
import scala.concurrent.ExecutionContext;
import scala.concurrent.duration.Duration;
import logger.services.MongoDBController;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

public class TestTask extends TimeframeTask {


    @Inject
    public TestTask(ActorSystem actorSystem, ExecutionContext executionContext, GamificationService gamificationController, UserProfileService userProfileController, MongoDBController mongoDBController, NotificationController notificationController) {
        super(actorSystem, executionContext, gamificationController, userProfileController, mongoDBController, notificationController, TemporalBadgeContainer.Timeframe.MONTH);
    }


    protected void initialize() {
        actorSystem
                .scheduler()
                .scheduleOnce(
                        Duration.create(0, TimeUnit.SECONDS), // initialDelay
                        // interval
                        () -> {
//                            try (ClientSession dbSession = mongoDBController.getSession()) {
//                                dbSession.startTransaction();
//                                List<JsonNode> logs = new ArrayList<>();
//                                try {
//                                    Map<ObjectId, MainPersona> mainPersonaCache =userProfileController.removeAllExpiredBadges(dbSession);
//                                     gamificationController.removeAcaBadges(dbSession,TemporalBadgeContainer.Timeframe.WEEK,mainPersonaCache);
//
//                                    Pair<Date,Date> lastWeek = TimeUtils.getPreviousWeek();
//
//                                     gamificationController.calculateRankingBadges(dbSession,timeframe,mainPersonaCache,lastWeek.getKey());
//                                     userProfileController.resetRankingVariables(timeframe,mainPersonaCache);
//                                    userProfileController.saveMany(dbSession,mainPersonaCache.values());
//                                    dbSession.commitTransaction();
//                                } catch (MongoWriteException ex) {
//                                    dbSession.abortTransaction();
//                                } finally {
//                                    dbSession.close();
////                                        for(JsonNode log: logs){
////                                            long actor = log.get("actor").asLong();
////                                            notificationController.sendNotification(actor, NotificationController.Channel.GAMIFICATION,log);
////                                        }
//                                }
                             taskJob();
                        },
                        executionContext);
    }
}

