package gamification.supports.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import gamification.models.Rule;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class RuleSerializer extends StdSerializer<Rule> {
    public RuleSerializer() {
        super((Class) null);
    }

    protected RuleSerializer(Class<Rule> t) {
        super(t);
    }

    @Override
    public void serialize(Rule value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeStringField("id", value.getId().toString());
        gen.writeStringField("activity_type", value.getActivityType());
        if (value.getActivityObjectProperties() != null) {
            gen.writeObjectField("activityObjectProperties", value.getActivityObjectProperties());
        }
        if (value.getReferenceObjectProperties() != null)
            gen.writeObjectField("referenceObjectProperties", value.getReferenceObjectProperties());
        gen.writeArrayFieldStart("rewards");
        for(Rule.Reward reward : value.getRewards()){
            gen.writeObject(reward);
        }
        gen.writeEndArray();
//        if(value.getRefRule() != null)
//            gen.writeStringField("refRule",value.getRefRule().toString());
        if (value.getMaxEvEver() > 0) {
            gen.writeNumberField("maxEvEver", value.getMaxEvEver());
        }
        if (value.getMaxEvDay() > 0) {
            gen.writeNumberField("maxEvDay", value.getMaxEvDay());
        }
        if (value.getValidFrom() != null) {
            gen.writeNumberField("validFrom", value.getValidFrom().getTime());
        }
        if (value.getValidTo() != null) {
            gen.writeNumberField("validFrom", value.getValidTo().getTime());
        }
        gen.writeBooleanField("active", value.isActive());

        gen.writeEndObject();
    }
}
