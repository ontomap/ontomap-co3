package gamification.supports.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import gamification.models.Rule;

import java.io.IOException;

public class RoleSerializer extends JsonSerializer<Rule.Role> {
    @Override
    public void serialize(Rule.Role value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
        gen.writeString(value.toString().toLowerCase());
    }
}
