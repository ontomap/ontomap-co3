package gamification.supports.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import gamification.controllers.ViewMode;
import gamification.models.profiles.users.AcaPersona;
import gamification.models.profiles.users.GroupPersona;
import gamification.models.profiles.users.MainPersona;
import gamification.models.profiles.users.UserProfile;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class UserProfileSerializer extends StdSerializer<UserProfile> {

    private ViewMode viewMode;

    public UserProfileSerializer(ViewMode viewMode) {
        super((Class) null);
        this.viewMode = viewMode;
    }

    protected UserProfileSerializer(Class<UserProfile> t) {
        super(t);
    }

    @Override
    public void serialize(UserProfile value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        if (value instanceof AcaPersona){
            gen.writeStringField("aca",((AcaPersona) value).getAca());
        } else if (value instanceof GroupPersona){
            gen.writeStringField("group",((GroupPersona) value).getGroup());
        }
        switch (viewMode) {
            case HIERARCHY:
                if (value instanceof MainPersona) {
                    gen.writeArrayFieldStart("acaPersonas");
                    for (AcaPersona userProfile : ((MainPersona) value).getAcaPersonas()) {
                        gen.writeObject(userProfile);
                    }
                    gen.writeEndArray();
                } else if (value instanceof AcaPersona){
                    gen.writeArrayFieldStart("groupPersonas");
                    for (GroupPersona userProfile : ((AcaPersona) value).getGroupPersonas()) {
                        gen.writeObject(userProfile);
                    }
                    gen.writeEndArray();
                }
            case SINGLE:
                gen.writeObjectField("badges",value.getBadges());
//                Set<String> singleVariableNames = value.getMaxVariablesValues().keySet();
//                gen.writeObjectField("variables",value.getVariables().entrySet().stream().filter(entry-> singleVariableNames.contains(entry.getKey())).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
                gen.writeObjectField("variables",value.getVariables().entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
                gen.writeObjectField("variablesMaxValue",value.getMaxVariablesValues());
                gen.writeObjectField("temporalVariables",value.getTemporalVariables());
                break;
            case AGGREGATE:
                gen.writeObjectField("badges",value.getFullBadges());
//                Set<String>  aggregateVariableVariableNames = value.getFullMaxVariables().keySet();
//                gen.writeObjectField("variables",value.getFullVariables().entrySet().stream().filter(entry-> aggregateVariableVariableNames.contains(entry.getKey())).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
                gen.writeObjectField("variables",value.getFullVariables().entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
                gen.writeObjectField("variablesMaxValue",value.getFullMaxVariables());
                gen.writeObjectField("temporalVariables",value.getFullTemporalVariables());
                break;
        }
        if( value instanceof  MainPersona){
            gen.writeObjectField("level",((MainPersona) value).getBadgeLevel());
        }
        gen.writeEndObject();
    }
}
