package gamification.supports.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.ObjectNode;
import gamification.models.Rule;
import logger.models.ActivityType;
import logger.models.Concept;
import logger.services.OntologyController;

import java.io.IOException;
import java.lang.reflect.MalformedParametersException;
import java.util.*;
import java.util.stream.Collectors;

import static logger.services.LoggerMongoController.HAS_TYPE_FIELD;

public class RuleDeserializer extends JsonDeserializer<Rule> {

    OntologyController ontologyController;
    ObjectMapper om;

    public RuleDeserializer(OntologyController ontologyController) {
        this.ontologyController = ontologyController;
        this.om = new ObjectMapper();
    }

    @Override
    public Rule deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode ruleJson = jp.getCodec().readTree(jp);
        Rule rule = new Rule();
//        rule.setInstance(instance);
        rule.setActive(true);
        Iterator<String> fieldNames = ruleJson.fieldNames();
        Set<String> conceptSet = ontologyController.getSubConceptsSet("SchemaThing");
        while (fieldNames.hasNext()) {
            String key = fieldNames.next();
            switch (key) {
                case "activity_type":
                    String activityType = ruleJson.get("activity_type").asText().toLowerCase();

                    try {
                        activityType = ActivityType.valueOf(activityType).name();
                    } catch (IllegalArgumentException e) {
                        throw new MalformedParametersException(activityType + " is not a supported activity_type");
                    }
                    rule.setActivityType(activityType);
                    break;
                case "rewards":
                    rule.setRewards(new HashSet<>());
//                    for (int i = 0; i < Rule.Role.values().length; i++) {
//                        Rule.Role role = Rule.Role.values()[i];
//                        if (ruleJson.get(key).has(role.name().toLowerCase())) {
//                            Set<Rule.Variable> variables = om.readValue(ruleJson.get(key).get(role.name()).traverse(), new TypeReference<Set<Rule.Variable>>() {
//                            });
//                            rule.getRewards().put(role.name(), variables);
//                        }
//                    }
                    for(JsonNode jsonReward : ruleJson.get(key)){
                        String role = ((ObjectNode)jsonReward).remove("role").asText();
                        Rule.Reward reward = om.readValue(jsonReward.traverse(), new TypeReference<Rule.Reward>() {});
                        reward.setRole(Rule.Role.valueOf(role.toUpperCase()));
                       rule.getRewards().add(reward);
                    }
                    break;
                case "activityObjectProperties":
                    List<Rule.ActivityObjectProperty> activityObjectProperties = om.readValue(ruleJson.get(key).traverse(), new TypeReference<List<Rule.ActivityObjectProperty>>() {
                    });
                    Optional<String> conceptOpt = activityObjectProperties.stream()
                            .filter(activityObjectProperty -> activityObjectProperty.getPropertyName().equals(HAS_TYPE_FIELD))
                            .map(activityObjectProperty -> activityObjectProperty.getPropertyValue()).findAny();
                    if (!conceptOpt.isPresent()) {
                        throw new MalformedParametersException("No OnToMap Concept");
                    }
                    String concept = conceptOpt.get();
                    if (!conceptSet.contains(concept)) {
                        throw new MalformedParametersException(concept + " is not valid OnToMap Concept");
                    }
                    Concept concept1 = ontologyController.getOntologyConcept(concept);
                    Set<String> properties = concept1.getPropertyList().stream().map(Concept.Property::getName).collect(Collectors.toSet());
                    properties.add(HAS_TYPE_FIELD);
                    rule.setActivityObjectProperties(new ArrayList<>());
                    activityObjectProperties.forEach(activityObjectProperty -> {
                        if (!properties.contains(activityObjectProperty.getPropertyName())) {
                            throw new MalformedParametersException(activityObjectProperty.getPropertyName() + " is not valid OnToMap Property of " + concept);
                        }
                        rule.getActivityObjectProperties().add(activityObjectProperty);
                    });
                    break;
                case "referenceObjectProperties":
                    List<Rule.ActivityObjectProperty> referenceObjectProperties = om.readValue(ruleJson.get(key).traverse(), new TypeReference<List<Rule.ActivityObjectProperty>>() {
                    });
                    Optional<String> referenceConceptOpt = referenceObjectProperties.stream()
                            .filter(activityObjectProperty -> activityObjectProperty.getPropertyName().equals(HAS_TYPE_FIELD))
                            .map(activityObjectProperty -> activityObjectProperty.getPropertyValue()).findAny();
                    if (!referenceConceptOpt.isPresent()) {
                        throw new MalformedParametersException("No OnToMap Concept");
                    }
                    String referenceConceptName = referenceConceptOpt.get();
                    if (!conceptSet.contains(referenceConceptName)) {
                        throw new MalformedParametersException(referenceConceptName + " is not valid OnToMap Concept");
                    }
                    Concept referenceConcept = ontologyController.getOntologyConcept(referenceConceptName);
                    Set<String> referenceConceptProperties = referenceConcept.getPropertyList().stream().map(Concept.Property::getName).collect(Collectors.toSet());
                    referenceConceptProperties.add(HAS_TYPE_FIELD);
                    rule.setReferenceObjectProperties(new ArrayList<>());
                    referenceObjectProperties.forEach(activityObjectProperty -> {
                        if (!referenceConceptProperties.contains(activityObjectProperty.getPropertyName())) {
                            throw new MalformedParametersException(activityObjectProperty.getPropertyName() + " is not valid OnToMap Property of " + referenceConceptName);
                        }
                        rule.getReferenceObjectProperties().add(activityObjectProperty);
                    });
                    break;
                case "maxEvDay":
                    int maxEvDay = ruleJson.get(key).asInt();
                    rule.setMaxEvDay(maxEvDay);
                    break;
                case "maxEvEver":
                    int maxEvEver = ruleJson.get(key).asInt();
                    rule.setMaxEvEver(maxEvEver);
                    break;
                case "acas":
                    List<String> acas = om.readValue(ruleJson.get(key).toString(), new TypeReference<List<String>>() {
                    });
                    rule.setAcas(acas);
                    break;
                case "validFrom":
                    // Date date = formatter.parse(ruleJson.get(key).asText());
                    Date date = new Date(ruleJson.get(key).asLong());
                    rule.setValidFrom(date);
                    break;
                case "validTo":
                    // Date date = formatter.parse(ruleJson.get(key).asText());
                    date = new Date(ruleJson.get(key).asLong());
                    rule.setValidTo(date);
                    break;
            }
        }
        if (rule.getMaxEvEver() != -1 && rule.getMaxEvDay() > rule.getMaxEvEver()) {
            throw new MalformedParametersException("maxEvDay is more than maxEvEver");
        }

        if (rule.getValidFrom() != null && rule.getValidTo() != null && rule.getValidTo().getTime() < rule.getValidFrom().getTime())
            throw new MalformedParametersException("validTo is before than validFrom");
        return rule;
    }
}
