package gamification.supports.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import gamification.models.badges.*;
import gamification.services.BadgeService;
import org.bson.types.ObjectId;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BadgeContainerDeserializer extends JsonDeserializer<BadgeContainer> {
    ObjectMapper om;

    public BadgeContainerDeserializer(BadgeService badgeService) {
        this.om = new ObjectMapper();
        SimpleModule simpleModule = new SimpleModule();
        simpleModule.addDeserializer(Badge.class, new BadgeDeserializer(badgeService));
        this.om.registerModule(simpleModule);
    }


    @Override
    public BadgeContainer deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode badgeContainerJson = jp.getCodec().readTree(jp);
        switch (badgeContainerJson.get("type").asText().toLowerCase()) {
            case "aca":
                return this.deserializeAca(badgeContainerJson);
            case "ranking":
                return this.deserializeRanking(badgeContainerJson);
            default:
                return this.deserializeIndividual(badgeContainerJson);
        }
    }

    private IndividualBadgeContainer deserializeIndividual(JsonNode badgeContainerJson) throws IOException {
        IndividualBadgeContainer badgeContainer = new IndividualBadgeContainer();
        badgeContainer.setId(ObjectId.get());
        Iterator<String> fieldNames = badgeContainerJson.fieldNames();
        while (fieldNames.hasNext()) {
            String key = fieldNames.next();
            switch (key) {
                case "name":
                    badgeContainer.setName(badgeContainerJson.get(key).asText());
                    break;
                case "badgeList":
                    List<BadgeStep> steps = parseBadgeList(badgeContainerJson.get(key));
                    badgeContainer.setBadgeList(steps);
                    break;
                case "levelsContainer":
                    boolean isLevelContainer = badgeContainerJson.get(key).asBoolean();
                    badgeContainer.setLevelsContainer(isLevelContainer);
            }
        }
        return badgeContainer;
    }

    private AcaBadgeContainer deserializeAca(JsonNode badgeContainerJson) throws IOException {
        AcaBadgeContainer badgeContainer = new AcaBadgeContainer();
        badgeContainer.setId(ObjectId.get());
        Iterator<String> fieldNames = badgeContainerJson.fieldNames();
        while (fieldNames.hasNext()) {
            String key = fieldNames.next();
            switch (key) {
                case "name":
                    badgeContainer.setName(badgeContainerJson.get(key).asText());
                    break;
                case "badgeList":
                    List<BadgeStep> steps = parseBadgeList(badgeContainerJson.get(key));
                    badgeContainer.setBadgeList(steps);
                    break;
                case "timeframe":
                    TemporalBadgeContainer.Timeframe timeframe = AcaBadgeContainer.Timeframe.valueOf(badgeContainerJson.get(key).asText());
                    badgeContainer.setTimeframe(timeframe);
                    break;
                case "aca":
//                    List<String> acas = new ObjectMapper().readValue(badgeJson.get(key).toString(), new TypeReference<List<String>>() {
//                    });
//                    badgeContainer.setAcas(acas);
                    String aca = badgeContainerJson.get(key).asText();
                    badgeContainer.setAca(aca);
                    break;
            }
        }
        return badgeContainer;
    }

    private RankingBadgeContainer deserializeRanking(JsonNode badgeContainerJson) throws IOException {
        RankingBadgeContainer badgeContainer = new RankingBadgeContainer();
        badgeContainer.setId(ObjectId.get());
        Iterator<String> fieldNames = badgeContainerJson.fieldNames();
        while (fieldNames.hasNext()) {
            String key = fieldNames.next();
            switch (key) {
                case "name":
                    badgeContainer.setName(badgeContainerJson.get(key).asText());
                    break;
                case "context":
                    String ctx = badgeContainerJson.get(key).asText();
                    break;
                case "variable":
                    String variable = badgeContainerJson.get(key).asText();
                    badgeContainer.setVariable(variable);
                    break;
                case "badgeList":
                    List<BadgePosition> badges = parseBadgePositionList(badgeContainerJson.get(key));
                    badgeContainer.setBadgeList(badges);
                    break;
                case "timeframe":
                    TemporalBadgeContainer.Timeframe timeframe = AcaBadgeContainer.Timeframe.valueOf(badgeContainerJson.get(key).asText());
                    badgeContainer.setTimeframe(timeframe);
                    break;
                case "aca":
//                    List<String> acas = new ObjectMapper().readValue(badgeJson.get(key).toString(), new TypeReference<List<String>>() {
//                    });
                    String aca = badgeContainerJson.get(key).asText();
                    badgeContainer.setAca(aca);
                    break;
            }
        }
        return badgeContainer;
    }

    private List<BadgeStep> parseBadgeList(JsonNode badgeListJson) throws JsonProcessingException {
        List<BadgeStep> steps = new ArrayList<>();
        BadgeStep prec = null;
        for (JsonNode badgeJson : badgeListJson) {
            BadgeStep badge = (BadgeStep) om.treeToValue(badgeJson, Badge.class);
            if (prec != null) {
                badge.getPreconditions().getBadgeList().add(prec.getId());
            }
            prec = badge;
            steps.add(badge);
        }
        return steps;
    }

    private List<BadgePosition> parseBadgePositionList(JsonNode badgeListJson) throws JsonProcessingException {
        List<BadgePosition> badges = new ArrayList<>();
        for (JsonNode badgeJson : badgeListJson) {
            BadgePosition badgePosition = (BadgePosition) om.treeToValue(badgeJson,Badge.class);
            badges.add(badgePosition);
        }
        return badges;
    }


}
