package gamification.supports.deserializers;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import gamification.models.badges.Badge;
import gamification.models.badges.BadgePosition;
import gamification.models.badges.BadgeStep;
import gamification.services.BadgeService;
import org.bson.types.ObjectId;

import java.io.IOException;
import java.lang.reflect.MalformedParametersException;
import java.util.Arrays;
import java.util.HashSet;

public class BadgeDeserializer  extends JsonDeserializer<Badge> {
    private final ObjectMapper objectMapper;
    private final BadgeService badgeService;

    public BadgeDeserializer(BadgeService badgeService){
        this.objectMapper = new ObjectMapper();
        this.badgeService = badgeService;
    }


    @Override
    public Badge deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode badgeJson = jp.getCodec().readTree(jp);
        if (badgeJson.has("preconditions"))
            return this.deserializeStep(badgeJson);
        return this.deserializePosition(badgeJson);
    }

    private BadgeStep deserializeStep(JsonNode badgeJson) throws IOException, JsonProcessingException {
        BadgeStep badge = new BadgeStep();
        if( badgeJson.has("id") && ObjectId.isValid(badgeJson.get("id").asText())){
            badge.setId(new ObjectId(badgeJson.get("id").asText()));
        }

        String name = badgeJson.get("name").asText();
        badge.setName(name);
        badge.setActive(true);

        if(badgeJson.has("logo")){
            badge.setLogo(badgeJson.get("logo").asText());
        }

        JsonNode preconditionJson = badgeJson.get("preconditions");
        if (preconditionJson.has("variables")) {
            BadgeStep.VariableCondition[] variables = objectMapper.treeToValue(preconditionJson.get("variables"), BadgeStep.VariableCondition[].class);
            badge.getPreconditions().setVariables(new HashSet<>(Arrays.asList(variables)));
            for(BadgeStep.VariableCondition variableCondition : badge.getPreconditions().getVariables()){
                variableCondition.setVariable(variableCondition.getVariable().toLowerCase());
            }
        }
        if (preconditionJson.has("badgeList")) {
            String[] ids = objectMapper.treeToValue(preconditionJson.get("badgeList"), String[].class);
            for (String id : ids) {
                Badge b = badgeService.getBadge(null,id);
                if (b!= null)
                    badge.getPreconditions().getBadgeList().add(b.getId());
                else
                    throw new MalformedParametersException("not_valid_badge_precondition_badge: " + id);
            }
        }
        return badge;
    }

    private BadgePosition deserializePosition(JsonNode badgeJson) throws IOException, JsonProcessingException {
        BadgePosition badgePosition = new BadgePosition();

        String name = badgeJson.get("name").asText();
        int position = badgeJson.get("position").asInt();

        if(badgeJson.has("logo")){
            badgePosition.setLogo(badgeJson.get("logo").asText());
        }

        badgePosition.setPosition(position);
        badgePosition.setName(name);
        badgePosition.setActive(true);
        return badgePosition;
    }


}
