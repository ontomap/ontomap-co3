package gamification.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gamification.models.badges.TemporalBadgeContainer;
import logger.supports.TimeUtils;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.types.ObjectId;

import java.util.*;
import java.util.stream.Collectors;


public class Ranking {
    @JsonIgnore
    ObjectId id;
    @JsonIgnore
    String instance;
    String variable;
    String aca;
    TemporalBadgeContainer.Timeframe timeframe;
    List<Rank> rankList;
    int totalCount;
    Date endingDate;

    public Ranking() {
    }

    public Ranking(String variable) {
        this.rankList = new ArrayList<>();
        this.variable = variable;
    }

    public Ranking(String instance, String aca, String variable, TemporalBadgeContainer.Timeframe timeframe, Date validDate) {
        this(variable);
        this.setTimeframe(timeframe);
        this.setInstance(instance);

        this.setAca(aca);
        Date ending = null;
        if (timeframe.equals(TemporalBadgeContainer.Timeframe.WEEK)) {
            ending = TimeUtils.nextFirstOfTheWeek(validDate);
        } else {
            ending = TimeUtils.nextFirstOfTheMonth(validDate);
        }
        Calendar calendarEnding = Calendar.getInstance();
        calendarEnding.setTime(ending);
        calendarEnding.add(Calendar.MILLISECOND, -1);
        this.setEndingDate(calendarEnding.getTime());
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public void setRankList(List<Rank> rankList) {
        this.rankList = rankList;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public void addRank(long actor, double value) {
        rankList.add(new Rank(actor, value));
    }

    public TemporalBadgeContainer.Timeframe getTimeframe() {
        return timeframe;
    }

    public void setTimeframe(TemporalBadgeContainer.Timeframe timeframe) {
        this.timeframe = timeframe;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public String getAca() {
        return aca;
    }

    public void setAca(String aca) {
        this.aca = aca;
    }

    public Date getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(Date endingDate) {
        this.endingDate = endingDate;
    }

    public void calculatePositions() {
        Set<Double> valuesSet = new HashSet<>();
        HashMap<Double, Integer> sameValueMap = new HashMap<>();
        for (Rank rank : rankList) {
            valuesSet.add(rank.getValue());
            sameValueMap.merge(rank.getValue(), 1, Integer::sum);
        }
        List<Double> values = new ArrayList<>(valuesSet);
        values.sort(Comparator.reverseOrder());
        double lastValue = -1;
        int position = 0;
        int sameValue = 0;
        for(Rank rank: rankList){
           if(lastValue == rank.getValue()){
               rank.setPosition(position);
               sameValue++;
           }else{
               position = position + 1 + sameValue;
               rank.setPosition(position);
               sameValue = 0;
               lastValue = rank.getValue();
           }
        }
    }

    @BsonIgnore
    public List<Rank> getRankByPosition(int position){
        return this.rankList.stream().filter(rank -> rank.getPosition() == position).collect(Collectors.toList());
    }


    public List<Rank> getRankList() {
        return rankList;
    }

    public static class Rank {
        Long actor;
        double value;
        int position;

        public Rank() {
        }

        public Rank(Long actor, double value) {
            this.actor = actor;
            this.value = value;
        }

        public Long getActor() {
            return actor;
        }

        public void setActor(Long actor) {
            this.actor = actor;
        }


        public double getValue() {
            return value;
        }

        public void setValue(double value) {
            this.value = value;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }
}
