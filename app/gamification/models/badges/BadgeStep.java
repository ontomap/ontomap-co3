package gamification.models.badges;

import org.bson.types.ObjectId;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class BadgeStep  extends Badge{
    private BadgeStep.Preconditions preconditions;

    public BadgeStep() {
        super();
        this.preconditions = new BadgeStep.Preconditions();
    }


    public BadgeStep.Preconditions getPreconditions() {
        return preconditions;
    }

    public void setPreconditions(BadgeStep.Preconditions preconditions) {
        this.preconditions = preconditions;
    }


    public static class Preconditions {
        private Set<BadgeStep.VariableCondition> variables;
        private Set<ObjectId> badgeList;

        public Preconditions() {
            this.variables = new HashSet<>();
            this.badgeList = new HashSet<>();
        }

        public Set<BadgeStep.VariableCondition> getVariables() {
            return variables;
        }

        public void setVariables(Set<BadgeStep.VariableCondition> variables) {
            this.variables = variables;
        }

        public Set<ObjectId> getBadgeList() {
            return badgeList;
        }

        public void setBadgeList(Set<ObjectId> badgeList) {
            this.badgeList = badgeList;
        }
    }


    public static class VariableCondition {
        private String variable;
        private double threshold;

        public VariableCondition() {
        }

        public VariableCondition(String variable, double threshold) {
            this.variable = variable;
            this.threshold = threshold;
        }

        public String getVariable() {
            return variable;
        }

        public void setVariable(String variable) {
            this.variable = variable;
        }

        public double getThreshold() {
            return threshold;
        }

        public void setThreshold(double threshold) {
            this.threshold = threshold;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            BadgeStep.VariableCondition that = (BadgeStep.VariableCondition) o;
            return Objects.equals(variable, that.variable);
        }

        @Override
        public int hashCode() {
            return Objects.hash(variable);
        }
    }

}
