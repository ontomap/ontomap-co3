package gamification.models.badges;

public class BadgePosition extends Badge {
    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
