package gamification.models.badges;

public abstract class TemporalBadgeContainer<B extends Badge> extends BadgeContainer {
    private Timeframe timeframe;

    public Timeframe getTimeframe() {
        return timeframe;
    }

    public void setTimeframe(Timeframe timeframe) {
        this.timeframe = timeframe;
    }

    public enum Timeframe {
        WEEK, MONTH
    }
}
