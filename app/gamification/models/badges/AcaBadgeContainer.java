package gamification.models.badges;

import java.util.List;

public class AcaBadgeContainer extends TemporalBadgeContainer<BadgeStep> {
    private final String type = "aca";

    public AcaBadgeContainer() {
        super();
    }

    @Override
    public String getType() {
        return type;
    }


//    @Override
//    public boolean isAcaApplicable(String aca) {
////        return this.getAcas() == null || this.getAcas().size()>0 && !this.getAcas().contains(aca);
//        return aca == null || this.getAca() == null || this.getAca().equals(aca);
//    }

    @Override
    public List<BadgeStep> getBadgeList() {
        return super.getBadgeList();
    }
}
