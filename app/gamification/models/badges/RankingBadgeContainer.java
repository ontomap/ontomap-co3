package gamification.models.badges;

import java.util.List;

public class RankingBadgeContainer extends TemporalBadgeContainer<BadgePosition> {
    private final String type = "ranking";
    private String variable;

    public RankingBadgeContainer(){
        super();
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getType() {
        return type;
    }

//    @Override
//    public boolean isAcaApplicable(String aca) {
//        return variableStorageContext.equals(VariableStorageContext.COMPETITIVE_ACA) && (this.getAcas() == null || this.getAcas().size()>0 && !this.getAcas().contains(aca));
//    }

    @Override
    public List<BadgePosition> getBadgeList() {
        return super.getBadgeList();
    }
}
