package gamification.models.badges;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gamification.supports.serializer.ObjectIdSerializer;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.types.ObjectId;

import java.util.*;

public abstract class Badge {

    @JsonSerialize(using = ObjectIdSerializer.class)
    private ObjectId id;
    private String name;
    private boolean active;
    private String logo;

    private ContainerReference containerReference;


    public Badge() {
        this.id = ObjectId.get();
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    @BsonIgnore
    public ContainerReference getContainerReference() {
        return containerReference;
    }

    public void setContainerReference(ContainerReference containerReference) {
        this.containerReference = containerReference;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Badge badge = (Badge) o;
        return Objects.equals(id, badge.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public static class ContainerReference{
        @JsonSerialize(using = ObjectIdSerializer.class)
        private ObjectId id;
        private String name;
        private String logo;
        private String type;

        public ContainerReference(){}

        public ContainerReference(BadgeContainer badgeContainer){
            this.id = badgeContainer.id;
            this.name = badgeContainer.name;
            this.type = badgeContainer.getType();
        }


        public ObjectId getId() {
            return id;
        }

        public void setId(ObjectId id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLogo() {
            return logo;
        }

        public void setLogo(String logo) {
            this.logo = logo;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}

