package gamification.models.badges;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class BadgeContainer<T extends Badge> {

    ObjectId id;
    @JsonIgnore
    String instance;
    String name;
    List<T> badgeList;
    String aca;

    public BadgeContainer(){
        badgeList = new ArrayList<>();
    }

    public List<T> getBadgeList() {
        return badgeList;
    }

    public void setBadgeList(List<T> badgeList) {
        this.badgeList = badgeList;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract String getType();

    public String getAca() {
        return aca;
    }

    public void setAca(String aca) {
        this.aca = aca;
    }

//    public abstract boolean isAcaApplicable(String aca);
    public boolean isAcaApplicable(String aca) {
        return aca == null || this.getAca() == null || this.getAca().equals(aca);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BadgeContainer<?> that = (BadgeContainer<?>) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
