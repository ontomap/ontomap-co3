package gamification.models.badges;

public class IndividualBadgeContainer extends BadgeContainer<BadgeStep> {
    private final String type = "individual";
    private boolean levelsContainer;

    public String getType() {
        return type;
    }

    public boolean isLevelsContainer() {
        return levelsContainer;
    }

    public void setLevelsContainer(boolean levelsContainer) {
        this.levelsContainer = levelsContainer;
    }
}
