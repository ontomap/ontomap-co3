package gamification.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gamification.supports.serializer.RoleSerializer;
import logger.models.OTMEvent;

import java.util.Date;

public class LogRule extends LogAssignment {
    private OTMEvent event;
    //@JsonSerialize(using = ObjectIdSerializer.class)
    private Rule rule;

    @JsonSerialize(using = RoleSerializer.class)
    private Rule.Role role;

    public LogRule(){}

    public LogRule(OTMEvent event, Rule rule, Rule.Role role, String instance, long actor, String aca, String group) {
        super(new Date(event.getTimestamp()),instance,actor,aca,group);
        this.setRole(role);
        this.setEvent(event);
        this.setRule(rule);
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public OTMEvent getEvent() {
        return event;
    }

    public void setEvent(OTMEvent event) {
        this.event = event;
    }

    public Rule.Role getRole() {
        return role;
    }

    public void setRole(Rule.Role role) {
        this.role = role;
    }


}
