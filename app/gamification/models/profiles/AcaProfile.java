package gamification.models.profiles;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gamification.models.profiles.users.UserProfile;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.types.ObjectId;

public class AcaProfile extends TemporalProfile {
    @JsonIgnore
    private ObjectId id;
    private String instance;
    private String url;
    private UserProfile.Badges badges;
    @BsonIgnore
    private boolean pilotProfile;

    public AcaProfile() {
        super();
        this.badges = new UserProfile.Badges();
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public UserProfile.Badges getBadges() {
        return badges;
    }

    public void setBadges(UserProfile.Badges badges) {
        this.badges = badges;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isPilotProfile() {
        return pilotProfile;
    }

    public void setPilotProfile(boolean pilotProfile) {
        this.pilotProfile = pilotProfile;
    }
}
