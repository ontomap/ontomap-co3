package gamification.models.profiles;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gamification.models.badges.TemporalBadgeContainer;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import logger.supports.TimeUtils;

import java.util.*;
import java.util.stream.Collectors;

public class TemporalProfile {

    //    @JsonProperty("variables")
    Set<TemporalVariableContainer> temporalVariableContainers;

    public TemporalProfile() {
        this.temporalVariableContainers = new HashSet<>();
    }

    @BsonIgnore
    public TemporalVariableContainer getTemporalVariableContainer(TemporalBadgeContainer.Timeframe timeframe, Date date) {
        Optional<TemporalVariableContainer> temporalVariableOptional = this.getTemporalVariableContainers().stream().filter(t -> t.isInRange(timeframe, date)).findAny();
        return temporalVariableOptional.orElse(null);
    }

    @BsonIgnore
    public Set<TemporalVariableContainer> getTemporalVariableContainers(Date start, Date end) {
        if (start == null && end == null){
            return this.temporalVariableContainers;
        }
        return this.temporalVariableContainers.stream().filter(temporalVariableContainer -> {
            if (start == null &&  (temporalVariableContainer.getEnd().before(end) || temporalVariableContainer.getStart().before(end)))
                return true;
            else if (end == null &&  temporalVariableContainer.getStart().after(start) || temporalVariableContainer.getEnd().after(start))
                return true;
            else if (start != null && temporalVariableContainer.getStart().before(start) && temporalVariableContainer.getEnd().after(start))
                return true;
            else if (end != null && temporalVariableContainer.getStart().before(end) && temporalVariableContainer.getEnd().after(end))
                return true;
            else return temporalVariableContainer.getStart().after(start) && temporalVariableContainer.getEnd().before(end);
        }).collect(Collectors.toSet());
    }

    @BsonIgnore
    public Set<TemporalVariableContainer> getTemporalVariableContainers(Date date) {
        return this.getTemporalVariableContainers(date, date);
    }

    @BsonIgnore
    public Map<String, Double> getTemporalVariables(TemporalBadgeContainer.Timeframe timeframe, Date date) {
        Optional<TemporalVariableContainer> temporalVariableOptional = this.getTemporalVariableContainers().stream().filter(t -> t.isInRange(timeframe, date)).findAny();
        if (temporalVariableOptional.isPresent()) {
            return temporalVariableOptional.get().getVariables();
        } else {
            return new HashMap<>();
        }
    }

    /**
     * @param timeframe Timeframe desidered
     * @return Temporal variable of the current period
     */
    @BsonIgnore
    public Map<String, Double> getTemporalVariables(TemporalBadgeContainer.Timeframe timeframe) {
        return this.getTemporalVariables(timeframe, new Date());
    }

    /**
     * @return Temporal variable (both Weekly and Montly) of the current period
     */
    @BsonIgnore
    @JsonIgnore
    public Map<String, Map<String, Double>> getTemporalVariables() {
        Map<String, Map<String, Double>> map = new HashMap<>();
        for (TemporalBadgeContainer.Timeframe timeframe : TemporalBadgeContainer.Timeframe.values()) {
            map.put(timeframe.name(), this.getTemporalVariables(timeframe));
        }
        return map;
    }

    public Set<TemporalVariableContainer> getTemporalVariableContainers() {
        return temporalVariableContainers;
    }

    public void setTemporalVariableContainers(Set<TemporalVariableContainer> temporalVariableContainers) {
        this.temporalVariableContainers = temporalVariableContainers;
    }

    public void removeOldTemporalContainers() {
        Date now = new Date();
//        List<TemporalVariableContainer> toRemove = new ArrayList<>();
//        for (TemporalVariableContainer temporalVariableContainer : temporalVariableContainers) {
//            if (temporalVariableContainer.getEnd().before(now)) {
//                toRemove.add(temporalVariableContainer);
//            }
//        }
        this.temporalVariableContainers = this.temporalVariableContainers.stream()
            .filter(temporalVariableContainer ->  temporalVariableContainer.getEnd().after(now))
            .collect(Collectors.toSet());
//        this.temporalVariableContainers.removeAll(toRemove);
    }

    public TemporalVariableContainer addTemporalVariableContainer(TemporalBadgeContainer.Timeframe timeframe, Date date) {
        TemporalVariableContainer temporalVariableContainer = new TemporalVariableContainer();
        temporalVariableContainer.setTimeframe(timeframe);
        if (timeframe.equals(TemporalBadgeContainer.Timeframe.WEEK)) {
            temporalVariableContainer.setStart(TimeUtils.getFirstOfTheWeek(date));
            Date end = TimeUtils.nextFirstOfTheWeek(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(end);
            calendar.add(Calendar.MILLISECOND, -1);
            temporalVariableContainer.setEnd(calendar.getTime());
        } else {
            temporalVariableContainer.setStart(TimeUtils.getFirstOfTheMonth(date));
            Date end = TimeUtils.nextFirstOfTheMonth(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(end);
            calendar.add(Calendar.MILLISECOND, -1);
            temporalVariableContainer.setEnd(calendar.getTime());
        }
        this.getTemporalVariableContainers().add(temporalVariableContainer);
        return temporalVariableContainer;
    }
}
