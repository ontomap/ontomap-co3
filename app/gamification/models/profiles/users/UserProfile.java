package gamification.models.profiles.users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gamification.models.Rule;
import gamification.models.badges.Badge;
import gamification.models.badges.TemporalBadgeContainer;
import gamification.models.profiles.TemporalProfile;
import gamification.models.profiles.TemporalVariableContainer;
import gamification.supports.serializer.ObjectIdSerializer;
import logger.supports.TimeUtils;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.types.ObjectId;

import java.util.*;

public abstract class UserProfile extends TemporalProfile {

    private Badges badges;
    private Map<String,Double> variables;
    private Map<String,Double> maxVariablesValues;


    public UserProfile() {
        this.badges = new Badges();
        this.variables = new HashMap<>();
        this.maxVariablesValues = new HashMap<>();
    }

    public Badges getBadges() {
        return badges;
    }

    public void setBadges(Badges badges) {
        this.badges = badges;
    }

    public Map<String, Double> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, Double> variables) {
        this.variables = variables;
    }

    @BsonIgnore
    public abstract Badges getFullBadges();

    public abstract  Map<String, Double> getFullVariables();

    @BsonIgnore
    public abstract  Map<String, Double> getFullMaxVariables();

    @BsonIgnore
    public abstract  Map<String, Double> getFullTemporalVariables(TemporalBadgeContainer.Timeframe timeframe,Date date);

    @BsonIgnore
    public abstract  Map<String,Map<String, Double>> getFullTemporalVariables();

    @BsonIgnore
    public abstract UserProfile getPersona(String aca,String group);

    public Map<String, Double> getMaxVariablesValues() {
        return maxVariablesValues;
    }

    public void setMaxVariablesValues(Map<String, Double> maxVariablesValues) {
        this.maxVariablesValues = maxVariablesValues;
    }

    public void applyVariable( Rule.Variable variable){
        this.getVariables().merge(variable.getName(), variable.getAssignment(), Double::sum);
    }

    public void applyRuleMaxVariable( Rule.Variable variable){
        this.getMaxVariablesValues().merge(variable.getName(), variable.getAssignment() > 0 ? variable.getAssignment() : 0, Double::sum);
    }

    public void applyRuleRankingVariable(Rule.Variable variable, Date date) {
        for (TemporalBadgeContainer.Timeframe timeframe : TemporalBadgeContainer.Timeframe.values()) {
            TemporalVariableContainer temporalVariableContainer = this.getTemporalVariableContainer(timeframe, date);
            if (temporalVariableContainer == null) {
                temporalVariableContainer = this.addTemporalVariableContainer(timeframe, date);
            }
            temporalVariableContainer.getVariables().merge(variable.getName().toLowerCase(), variable.getAssignment(), Double::sum);
        }
    }

    public static class Badges{
        Set<AssignedBadge> currentBadges;
        Set<AssignedBadge> removedBadges;

        public Badges() {
            this.currentBadges = new HashSet<>();
            this.removedBadges = new HashSet<>();
        }

        public Set<AssignedBadge> getRemovedBadges() {
            return removedBadges;
        }

        public void setRemovedBadges(Set<AssignedBadge> removedBadges) {
            this.removedBadges = removedBadges;
        }

        public Set<AssignedBadge> getCurrentBadges() {
            return currentBadges;
        }

        public void setCurrentBadges(Set<AssignedBadge> currentBadges) {
            this.currentBadges = currentBadges;
        }
    }

    public static class AssignedBadge{
        @JsonSerialize(using = ObjectIdSerializer.class)
        private ObjectId idBadge;
        private Date assignedDate;
        private Date expiredDate;
        private String timeframe;

        public AssignedBadge(){}

        public AssignedBadge(Badge badge){
            this.idBadge = badge.getId();
        }

        public AssignedBadge(Badge badge,Date dateInsideTheTimeFrame, TemporalBadgeContainer.Timeframe timeframe){
            this(badge);
            Calendar expireDate = Calendar.getInstance(), assignDate = Calendar.getInstance();
            assignDate.setTime(dateInsideTheTimeFrame);
            assignDate.add(Calendar.MILLISECOND, 1);
            this.setAssignedDate(assignDate.getTime());
            this.setTimeframe(timeframe.name());

            if (timeframe.equals(TemporalBadgeContainer.Timeframe.WEEK)) {
                expireDate.setTime(TimeUtils.nextFirstOfTheWeek(dateInsideTheTimeFrame));
                expireDate.add(Calendar.DATE, 7);
                expireDate.add(Calendar.MILLISECOND, -1);
            } else {
                expireDate.setTime(TimeUtils.nextFirstOfTheMonth(dateInsideTheTimeFrame));
            }
            this.expiredDate = expireDate.getTime();
        }

        public ObjectId getIdBadge() {
            return idBadge;
        }

        public void setIdBadge(ObjectId idBadge) {
            this.idBadge = idBadge;
        }

        public Date getAssignedDate() {
            return assignedDate;
        }

        public void setAssignedDate(Date assignedDate) {
            this.assignedDate = assignedDate;
        }

        public Date getExpiredDate() {
            return expiredDate;
        }

        public void setExpiredDate(Date expiredDate) {
            this.expiredDate = expiredDate;
        }

        public String getTimeframe() {
            return timeframe;
        }

        public void setTimeframe(String timeframe) {
            this.timeframe = timeframe;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            AssignedBadge that = (AssignedBadge) o;
            return Objects.equals(idBadge, that.idBadge) &&
                    Objects.equals(assignedDate, that.assignedDate) &&
                    Objects.equals(expiredDate, that.expiredDate);
        }

        @Override
        public int hashCode() {
            return Objects.hash(idBadge, assignedDate, expiredDate);
        }

        @BsonIgnore
        @JsonIgnore
        public boolean isExpired() {
            return this.expiredDate!= null && new Date().after(this.expiredDate);
        }
    }
}
