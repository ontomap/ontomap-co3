package gamification.models.profiles.users;

import gamification.models.badges.TemporalBadgeContainer;
import org.bson.codecs.pojo.annotations.BsonIgnore;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AcaPersona extends UserProfile {
    private String aca;
    List<GroupPersona> groupPersonas;


    public AcaPersona(){
        super();
        this.groupPersonas = new ArrayList<>();
    }

    public String getAca() {
        return aca;
    }

    public void setAca(String aca) {
        this.aca = aca;
    }

    @BsonIgnore
    @Override
    public UserProfile.Badges getFullBadges() {
        return this.getBadges();
    }

    @BsonIgnore
    public Map<String, Double> getFullVariables() {
        Map<String, Double> inner =  groupPersonas.stream()
                .flatMap(
                        personas -> personas.getVariables()
                                .entrySet()
                                .stream())
                .collect(
                        Collectors.groupingBy(
                                Map.Entry::getKey, Collectors.summingDouble(Map.Entry::getValue)
                        ));
        return Stream.of(this.getVariables(), inner)
                .flatMap(map -> map.entrySet().stream())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        Double::sum));
    }

    @BsonIgnore
    public Map<String, Double> getFullTemporalVariables(TemporalBadgeContainer.Timeframe timeframe) {
       return this.getTemporalVariables(timeframe);
    }


    @Override
    public Map<String, Map<String, Double>> getFullTemporalVariables() {
        return this.getTemporalVariables();
    }

    @BsonIgnore
    @Override
    public Map<String, Double> getFullTemporalVariables(TemporalBadgeContainer.Timeframe timeframe,Date date) {
        return this.getTemporalVariables(timeframe,date);
    }


    @BsonIgnore
    public Map<String, Double> getFullMaxVariables() {
        Map<String, Double> inner =  groupPersonas.stream()
                .flatMap(
                        persona -> persona.getMaxVariablesValues()
                                .entrySet()
                                .stream())
                .collect(
                        Collectors.groupingBy(
                                Map.Entry::getKey, Collectors.summingDouble(Map.Entry::getValue)
                        ));
        return Stream.of(this.getMaxVariablesValues(), inner)
                .flatMap(map -> map.entrySet().stream())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        Double::sum));
    }

    @BsonIgnore
    @Override
    public UserProfile getPersona(String aca, String group) {
        if(group==null)
            return this;
        GroupPersona persona = groupPersonas.stream().filter(acaPersona -> acaPersona.getGroup().compareTo(group) == 0).findAny().orElse(null);
        return  persona != null ? persona.getPersona(aca,group) : null;
    }

    public List<GroupPersona> getGroupPersonas() {
        return groupPersonas;
    }

    public void setGroupPersonas(List<GroupPersona> groupPersonas) {
        this.groupPersonas = groupPersonas;
    }
}
