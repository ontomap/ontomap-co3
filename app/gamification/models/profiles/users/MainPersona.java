package gamification.models.profiles.users;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gamification.models.badges.Badge;
import gamification.models.badges.TemporalBadgeContainer;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import org.bson.types.ObjectId;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MainPersona extends UserProfile {
    @JsonIgnore
    private ObjectId id;
    private long actor;
    @JsonIgnore
    private String instance;
    private Badge badgeLevel;
    private List<AcaPersona> acaPersonas;

    public MainPersona() {
        super();
        acaPersonas = new ArrayList<>();
    }

    public long getActor() {
        return actor;
    }

    public void setActor(long actor) {
        this.actor = actor;
    }

    @BsonIgnore
    @Override
    public UserProfile.Badges getFullBadges() {
        Badges badges = new Badges();
        badges.setCurrentBadges(acaPersonas.stream()
                .flatMap(
                        acaPersona -> acaPersona.getBadges().getCurrentBadges().stream())
                .collect(Collectors.toSet()));

        badges.setRemovedBadges(acaPersonas.stream()
                .flatMap(
                        acaPersona -> acaPersona.getBadges().getRemovedBadges().stream())
                .collect(Collectors.toSet()));

        badges.getCurrentBadges().addAll(this.getBadges().currentBadges);
        badges.getRemovedBadges().addAll(this.getBadges().removedBadges);
        return badges;
    }

    public Map<String, Double> getFullVariables() {
        Map<String, Double> inner =  acaPersonas.stream()
                .flatMap(
                        acaPersona -> acaPersona.getFullVariables()
                                .entrySet()
                                .stream())
                .collect(
                        Collectors.groupingBy(
                                Map.Entry::getKey, Collectors.summingDouble(Map.Entry::getValue)
                        ));
        return Stream.of(this.getVariables(), inner)
                .flatMap(map -> map.entrySet().stream())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        Double::sum));
    }

    public Map<String, Double> getFullTemporalVariables(TemporalBadgeContainer.Timeframe timeframe) {
        Map<String, Double> inner =  acaPersonas.stream()
                .flatMap(
                        acaPersona -> acaPersona.getFullTemporalVariables(timeframe)
                                .entrySet()
                                .stream())
                .collect(
                        Collectors.groupingBy(
                                Map.Entry::getKey, Collectors.summingDouble(Map.Entry::getValue)
                        ));
        return Stream.of(this.getTemporalVariables(timeframe), inner)
                .flatMap(map -> map.entrySet().stream())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        Double::sum));
    }

    @Override
    public Map<String, Double> getFullTemporalVariables(TemporalBadgeContainer.Timeframe timeframe, Date date) {
        Map<String, Double> inner =  acaPersonas.stream()
                .flatMap(
                        acaPersona -> acaPersona.getFullTemporalVariables(timeframe,date)
                                .entrySet()
                                .stream())
                .collect(
                        Collectors.groupingBy(
                                Map.Entry::getKey, Collectors.summingDouble(Map.Entry::getValue)
                        ));
        return Stream.of(this.getTemporalVariables(timeframe,date), inner)
                .flatMap(map -> map.entrySet().stream())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        Double::sum));
    }

    public Map<String,Map<String,Double>> getFullTemporalVariables() {
        Map<String,Map<String,Double>> fullTemporalVars = new HashMap<>();
        for(TemporalBadgeContainer.Timeframe timeframe: TemporalBadgeContainer.Timeframe.values()) {
            Map<String, Double> inner = acaPersonas.stream()
                    .flatMap(
                            acaPersona -> acaPersona.getFullTemporalVariables(timeframe)
                                    .entrySet()
                                    .stream())
                    .collect(
                            Collectors.groupingBy(
                                    Map.Entry::getKey, Collectors.summingDouble(Map.Entry::getValue)
                            ));
            if(this.getTemporalVariables().containsKey(timeframe.name())) {
                fullTemporalVars.put(timeframe.name(), Stream.of(this.getTemporalVariables(timeframe), inner)
                        .flatMap(map -> map.entrySet().stream())
                        .collect(Collectors.toMap(
                                Map.Entry::getKey,
                                Map.Entry::getValue,
                                Double::sum)));
            } else {
                fullTemporalVars.put(timeframe.name(),inner);
            }
        }
        return fullTemporalVars;
    }

    @BsonIgnore
    public Map<String, Double> getFullMaxVariables() {
        Map<String, Double> inner =  acaPersonas.stream()
                .flatMap(
                        acaPersona -> acaPersona.getFullMaxVariables()
                                .entrySet()
                                .stream())
                .collect(
                        Collectors.groupingBy(
                                Map.Entry::getKey, Collectors.summingDouble(Map.Entry::getValue)
                        ));
        return Stream.of(this.getMaxVariablesValues(), inner)
                .flatMap(map -> map.entrySet().stream())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        Double::sum));
    }

    @BsonIgnore
    @Override
    public UserProfile getPersona(String aca, String group) {
        if(aca== null && group != null){
            return null;
        }
        if(aca==null && group == null)
            return this;
        AcaPersona persona = acaPersonas.stream().filter(acaPersona -> acaPersona.getAca().compareTo(aca) == 0).findAny().orElse(null);
        return  persona != null ? persona.getPersona(aca,group) : null;
    }

    public List<AcaPersona> getAcaPersonas() {
        return acaPersonas;
    }

    public void setAcaPersonas(List<AcaPersona> acaPersonas) {
        this.acaPersonas = acaPersonas;
    }

    @BsonIgnore
    public UserProfile getPersona(String aca) {
        return getPersona(aca,null);
    }

    @BsonIgnore
    public Badge getBadgeLevel() {
        return badgeLevel;
    }

    @BsonIgnore
    public void setBadgeLevel(Badge badgeLevel) {
        this.badgeLevel = badgeLevel;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public String getInstance() {
        return instance;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MainPersona that = (MainPersona) o;
        return actor == that.actor &&
                Objects.equals(instance, that.instance);
    }

    @Override
    public int hashCode() {
        return Objects.hash(actor, instance);
    }
}
