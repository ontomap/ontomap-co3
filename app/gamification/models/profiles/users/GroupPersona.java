package gamification.models.profiles.users;

import gamification.models.badges.TemporalBadgeContainer;
import org.bson.codecs.pojo.annotations.BsonIgnore;

import java.util.Date;
import java.util.Map;

public class GroupPersona  extends UserProfile{
    private String group;

    public GroupPersona(){
     super();
    }

    @BsonIgnore
    @Override
    public UserProfile.Badges getFullBadges() {
        return this.getBadges();
    }

    @BsonIgnore
    @Override
    public  Map<String, Double> getFullVariables() {
        return this.getVariables();
    }

    @BsonIgnore
    @Override
    public Map<String, Double> getFullMaxVariables() {
        return this.getMaxVariablesValues();
    }

    @BsonIgnore
    @Override
    public Map<String, Double> getFullTemporalVariables(TemporalBadgeContainer.Timeframe timeframe,Date date) {
        return this.getTemporalVariables(timeframe,date);
    }

    @BsonIgnore
    @Override
    public Map<String, Map<String, Double>> getFullTemporalVariables() {
        return this.getTemporalVariables();
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public UserProfile.Badges getBadges() {
       return null;
    }

    @Override
    public void setBadges(UserProfile.Badges badges) {
    }

    @BsonIgnore
    @Override
    public UserProfile getPersona(String aca, String group) {
       return this;
    }


}
