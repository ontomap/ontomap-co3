package gamification.models.profiles;

import com.fasterxml.jackson.annotation.JsonIgnore;
import gamification.models.badges.TemporalBadgeContainer;
import org.bson.codecs.pojo.annotations.BsonIgnore;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class TemporalVariableContainer {
    private TemporalBadgeContainer.Timeframe timeframe;
    private Date start;
    private Date end;
    Map<String,Double> variables;

    public TemporalVariableContainer() {
        variables = new HashMap<>();
    }

    public TemporalBadgeContainer.Timeframe getTimeframe() {
        return timeframe;
    }

    public void setTimeframe(TemporalBadgeContainer.Timeframe timeframe) {
        this.timeframe = timeframe;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Map<String, Double> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, Double> variables) {
        this.variables = variables;
    }

    @JsonIgnore
    @BsonIgnore
    public boolean isInRange(TemporalBadgeContainer.Timeframe timeframe,Date date){
        return timeframe.equals(this.timeframe) && (date.after(start)  || date.equals(start)) && (date.before(end) || date.equals(end))  ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TemporalVariableContainer that = (TemporalVariableContainer) o;
        return timeframe == that.timeframe &&
                Objects.equals(start, that.start) &&
                Objects.equals(end, that.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timeframe, start, end);
    }
}
