package gamification.models.profiles;

import gamification.models.badges.TemporalBadgeContainer;
import org.bson.codecs.pojo.annotations.BsonIgnore;
import logger.supports.TimeUtils;

import java.util.*;

public class TemporalVariableList {
   Set<TemporalVariableContainer> temporalVariableContainers;

    public TemporalVariableList(){
        this.temporalVariableContainers = new HashSet<>();
    }

    @BsonIgnore
    public TemporalVariableContainer getTemporalVariableContainer(TemporalBadgeContainer.Timeframe timeframe, Date date){
        Optional<TemporalVariableContainer> temporalVariableOptional = this.getTemporalVariableContainers().stream().filter(t -> t.isInRange(timeframe,date)).findAny();
        return temporalVariableOptional.orElse(null);
    }

    @BsonIgnore
    public Map<String,Double> getVariables(TemporalBadgeContainer.Timeframe timeframe, Date date){
        Optional<TemporalVariableContainer> temporalVariableOptional = this.getTemporalVariableContainers().stream().filter(t -> t.isInRange(timeframe,date)).findAny();
        if(temporalVariableOptional.isPresent()){
            return temporalVariableOptional.get().getVariables();
        }else{
            return new HashMap<>();
        }
    }

    @BsonIgnore
    public Map<String,Double> getVariables(TemporalBadgeContainer.Timeframe timeframe){
        return this.getVariables(timeframe,new Date());
    }

    @BsonIgnore
    public Map<String, Map<String, Double>> getVariables() {
        Map<String, Map<String, Double>> map = new HashMap<>();
        for(TemporalBadgeContainer.Timeframe timeframe: TemporalBadgeContainer.Timeframe.values()){
            map.put(timeframe.name(),this.getVariables(timeframe));
        }
        return map;
    }

    public Set<TemporalVariableContainer> getTemporalVariableContainers() {
        return temporalVariableContainers;
    }

    public void setTemporalVariableContainers(Set<TemporalVariableContainer> temporalVariableContainers) {
        this.temporalVariableContainers = temporalVariableContainers;
    }

    public void removeOld(){
        Date now = new Date();
        List<TemporalVariableContainer> toRemove = new ArrayList<>();
        for(TemporalVariableContainer temporalVariableContainer : temporalVariableContainers){
            if(temporalVariableContainer.getEnd().before(now)){
                toRemove.add(temporalVariableContainer);
            }
        }
        this.temporalVariableContainers.removeAll(toRemove);
    }

    public TemporalVariableContainer add(TemporalBadgeContainer.Timeframe timeframe, Date date){
        TemporalVariableContainer temporalVariableContainer = new TemporalVariableContainer();
        temporalVariableContainer.setTimeframe(timeframe);
        if(timeframe.equals(TemporalBadgeContainer.Timeframe.WEEK)){
            temporalVariableContainer.setStart(TimeUtils.getFirstOfTheWeek(date));
            Date end = TimeUtils.nextFirstOfTheWeek(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(end);
            calendar.add(Calendar.SECOND,-1);
            temporalVariableContainer.setEnd(calendar.getTime());
        }else{
            temporalVariableContainer.setStart(TimeUtils.getFirstOfTheMonth(date));
            Date end = TimeUtils.nextFirstOfTheMonth(date);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(end);
            calendar.add(Calendar.SECOND,-1);
            temporalVariableContainer.setEnd(calendar.getTime());
        }
        this.getTemporalVariableContainers().add(temporalVariableContainer);
        return temporalVariableContainer;
    }


}
