package gamification.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gamification.supports.serializer.ObjectIdSerializer;
import logger.models.OTMEvent;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.types.ObjectId;

import java.util.Date;

public abstract class LogAssignment {
    @JsonSerialize(using = ObjectIdSerializer.class)
    @BsonId
    protected ObjectId id;
    @JsonIgnore
    protected String instance;
    protected long actor;
    protected String aca;
    protected String group;


    private Date date;

    public LogAssignment() {   }

    public LogAssignment(Date date, String instance, long actor, String aca, String group) {
        this.date = date;
        this.instance = instance;
        this.actor = actor;
        if (aca != null) {
            this.aca = aca;
            this.group = group;
        }
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public long getActor() {
        return actor;
    }

    public void setActor(long actor) {
        this.actor = actor;
    }

    public String getAca() {
        return aca;
    }

    public void setAca(String aca) {
        this.aca = aca;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    @JsonProperty("timestamp")
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
