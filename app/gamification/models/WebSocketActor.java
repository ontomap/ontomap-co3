package gamification.models;

import akka.actor.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import gamification.controllers.NotificationController;


public class WebSocketActor extends AbstractActor {

    private NotificationController notificationController;
    private long idActor;


    public static Props props(ActorRef out, long actor, NotificationController notificationController) {
        return Props.create(WebSocketActor.class, out,actor,notificationController);
    }

    private final ActorRef out;

    public WebSocketActor(ActorRef out, long actor, NotificationController notificationController) {
        this.notificationController = notificationController;
        this.idActor = actor;
        this.out = out;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(JsonNode.class, message -> {
                    ObjectNode response = new ObjectMapper().createObjectNode();
                    response.put("error","This is a single way channel!");
                    out.tell(response, self());
                })
                .build();
    }

    @Override
    public void postStop() throws Exception {
        super.postStop();
        notificationController.removeActor(idActor);
    }
}
