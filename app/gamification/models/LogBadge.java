package gamification.models;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gamification.models.profiles.users.MainPersona;
import gamification.supports.serializer.ObjectIdSerializer;
import org.bson.types.ObjectId;

import java.util.Date;

public class LogBadge extends LogAssignment{
    @JsonSerialize(using = ObjectIdSerializer.class)
    private ObjectId idBadge;

    public LogBadge() {super();}

    public LogBadge(long timestamp, ObjectId idBadge, MainPersona mainPersona, String aca)  {
        super(new Date(timestamp),mainPersona.getInstance(), mainPersona.getActor(), aca,null);
        this.idBadge =idBadge;
    }

    public ObjectId getIdBadge() {
        return idBadge;
    }

    public void setIdBadge(ObjectId idBadge) {
        this.idBadge = idBadge;
    }
}
