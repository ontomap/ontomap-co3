package gamification.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gamification.supports.serializer.ObjectIdSerializer;
import gamification.supports.serializer.RoleSerializer;
import gamification.supports.serializer.RuleSerializer;
import org.bson.types.ObjectId;

import java.util.*;

@JsonSerialize(using = RuleSerializer.class)
public class Rule {
    @JsonSerialize(using = ObjectIdSerializer.class)
    private ObjectId id;
    @JsonIgnore
    private String instance;
    private List<String> acas;
    private Set<Reward> rewards;

    private String activityType;
    private List<ActivityObjectProperty> activityObjectProperties;
    private List<ActivityObjectProperty> referenceObjectProperties;

    private int maxEvDay;
    private int maxEvEver;
    private Date validFrom;
    private Date validTo;
    private boolean active;

    public Rule() {
        maxEvDay = -1;
        maxEvEver = -1;
    }

    public List<String> getAcas() {
        return acas;
    }

    public void setAcas(List<String> acas) {
        this.acas = acas;
    }


    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public List<Variable> getRewards(Role role) {
        return this.rewards.stream().filter(reward -> reward.role.equals(role)).findAny().map(Reward::getVariables).orElse(new ArrayList<>());
//        if (this.rewards.get(role.name()) != null) {
//            return this.rewards.get(role.name());
//        } else {
//            return new HashSet<>();
//        }
    }

    public Set<Reward> getRewards() {
        return rewards;
    }

    public void setRewards(Set<Reward> rewards) {
        this.rewards = rewards;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public List<ActivityObjectProperty> getActivityObjectProperties() {
        return activityObjectProperties;
    }

    public void setActivityObjectProperties(List<ActivityObjectProperty> activityObjectProperties) {
        this.activityObjectProperties = activityObjectProperties;
    }

    public List<ActivityObjectProperty> getReferenceObjectProperties() {
        return referenceObjectProperties;
    }

    public void setReferenceObjectProperties(List<ActivityObjectProperty> referenceObjectProperties) {
        this.referenceObjectProperties = referenceObjectProperties;
    }

    public int getMaxEvDay() {
        return maxEvDay;
    }

    public void setMaxEvDay(int maxEvDay) {
        this.maxEvDay = maxEvDay;
    }

    public int getMaxEvEver() {
        return maxEvEver;
    }

    public void setMaxEvEver(int maxEvEver) {
        this.maxEvEver = maxEvEver;
    }

    public String getInstance() {
        return instance;
    }

    public void setInstance(String instance) {
        this.instance = instance;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTo() {
        return validTo;
    }

    public void setValidTo(Date validTo) {
        this.validTo = validTo;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public static class Reward {
        @JsonSerialize(using = RoleSerializer.class)
        private Role role;
        private String comment;
        private List<Variable> variables;

        public Reward() {
        }

        public Role getRole() {
            return role;
        }

        public void setRole(Role role) {
            this.role = role;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public List<Variable> getVariables() {
            return variables;
        }

        public void setVariables(List<Variable> variables) {
            this.variables = variables;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Reward reward = (Reward) o;
            return Objects.equals(role, reward.role);
        }

        @Override
        public int hashCode() {
            return Objects.hash(role);
        }
    }

    public enum Role {
        ACTOR, REFERENCE_OWNER;
    }

    public static class Variable {
        String name;
        double assignment;


        public Variable(String name, double assignment) {
            this.name = name;
            this.assignment = assignment;
        }

        public Variable() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getAssignment() {
            return assignment;
        }

        public void setAssignment(double assignment) {
            this.assignment = assignment;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Variable variable = (Variable) o;
            return Objects.equals(name, variable.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name);
        }
    }

    public static class ActivityObjectProperty {
        private String propertyName;
        private String propertyValue;
        private String propertyType;

        public ActivityObjectProperty() {
        }

        public ActivityObjectProperty(String propertyName, String propertyValue) {
            this.propertyName = propertyName;
            this.propertyValue = propertyValue;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public void setPropertyName(String propertyName) {
            this.propertyName = propertyName;
        }

        public String getPropertyValue() {
            return propertyValue;
        }

        public void setPropertyValue(String propertyValue) {
            this.propertyValue = propertyValue;
        }
    }
}
