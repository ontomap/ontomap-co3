import com.google.inject.AbstractModule;
import play.Logger;
import logger.services.EntityRegistryController;

/**
 * This class is a play module that is executed at the startup of the application.
 * The {@link EntityRegistryController} is loaded and declared Sigleton to cache a map of association between a couples of <Application, Url of Feature>
 * @see EntityRegistryController
 */

public class EntityRegistryModule extends AbstractModule {

    Logger.ALogger aLogger;


    @Override
    protected void configure() {
        aLogger = new Logger.ALogger(play.api.Logger.apply("EntityRegistryModule"));
        aLogger.debug("Binding EntityRegistryModule...");
        bind(EntityRegistryController.class).asEagerSingleton();
    }
}
