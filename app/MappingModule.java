import com.google.inject.AbstractModule;
import play.Logger;
import logger.services.MappingTranslationController;

/**
 * This class is a play module that is executed at the startup of the application.
 * The {@link MappingTranslationController} is loaded and declared Singleton to cache a mapping between application properties
 * and OnToMap properties.
 *
 * @see MappingTranslationController
 */


public class MappingModule extends AbstractModule {
    private Logger.ALogger logger;

    public MappingModule() {
        logger = new Logger.ALogger(play.api.Logger.apply(getClass()));
    }

    @Override
    protected void configure() {
        logger.debug("Binding MappingModule...");
        bind(MappingTranslationController.class).asEagerSingleton();
    }
}
