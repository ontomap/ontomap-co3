import com.google.inject.AbstractModule;
import play.Logger;
import play.Logger.ALogger;
import logger.services.MongoDBController;


/**
 * This class is a play module that is executed at the startup of the application.
 * The {@link MongoDBController} is loaded and declared Singleton.
 *
 * @see MongoDBController
 */

public class MongoDBModule extends AbstractModule {
    private ALogger logger;

    public MongoDBModule() {
        logger = new Logger.ALogger(play.api.Logger.apply(getClass()));
    }

    @Override
    protected void configure() {
        logger.debug("Binding MongoDBModule...");
        bind(MongoDBController.class).asEagerSingleton();
    }
}
