import com.google.inject.AbstractModule;
import play.Logger;
import logger.services.LoggerMongoController;

/**
 * This class is a play module that is executed at the startup of the application.
 * The {@link LoggerMongoController} is loaded and declared Sigleton.
 * @see LoggerMongoController
 */

public class LoggerMongoModule extends AbstractModule {
    private  Logger.ALogger logger;

    public LoggerMongoModule(){
        logger = new  Logger.ALogger(play.api.Logger.apply(getClass()));
    }
    @Override
    protected void configure() {
        logger.debug("Binding LoggerMongoModule...");
        bind(LoggerMongoController.class).asEagerSingleton();
    }
}
